import re
import base64
import datetime
import pickle

import json
import numpy as np
import arviz
import xarray as xr
import pandas as pd

import nitrogene.reaction_model as reaction_model


def assign_concentration_units(var, ds):
    concentration_names = {
        "nitrogen": "mol/L",
        "mrna_norm": "transcripts/cell",
        "mrna": "transcripts/L",
        "enz": "mol/L",
        "gas": "mol/L",
        "bio": "cells/L",
        "o2": "mol/L",
        "doc": "mol/L",
        "co2": "mol/L",
    }
    for name, unit in concentration_names.items():
        m = re.match(f"(c_dense|c)_{name}", var)
        if m is not None:
            ds[var].attrs["units"] = unit
            return True
    return False


def assign_param_units(var, ds):
    param_names = {
        "kmax_no[23]_log": "log(1/s)",
        "t12_mrna_log": "log(min)",
        "t12_enz_log": "log(h)",
        "rmax_.*_log": "log(mol/cell/s)",
        "r_cfix_.*_log": "log(mol/cell/s)",
        "r_mrna_log": "log(transcripts/cell)",
        "K.*_log": "log(mol/L)",
        "Y_.*_log": "log(cells/mol_DOC)",
    }
    for name, unit in param_names.items():
        m = re.match(name, var)
        if m is not None:
            ds[var].attrs["units"] = unit
            return True
    return False


def assign_coord_units(var, ds):
    coord_units = {".*time": "h"}
    for name, unit in coord_units.items():
        m = re.match(name, var)
        if m is not None:
            ds[var].attrs["units"] = unit
            return True
    return False


def assign_units_to_group(group):
    for var in group.data_vars:
        matched_concentration = assign_concentration_units(var, group)
        if matched_concentration:
            continue
        matched_parameter = assign_param_units(var, group)
    for var in group.coords:
        matched_coords = assign_coord_units(var, group)


def assign_units_to_inference_data(data):
    for group_name in data._groups:
        group = getattr(data, group_name)
        assign_units_to_group(group)


def save_model_output(inference_data, path, description, model_builder, problem):
    if "posterior" in inference_data._groups:
        group = inference_data.posterior
    elif "prior" in inference_data._groups:
        group = inference_data.prior
    else:
        assert False
    group.attrs["sym_dydt"] = base64.b64encode(pickle.dumps(problem._sym_dydt)).decode()
    group.attrs["model_args"] = json.dumps(model_builder.model_args)
    group.attrs["model_description"] = description
    group.attrs[
        "decoding_instructions"
    ] = 'For backconversion of the "sym_dydt" attribute to an array of sympy equations do the following: pickle.loads(base64.b64decode(ds.attrs["sym_dydt"]))'
    inference_data.to_netcdf(path)


def create_rates_ds(dets_ds, solver, problem, model_builder):
    e_accceptor_rates = model_builder.compute_electron_acceptor_consumption_rate()
    rates_exprs = e_accceptor_rates._asdict()
    return reaction_model.compute_rates(rates_exprs, dets_ds, solver, problem)


def create_qss_mrna_ds(tr, solver, problem, genes):
    """Compute quasi-steady state mRNA concentrations from model results."""
    mrna_qss_vectorized = []
    for gene in genes:
        mrna_qss_vectorized.append(
            reaction_model.compute_qss_mrna(tr, solver, problem, gene)
        )
    mrna_qss = xr.concat(mrna_qss_vectorized, dim=pd.Index(genes, name="gene"))
    return mrna_qss


def create_qss_enz_ds(tr, solver, problem, genes):
    """Compute quasi-steady state enzyme concentrations from model results."""
    enz_qss_vectorized = []
    for gene in genes:
        enz_qss_vectorized.append(
            reaction_model.compute_qss_enz(tr, solver, problem, gene)
        )
    return xr.concat(enz_qss_vectorized, dim=pd.Index(genes, name="gene"))


def save_map_solution(
    path,
    dets_ds,
    rates_ds,
    model_builder,
    problem,
    description=None,
    date=None,
    backend=None,
):
    if date is None:
        date = str(datetime.datetime.now())
    # Set attributes
    dets_ds.attrs["sym_dydt"] = base64.b64encode(
        pickle.dumps(problem._sym_dydt)
    ).decode()
    dets_ds.attrs["model_args"] = tuple(model_builder.model_args.items())
    dets_ds.attrs["model_description"] = description
    dets_ds.attrs["created"] = date
    dets_ds.attrs[
        "decoding_instructions"
    ] = 'For backconversion of the "sym_dydt" attribute to an array of sympy equations do the following: pickle.loads(base64.b64decode(ds.attrs["sym_dydt"]))'

    # Infer backend used for saving
    file_ending = None
    if "." in path:
        path, file_ending = path.rsplit(".")
    if backend is None:
        if file_ending is None:
            raise ValueError(
                "Either backend must be given or file must have a file ending."
            )
        if file_ending == "nc":
            backend = "netcdf"
        elif file_ending == "zarr":
            backend = "zarr"
        else:
            backend = "netcdf"
    # Set the correct file ending
    if backend == "zarr":
        file_ending = "zarr"
    elif backend == "netcdf":
        file_ending = "nc"
    else:
        raise ValueError("backend must be one of {'netcdf', 'zarr'}.")
    path = ".".join([path, file_ending])
    # Save the file
    if backend == "netcdf":
        dets_ds.to_netcdf(path=path, group="variables_parameters")
        rates_ds.to_dataset(name="turnover_rate").to_netcdf(
            path=path, group="rates", mode="a"
        )
    elif backend == "zarr":
        dets_ds.to_zarr(store=path, group="variables_parameters")
        rates_ds.to_dataset(name="turnover_rate").to_zarr(
            store=path, group="rates", mode="a"
        )
    print(f"Saved to {path}.")
