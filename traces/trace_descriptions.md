# trace_2020-02-17_classical_monod_full_mass_matrix.zarr
- two-step classical Monod model
- based on maximum a posteriori solution and sampled with full mass matrix adaptation
- related commits:
  - 5d6916d082c367652ed0c4febe2936632e7d65f6 
  - 907ec51ed67b9a0992b0e8ebe790cae646ad0337

# trace_2020-02-18_classical_monod_full_mass_matrix_reparametrization.zarr
- two-step classical Monod model
- based on maximum a posteriori solution and sampled with full mass matrix adaptation
- reparametrization for r_max_no3 and K_i_o2
- Only three chains because the last chain was very slow and I interrupted the sampler. However, the three chains that remain show very good results.
- related commits:
  - 20345e94aa9702ba5d31906eba18e813d5b45707
 
 # map_solution_2020-02-20_two_genes_no_nar_data.zarr
 - two step model with nar and nir mRNA and enzymes
 - mRNA *not* at quasi-steady state
 - only nir data used for calibraiton, not nar
 - optimized with CG solver
 - related commits:
   - 740e2fdd7dff707bba162e389a1e059e6704b8bf

# trace_2020-02-20_mrna-nir_52h.zarr
- two step model with nir mRNA and enzymes
- mRNA at quasi steady state
- Three chains look good, (0, 1, 3), the other 3 chains seem do do strange things.
- more details on the model are in trace_2020-02-20_mrna-nir_52h.zarr.model.py 
  (Unfortunately, there is no commit with the exact model code.)
   
 # map_solution_2020-02-21_two_genes.zarr and dets_map_solution_2020-02-21_two_genes.zarr
 - two step model with nar and nir mRNA and enzymes
 - mRNA *not* at quasi-steady state
 - mRNA concentrations initialized with quasi-steady state values
 - nar and nir data used for likelihood
 - optimized with standard solver (BFGS)
 - fit looks pretty decent
 - related commits:
   - 57b03999c00d7d7fc62decf7ea472b466b4be572

# inference_data_2020-02-24_classical_monod_full_mass_matrix_estimated_measurement_error.nc
- two-step classical Monod model
- based on maximum a posteriori solution and sampled with full mass matrix adaptation
- reparametrization for r_cfix_o2_log, r_cfix_no3_log and r_cfix_no2_log
- relative measurement error as a parameter
- replicate measurements provided as observations instead of mean values
- related commits:
  - d6f42831dd6fd1b73f7b6efb8b53ded729105afa
  - 4c88a951509332c00f48d9c609aa1b6739c61729
  - 80ef2010e0935fc57176f7b97b903ba59c01877e
  
# dets_map_solution_2020-03-06_two_genes_boxcox_error.zarr
- two step model with nar and nir
- data error student's t distributed for Box-Cox transformed data
- data error estimated from the data (see error_model.ipynb and prepare_data_for_likelihood in params_model_qu.py)
- maximum a posteriori solution with BFGS solver
- related commits:
  - 0609495fc2d74bbb679014007984f4f8b540638f
  - 56feef704e8518f7b07be62346a13b6c4cf82175
  
# dets_map_solution_2020-03-06_two_genes_estimated_boxcox_error.zarr
- two step model with nar and nir
- data error student's t distributed for Box-Cox transformed data
- data error as parameter (one per time series)
- maximum a posteriori solution with BFGS solver
- related commits:
  - 7d8580493e8bc8a4c384556210a28998b3b97159
  
# map_2020_03_30_nar_nir_kinetic_toxicity.nc
- two step model with nar and nir
- data error student's t distributed for Box-Cox transformed data
- data error as parameter (one per time series)
- maximum a posteriori solution with BFGS solver
- kinetic toxicity function
- related commits:
  - 2929eef93f12ad35f96341b4d30f298855e9d1c4
  
# smc_trace_2020_04_08_sum_of_gamma_conc_toxicity.nc
- two step model with nar and nir
- data error fixed at measurement standard deviation
- toxicity based on lagged nitrite concentrations with sum of gamma distributions as weights
- sampled with PyMC's sample_smc method
- related commits:
  - bbbdc2ab88e249b217a44a672786d9d5526974ac

# smc_trace_2020_04_11_test_toxicity_{dist}_{conv_var}.nc
- two step model with nar and nir
- estimated data error
- different toxicity models (see file names)
- sampled with PyMC's sample_smc method
- related commits:
  - 3244026c5a505183c8c5e9a59d4307fde202b952

# smc_trace_2020_04_12_classical_monod.nc
- two step model without genes
- estimated data error
- sampled with PyMC's sample_smc method
- related commits:
  - 0227c92d25253c6780a77596cd71e0bf3356473d

# smc_trace_2020_04_12_nar_nir_without_tox.nc
- two step model with nar and nir
- estimated data error
- no toxicity
- sampled with PyMC's sample_smc method
- related commits:
  - 40cde775dd4fa82b934acb924fb1f2f0a45d717f

# smc_trace_2020_04_23_test_toxicity_{dist}_{conv_var}.nc
- two step model with nar and nir
- estimated data error
- different toxicity models (see file names)
- sampled with PyMC's sample_smc method
- related commits:
  - aab2b7304a0b70bd5f2bb071b34b0c571845066b
  - d8046db3a654e198fb9ef7f66cbd5539b63b192b