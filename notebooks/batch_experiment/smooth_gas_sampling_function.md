---
jupyter:
  jupytext:
    formats: ipynb,md
    main_language: python
    notebook_metadata_filter: -kernelspec
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.11.5
---

```python
import numpy as np
import matplotlib.pyplot as plt
import sklearn
import xarray as xr
import seaborn as sns

import pandas as pd
import patsy
from sklearn import linear_model
```

```python
import plotting
```

# Dilution of gas phase concentrations due to gas sampling


## Sampling events as rectangular pulses
The dilution is described by a first order rate law
$$r_{dil} = -f_{dil} k_{sample} c$$
with the time dependent scaled rate constant $k_{sample}$ [1/s] and the fraction of the gas phase replaced during a single sampling event $f_{dil}$.
Sampling can be described as a series of rectangular pulses of duration $\triangle t_{sample}$.

$$
k_{sample} (t) =
\begin{cases}
\begin{align}
\frac{1}{\triangle t_{sample}}, \qquad&\text{during sampling}\\
0, \qquad &\text{else}
\end{align}
\end{cases}
$$

## Replacing rectangular pulses by a smoother function
The rectangular pulses make numerical integration difficult. 
Replacing them with a smoother function while modifying the resulting concentration time series only slightly would be desirable.
The integral of the smoothed dilution rate should be approximately the same as the integral of the actual rate at any time point:
$$- f_{dil} \int_0^t k_{sample}(t^\prime) \cdot c \,dt^\prime \approx - f_{dil} \int_0^t \tilde{k}_{sample}(t^\prime) \cdot \tilde{c} \,dt^\prime$$
(In absence of other processes that would mean that the concentration time series would be approximately the same.)

If the concentration does not change a lot over the time course of a single sampling event we can relax that condition to:
$$\int_0^t \tilde{k}_{sample}(t^\prime)\,dt^\prime \approx \int_0^t k_{sample}(t^\prime)\,dt^\prime$$
That is, we want to fit a smooth function to the integral of the rectangular pulses.
As the duration of sampling events is short compared the whole experiment, we fit the smooth function to a series of Dirac pulses instead of rectangular pulses.

```python
# Read in the sampling times
ds = xr.open_dataset("../../experimental_data/data_Qu_2015.nc")
amount = 1
sampling_times = ds.time[ds["gas sample"].all(dim="replicate")] * 3600
```

```python
# Compute the integral of the pulses
remove = pd.Series(index=sampling_times, data=amount)
remove = pd.Series(0, index=pd.Index([0], name="time")).append(remove)
removed = remove.cumsum()
removed.name = "cum_amount"
```

We fit a quadratic function to the integral of the pulses.

```python
# Fit a quadratic function to the cumulative samplings
y, A = patsy.dmatrices("cum_amount ~ 0 + time + I(time*time)", removed.reset_index())
reg = linear_model.LinearRegression(fit_intercept=False)
# Give early meausurements a higher weight for fitting as there are less of them
weights = 1 / (y.ravel() / (5000 / 1200 * amount) + 1) ** 2
reg.fit(A, y, sample_weight=weights)
F = pd.Series(reg.predict(A).ravel(), index=removed.index / 3600)
reg.score(A, y)
```

The coefficients of the quadratic function are:

```python
reg.coef_
```

```python
sns.set(
    context="paper",
    style="ticks",
    palette=plotting.color_palettes["paul_tol_high_contrast"],
    font="Source Sans Pro",
    rc={
        "mathtext.fontset": "custom",
        "mathtext.sf": "Source Sans Pro",
        "mathtext.it": "Source Sans Pro:italic",
        "mathtext.rm": "Source Sans Pro",
        "mathtext.bf": "Source Sans Pro:bold",
    },
)
```

```python
# Plot the quadratic fit to the integrated pulses
fig, ax = plt.subplots(
    ncols=2,
    figsize=(5.78, 2.0),
    gridspec_kw=dict(wspace=0.35, left=0.1, right=0.98, top=0.85, bottom=0.23),
)

t_ext = np.r_[0, sampling_times]
ax[0].step(removed.index / 3600, removed, where="post", label="experiment")
F.plot(ax=ax[0], label="quadratic fit")
ax[0].set_xlabel("time [h]")
ax[0].set_ylabel("Number of sampling events")
ax[0].legend()
ax[0].set_title("(A)", pad=10)


#
k_sample = pd.Series(
    data=(reg.coef_[0, 0] + 2 * reg.coef_[0, 1] * t_ext), index=t_ext / 3600
)
(k_sample * 3600).plot(ax=ax[1])
plt.ylabel("$k_{\mathrm{sample}} \,[\mathrm{h}^{-1}]$")
plt.xlabel("time [h]")
ax[1].set_ylim(bottom=0)
ax[1].set_title("(B)", pad=10)
plt.annotate(
    text=(
        fr"$k(t) = {reg.coef_[0, 0] * 3600:0.2g}\ \mathrm{{h}}^{{-1}} "
        fr"+ {reg.coef_[0, 1] * (3600**2) * 2:0.2g}\ \mathrm{{h}}^{{-2}} t$"
    ),
    xy=(0, 0.8),
    fontfamily="Source Sans Pro",
)
plt.ticklabel_format(scilimits=(-3, 3))
sns.despine()
fig.savefig(
    "../../plots/manuscript/gas_sampling.pdf",
    metadata={
        "Author": "Anna Störiko",
        "Title": "Fit of a quadratic function to the gas sampling events.",
    },
)
```

Now let's compare the results of the smoothed sampling rate compared to the original pulses.
We simulate the dilution of a non-reactive compound as the only process.
In the case of retangular pulses, the rate constant is constant during a sampling event. The solution therefore is a simple exponential function during the sampling wheareas the conconcentration stays constant in between sampling events.

```python
f_sample = 0.0013
dt_sample = 60 * 5
t = np.linspace(0, 65 * 3600, 1000)
y0 = np.array([1])
```

```python
# Compute concentrations at the start of each sampling
ext_sampling_times = xr.concat(
    (xr.DataArray(0, coords={"time": 0}), sampling_times), dim="time"
)
n_times = sampling_times.shape[0]
c_sampling_start = np.exp(-f_sample) ** np.arange(n_times)
```

```python
# Compute concentrations at time points in between
c = np.zeros_like(t)
for i, ti in enumerate(t):
    idx_last_sampling = np.argmax((sampling_times > ti).values) - 1
    last_sampling_start = sampling_times.isel(time=idx_last_sampling)
    c_last_sampling_start = c_sampling_start[idx_last_sampling]
    during_sampling = (last_sampling_start <= ti) and (
        ti < last_sampling_start + dt_sample
    )
    if during_sampling:
        c_current = c_last_sampling_start * np.exp(
            -f_sample / dt_sample * (ti - last_sampling_start)
        )
        c[i] = c_current
    else:
        c_last_sampling_end = c_sampling_start[idx_last_sampling + 1]
        c[i] = c_last_sampling_end
c_non_smooth = pd.Series(c, index=pd.Index(t / 3600, name="time"), name="non-smooth")
```

We can compute the solution to the smoothed function with an ODE solver.

```python
def smooth_dilution(t, c):
    return -f_sample * (a + 2 * b * t) * c
```

```python
from scipy.integrate import solve_ivp
```

```python
a, b = reg.coef_[0]
sol = solve_ivp(smooth_dilution, t_span=(0, t[-1]), y0=y0, t_eval=t, atol=1e-12)
c_smoothed = pd.Series(sol.y[0], index=sol.t / 3600, name="smoothed")
```

```python
c_non_smooth.plot()
c_smoothed.plot()
plt.legend()
plt.ylabel("concentration")
```

The good agreement between the two curves shows that our approximation is a valid simplification of the model.
