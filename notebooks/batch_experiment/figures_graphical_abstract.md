---
jupyter:
  jupytext:
    formats: ipynb,md
    main_language: python
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.11.5
---

```python
import arviz
import matplotlib.pyplot as plt
import seaborn as sns
import xarray as xr

import nitrogene.plotting as plotting
```

```python
ds = xr.open_dataset("../../experimental_data/data_Qu_2015.nc")
```

```python
tr = arviz.from_netcdf(
    "../../traces/nuts_trace_pymc3-ext_2021_02_05_nar_nir_tf_activate_nnr_by_no2_only.nc"
)
```

```python
sns.set(
    context="paper",
    style="white",
    font="Source Sans Pro",
    rc={"axes.labelsize": 6, "axes.edgecolor": "gray"},
)
```

```python
ds["narG"].dropna("time").mean("replicate").plot.line(
    x="time", marker=".", linewidth=0, figsize=(0.8, 0.4)
)
ds["nirS"].dropna("time").mean("replicate").plot.line(
    x="time", marker=".", linewidth=0
)
tr.posterior.c_dense_mrna_norm_nar.mean(["chain", "draw"]).plot(color="C0")
tr.posterior.c_dense_mrna_norm_nir.mean(["chain", "draw"]).plot(color="C1")
plt.tick_params(
    axis="both",
    labelbottom=False,
    labelleft=False,
)
plt.ylabel("")
plt.xlabel("time")
sns.despine()
plt.savefig(
    "../../plots/manuscript/graphical_abstract_mrna.pdf",
    bbox_inches="tight",
)
```

```python
tr.posterior.c_dense_enz_nar.mean(["chain", "draw"]).plot(
    color="C0", figsize=(0.8, 0.4)
)
tr.posterior.c_dense_enz_nir.mean(["chain", "draw"]).plot(color="C1")
plt.gca().ticklabel_format(useOffset=False, style="plain")
plt.tick_params(
    axis="both",
    labelbottom=False,
    labelleft=False,
)
plt.ylabel("")
plt.xlabel("time")
sns.despine()
plt.savefig(
    "../../plots/manuscript/graphical_abstract_enzymes.pdf",
    bbox_inches="tight",
)
```

```python
time_mask = ~ds.time[ds["gas sample"].all(dim="replicate")].astype("bool")
time_mask[:18:2] = True
time_mask[19:26:3] = True
time_mask[28::5] = True
```

```python
tr.posterior.c_dense_nitrogen_no3.mean(["chain", "draw"]).plot(
    figsize=(1.6, 0.8)
)
tr.posterior.c_dense_nitrogen_no2.mean(["chain", "draw"]).plot()
ds["NO2-"].dropna("time").isel(time=time_mask).mean("replicate").plot.line(
    x="time", marker=".", linewidth=0, color="C1"
)
tr.posterior.c_dense_gas_n2.mean(["chain", "draw"]).plot()
ds["N2"].dropna("time").isel(time=time_mask).mean("replicate").plot.line(
    x="time", marker=".", linewidth=0, color="C2"
)
plt.tick_params(
    axis="both",
    labelbottom=False,
    labelleft=False,
)
plt.ylabel("")
plt.xlabel("time")
sns.despine()
plt.savefig(
    "../../plots/manuscript/graphical_abstract_nitrogen.pdf",
    bbox_inches="tight",
)
```

```python
ds["NO2-"].dropna(dim="time").isel(time=time_mask).mean("replicate").plot(
    marker="o", linewidth=0
)
ds["NO2-"].dropna(dim="time").isel(time=~time_mask).mean("replicate").plot(
    marker="o", linewidth=0
)
```
