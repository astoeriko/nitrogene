import os

import nesttool
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import sympy as sym
import sunode

import nitrogene.reaction_model as reaction_model


def test_solution_with_reference_params():
    def get_shape(obj):
        if hasattr(obj, "shape"):
            return obj.shape
        return ()

    def set_initial_values(y0_dict, y0_array):
        for key, val in y0_dict.items():
            if isinstance(val, dict):
                set_initial_values(val, y0_array[key])
            else:
                y0_array[key] = val

    model_args = {
        "gas_phase": "transient",
        "mrna_at_quasi_steady_state": False,
        "classical_monod": False,
        "two_step_model": True,
        "cell_decay": False,
        "growth_substrates": ["o2"],
        "growth_yield_units": "cells/mol_DOC",
        "carbon_for_growth": False,
        "doc_rate_limitation": False,
        "smooth_gas_sampling": True,
        "enzymes_to_simulate": ["nir", "nar"],
        "transcription_self_inhibition": False,
        "nitrite_toxicity": False,
    }
    kwargs = {"log_scale": True}

    model_builder = reaction_model.ModelBuilder(**model_args, **kwargs)
    model_builder.define_model()

    t = np.linspace(0, 60 * 3600, 1000)
    t0 = 0
    params = model_builder.default_params
    initial_values = model_builder.generate_initial_values()
    rhs = model_builder.generate_rhs_func()
    state_shapes = nesttool.apply_func_nested(initial_values, get_shape)
    param_shapes = nesttool.apply_func_nested(params, get_shape)

    problem = sunode.SympyProblem(
        params=param_shapes, states=state_shapes, rhs_sympy=rhs, derivative_params=()
    )
    solver = sunode.solver.Solver(problem, compute_sens=False, solver="BDF")

    y0 = np.zeros((), dtype=problem.state_dtype)
    set_initial_values(initial_values, y0)
    y0["mrna"]["nar"] = -8
    y0["mrna"]["nir"] = -8
    solver.set_params_dict(params)

    output = solver.make_output_buffers(t)
    solver.solve(t0=0, tvals=t, y0=y0, y_out=output)
    solution = solver.as_xarray(t, output)

    directory = os.path.dirname(__file__)
    path = os.path.join(directory, "reference_solution_default_params.nc")
    print(path)
    reference_solution = xr.open_dataset(path)

    assert np.allclose(
        a=solution.solution_gas_o2.values,
        b=reference_solution.solution_gas_o2.values,
        rtol=1e-8,
        atol=1e-13,
    )
    assert np.allclose(
        a=solution.solution_gas_n2.values,
        b=reference_solution.solution_gas_n2.values,
        rtol=1e-8,
        atol=1e-13,
    )
    assert np.allclose(
        a=solution.solution_mrna_nar.values,
        b=reference_solution.solution_mrna_nar.values,
        rtol=1e-8,
        atol=1e-13,
    )
