---
jupyter:
  jupytext:
    formats: ipynb,md
    main_language: python
    notebook_metadata_filter: -kernelspec
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.11.5
---

```python
import os

# os.environ['NUMBA_DEVELOPER_MODE'] = '1'
# os.environ['NUMBA_BOUNDSCHECK'] = '1'
# os.environ['NUMBA_ENABLE_PROFILING'] = '1'
```

```python
import numpy as np
import matplotlib.pyplot as plt
import operator
from functools import reduce

import pandas as pd
import seaborn as sns
import pymc3 as pm
import xarray as xr
import sympy as sym
import theano
import theano.tensor as tt
import sunode.symode.paramset
import sunode.symode.problem
import sunode.wrappers.as_theano

import nitrogene.reaction_model as reaction_model
import nitrogene.utils
```

```python
model_args = {
    "gas_phase": "transient",
    "mrna_at_quasi_steady_state": False,
    "classical_monod": False,
    "two_step_model": False,
    "cell_decay": False,
    "growth_substrates": ["o2"],
    "growth_yield_units": "cells/mol_DOC",
    "carbon_for_growth": False,
    "doc_rate_limitation": False,
    "smooth_gas_sampling": True,
    "enzymes_to_simulate": ["nir", "nar", "nor", "nos"],
    "transcription_self_inhibition": False,
}
kwargs = {
    # "rhs_as_dict": True,
    "log_scale": True
}

model_builder = reaction_model.ModelBuilder(**model_args, **kwargs)
model_builder.define_model()
```

```python
ds = xr.open_dataset("../../experimental_data/data_Qu_2015.nc")
data = nitrogene.utils.process_data_for_likelihood(
    ds.drop("sample").apply(np.log),
    classical_monod=model_builder.classical_monod,
    two_step_model=model_builder.two_step_model,
)
```

```python
def apply_nested(vals, func):
    result = {}
    for key, val in vals.items():
        if isinstance(val, dict):
            result[key] = apply_nested(val, func)
        else:
            result[key] = func(val)
    return result


def as_double(val):
    if isinstance(val, tuple):
        val, shape = val
        if hasattr(val, "astype"):
            return (val.astype("float64"), shape)
        return (np.array(val, dtype=np.float64), shape)

    if hasattr(val, "astype"):
        return val.astype("float64")
    return np.array(val, dtype=np.float64)


def get_first_tuple_elem(val):
    if isinstance(val, tuple):
        return val[0]
    else:
        return val
```

```python
with pm.Model() as model:
    p = model_builder.default_params  # TODO rename to params
    # Define changeable variables with their prior distributions
    p["Y"]["o2"]["log"] = (pm.StudentT("Y_o2_log", mu=37, sigma=0.5, nu=10), ())
    p["k_max"]["no3"]["log"] = (pm.StudentT("kmax_no3_log", mu=7, sigma=2, nu=10), ())
    p["k_max"]["no2"]["log"] = (pm.StudentT("kmax_no2_log", mu=7, sigma=2, nu=10), ())
    p["k_max"]["no"]["log"] = (pm.StudentT("kmax_no_log", mu=7, sigma=2, nu=10), ())
    p["k_max"]["n2o"]["log"] = (pm.StudentT("kmax_n2o_log", mu=7, sigma=2, nu=10), ())

    p["K_subs"]["no3"]["log"] = (pm.Normal("Ks_no3_log", mu=-11, sigma=3), ())
    p["K_subs"]["no2"]["log"] = (pm.Normal("Ks_no2_log", mu=-11, sigma=3), ())
    p["K_subs"]["no"]["log"] = (pm.Normal("Ks_no_log", mu=-11, sigma=3), ())
    p["K_subs"]["n2o"]["log"] = (pm.Normal("Ks_n2o_log", mu=-11, sigma=3), ())
    p["t_1_2"]["mrna"]["log"] = (pm.Normal("t12_mrna_log", mu=1.8, sigma=0.5), ())
    p["t_1_2"]["enz"]["log"] = (
        pm.StudentT("t12_enz_log", mu=3.0, sigma=0.8, nu=10),
        (),
    )

    r_mrna_log = pm.StudentT("r_mrna_log", mu=-8, sigma=1.5, nu=10)
    p["r_mrna_log"] = (r_mrna_log, ())
    p["K_i_o2"]["transcription"]["log"] = (
        pm.StudentT("K_i_o2_transcription_log", mu=-11, sigma=3, nu=10),
        (),
    )
    p["K_transcription"]["nir"]["log"] = (
        pm.StudentT("K_transcription_nir_log", mu=-11, sigma=3, nu=10),
        (),
    )
    p["K_transcription"]["nar"]["no3"]["log"] = (
        pm.StudentT("K_transcription_nar_no3_log", mu=-11, sigma=3, nu=10),
        (),
    )
    p["K_transcription"]["nar"]["no2"]["log"] = (
        pm.StudentT("K_transcription_nar_no2_log", mu=-11, sigma=3, nu=10),
        (),
    )
    p["K_transcription"]["nor"]["log"] = (
        pm.StudentT("K_transcription_nor_log", mu=-11, sigma=3, nu=10),
        (),
    )
    p["K_transcription"]["nos"]["log"] = (
        pm.StudentT("K_transcription_nos_log", mu=-11, sigma=3, nu=10),
        (),
    )

    Ks_log = pm.StudentT("Ks_o2_log", mu=-13, sigma=2, nu=10)
    cfix = 0.6e-4
    r_cfix_log = pm.StudentT(
        "r_cfix_o2_log", mu=-44.160977263447144, sd=1.514192676008769, nu=10
    )
    rmax_log = r_cfix_log + np.log((np.exp(Ks_log) + cfix) / cfix)
    pm.Deterministic("rmax_o2_log", rmax_log)

    p["K_subs"]["o2"]["log"] = (Ks_log, ())
    p["r_max"]["o2"]["log"] = (rmax_log, ())
    # p["r_max"]["o2"]["val"] = (np.exp(pm.Normal("rmax_o2_log", mu=-44, sigma=1.5)), ())

    p["K_i_o2"]["reaction"]["log"] = (
        pm.StudentT("K_i_o2_log", mu=-15, sigma=3, nu=10),
        (),
    )

    y0 = model_builder.generate_initial_values()
    #     c0["bio"]["alive"] = (np.exp(pm.Normal("c0_bio_alive_log", mu=18, sigma=1.5)), ())

    # Set initial mRNA concentrations to their quasi-steady state value
    params_without_shapes = apply_nested(p, get_first_tuple_elem)
    if model_builder.log_scale:
        for gene in model_builder.genes:
            y0["mrna"][gene] = (
                reaction_model.qss_mrna_concentration_val(
                    gene=gene,
                    p=params_without_shapes,
                    log_c=y0,
                    log_scale=model_builder.log_scale,
                ),
                (),
            )
    else:
        for gene in model_builder.genes:
            y0["mrna"][gene] = (
                reaction_model.qss_mrna_concentration_val(
                    gene=gene,
                    p=params_without_shapes,
                    c=y0,
                    log_scale=model_builder.log_scale,
                ),
                (),
            )

    y0 = apply_nested(y0, as_double)
    params = apply_nested(p, as_double)

    rhs = model_builder.generate_rhs_func()

    # Solve the ode
    (
        y,
        flat_y,
        problem,
        solver,
        y0_flat,
        params_flat,
    ) = sunode.wrappers.as_theano.solve_ivp(
        y0=y0,
        params=params,
        rhs=rhs,
        tvals=ds.time.values * 3600,
        t0=0,
        solver_kwargs={},
    )

    pm.Deterministic("y0_flat", y0_flat)
    pm.Deterministic("params_flat", params_flat)
    pm.Deterministic("flat_y", flat_y)

    y_dense, *_ = sunode.wrappers.as_theano.solve_ivp(
        y0=y0,
        params=params,
        rhs=rhs,
        tvals=np.linspace(0, (ds.time.values * 3600).max(), 1000),
        t0=0,
        solver_kwargs={},
    )

    # Unpack solution and save outpute in pymc variables
    if model_builder.log_scale:
        c_dense = reaction_model.apply_func_nested(y_dense, np.exp)
    else:
        c_dense = model_builder.unnormalize_state(y_dense)

    # Unpack solution and save outpute in pymc variables
    if model_builder.log_scale:
        c = reaction_model.apply_func_nested(y, np.exp)
        log_c = y
    else:
        c = model_builder.unnormalize_state(y)
        log_c = reaction_model.apply_func_nested(y, np.log)

    #     c_mrna_nir_norm = (np.exp(p['r_mrna_log'][0])
    #         * (c['nitrogen']['no2']
    #            / (
    #                c['nitrogen']['no2']
    #                + np.exp(p["K_transcription"]["nir"]["log"][0])
    #            )
    #         )
    #         * (np.exp(p["K_i_o2"]["transcription"]["log"][0])
    #            / (c['o2'] + np.exp(p["K_i_o2"]["transcription"]["log"][0])))
    #     )
    #     c_mrna_nir_norm_log = (
    #         p['r_mrna_log'][0]
    #         + log_c['nitrogen']['no2']
    #         - (tt.log1p(np.exp(log_c['nitrogen']['no2'] - p['K_transcription']['nir']['log'][0]))
    #            + p['K_transcription']['nir']['log'][0])
    #         + p["K_i_o2"]["transcription"]["log"][0]
    #         - (tt.log1p(np.exp(log_c['o2'] - p['K_i_o2']['transcription']['log'][0]))
    #            + p['K_i_o2']['transcription']['log'][0])
    #     )
    #     mrna_norm = c.setdefault('mrna_norm', {})
    #     log_mrna_norm = log_c.setdefault('mrna_norm', {})
    #     mrna_norm['nir'] = c_mrna_nir_norm
    #     log_mrna_norm['nir'] = c_mrna_nir_norm_log

    # Normalize mRNA concentrations with biomass
    mrna_norm = c.setdefault("mrna_norm", {})
    log_mrna_norm = log_c.setdefault("mrna_norm", {})
    for gene in c["mrna"].keys():
        mrna_norm[gene] = c["mrna"][gene] / c["bio"]["alive"]
        log_mrna_norm[gene] = log_c["mrna"][gene] - log_c["bio"]["alive"]

    for key in c_dense:
        if isinstance(c_dense[key], dict):
            for key_inner in c_dense[key]:
                path = ["c_dense", key, key_inner]
                pm.Deterministic("_".join(path), c_dense[key][key_inner])
        else:
            path = ["c_dense", key]
            pm.Deterministic("_".join(path), c_dense[key])

    for key in c:
        if isinstance(c[key], dict):
            for key_inner in c[key]:
                path = ["c", key, key_inner]
                pm.Deterministic("_".join(path), c[key][key_inner])
        else:
            path = ["c", key]
            pm.Deterministic("_".join(path), c[key])

    # StudentT distributed Data errors
    observed = {}
    for data_var in data.keys():
        if data_var not in [
            "bio_alive",
            "gas_o2",
            "nitrogen_no2",
            "gas_no",
            "gas_n2o",
            "gas_n2",
            "mrna_norm_nir",
            "mrna_norm_nar",
            "mrna_norm_nor",
            "mrna_norm_nos",
        ]:
            continue
        sim_var = "c_" + data_var
        if "mrna_norm" in data_var:
            path = data_var.rsplit("_", 1)
        else:
            path = data_var.split("_")
        simulated = reduce(operator.getitem, path, log_c)
        indices = np.where(data[data_var]["indices"])[0]

        if data_var == "bio_alive":
            mu = simulated[indices]
        else:
            background = pm.Normal(f"background_{data_var}", mu=-18, sd=3)
            mu = tt.log1p(np.exp(simulated - background))[indices] + background

        pm.Deterministic(f"mu_{data_var}", mu)
        observed["sim_var"] = pm.StudentT(
            "_".join([data_var, "observed"]),
            nu=7,
            mu=mu,
            sigma=data[data_var]["std_vals"] + 0.1,
            observed=data[data_var]["mean_vals"],
        )
```

```python
lib = sunode._cvodes.lib
lib.CVodeSStolerances(solver._ode, 1e-14, 1e-14)
lib.CVodeSStolerancesB(solver._ode, solver._odeB, 1e-12, 1e-12)
lib.CVodeQuadSStolerancesB(solver._ode, solver._odeB, 1e-12, 1e-12)
lib.CVodeSetMaxNumSteps(solver._ode, 5000)
lib.CVodeSetMaxNumStepsB(solver._ode, solver._odeB, 5000)
lib.CVodeSetMaxStep(solver._ode, 60)
lib.CVodeSetMaxStepB(solver._ode, solver._odeB, 300)
```

```python
func = model.logp_dlogp_function()
func.set_extra_values({})
```

```python
N_prior = 100
with model:
    prior = pm.sample_prior_predictive(N_prior, random_seed=42)
```

```python
np.log(ds["NO2-"].dropna(dim="time")).plot.line("x", x="time")
```

```python
ds["NO2-"].dropna(dim="time")
```

```python
np.log(ds["N2O"].dropna(dim="time")).plot.line("x", x="time")
```

```python
data["gas_n2o"]
```

```python
prior_points = [
    {var.name: prior[var.name][i] for var in model.free_RVs} for i in range(N_prior)
]
```

```python
logps = []
for point in prior_points:
    logps.append(func(func.dict_to_array(point))[0])
logps = np.array(logps)
```

```python
np.isfinite(logps).mean()
```

```python
sns.distplot(logps[np.isfinite(logps)])
```

### Check gradients and stability numerically

```python
for i in range(5):
    x0 = {var.name: prior[var.name][i] for var in model.free_RVs}
    x0 = func.dict_to_array(x0)
    %timeit func(x0)
```

```python
eps = 5e-8
h = np.zeros_like(x0)
# h[]

h = func.array_to_dict(h)
h["r_mrna_log"] += eps
h = func.dict_to_array(h)

a, grad_a = func(x0)
b, grad_b = func(x0 + h)

(b - a) / eps
```

```python
func.array_to_dict(grad_a)
```

```python
# Stability of gradient
func.array_to_dict((grad_a - grad_b) / eps)
```

### Check prior predictive

```python
too_slow = prior["c_nitrogen_n2"][:, -1] < 0.2 * model_builder.c_norm["nitrogen"]["n2"]
too_fast = prior["c_nitrogen_n2"][:, 4] > 1.0 * model_builder.c_norm["nitrogen"]["n2"]
good = ~(too_slow | too_fast)
s = pd.Series(index=pd.RangeIndex(0, len(good), name="realization"), name="quality")
s[good] = "good"
s[too_fast] = "too_fast"
s[too_slow] = "too_slow"
```

```python
print(sum(good), sum(too_fast), sum(too_slow))
```

```python
plt.plot(ds.time.values, prior["c_bio_alive"][good, :].T, "C0", alpha=0.08)
plt.plot(ds.time.values, prior["c_bio_alive"][too_slow, :].T, "C1", alpha=0.08)
plt.plot(ds.time.values, prior["c_bio_alive"][too_fast, :].T, "C3", alpha=0.08);
```

```python
plt.plot(ds.time.values, prior["c_gas_o2"][good, :].T, "C0", alpha=0.08)
plt.plot(ds.time.values, prior["c_gas_o2"][too_slow, :].T, "C1", alpha=0.08)
plt.plot(ds.time.values, prior["c_gas_o2"][too_fast, :].T, "C3", alpha=0.08);
```

```python
plt.plot(ds.time.values, prior["c_o2"][good, :].T, "C0", alpha=0.08)
plt.plot(ds.time.values, prior["c_o2"][too_slow, :].T, "C1", alpha=0.08)
plt.plot(ds.time.values, prior["c_o2"][too_fast, :].T, "C3", alpha=0.08);
```

```python
plt.plot(ds.time.values, prior["c_nitrogen_no3"][good, :].T, "C0", alpha=0.08)
plt.plot(ds.time.values, prior["c_nitrogen_no3"][too_slow, :].T, "C1", alpha=0.08)
plt.plot(ds.time.values, prior["c_nitrogen_no3"][too_fast, :].T, "C3", alpha=0.08);
```

```python
plt.plot(ds.time.values, prior["c_nitrogen_no2"][good, :].T, "C0", alpha=0.08)
plt.plot(ds.time.values, prior["c_nitrogen_no2"][too_slow, :].T, "C1", alpha=0.08)
plt.plot(ds.time.values, prior["c_nitrogen_no2"][too_fast, :].T, "C3", alpha=0.08)
# plt.plot(ds.time.values[data['nitrogen_no2']['indices']], data['nitrogen_no2']['mean_vals'])
```

```python
plt.plot(ds.time.values, prior["c_nitrogen_no"][good, :].T, "C0", alpha=0.08)
plt.plot(ds.time.values, prior["c_nitrogen_no"][too_slow, :].T, "C1", alpha=0.08)
plt.plot(ds.time.values, prior["c_nitrogen_no"][too_fast, :].T, "C3", alpha=0.08);
```

```python
plt.plot(ds.time.values, prior["c_nitrogen_n2o"][good, :].T, "C0", alpha=0.08)
plt.plot(ds.time.values, prior["c_nitrogen_n2o"][too_slow, :].T, "C1", alpha=0.08)
plt.plot(ds.time.values, prior["c_nitrogen_n2o"][too_fast, :].T, "C3", alpha=0.08);
```

```python
plt.plot(ds.time.values, prior["c_nitrogen_n2"][good, :].T, "C0", alpha=0.08)
plt.plot(ds.time.values, prior["c_nitrogen_n2"][too_slow, :].T, "C1", alpha=0.08)
plt.plot(ds.time.values, prior["c_nitrogen_n2"][too_fast, :].T, "C3", alpha=0.08);
```

```python
plt.plot(ds.time.values, prior["c_co2"][good, :].T, "C0", alpha=0.08)
plt.plot(ds.time.values, prior["c_co2"][too_slow, :].T, "C1", alpha=0.08)
plt.plot(ds.time.values, prior["c_co2"][too_fast, :].T, "C3", alpha=0.08);
```

```python
plt.plot(
    ds.time.values[data["mrna_norm_nir"]["indices"]],
    prior["mu_mrna_norm_nir"][good, :].T,
    "C0",
    alpha=0.08,
)
plt.plot(
    ds.time.values[data["mrna_norm_nir"]["indices"]],
    prior["mu_mrna_norm_nir"][too_slow, :].T,
    "C1",
    alpha=0.08,
)
plt.plot(
    ds.time.values[data["mrna_norm_nir"]["indices"]],
    prior["mu_mrna_norm_nir"][too_fast, :].T,
    "C3",
    alpha=0.08,
)
plt.plot(
    ds.time.values[data["mrna_norm_nir"]["indices"]], data["mrna_norm_nir"]["mean_vals"]
)
```

```python
plt.plot(
    ds.time.values[data["mrna_norm_nar"]["indices"]],
    prior["mu_mrna_norm_nar"][good, :].T,
    "C0",
    alpha=0.08,
)
plt.plot(
    ds.time.values[data["mrna_norm_nar"]["indices"]],
    prior["mu_mrna_norm_nar"][too_slow, :].T,
    "C1",
    alpha=0.08,
)
plt.plot(
    ds.time.values[data["mrna_norm_nar"]["indices"]],
    prior["mu_mrna_norm_nar"][too_fast, :].T,
    "C3",
    alpha=0.08,
)
plt.plot(
    ds.time.values[data["mrna_norm_nar"]["indices"]], data["mrna_norm_nar"]["mean_vals"]
)
```

```python
plt.plot(
    ds.time.values[data["mrna_norm_nor"]["indices"]],
    prior["mu_mrna_norm_nor"][good, :].T,
    "C0",
    alpha=0.08,
)
plt.plot(
    ds.time.values[data["mrna_norm_nor"]["indices"]],
    prior["mu_mrna_norm_nor"][too_slow, :].T,
    "C1",
    alpha=0.08,
)
plt.plot(
    ds.time.values[data["mrna_norm_nor"]["indices"]],
    prior["mu_mrna_norm_nor"][too_fast, :].T,
    "C3",
    alpha=0.08,
)
plt.plot(
    ds.time.values[data["mrna_norm_nor"]["indices"]], data["mrna_norm_nor"]["mean_vals"]
)
```

```python
var_names = [var.name for var in model.free_RVs]
df = pd.DataFrame(
    {name: prior[name] for name in var_names},
    index=pd.RangeIndex(0, N_prior, name="realization"),
)
df.columns.name = "variable"
df_tidy = df.unstack().reset_index()

df_tidy = df_tidy.join(s, on="realization")
```

```python
g = sns.FacetGrid(
    df_tidy, hue="quality", col="variable", col_wrap=4, sharex=False, sharey=False
)
g.map(sns.distplot, 0, rug=True)
g.add_legend();
```

```python
df = df.join(s, on="realization")
```

```python
sns.pairplot(df, hue="quality");
```

```python
var1 = "rmax_o2_log"
var2 = "rmax_no3_log"
sns.scatterplot(var1, var2, data=df, hue="quality")
```

### Find MAP

```python
from scipy import optimize, linalg
```

```python
def loss(x):
    logp, grad = func(x)
    return -logp, -grad


path = []


def callback(x):
    path.append(x)


minima = []
func_minima = []
context_minima = []


def callback2(x, f, context):
    minima.append(x)
    func_minima.append(x)
    context_minima.append(context)


start_point = prior_points[0]
```

```python
%%time
opt = optimize.dual_annealing(
    func=lambda x: loss(x)[0],
    bounds=[
        (-1e3, 1e3),
    ]
    * len(prior_points[1]),
    x0=func.dict_to_array(start_point),
    callback=callback2,
    local_search_options={
        "method": "BFGS",
        "jac": lambda x: loss(x)[1],
        "options": {"maxiter": 2000},
        "callback": callback,
    },
)
```

```python
%%time
opt = optimize.shgo(
    func=lambda x: loss(x)[0],
    bounds=[
        (None, None),
    ]
    * len(prior_points[1]),
    callback=callback,
    options={"jac": True, "maxiter": 2000},
    minimizer_kwargs={"method": "BFGS"},
)
```

```python
%%time
opt = optimize.minimize(
    fun=loss,
    x0=func.dict_to_array(start_point),
    #     method='CG',
    jac=True,
    options={"maxiter": 2000},
    callback=callback,
)
```

```python
opt
```

```python
path = np.array(path)
```

```python
losses = [loss(x)[0] for x in path[::10]]
```

```python
plt.plot(losses)
```

```python
map_params = xr.DataArray.from_dict(
    {
        "dims": ("parameter"),
        "coords": {
            "parameter": {
                "dims": "parameter",
                "data": list(func.array_to_dict(opt.x).keys()),
            }
        },
        "data": opt.x,
        "name": "map_solution",
    }
)
```

```python
map_params
```

```python
# map_params.to_dataset().to_zarr("traces/map_solution_2020-02-21_two_genes.zarr")
```

```python
map_ds = xr.open_zarr("traces/map_solution_2020-02-21_two_genes.zarr").load()
```

```python
map_ds.map_solution.values
```

```python
stds = func.array_to_dict(np.sqrt(np.diag(opt.hess_inv)))
means = func.array_to_dict(opt.x)
```

```python
start_map = []
for i in range(6):
    vals = {}
    for name in means:
        vals[name] = 0.2 * stds[name] * np.random.randn() + means[name]
    start_map.append(vals)
```

```python
for point in start_map:
    print(func(func.dict_to_array(point))[0])
```

```python
loss(func.dict_to_array(start_point))
```

```python
func.array_to_dict(opt.x / func.dict_to_array(start_point))
```

```python
dets = theano.function(model.free_RVs, model.deterministics)(
    **func.array_to_dict(map_ds.map_solution.values)
)
dets = {var.name: dets[i] for i, var in enumerate(model.deterministics)}
```

```python
def create_dict(d):
    x = {}
    for key, vals in d.items():
        if vals.ndim == 0:
            x[key] = vals
        elif "c_" in key and not "c_dense" in key:
            x[key] = ("time", vals)
        elif "c_dense" in key:
            x[key] = ("time_dense", vals)
        else:
            x[key] = (tuple(f"{key}_dim_{i}" for i in range(vals.ndim)), vals)
    x["time"] = ds.time
    x["time_dense"] = np.linspace(0, (ds.time.values).max(), 1000)
    return x


d = create_dict(dets)
# d_params = dict(map_params)
d_params = dict(map_ds.map_solution.to_series())
map_data = xr.Dataset(
    data_vars={**d, **d_params}, attrs={"created_at": "2020-02-21", "optimizer": "BFGS"}
)
```

```python
# map_data.to_zarr("traces/dets_map_solution_2020-02-21_two_genes.zarr")
```

```python
t = ds.time[data["mrna_norm_nir"]["indices"]]
plt.plot(t, np.exp(dets["mu_mrna_norm_nir"]))
plt.plot(t, np.exp(data["mrna_norm_nir"]["measures"]), "x")
```

```python
t = ds.time[data["mrna_norm_nar"]["indices"]]
plt.plot(t, np.exp(dets["mu_mrna_norm_nar"]))
plt.plot(t, np.exp(data["mrna_norm_nar"]["measures"]), "x");
```

```python
t = ds.time[data["gas_n2"]["indices"]]
plt.plot(t, np.exp(dets["mu_gas_n2"]))
plt.plot(t, np.exp(data["gas_n2"]["measures"]), "x");
```

```python
t = ds.time[data["gas_o2"]["indices"]]
plt.plot(t, np.exp(dets["mu_gas_o2"]))
plt.plot(t, np.exp(data["gas_o2"]["measures"]), "x")
```

```python
t = ds.time[data["nitrogen_no2"]["indices"]]
plt.plot(t, np.exp(dets["mu_nitrogen_no2"]))
plt.plot(t, np.exp(data["nitrogen_no2"]["measures"]), "x");
```

```python
t = ds.time[data["bio_alive"]["indices"]]
plt.plot(t, np.exp(dets["mu_bio_alive"]))
plt.plot(t, np.exp(data["bio_alive"]["measures"]), "x");
```

```python
from scipy import linalg
```

```python
linalg.eigvalsh(opt.hess_inv + 1e-8 * np.eye(len(opt.x)))
```

```python
# start = [{var.name: prior[var.name][i] for var in model.free_RVs} for i in range(6)]
start = start_map
```

#### Call solver manually

```python
# point = {var.name: tr[var.name].isel(draw=1, chain=1).values for var in model.free_RVs}
# point = div_points[-2]
# point = trace2[-5]
# point = {var.name: point[var.name] for var in model.free_RVs}
# point = prior_points[1]
point = func.array_to_dict(opt.x)
point = {var.name: point[var.name] for var in model.free_RVs}

func_y0 = theano.function(model.free_RVs, model["y0_flat"], on_unused_input="ignore")
y0_div = func_y0(**point)

params_func = theano.function(
    model.free_RVs, model["params_flat"], on_unused_input="ignore"
)
params_div = params_func(**point)

solver.set_derivative_params(params_div.view(solver.derivative_params_dtype)[0])

# tvals = np.linspace(0, (ds.time.values * 3600).max(), 1_000_000)
tvals = ds.time.values * 3600

y_out, grad_out, lamda_out = solver.make_output_buffers(tvals)
y_out[...] = np.nan

# sunode._cvodes.lib.CVodeSetConstraints(solver._ode, sunode._cvodes.ffi.NULL)

point_ = {var: point[var.name] for var in model.free_RVs}
# point_[y0_flat] = y0_div
flat_y_vals = theano.function(model.free_RVs, flat_y, on_unused_input="ignore")(**point)
grads_func = theano.function(
    [flat_y] + model.free_RVs,
    tt.grad(sum(model[var.name].logpt for var in model.observed_RVs), flat_y),
    on_unused_input="ignore",
)
grads = grads_func(flat_y_vals, **point)


# TODO
# grads[...] = 1

solver.solve_forward(0, tvals, y0_div[None], y_out)
# solver.solve_backward(tvals[-1], 0, tvals, grads, grad_out, lamda_out)
```

```python
lamda_out
```

```python
plt.plot(y_out[:, 4])
```

```python
odeB_mem = sunode._cvodes.lib.CVodeGetAdjCVodeBmem(solver._ode, solver._odeB)
```

```python
out = sunode._cvodes.ffi.new("double*")
sunode._cvodes.lib.CVodeGetActualInitStep(odeB_mem, out)
out[0]
```

```python
out = sunode._cvodes.ffi.new("long*")
sunode._cvodes.lib.CVodeGetNumRhsEvals(odeB_mem, out)
out[0]
```

```python
quad_all_out = np.full((len(tvals), problem.n_params), np.nan)
lamda_all_out = np.full((len(tvals), len(lamda_out)), np.nan)

solver.solve_forward(0, tvals, y0_div[None].view(np.float64), y_out)
solver.solve_backward(
    tvals[-1],
    0,
    tvals,
    grads,
    grad_out,
    lamda_out,
    lamda_all_out=lamda_all_out,
    quad_all_out=quad_all_out,
)
```

```python
solver._user_data.error_states
```

```python
tvals[-1]
```

```python
grads[-1]
```

```python
plt.plot(tvals, np.arcsinh(quad_all_out[::-1]));
```

```python
plt.plot(np.arcsinh(lamda_all_out));
```

```python
y_np = np.full(problem.n_states, np.nan)
y = sunode.from_numpy(y_np)
sunode._cvodes.lib.CVodeGetAdjY(solver._ode, tvals[-1], y.c_ptr)
```

```python
# out_t = sunode._cvodes.ffi.new('double[1]')
```

```python
lamda_np = np.full(problem.n_states, np.nan)
lamda = sunode.from_numpy(lamda_np)
sunode._cvodes.lib.CVodeGetB(solver._ode, solver._odeB, tvals[-1], lamda.c_ptr)
```

### Investigate adjoint solution

```python
plt.plot(tvals, y_out.view(problem.state_dtype)["nitrogen"]["no3"])
plt.plot(tvals, y_out.view(problem.state_dtype)["nitrogen"]["no2"])
plt.plot(tvals, y_out.view(problem.state_dtype)["nitrogen"]["n2"])
plt.plot(tvals, y_out.view(problem.state_dtype)["o2"])
```

```python
plt.plot(tvals[::-1], quad_all_out);
```

```python
plt.plot(
    tvals[::-1],
    quad_all_out.view(problem.derivative_subset.subset_dtype)["Y"]["o2"]["log"],
);
```

```python
plt.plot(
    tvals[::-1],
    quad_all_out.view(problem.derivative_subset.subset_dtype)["K_subs"]["o2"]["log"],
);
```

```python
plt.plot(
    tvals[::-1],
    quad_all_out.view(problem.derivative_subset.subset_dtype)["K_i_o2"]["reaction"][
        "log"
    ],
);
```

```python
plt.plot(tvals[::-1], lamda_all_out);
```

```python
plt.plot(tvals[::-1], lamda_all_out.view(problem.state_dtype)["nitrogen"]["no2"])
```

```python
plt.plot(tvals[::-1], lamda_all_out.view(problem.state_dtype)["nitrogen"]["no3"])
```

```python
plt.plot(tvals[::-1], lamda_all_out.view(problem.state_dtype)["nitrogen"]["n2"])
```

```python
plt.plot(tvals[::-1], lamda_all_out.view(problem.state_dtype)["bio"]["alive"])
```

```python
plt.plot(tvals[::-1], lamda_all_out.view(problem.state_dtype)["gas"]["o2"])
```

```python
plt.plot(
    tvals[::-1], np.arcsinh(lamda_all_out.view(problem.state_dtype)["bio"]["alive"])
)
```

```python
rhs = problem.make_rhs()
```

```python
out = np.full((problem.n_states,), np.nan)
```

```python
# idx = (~np.isfinite(y_out).all(1)).nonzero()[0][0] - 3
idx = 406270
t = tvals[idx]
y = y_out[idx].view(problem.state_dtype)[0]
```

```python
plt.plot(y_out.view(problem.state_dtype)["nitrogen"]["no3"][idx - 13000 : idx + 100000])
plt.axhline(0)
```
