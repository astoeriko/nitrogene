---
jupyter:
  jupytext:
    formats: ipynb,md
    main_language: python
    notebook_metadata_filter: -kernelspec
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.11.5
---

<!-- #region -->
# Delayed Toxicity Effects on Transcription

## Toxicity Effets on Transcription
To describe the inhibiting effects of toxicity on transcription we multiply the transcription rate with a toxicity function $f_{\text{tox}}$. It scales the rate with a values between 0 and 1. For instance, we can use:
$$f_{\text{tox}} = \frac{{K_{\text{tox}}}^p}{{K_{\text{tox}}}^p + {C_{NO_2^-}}^p}$$
However, toxicity effects do not necessarily act immediately on the organism and they could persist for some time even when the level of toxic substance already declined. In that case we need to account for the delayed effect in our description.


## Describing Toxicity With a Distributed Delay
We can describe a delayed toxicity effect by applying a convolution with a distribution $p$ of time delays $\tau$.
We consider only positive time delays, that is $p(τ) = 0$ for $τ < 0$.
If we describe the undelayed toxicity $T$ with a function $f_{\text{tox}}$ of the concentration $c$, there is to ways of applying the convolution:
Either, we convolute the concentration and compute the toxicity from the averaged concentration, or we compute the toxicity first and convolute it.
More generally, let us define two functions $f$, $g$.
We apply $f$ before the convolution,
\begin{equation}
y = f(c)\,,
\end{equation}
and obtain the toxicity $T$ by applying $g$ to the convoluted quantity $\bar{y}$:
\begin{equation}
T = g(\bar{y})
\end{equation}
In most cases, we will choose either $f$ or $g$ to be the identity function while the other one is $f_{\text{tox}}$.

The delayed quantity $\bar{y}$ is given by
\begin{align}
\bar{y} (t) &= (y * p) (t) = \int_{-\infty}^{\infty} y(t -\tau) p(τ)d τ\\
&= (y * p) (t) = \int_{0}^{\infty} y(t -\tau) p(τ)d τ\,,
\end{align}
the latter equality holding as $p = 0$ for negative $\tau$.
Instead of integrating over several time differences $\tau$ we can also use a time point $t^\prime<t$ as integration variable:
\begin{equation}
\bar{y} (t) = \int_{-\infty}^{t} y(t^\prime) p(t-t^\prime)d t^\prime
\end{equation}

## Solution for Particular Distribution Functions
For certain distribution functions $p$ we can transform the delay differential equation into a system of ordinary differential equations by introducing additional state variables.
This requires specifying initial conditions for $\bar{y}$.
We assume that $y$ has constant known value $y_{t\leq t_0}$ for $t\leq t_0$ so that we can evaluate the convolution integral defining $\bar{y}$ up to $t_0$ and obtain initial conditions.

### Exponential Distribution
The normalized exponential distribution is given by
\begin{equation}
p(τ) = λ\exp\left(-λτ\right)\,,
\end{equation}
with normalization constant $\lambda$.
Thus
\begin{equation}
\bar{y} (t) = λ \int_{-\infty}^{t} y(t^\prime) \exp\left(-λ(t-t^\prime)\right)d t^\prime\,.
\end{equation}
Differentiating with respect to $t$ yields
\begin{equation}
\frac{d \bar{y}}{d t} = λ \left(y - \bar{y}\right)
\end{equation}
with initial condition
\begin{equation}
\bar{y}_0 = y_{t\leq t_0}\,.
\end{equation}

### Gamma Distribution
The gamma distribution with shape parameters $α$ and $λ$ is given by
\begin{equation}
p(τ) = \frac{λ^α}{Γ(α)} τ^{α-1} \exp\left(-λτ\right)\,,
\end{equation}
where $Γ$ is the gamma function.
For integer values of α we can obtain $\bar{y}$ by solving $α$ ordinary differential equations (MacDonald, 1978):
$\bar{y} = u_{α}$

\begin{equation}
\frac{d u_i}{d t} = λ \left(u_{i-1} - u_i\right) \qquad\text{for } 1<i\leq α,
\end{equation}
\begin{equation}
\frac{d u_1}{d t} = λ \left(y - u_1\right)
\end{equation}
with initial condition
\begin{equation}
u_{i, 0} = y_{t\leq t_0}\,.
\end{equation}

### Sum of Gamma Distributions
The distribution is given by the sum of gamma distributions with shape parameters $α$ and $λ$,
\begin{equation}
p(τ) = \frac{1}{α} \sum_{i=1}^{α}\frac{λ^i}{Γ(i)} τ^{i-1} \exp\left(-λτ\right)\,,
\end{equation}
where $Γ$ is the gamma function.
As in the case of the gamma distribution, we solve $α$ additional ODEs:
\begin{equation}
\frac{d u_i}{d t} = λ \left(u_{i-1} - u_i\right) \qquad\text{for } 1<i\leq α,
\end{equation}
\begin{equation}
\frac{d u_1}{d t} = λ \left(y - u_1\right)
\end{equation}
with initial condition
\begin{equation}
u_{i, 0} = y_{t\leq t_0}\,.
\end{equation}

We compute $\bar{y}$ with
\begin{equation}
\bar{y} = \frac{1}{α} \sum_{i=1}^{α} u_{i}\,.
\end{equation}


### Uniform Distribution
The normalized exponential distribution is given by
\begin{equation}
p(τ) = \begin{cases}
1, \text{ for } \tau \leq t + \Delta t_{\text{before}}\\
0, \text{ otherwise}
\end{cases}\,,
\end{equation}

\begin{equation}
\frac{d \bar{y}}{d t} = \frac{1}{t - t_0 + \Delta t_{\text{before}}} \left(y - \bar{y}\right)
\end{equation}
with initial condition
\begin{equation}
\bar{y}_0 = y_{t\leq t_0}\,.
\end{equation}

## References
MacDonald, N. (1978). *Time lags in biological models* (Vol. 27). Springer Berlin Heidelberg. https://doi.org/10.1007/978-3-642-93107-9

<!-- #endregion -->

# Comparison of different toxicity models
We tested different toxicity models using different delay distributions and applying the dealy either to nitrite concentrations or to the toxicity function.

In the following, we compare the results of the different models.

```python
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import pandas as pd
import arviz as az
import seaborn as sns

import nitrogene.model.plotting as plotting
import nitrogene.model.reaction_model as reaction_model
```

### Read in the results

```python
trace_files = {
    (
        "sum_of_gamma",
        "concentration",
        "unbounded",
    ): "traces/smc_trace_2020_04_11_test_toxicity_sum_of_gamma_conc.nc",
    (
        "sum_of_gamma",
        "toxicity",
        "unbounded",
    ): "traces/smc_trace_2020_04_11_test_toxicity_sum_of_gamma_tox.nc",
    (
        "gamma",
        "concentration",
        "unbounded",
    ): "traces/smc_trace_2020_04_11_test_toxicity_gamma_conc.nc",
    (
        "gamma",
        "toxicity",
        "unbounded",
    ): "traces/smc_trace_2020_04_11_test_toxicity_gamma_tox.nc",
    (
        "uniform",
        "concentration",
        "unbounded",
    ): "traces/smc_trace_2020_04_23_test_toxicity_uniform_conc.nc",
    (
        "uniform",
        "toxicity",
        "unbounded",
    ): "traces/smc_trace_2020_04_23_test_toxicity_uniform_tox.nc",
    (
        "exponential",
        "concentration",
        "unbounded",
    ): "traces/smc_trace_2020_04_23_test_toxicity_exponential_conc.nc",
    (
        "exponential",
        "toxicity",
        "unbounded",
    ): "traces/smc_trace_2020_04_23_test_toxicity_exponential_tox.nc",
    ("none", "none", "none"): "traces/smc_trace_2020_04_12_nar_nir_without_tox.nc",
    (
        "sum_of_gamma",
        "concentration",
        "bounded",
    ): "traces/smc_trace_2020_04_30_test_bounded_toxicity_sum_of_gamma_conc.nc",
    (
        "sum_of_gamma",
        "toxicity",
        "bounded",
    ): "traces/smc_trace_2020_04_30_test_bounded_toxicity_sum_of_gamma_tox.nc",
    (
        "gamma",
        "concentration",
        "bounded",
    ): "traces/smc_trace_2020_04_29_test_bounded_toxicity_gamma_conc.nc",
    (
        "gamma",
        "toxicity",
        "bounded",
    ): "traces/smc_trace_2020_04_29_test_bounded_toxicity_gamma_tox.nc",
    (
        "uniform",
        "concentration",
        "bounded",
    ): "traces/smc_trace_2020_04_30_test_bounded_toxicity_uniform_conc.nc",
    (
        "uniform",
        "toxicity",
        "bounded",
    ): "traces/smc_trace_2020_04_30_test_bounded_toxicity_uniform_tox.nc",
    (
        "exponential",
        "concentration",
        "bounded",
    ): "traces/smc_trace_2020_04_29_test_bounded_toxicity_exponential_conc.nc",
    (
        "exponential",
        "toxicity",
        "bounded",
    ): "traces/smc_trace_2020_04_29_test_bounded_toxicity_exponential_tox.nc",
    (
        "none",
        "none",
        "bounded",
    ): "traces/smc_trace_2020_04_30_test_bounded_toxicity_no_dist.nc",
}
```

```python
model_data = {key: az.from_netcdf(path) for key, path in trace_files.items()}
```

```python
posteriors = {key: data.posterior for key, data in model_data.items()}
```

```python
from functools import partial


def compute_toxicity(trace, dist, conv_var, tox_func):
    # Check what variable to use
    if dist == "gamma":
        var = trace["c_dense_X"].isel(lag_order=-1)
    elif dist == "sum_of_gamma":
        var = trace["c_dense_X"].sum(dim="lag_order") / (trace["lag_order"].max() + 1)
    elif dist == "none":
        var = trace.c_dense_nitrogen_no2
    else:
        var = trace["c_dense_X"]

    # Check if toxicity function need to be applied
    if conv_var == "toxicity":
        return var
    elif conv_var in ["concentration", "none"]:
        # define the toxicity function to use
        if tox_func == "unbounded":
            K = np.exp(trace["K_tox_log"])
            p = trace["p_tox"]
            return reaction_model.tox(c=var, K=K, p=p)
        elif tox_func == "bounded":
            K = np.exp(trace["K_tox_log"])
            p = trace["p_tox"]
            f_min = trace["f_min"]
            return reaction_model.tox_bounded(c=var, K=K, p=p, f_min=f_min)
        elif tox_func == "none":
            return var.where(False)
        else:
            raise ValueError(f"Unknown option for toxicity function {tox_func}")
    else:
        assert False
```

```python
tox_data = {}
for (dist, conv_var, tox_func), trace in posteriors.items():
    tox = compute_toxicity(
        posteriors[(dist, conv_var, tox_func)], dist, conv_var, tox_func
    )
    trace["toxicity"] = tox
    tox_vars = [
        "toxicity",
        "c_dense_mrna_norm_nar",
        "c_dense_mrna_norm_nir",
        "c_dense_nitrogen_no3",
        "c_dense_nitrogen_no2",
    ]
    tox_data[(dist, conv_var, tox_func)] = trace[tox_vars]
```

```python
tox_ds = xr.concat(
    tox_data.values(),
    dim=pd.MultiIndex.from_tuples(
        tox_data, names=["distribution", "convoluted_variable", "toxicity_function"]
    ),
).rename({"concat_dim": "model"})
# Renaming dims and vars separately does not work—Ishould file a bug report with a simple example
# .rename_dims({"concat_dim": "model"}).rename_vars({"concat_dim": "model"})
```

## Compare transcript time series

```python
sns.set(
    context="notebook",
    palette=plotting.color_palettes["paul_tol_muted"],
    color_codes=True,
    font="Libertinus Sans",
    font_scale=1.2,
)
```

```python
colors = sns.color_palette()
blue = colors[0]
red = colors[1]
green = colors[2]

ds = xr.open_dataset("../../experimental_data/data_Qu_2015.nc")
color_mapping = plotting.make_color_mapping("muted")
data_plotter = plotting.DataPlotter(ds, color_mapping)
```

```python
tox_vars = ["toxicity", "c_dense_mrna_norm_nar", "c_dense_mrna_norm_nir"]
g = (
    tox_ds[tox_vars]
    .to_array()
    .isel(chain=0, draw=slice(0, 50))
    .plot.line(
        x="time_dense",
        col="variable",
        row="model",
        sharey=False,
        figsize=(10, 24),
        add_legend=False,
        hue="draw",
        alpha=0.3,
    )
)
g.fig.subplots_adjust(hspace=0.22)
g.set_titles(template="{value}", rotation=0)
for l, n in zip(g.row_labels, g.row_names):
    l.set_rotation(0)
    l.set_text(
        f"distribution={n[0]}\nconvoluted variable={n[1]}\ntoxicity function={n[2]}"
    )
for axes in g.axes:
    # Adjust the axis limits
    axes[0].set_ylim((0, 1.1))
    axes[1].set_ylim((-3e-6, 6e-5))
    axes[2].set_ylim((-3e-6, 6e-5))
    # Set the colors according to the variable
    for l in axes[0].lines:
        l.set_color(green)
    for l in axes[1].lines:
        l.set_color(red)
    for l in axes[2].lines:
        l.set_color(blue)
    # Plot the data
    data_plotter.plot_mrna_data(["nar"], axes[1])
    axes[1].xaxis.label.set_text("")
    axes[1].yaxis.label.set_visible(False)
    data_plotter.plot_mrna_data(["nir"], axes[2])
    axes[2].xaxis.label.set_text("")
    axes[2].yaxis.label.set_visible(False)
g.set_xlabels("time [h]")
column_labels = {
    "toxicity": "Toxicity [-]",
    "c_dense_mrna_norm_nar": "narG [transcripts/cell]",
    "c_dense_mrna_norm_nir": "nirS [transcripts/cell]",
}
for l, n in zip(g.col_labels, g.col_names):
    l.set_text(column_labels[n])
# g.fig.savefig("plots/toxicity_model_comparison.pdf", bbox_inches="tight")
```

## Compute model comparison criteria
Compare the models based on Pareto Smoothed importance sampling  Leave One Out (PSIP-LOO) cross validation and the widely applicable information criterion (WAIC).

```python
model_data_single_key = {"_".join(key): data for key, data in model_data.items()}
```

```python
import copy


def concat_log_likelihood(tr):
    tr = copy.copy(tr)
    vals = []
    for val in tr.log_likelihood.data_vars:
        vals.append(tr.log_likelihood[val].values)

    empty = tr.log_likelihood.drop_vars(tr.log_likelihood.data_vars)

    empty["all_observed"] = (
        ("chain", "draw", "observation"),
        np.concatenate(vals, axis=-1),
    )

    tr.log_likelihood = empty
    return tr
```

```python
model_data_single_observed = {
    model: concat_log_likelihood(tr) for model, tr in model_data_single_key.items()
}
```

```python
logps = {model: tr.log_likelihood for model, tr in model_data_single_key.items()}
logps = (
    xr.concat(
        logps.values(),
        dim=pd.MultiIndex.from_tuples(
            tox_data, names=["distribution", "convoluted_variable", "toxicity_function"]
        )
        #     dim=pd.Index(logps.keys(), name="model")
    )
    .rename({"concat_dim": "model"})
    .sel(chain=0)
)
# Convert back to flat index
# logps["model"] = logps.indexes["model"].to_flat_index().map("_".join)
```

```python
logp_diffs = logps - logps.mean("model")
```

```python
def apply_keep(keep, funcname):
    def apply(ds):
        dims = [dim for dim in ds.dims if dim not in keep]
        return getattr(ds, funcname)(dims)

    return apply


logp_diffs.mean("draw").apply(apply_keep(["model"], "min")).to_dataframe().T.plot.bar(
    figsize=(13, 5)
)
```

```python
logp_diffs.nitrogen_no2_observed.mean("draw").min("nitrogen_time")
```

```python
data = (
    logp_diffs.assign_coords(
        model=logp_diffs.indexes["model"].to_flat_index().map("_".join)
    )
    .nitrogen_no2_observed.isel(nitrogen_time=58)
    .to_dataframe("diff")
    .reset_index()
)
sns.violinplot(
    x="model",
    y="diff",
    data=data,
)
plt.xticks(rotation=90)
```

```python
logp_diffs.nitrogen_time[18]
```

```python
tr = model_data_single_key["sum_of_gamma_concentration_unbounded"]
for var in tr.log_likelihood.data_vars:
    sns.distplot(tr.log_likelihood[var], label=var)
plt.legend()
```

```python
tr.log_likelihood.nitrogen_no2_observed.sel(chain=0, draw=slice(0, 100)).plot.line(
    x="nitrogen_time", add_legend=False
);
```

```python
ics_loo_stacking = az.compare(model_data_single_observed, ic="loo", method="stacking")
ics_loo_pseudo_bma = az.compare(
    model_data_single_observed, ic="loo", method="BB-pseudo-BMA"
)
```

```python
ics_loo_pseudo_bma
```

```python
ics_waic_stacking = az.compare(model_data_single_observed, ic="waic", method="stacking")
ics_waic_pseudo_bma = az.compare(
    model_data_single_observed, ic="waic", method="BB-pseudo-BMA"
)
```

```python
ics_waic_pseudo_bma
```

```python
ics_waic_stacking.to_latex(
    buf="plots/toxicity_model_comparison_waic_stacking.tex",
    #     float_format="{:0.2e}".format,
    caption="Model comparison based on the widely applicable information criterion (WAIC) using stackingfor averaging.",
)
ics_loo_stacking.to_latex(
    buf="plots/toxicity_model_comparison_loo_stacking.tex",
    #     float_format="{:0.2e}".format,
    caption="Model comparison based on pareto-smoothed importance sampling leave-one-out cross-validation (PSIS-LOO) using stacking for averaging.",
)
ics_waic_pseudo_bma.to_latex(
    buf="plots/toxicity_model_comparison_waic_pseudo_bma.tex",
    #     float_format="{:0.2e}".format,
    caption="Model comparison based on the widely applicable information criterion (WAIC) using pseudo-BMA for averaging.",
)
ics_loo_pseudo_bma.to_latex(
    buf="plots/toxicity_model_comparison_loo_pseudo_bma.tex",
    #     float_format="{:0.2e}".format,
    caption="Model comparison based on pareto-smoothed importance sampling leave-one-out cross-validation (PSIS-LOO) using pseudo-BMA for averaging.",
)
```

```python
ax = az.plot_density(
    list(model_data_single_key.values()),
    data_labels=model_data_single_key.keys(),
    var_names=["K_tox_log", "p_tox", "f_min", "t12_tox_log"],
    textsize=20,
)
plt.subplots_adjust(left=0.18, wspace=0.05)
ax[0].legend(bbox_to_anchor=(-1.0, 0.5), loc="center left", fontsize=20)
# plt.figlegend(loc="center left", title="Model",)
```

## Plot the time lag distributions

```python
from scipy import stats
```

```python
def compute_delay_distribution(tr, dist):
    tau = xr.DataArray(np.linspace(0, 7, 1000), dims="time_lag", attrs={"units": "h"})
    dims = ["time_lag"]
    coords = {"time_lag": tau}
    if dist in ["gamma", "sum_of_gamma", "exponential"]:
        lmbda = np.log(2) * np.exp(-tr["t12_tox_log"])
        tau_ext = tau.expand_dims(dim=lmbda.coords, axis=list(range(-lmbda.ndim, 0)))
        dims.extend(lmbda.dims)
        coords = {**coords, **lmbda.coords}
        if dist == "gamma":
            alpha = 4
            d = stats.gamma(a=alpha, scale=1 / lmbda)
            vals = d.pdf(tau_ext)
        elif dist == "sum_of_gamma":
            alpha = np.arange(4) + 1
            vals = np.zeros_like(tau_ext)
            for a in alpha:
                d = stats.gamma(a=a, scale=1 / lmbda)
                vals += d.pdf(tau_ext)
            vals /= len(alpha)
        elif dist == "exponential":
            d = stats.expon(scale=1 / lmbda)
            vals = d.pdf(tau_ext)

        return xr.DataArray(data=vals, dims=dims, coords=coords)
    #     elif dist == "uniform":

    #     elif dist = "none":

    else:
        raise ValueError(f"Unknown distribution: {dist}")
```

```python
delay_dists = {}
for (dist, conv_var, tox_func), tr in model_data.items():
    if dist in ["uniform", "none"]:
        continue
    try:
        delay_dists[(dist, conv_var, tox_func)] = compute_delay_distribution(
            tr.posterior, dist
        )
    except:
        print(dist, conv_var, tox_func, tr.posterior)
delay_dists = (
    xr.concat(
        delay_dists.values(),
        dim=pd.MultiIndex.from_tuples(
            delay_dists,
            names=["distribution", "convoluted_variable", "toxicity_function"],
        ),
    )
    .rename({"concat_dim": "model"})
    .sel(chain=0)
)
```

```python
ds_plot = (
    delay_dists.sortby(["toxicity_function", "convoluted_variable"])
    .unstack(dim="model")
    .stack(model=("toxicity_function", "convoluted_variable"))
    #            .rename({"convoluted_variable": "convoluted variable",})
    .sel(draw=slice(0, 30), time_lag=slice(0, 4))
)
ds_plot["distribution"] = ds_plot.distribution.str.replace("_", " ")

g = ds_plot.plot.line(
    x="time_lag", col="distribution", row="model", add_legend=False, figsize=(7, 5)
)
g.set_xlabels("time lag [h]")
g.set_titles("{coord}:\n{value}", horizontalalignment="left")
for l, n in zip(g.row_labels, g.row_names):
    l.set_rotation(0)
    l.set_text(f"toxicity function={n[0]}\nconvoluted variable={n[1]}")
g.fig.subplots_adjust(hspace=0.08, wspace=0.08)
g.fig.savefig("plots/toxicity_model_delay_distributions.pdf", bbox_inches="tight")
```

```python
sns.heatmap(delay_dists.integrate(dim="time_lag").to_pandas(), center=1)
```
