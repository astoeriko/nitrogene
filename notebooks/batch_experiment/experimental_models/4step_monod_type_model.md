---
jupyter:
  jupytext:
    formats: ipynb,md
    main_language: python
    notebook_metadata_filter: -kernelspec
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.11.5
---

```python
import os

# os.environ['NUMBA_DEVELOPER_MODE'] = '1'
# os.environ['NUMBA_BOUNDSCHECK'] = '1'
# os.environ['NUMBA_ENABLE_PROFILING'] = '1'
```

```python
import numpy as np
import matplotlib.pyplot as plt
import operator
from functools import reduce

import pandas as pd
import seaborn as sns
import pymc3 as pm
import nesttool
import xarray as xr
import arviz
import theano
import theano.tensor as tt
import sunode.symode.problem
import sunode.wrappers.as_theano

import nitrogene.reaction_model as reaction_model
import nitrogene.utils
import nitrogene.plotting as plotting
import nitrogene.pymc_model as pymc_model
```

```python
model_args = {
    "gas_phase": "transient",
    "mrna_at_quasi_steady_state": False,
    "classical_monod": True,
    "two_step_model": False,
    "cell_decay": False,
    "growth_substrates": ["o2"],
    "growth_yield_units": "cells/mol_DOC",
    "carbon_for_growth": False,
    "doc_rate_limitation": False,
    "smooth_gas_sampling": True,
    "enzymes_to_simulate": None,
    "transcription_self_inhibition": False,
    "nitrite_toxicity": False,
}
kwargs = {
    "log_scale": True,
}

model_builder = reaction_model.ModelBuilder(**model_args, **kwargs)
model_builder.define_model()
```

```python
ds = xr.open_dataset("../../experimental_data/data_Qu_2015.nc")
# Exclude third replicate during the peak as it is a bit delayed compared to the others.
exclude_times = ds.time[(ds.time > 23) & (ds.time < 30)]
ds["nirS"].loc[dict(time=exclude_times, replicate=2)] = np.nan
ds["narG"].loc[dict(time=exclude_times, replicate=2)] = np.nan
data = nitrogene.utils.process_data_for_likelihood(ds, transform="boxcox")
```

# Two step denitrification classical Monod model

## Simulated reactions
- oxic respiration (growth)
- denitrification (no growth): NO<sub>3</sub><sup>-</sup> -> NO<sub>2</sub><sup>-</sup> -> N<sub>2</sub>

## Reparametrizations
The classical fomulation of the Monod rate law leads to strong correlations between $r_{max}$ and $K_s$.
This can be alleviated by reparametrization.
Instead of choosing the rate at the limit of $c \to \infty$, $r_{max}$ as a parameter, we choose the rate $r_{c_{fix}}$ at a fixed concentration $c_{fix}$ as a parameter.
$r_{max}$ can then be recalculated via
$$r_{max} = r_{c_{fix}} \cdot \frac{K_s + c_{fix}}{c_{fix}} $$
(This is for the case without inhibition, like for oxygen.)

```python
with pm.Model() as model:
    p = model_builder.default_params  # TODO rename to params
    # Define changeable variables with their prior distributions
    p["Y"]["o2"]["log"] = pm.StudentT("Y_o2_log", mu=37, sigma=0.5, nu=10)
    K_i_o2_no3_log = pm.Normal("K_i_o2_no3_log", mu=-15, sigma=3)
    p["K_i_o2"]["reaction"]["no3"]["log"] = K_i_o2_no3_log
    K_i_o2_no2_log = pm.Normal("K_i_o2_no2_log", mu=-15, sigma=3)
    p["K_i_o2"]["reaction"]["no2"]["log"] = K_i_o2_no2_log

    cfix = 0.6e-4
    r_no3_cfix_log = pm.StudentT(
        "r_cfix_no3_log", mu=-49.74459069466712, sd=3.548070989375244, nu=10
    )
    rmax_no3_log = r_no3_cfix_log + np.log(
        (np.exp(K_i_o2_no3_log) + cfix) / np.exp(K_i_o2_no3_log)
    )
    pm.Deterministic("rmax_no3_log", rmax_no3_log)
    p["r_max"]["no3"]["log"] = rmax_no3_log
    #     p["r_max"]["no3"]["log"] = (pm.StudentT("rmax_no3_log", mu=-44, sigma=1.5, nu=10), ())

    p["r_max"]["no"]["log"] = pm.StudentT("rmax_no_log", mu=-44, sigma=1.5, nu=10)
    p["r_max"]["n2o"]["log"] = pm.StudentT("rmax_n2o_log", mu=-44, sigma=1.5, nu=10)

    cfix2 = 2e-9
    r_no2_cfix_log = pm.StudentT(
        "r_cfix_no2_log", mu=-44.20513286185366, sd=1.7210836095406539, nu=10
    )
    rmax_no2_log = r_no2_cfix_log + np.log(
        (np.exp(K_i_o2_no3_log) + cfix2) / np.exp(K_i_o2_no3_log)
    )
    pm.Deterministic("rmax_no2_log", rmax_no2_log)
    p["r_max"]["no2"]["log"] = rmax_no2_log
    #     p["r_max"]["no2"]["log"] = (pm.StudentT("rmax_no2_log", mu=-44, sigma=1.5, nu=10), ())

    p["K_subs"]["no3"]["log"] = pm.StudentT("Ks_no3_log", mu=-11, sigma=3, nu=10)
    p["K_subs"]["no2"]["log"] = pm.StudentT("Ks_no2_log", mu=-11, sigma=3, nu=10)
    p["K_subs"]["no"]["log"] = pm.StudentT("Ks_no_log", mu=-11, sigma=3, nu=10)
    p["K_subs"]["n2o"]["log"] = pm.StudentT("Ks_n2o_log", mu=-11, sigma=3, nu=10)

    Ks_log = pm.Normal("Ks_o2_log", mu=-13, sigma=2)
    cfix = 0.6e-4
    r_cfix_log = pm.StudentT(
        "r_cfix_o2_log", mu=-44.160977263447144, sd=1.514192676008769, nu=10
    )
    rmax_log = r_cfix_log + np.log((np.exp(Ks_log) + cfix) / cfix)
    pm.Deterministic("rmax_o2_log", rmax_log)

    p["K_subs"]["o2"]["log"] = Ks_log
    p["r_max"]["o2"]["log"] = rmax_log
    # p["r_max"]["o2"]["val"] = (np.exp(pm.Nor mal("rmax_o2_log", mu=-44, sigma=1.5)), ())

    model_builder.transform_parameters(p, module=tt)
    y0 = model_builder.generate_initial_values()
    #     y0["bio"]["alive"] = (pm.StudentT("y0_bio_alive_log", mu=18, sigma=1.5, nu=10), ())

    y0 = nesttool.apply_func_nested(y0, nitrogene.utils.as_double)
    params_with_shapes = nesttool.apply_func_nested(
        p, pymc_model.add_shapes_to_theano_vars
    )
    params = nesttool.apply_func_nested(params_with_shapes, nitrogene.utils.as_double)

    rhs = model_builder.generate_rhs_func()

    # Solve the ode
    rhs = model_builder.generate_rhs_func()
    t = ds.time.values * 3600
    t_dense = np.linspace(0, (ds.time.values * 3600).max(), 1000)
    t0 = 0
    (
        y,
        flat_y,
        problem,
        solver,
        y0_flat,
        params_flat,
        y_dense,
        flat_y_dense,
    ) = pymc_model.solve_ode(
        t,
        t_dense,
        y0,
        params,
        rhs,
        t0,
        solver_kwargs={"checkpoint_n": 500_000},
        dense_solver_kwargs={"checkpoint_n": 10},
    )

    pm.Deterministic("y0_flat", y0_flat)
    pm.Deterministic("params_flat", params_flat)
    pm.Deterministic("flat_y", flat_y)
    pm.Deterministic("flat_y_dense", flat_y_dense)

    # Unpack solution and save outpute in pymc variables
    c, c_dense, log_c = pymc_model.unpack_ode_solution(y, y_dense, model_builder)
    pymc_model.add_concentration_to_deterministics(c_dense, "c_dense")
    pymc_model.add_concentration_to_deterministics(c, "c")

    # # Define the likelihood
    observed = {}
    observed_var_names = [
        "bio_alive",
        "gas_o2",
        "nitrogen_no2",
        "gas_no",
        "gas_n2o",
        "gas_n2",
    ]
    for data_var in observed_var_names:
        # Get the simulation results at the right time points
        simulated, sim_var_name = pymc_model.get_simulated(data_var, log_c)
        indices = data[data_var]["flat_time_indices"]

        if data_var == "bio_alive":
            mu = simulated[indices]
        else:
            # Simulated measurements are the ode solution plus a background value
            background = pm.Normal(f"background_{data_var}", mu=-18, sd=3)
            mu = tt.log1p(np.exp(simulated - background))[indices] + background

        # transform from log to box-cox
        lmbda = data[data_var]["lambda"]
        mu = tt.expm1(mu * lmbda) / lmbda
        pm.Deterministic(f"mu_{data_var}", mu)

        # Estimate the data measurement error
        measure_error = pm.HalfNormal(f"measure_error_{data_var}", sd=0.1)

        # Model measurements as the simulation results with a Student's T
        # distributed data error (of the Box-Cox transformed values)
        observed[sim_var_name] = pm.StudentT(
            "_".join([data_var, "observed"]),
            nu=7,
            mu=mu,
            #             sigma=data[data_var]["constant_std"],
            sigma=measure_error,
            observed=data[data_var]["flat_measurements"],
        )
```

```python
# Adjust solver tolerances and maximum step numbers
lib = sunode._cvodes.lib
lib.CVodeSStolerances(solver._ode, 1e-14, 1e-14)
lib.CVodeSStolerancesB(solver._ode, solver._odeB, 1e-12, 1e-12)
lib.CVodeQuadSStolerancesB(solver._ode, solver._odeB, 1e-12, 1e-12)
lib.CVodeSetMaxNumSteps(solver._ode, 5000)
lib.CVodeSetMaxNumStepsB(solver._ode, solver._odeB, 5000)
lib.CVodeSetMaxStep(solver._ode, 60)
lib.CVodeSetMaxStepB(solver._ode, solver._odeB, 300)
```

```python
N_prior = 10
with model:
    prior = pm.sample_prior_predictive(N_prior, random_seed=42)
```

```python
prior_points = [
    {var.name: prior[var.name][i] for var in model.free_RVs} for i in range(N_prior)
]
```

```python
func = model.logp_dlogp_function()
func.set_extra_values({})
```

```python
logps = []
for point in prior_points:
    logps.append(func(func.dict_to_array(point))[0])
logps = np.array(logps)
```

```python
np.isfinite(logps).mean()
```

```python
sns.distplot(logps[np.isfinite(logps)])
```

### Check gradients and stability numerically

```python
for i in range(5):
    x0 = {var.name: prior[var.name][i] for var in model.free_RVs}
    x0 = func.dict_to_array(x0)
    %timeit func(x0)
```

```python
eps = 1e-7
h = np.zeros_like(x0)
# h[]

h = func.array_to_dict(h)
h["Y_o2_log"] += eps
h = func.dict_to_array(h)

a, grad_a = func(x0)
b, grad_b = func(x0 + h)

(b - a) / eps
```

```python
func.array_to_dict(grad_a)
```

```python
# Stability of gradient
func.array_to_dict((grad_a - grad_b) / eps)
```

### Check prior predictive

```python
too_slow = prior["c_nitrogen_n2"][:, -1] < 0.2 * model_builder.c_norm["nitrogen"]["n2"]
too_fast = prior["c_nitrogen_n2"][:, 4] > 1.0 * model_builder.c_norm["nitrogen"]["n2"]
good = ~(too_slow | too_fast)
s = pd.Series(index=pd.RangeIndex(0, len(good), name="realization"), name="quality")
s[good] = "good"
s[too_fast] = "too_fast"
s[too_slow] = "too_slow"
```

```python
print(sum(good), sum(too_fast), sum(too_slow))
```

```python
plt.plot(ds.time.values, prior["c_bio_alive"][good, :].T, "C0", alpha=0.08)
plt.plot(ds.time.values, prior["c_bio_alive"][too_slow, :].T, "C1", alpha=0.08)
plt.plot(ds.time.values, prior["c_bio_alive"][too_fast, :].T, "C3", alpha=0.08);
```

```python
plt.plot(ds.time.values, prior["c_gas_o2"][good, :].T, "C0", alpha=0.08)
plt.plot(ds.time.values, prior["c_gas_o2"][too_slow, :].T, "C1", alpha=0.08)
plt.plot(ds.time.values, prior["c_gas_o2"][too_fast, :].T, "C3", alpha=0.08);
```

```python
plt.plot(ds.time.values, prior["c_o2"][good, :].T, "C0", alpha=0.08)
plt.plot(ds.time.values, prior["c_o2"][too_slow, :].T, "C1", alpha=0.08)
plt.plot(ds.time.values, prior["c_o2"][too_fast, :].T, "C3", alpha=0.08);
```

```python
plt.plot(ds.time.values, prior["c_nitrogen_no3"][good, :].T, "C0", alpha=0.08)
plt.plot(ds.time.values, prior["c_nitrogen_no3"][too_slow, :].T, "C1", alpha=0.08)
plt.plot(ds.time.values, prior["c_nitrogen_no3"][too_fast, :].T, "C3", alpha=0.08);
```

```python
plt.plot(ds.time.values, prior["c_nitrogen_no2"][good, :].T, "C0", alpha=0.08)
plt.plot(ds.time.values, prior["c_nitrogen_no2"][too_slow, :].T, "C1", alpha=0.08)
plt.plot(ds.time.values, prior["c_nitrogen_no2"][too_fast, :].T, "C3", alpha=0.08)
# plt.plot(ds.time.values[data['nitrogen_no2']['indices']], data['nitrogen_no2']['mean_vals'])
```

```python
plt.plot(ds.time.values, prior["c_nitrogen_no"][good, :].T, "C0", alpha=0.08)
plt.plot(ds.time.values, prior["c_nitrogen_no"][too_slow, :].T, "C1", alpha=0.08)
plt.plot(ds.time.values, prior["c_nitrogen_no"][too_fast, :].T, "C3", alpha=0.08);
```

```python
plt.plot(ds.time.values, prior["c_nitrogen_n2o"][good, :].T, "C0", alpha=0.08)
plt.plot(ds.time.values, prior["c_nitrogen_n2o"][too_slow, :].T, "C1", alpha=0.08)
plt.plot(ds.time.values, prior["c_nitrogen_n2o"][too_fast, :].T, "C3", alpha=0.08);
```

```python
plt.plot(ds.time.values, prior["c_nitrogen_n2"][good, :].T, "C0", alpha=0.08)
plt.plot(ds.time.values, prior["c_nitrogen_n2"][too_slow, :].T, "C1", alpha=0.08)
plt.plot(ds.time.values, prior["c_nitrogen_n2"][too_fast, :].T, "C3", alpha=0.08);
```

```python
plt.plot(ds.time.values, prior["c_co2"][good, :].T, "C0", alpha=0.08)
plt.plot(ds.time.values, prior["c_co2"][too_slow, :].T, "C1", alpha=0.08)
plt.plot(ds.time.values, prior["c_co2"][too_fast, :].T, "C3", alpha=0.08);
```

```python
var_names = [var.name for var in model.free_RVs]
df = pd.DataFrame(
    {name: prior[name] for name in var_names},
    index=pd.RangeIndex(0, N_prior, name="realization"),
)
df.columns.name = "variable"
df_tidy = df.unstack().reset_index()

df_tidy = df_tidy.join(s, on="realization")
```

```python
g = sns.FacetGrid(
    df_tidy, hue="quality", col="variable", col_wrap=4, sharex=False, sharey=False
)
g.map(sns.distplot, 0, rug=True)
g.add_legend();
```

```python
df = df.join(s, on="realization")
```

```python
sns.pairplot(df, hue="quality");
```

## Sample with SMC

```python
import time
```

```python
start = time.time()
with model:
    trace = pm.sample_smc(draws=1000)
end = time.time()
```

```python
(end - start) / 3600
```

```python
import arviz
import re


def to_inference_data(
    *,
    measurement_dataset,
    data,
    prior=None,
    trace=None,
    posterior_predictive=None,
    model=None
):
    if prior is not None:
        test_data = prior
        keys = list(prior.keys())
    elif trace is not None:
        test_data = trace
        keys = trace.varnames
    elif posterior_predictive is not None:
        test_data = posterior_predictive
        keys = list(prior.keys())
    else:
        raise ValueError("One of prior, trace or posterior_predicitve must be given.")
    #     n_rep_nir = test_data["mrna_norm_nir_observed"].shape[-1]
    coords = {
        "time": measurement_dataset.time,
        "time_dense": np.linspace(0, measurement_dataset.time.max(), 1000),
        "gas_time": measurement_dataset.time[data["gas_n2"]["flat_time_indices"]],
        "no_time": measurement_dataset.time[data["gas_no"]["flat_time_indices"]],
        "n2o_time": measurement_dataset.time[data["gas_n2o"]["flat_time_indices"]],
        "nitrogen_time": measurement_dataset.time[
            data["nitrogen_no2"]["flat_time_indices"]
        ],
        "bio_time": measurement_dataset.time[data["bio_alive"]["flat_time_indices"]],
        "mrna_time": measurement_dataset.time[
            data["mrna_norm_nir"]["flat_time_indices"]
        ],
        "replicate": measurement_dataset.replicate,
        #         'replicate_nir': measurement_dataset.replicate[0:n_rep_nir],
    }
    #     nir_replicate_dim = "replicate_nir" if n_rep_nir < 3 else "replicate"
    dim_mapping = {
        "mu_bio_alive": ["bio_time"],
        "mu_nitrogen_no2": ["nitrogen_time"],
        "mu_mrna_norm_nir": ["mrna_time"],
        "mu_mrna_norm_nar": ["mrna_time"],
        "bio_alive_observed": ["bio_time"],
        "mrna_norm_nir_observed": ["mrna_time"],
        "mrna_norm_nar_observed": ["mrna_time"],
        "nitrogen_no2_observed": ["nitrogen_time"],
        "gas_o2_observed": ["gas_time"],
        "gas_no_observed": ["no_time"],
        "gas_n2o_observed": ["n2o_time"],
        "gas_n2_observed": ["gas_time"],
        "params_flat": ["parameter"],
        "flat_y": ["time", "variable"],
        "flat_y_dense": ["time_dense", "variable"],
        "y0_flat": ["variable"],
    }
    for var in keys:
        if re.match("c_dense_.*", var):
            substance = re.match("c_dense_(.*)", var).group(1)
            if substance == "X" and test_data[var].ndim > 1:
                dim_mapping[var] = ["time_dense", "lag_order"]
            else:
                dim_mapping[var] = ["time_dense"]
        elif re.match("c_.*", var):
            substance = re.match("c_(.*)", var).group(1)
            if substance == "X" and test_data[var].ndim > 1:
                dim_mapping[var] = ["time", "lag_order"]
            else:
                dim_mapping[var] = ["time"]
        elif re.match("mu_gas_.*", var):
            if var == "mu_gas_no":
                dim_mapping[var] = ["no_time"]
            elif var == "mu_gas_n2o":
                dim_mapping[var] = ["n2o_time"]
            else:
                dim_mapping[var] = ["gas_time"]

    return arviz.from_pymc3(
        prior=prior,
        trace=trace,
        posterior_predictive=posterior_predictive,
        model=model,
        coords=coords,
        dims=dim_mapping,
    )
```

```python
tr = to_inference_data(measurement_dataset=ds, data=data, trace=trace, model=model)
```

```python
tr.posterior.c_dense_nitrogen_no2.sel(chain=0, draw=slice(0, 50)).plot.line(
    x="time_dense", add_legend=False
)
ds["NO2-"].plot.line("x", x="time");
```

```python
tr.posterior.c_dense_gas_no.sel(chain=0, draw=slice(0, 50)).plot.line(
    x="time_dense", add_legend=False
)
ds.NO.plot.line("x", x="time");
```

```python
tr.posterior.c_dense_gas_n2o.sel(chain=0, draw=slice(0, 50)).plot.line(
    x="time_dense", add_legend=False
)
ds.N2O.plot.line("x", x="time");
```

```python
tr.to_netcdf("traces/smc_trace_2020_04_30_4step_classical_monod.nc")
```

### Sampling

```python
with model:
    pot = pm.step_methods.hmc.quadpotential.QuadPotentialFullAdapt(
        initial_cov=opt.hess_inv + 1e-8 * np.eye(len(opt.x)),
        initial_mean=func.dict_to_array(means),
        initial_weight=100,
        n=len(opt.x),
    )
    step = pm.NUTS(
        potential=pot,
        early_max_treedepth=9,
        max_treedepth=10,
        target_accept=0.9,
    )
    trace2 = pm.sample(
        discard_tuned_samples=False,
        tune=1000,
        draws=1000,
        cores=4,
        chains=4,
        start=start_map,
        step=step,
    )
```

```python
pm.traceplot(trace2, var_names=[var.name for var in model.free_RVs]);
```

```python
arviz.plot_trace(
    tr, var_names=[var.name for var in model.free_RVs if not var.name.endswith("__")]
);
```

```python
pm.rhat(tr)[[var.name for var in model.free_RVs if not var.name.endswith("__")]]
```

```python
pm.ess(tr)[[var.name for var in model.free_RVs if not var.name.endswith("__")]]
```
