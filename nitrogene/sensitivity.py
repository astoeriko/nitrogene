from functools import partial
from copy import deepcopy
import numpy as np
import nesttool
import sunode.dtypesubset
import nitrogene.reactive_transport_model as reactive_transport_model


def perturb_param(p, increment):
    """Compute the value of a parameter perturbed by a relative increment α.

    The perturbed parameter p* is related to the original parameter p by:
        p* = p + Δp = p + αp = (1 + α) p

    Parameters
    ----------
    p: float
        Original parameter value
    increment: float
        The relative increment α
    """
    return p * (1 + increment)


def perturb_log_param(logp, increment):
    """Compute the value of a log parameter perturbed by a relative increment α.

    The perturbed log parameter log(p*) is given by:
        log(p*) = log((1 + α) p) = log(1 + α) + log(p)

    Parameters
    ----------
    logp: float
        Log of the original parameter value
    increment: float
        The relative increment α
    """
    return logp + np.log1p(increment)


def set_perturbed_params(model, default_params_dict, param_paths, increments):
    perturbed_params = deepcopy(default_params_dict)
    for param_path, increment in zip(param_paths, increments):
        is_log_param = any("log" in subpath for subpath in param_path)
        if is_log_param:
            perturbed_log_parameter = perturb_log_param(
                nesttool.getitem_nested(perturbed_params, param_path),
                increment,
            )
            perturbed_params = nesttool.setitem_nested(
                perturbed_params, param_path, perturbed_log_parameter
            )
        else:
            perturbed_parameter = perturb_param(
                nesttool.getitem_nested(perturbed_params, param_path), increment
            )
            perturbed_params = nesttool.setitem_nested(
                perturbed_params, param_path, perturbed_parameter
            )
    # Now set the transformed parameter values
    model.reaction_model.transform_parameters(perturbed_params["reaction"], module=np)
    return perturbed_params


def compute_perturbed_ss_solution(
    model, t0, tvals, default_params_dict, param_path, increment=0.01
):
    return compute_perturbed_solution(
        model,
        t0,
        tvals,
        default_params_dict,
        param_path,
        increment=increment,
        time_slice=slice(-1, None),
    )


def compute_perturbed_solution(
    model,
    t0,
    tvals,
    default_params_dict,
    param_path,
    increment=0.01,
    time_slice=None,
):
    if time_slice is None:
        time_slice = slice(None, None)
    single_parameter = isinstance(param_path[0], str)
    multiple_parameters = isinstance(param_path[0], tuple)
    if single_parameter:
        param_paths = [param_path]
        assert isinstance(increment, float)
        increments = [increment]
    elif multiple_parameters:
        param_paths = param_path
        if isinstance(increment, float):
            increments = [increment] * len(param_paths)
        else:
            increments = increment
    else:
        assert False
    perturbed_params = set_perturbed_params(
        model, default_params_dict, param_paths, increments
    )
    try:
        model.reaction_params = perturbed_params["reaction"]
        model.transport_params = perturbed_params["transport"]
        # Compute the ODE solution
        raw_output, perturbed_solution = model.run_simulation(t0=0, tvals=tvals)
        # Select the steady state solution
        perturbed_solution_ss = perturbed_solution.isel(time=time_slice)
        # Compute reaction rates
        rates = model.compute_reaction_rates(
            raw_output[time_slice, :], perturbed_solution_ss
        )
        rates_states = reactive_transport_model.combine_rates_and_states(
            perturbed_solution_ss, rates
        )
    finally:
        model.reaction_params = default_params_dict["reaction"]
        model.transport_params = default_params_dict["transport"]
    return rates_states


def compute_numeric_sensitivity(perturbed_solution, solution, increment, is_log_param):
    """Compute a local sensitivity (relative to the parameter value) numerically.

    For non-log parameters:

    s = ∂f/∂p * p ≈ Δf/Δp * p = Δf/α

    For parameters on the log scale:

    s = ∂f/∂p * p = ∂f/∂logp ≈ Δf/Δlogp = Δf/log(1+α)

    Parameters
    ----------
    perturbed_solution: numpy array or xarray Dataarray
        solution obtained with the perturbed parameter
    solution: numpy array or xarray Dataarray
        solution obtained with the original parameter values
    increment: float
        relative increment α
    is_log_param: bool
        Flag indicating if the perturbed parameter is defined on a log scale
    """
    if is_log_param:
        return (perturbed_solution - solution) / np.log(1 + increment)
    else:
        return (perturbed_solution - solution) / increment
