---
jupyter:
  jupytext:
    formats: ipynb,md
    notebook_metadata_filter: -kernelspec
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.7
---

# Bank filtration (BF)
In this scenario, oxic river water infiltrates groundwater. The oxygen concentration in the river is either constant (BFC) or subject to diurnal fluctuations (BFP).

```python
import os
import textwrap

import arviz as az
import holoviews as hv
import hvplot.xarray
import matplotlib as mpl
import matplotlib.pyplot as plt
import nesttool as nt
import numpy as np
import pandas as pd
import seaborn as sns
import sunode
import sympy as sym
import xarray as xr

import nitrogene.reaction_model as reaction_model
import nitrogene.utils
import nitrogene.plotting
import nitrogene.reactive_transport_model as reactive_transport_model
```

```python
sns.set_theme(
    context="paper",
    palette=nitrogene.plotting.color_palettes["paul_tol_high_contrast"],
    rc=nitrogene.plotting.paper_rc,
)
```

```python
# Read in reaction parameters
tr = az.from_netcdf(
    "../../traces/nuts_trace_pymc3-ext_2021_02_05_nar_nir_tf_activate_nnr_by_no2_only.nc"
).posterior
exclude_patterns = [
    "c_",
    "mu_",
    "flat",
    "measure_error",
    "background",
    "y0",
    "r_cfix_o2_log",
]
var_names = [var for var in tr if not any(exlude in var for exlude in exclude_patterns)]
name_map = nitrogene.utils.make_parameter_name_map(tr, var_names)
```

```python
plot_path = "../../plots/groundwater_river_interface/bank_filtration"
```

## Model set-up
### BFC Bank filtration with a constant oxygen concentration

```python
# Initialize the model
model_constant_bc = reactive_transport_model.Model(
    calibrated_reaction_params=tr[var_names]
    .apply(lambda x: np.exp(x) if "n_hill" in x.name else x)
    .median(dim=("chain", "draw")),
    scenario="bank_filtration_constant_oxygen",
    parameter_name_map=name_map,
    solver_kw={"abstol": 1e-14, "reltol": 1e-10},
)
# Initialize biomass with an exponential profile (following the gradient in POC
# concentrations)
model_constant_bc.initial_values["bio"]["alive"] = (
    (np.exp(model_constant_bc.reaction_params["B_max"]["log"]) - 1e10)
    * np.exp(
        -model_constant_bc.reaction_params["lambda_poc"] * model_constant_bc.coords["x"]
    )
    + 1e10
) / model_constant_bc.reaction_params["c_norm"]["bio"]["alive"]
```

```python
%%time
# Run the simulation
raw_output_constant_bc, solution_constant_bc = model_constant_bc.run_simulation(
    t0=0, tvals=np.linspace(0, 8000 * 86400, 8000)
)
solution_constant_bc = solution_constant_bc.assign_coords(
    travel_time=(
        solution_constant_bc["x"] / model_constant_bc.transport_params["v"] / 86400
    ).assign_attrs(units="d", long_name="travel time")
)
```

```python
# Select steady state only
raw_output_constant_bc_ss = raw_output_constant_bc[-1, :]
solution_constant_bc_ss = solution_constant_bc.isel(time=slice(-1, None))
```

### BFC Bank filtration with a fluctuating oxygen concentration

```python
model = reactive_transport_model.Model(
    calibrated_reaction_params=tr[var_names]
    .apply(lambda x: np.exp(x) if "n_hill" in x.name else x)
    .median(dim=("chain", "draw")),
    scenario="bank_filtration_fluctuating_oxygen",
    parameter_name_map=name_map,
    solver_kw={"abstol": 1e-10, "reltol": 1e-8},
)
```

```python
# Set initial values based on the solution with constant O2 concentration
y0 = sunode.dtypesubset._as_dict(
    raw_output_constant_bc_ss.view(model.transport_problem.state_dtype)
)
y0 = nt.apply_func_nested(y0, np.squeeze)
model.initial_values = y0
```

```python
lib = sunode._cvodes.lib
ffi = sunode._cvodes.ffi
```

```python
%%time
# Solve the equation
t_vals = np.linspace(0, 100, 240 * 100) * 86400
raw_output, solution = model.run_simulation(t0=0, tvals=t_vals)
```

```python
out = ffi.new("long[1]")
lib.CVodeGetNumSteps(model.solver._ode, out)
out[0]
```

```python
solution = solution.assign_coords(
    travel_time=(solution["x"] / model.transport_params["v"] / 86400).assign_attrs(
        units="d", long_name="travel time"
    )
)
```

```python
times_per_day = int(1 // solution.time.diff("time").values[0])
```

## Concentration distributions
### BFC Constant oxygen concentration

```python
plotter_single_scenario = nitrogene.plotting.plot_periodic_constant_single_scenario(
    constant_solution=solution_constant_bc_ss.squeeze(drop=True),
    subplots_kw=dict(
        figsize=(7, 3),
        gridspec_kw=dict(hspace=0.25, wspace=0.4, top=0.85),
    ),
)
# plotter.fig.savefig(f"{plot_path}/concentration_distribution.pdf")
```

### BFP Fluctuating oxygen concentration

```python
import panel as pn
import panel.widgets as pnw
```

```python
plot_kwargs = {
    "width": 380,
    "height": 150,
    "fontsize": dict(legend="7pt"),
}
selection = solution.isel(time=slice(-times_per_day, None, 10)).isel(
    x=slice(None, None, 4)
)

hv.output(widget_location="bottom")
plot_all = nitrogene.plotting.concentration_distribution_animation(
    ds=selection, plot_kwargs=plot_kwargs
)
plot_all
```

```python
hv.save(
    plot_all,
    plot_path + "periodic_denitrification.html",
    fmt="scrubber",
    title="Concentration distributions",
)
```

### Comparison of the concentration distribution in both scenarios

```python
plotter = nitrogene.plotting.plot_periodic_constant_single_scenario(
    periodic_solution=solution.isel(time=slice(-times_per_day, None)).sel(
        x=slice(0, 2.5)
    ),
    constant_solution=solution_constant_bc_ss.isel(time=-1, drop=True).sel(
        x=slice(0, 2.5)
    ),
    subplots_kw=dict(figsize=(6, 3), gridspec_kw=dict(wspace=0.4, top=0.87)),
    legend_kw=dict(frameon=False),
    periodic_label="BFP",
    constant_label="BFC",
)

for a in plotter.axes_mapping.values():
    a.set_ylabel("\n".join(textwrap.wrap(a.get_ylabel(), 25)))
plotter.fig.align_ylabels()
```

## Shift in the location of the denitrification zone
Compute the location of the denitrification zone based on the peak nitrite concentrations.

```python
# Use only the last three days of the simulation when the concentrations reached stable
# cycles.
time_selection = slice(-3 * times_per_day, None)
solution_selection = solution.isel(time=time_selection)
```

```python
idx = solution_selection.nitrogen_no2.argmax("x")
peak_location = solution_selection.x.isel(x=idx).rename("nitrite peak location")
peak_location.plot(label="peak location")
plt.axhline(y=peak_location.mean(), label="mean value", linestyle="--")
plt.legend()
```

## Amplitude of transcript and enzyme concentration fluctuations relative to the mean

```python
def plot_coefficient_of_variation(var, clip=0.15):
    """Plot the coefficient of variation with respect to time.

    Parameters:
    var: xarray Dataarray
    clip: float
        Fraction of the maximum value at which to clip the values for plotting.
        (It does not make sense to look at large relative changes if the absolute values
        are tiny.)
    """
    cv = var.std("time") / var.mean("time")
    cv.where(var.mean("time") > clip * var.max()).plot()
```

```python
plot_coefficient_of_variation(solution_selection.mrna_nar, clip=0.2)
```

```python
plot_coefficient_of_variation(solution_selection.enz_nar, clip=0.2)
```

```python
path_periodic = "../../output_data/gw_river_denitrification/bank_filtration_fluctuating_oxygen"
path_constant = "../../output_data/gw_river_denitrification/bank_filtration_constant_oxygen"
solution.isel(time=slice(-times_per_day, None)).to_netcdf(
    os.path.join(path_periodic, "solution.nc")
)
solution_constant_bc_ss.isel(time=-1, drop=True).to_netcdf(
    os.path.join(path_constant, "solution.nc")
)
```

## Compute reaction rates

```python
rates = model.compute_reaction_rates(raw_output[time_selection, :], solution_selection)
rates_states = reactive_transport_model.combine_rates_and_states(
    solution_selection, rates
)
```

```python
rates_ds = reactive_transport_model.arange_rates_for_plotting(rates, solution_selection)
rates_ds = rates_ds.assign_coords(
    datetime=(
        "time",
        pd.to_datetime(
            rates_ds.time.values,
            unit="d",
        ),
    )
)
rates_ds = rates_ds.sortby("gene")
```

```python
# Compute rates for the constant boundary conditions
rates_constant_bc = model.compute_reaction_rates(
    raw_output_constant_bc_ss, solution_constant_bc_ss
)
rates_states_constant_bc = reactive_transport_model.combine_rates_and_states(
    solution_constant_bc_ss, rates_constant_bc
)
rates_ds_constant_bc = reactive_transport_model.arange_rates_for_plotting(
    rates_constant_bc, solution_constant_bc_ss
).isel(time=0, drop=True)
```

## Relationship between reaction rates and transcripts/enzymes
Transcript-to-gene ratios have been suggested as an estimator for per-cell reaciton rates.
With the model we can assess if that is true for the given set-up (using nitrifier cell concentrations instead of gene counts).
If we consider the fully transient transcript/enzyme concentrations and rates, the relationship is complicated.
The relationship between rates and transcripts has a different hysteretic daily cycle at each point in space.
Enzyme concentrations are very stable throughout the day but the rates are not, so the relationship is non-unique.

```python
def annotate_location(x, rates_ds, text_xy=None, **annot_kw):
    annot_data = (
        rates_ds.sel(variable="enzymes", gene="nar")
        .sel(x=x, method="nearest")
        .isel(time=slice(-4 * times_per_day, None, 3))
    )
    actual_x = annot_data.x
    annot_x = annot_data["concentration"].mean("time")
    annot_y = annot_data["turnover_rate"].mean("time") * 1.5
    return g.axes[0, 1].annotate(
        text=f"x = {actual_x.values:1.2f} m",
        xy=(annot_x.values, annot_y.values),
        xytext=text_xy,
        **annot_kw,
    )
```

```python
# Plotting
row_labels = {
    "nar": "$\\mathit{narG}$\n"
    + r"$\mathregular{{NO_3}^–}\, \to\, \mathregular{{NO_2}^–}$",
    "nir": "$\\mathit{nirS}$\n" + r"$\mathregular{{NO_2}^–}\, \to\, \mathregular{N_2}$",
}
selection = (
    rates_ds.isel(time=slice(-times_per_day, None, 5))
    .sel(x=slice(None, 1.5, 4))
    .assign(turnover_rate=lambda x: x.turnover_rate * 1e9)
)
selection = selection.assign(
    concentration=selection.where(
        lambda x: x.variable != "enzymes",
        selection["concentration"].sel(variable="enzymes") * 1e12,
    )["concentration"]
).assign_coords(variable=["transcripts", "enzymes"])
g = selection.plot.scatter(
    x="concentration",
    y="turnover_rate",
    col="variable",
    row="gene",
    hue="time_of_day",
    sharex=False,
    sharey="row",
    alpha=0.85,
    figsize=(5.5, 3.5),
    cmap="twilight_shifted",
    s=10,
)
# Set axis labels and titles
g.axes[1, 0].set_xlabel("$c$ [transcripts/L]")
g.axes[1, 1].set_xlabel("$c$ [pmol/L]")
g.set_ylabels("reaction rate [nM/s]")
g.set_titles(template="{value}")
# Customize the position of the row labels
for row, label, a in zip(g.row_names, g.row_labels, g.axes[:, 0]):
    left, lower, width, height = a.axes.get_position().bounds
    label.set(
        text=row_labels[row],
        anncoords="figure fraction",
        rotation="horizontal",
        horizontalalignment="center",
        linespacing=1.7,
        position=(0.07, lower + height / 2),
    )
# Make space for the new row labels
g.fig.subplots_adjust(left=0.23, right=0.85)
cbar_pos = g.fig.axes[-1].get_position().bounds
g.fig.axes[-1].set_position((0.88, cbar_pos[1], cbar_pos[2], cbar_pos[3]))

# Make the colorbar a bit thinner
cbar = g.fig.axes[-1]
cbar.set_aspect(1.8)
cbar.axes.set_facecolor((1, 1, 1, 0))

# Align labels and remove spines
g.fig.align_ylabels(g.axes)
sns.despine(bottom=True, left=True)

# Add continuous alphabetic subplot labels
for a, label in zip(g.axes.flat, "abcd"):
    a.annotate(text=label, xy=(0.1, 0.8), xycoords="axes fraction")

# Add arrows indicating the x-coordinate for single loops
annot_kw = dict(
    arrowprops={
        "arrowstyle": "-",
        "shrinkA": 0,
        "shrinkB": 3,
        "color": "k",
    },
    size=8,
)
x_values = (0.01, 0.09, 0.25, 0.49)
offsets = ((-30, 15), (-38, 25), (-38, -11), (-3, 20))
for x_val, offset in zip(x_values, offsets):
    annotate_location(
        x=x_val,
        rates_ds=selection,
        text_xy=offset,
        textcoords="offset points",
        **annot_kw,
    )
g.fig.savefig(
    f"{plot_path}/rate_correlation_time_variable.pdf",
    facecolor=(1, 1, 1, 0),
)
```

## Time averaged reaction rates
The weak correlation or hysteresis in the relationship between reaction rates and transcript and enzyme concentrations is at least partly related to the different response times of transcripts, enzymes and rates to changing subtrate and inhibitor concentrations, which fluctuate diurnally.
For the removal of nitrogen on the long term, these daily fluctuations are not interesting. Reaction rates averaged over a day are more representative when looking at this question. Therefore, it makes sense to check, how those time averaged rates would relate to molecular biological quantities.

```python
rates_states_average = rates_states_average = rates_states.isel(
    time=slice(-times_per_day, None)
).integrate("time")
```

```python
solution_selection = solution_selection.assign_coords(
    datetime=(
        "time",
        pd.to_datetime(
            solution_selection.time.values,
            unit="d",
        ),
    )
)
# Note: This only works because I have a constant time spacing
dt = rates_ds.time.diff("time").mean()
daily_averages = (
    rates_ds.swap_dims({"time": "datetime"})
    .reset_coords("time")
    .groupby("datetime.day")
    .reduce(np.trapz, dim="datetime", dx=dt)
    .swap_dims(day="time")
    .isel(time=slice(1, -1))
)
```

```python
rates_ds["turnover_rate_daily_average"] = daily_averages["turnover_rate"].reindex_like(
    rates_ds.time, method="pad"
)
```

```python
oxygen_concentration = solution["o2"].assign_coords(
    time=(
        "time",
        pd.to_datetime(
            solution.time.values,
            unit="d",
        ),
    )
)
```

```python
# Plotting
g = (
    rates_ds.assign(oxygen_concentration=oxygen_concentration).isel(
        time=slice(-times_per_day, None, 10), x=slice(None, None, 5)
    )
).plot.scatter(
    x="concentration",
    y="turnover_rate_daily_average",
    row="gene",
    col="variable",
    hue="travel_time",
    sharex="col",
    sharey="row",
    alpha=0.7,
    figsize=(6, 3.5),
    cmap="crest",
)
g.cbar.formatter.set_powerlimits((-3, 3))
g.axes[1, 0].set_xlabel("transcripts/cell")
g.axes[1, 1].set_xlabel("enzymes/cell")
nitrogen_labels = {"no3": "${\mathregular{NO_3}^-}$", "no2": "${\mathregular{NO_2}^-}$"}
for a, row_name in zip(g.axes[:, 0], g.row_names):
    n_compound = str(rates_ds.rate.sel(gene=row_name).values)
    a.set_ylabel(
        "\n".join(
            textwrap.wrap(
                f"average {nitrogen_labels[n_compound]} reaction rate\n [mol/cell/s]",
                width=45,
            )
        ),
        labelpad=12,
    )
sns.despine(bottom=True, left=True)
g.fig.align_ylabels(g.axes)

# g.fig.savefig(
#    f"{plot_path}rate_correlation_averaged_rates.pdf", bbox_inches="tight"
# )
```

The relationship between reaction rates and transcript concentrations is still very non-unique because the transcript concentrations fluctuate over the day.
Enzyme concentrations fluctuate less so that there is a clearer relationship with reaciton rates. Howefer, the relationship is not unique or linear.

```python
solution_daily_averages = (
    solution_selection.swap_dims({"time": "datetime"})
    .groupby("datetime.day")
    .reduce(np.trapz, dim="datetime", dx=dt)
    .swap_dims(day="time")
    .isel(time=slice(1, -1))
)
```

```python
# Plotting
g = (daily_averages.isel(time=-1)).plot.scatter(
    x="concentration",
    y="turnover_rate",
    row="gene",
    col="variable",
    sharex="col",
    sharey="row",
    hue="x",
    extend="min",
    alpha=0.7,
    figsize=(6, 3.5),
    cmap="crest",
)
g.axes[1, 0].set_xlabel("transcripts/cell")
g.axes[1, 1].set_xlabel("enzymes/cell")
for a, row_name in zip(g.axes[:, 0], g.row_names):
    n_compound = str(rates_ds.rate.sel(gene=row_name).values)
    a.set_ylabel(
        "\n".join(
            textwrap.wrap(
                f"average {nitrogen_labels[n_compound]} reaction rate\n [mol/cell/s]",
                width=45,
            )
        ),
        labelpad=12,
    )
sns.despine(bottom=True, left=True)
g.fig.align_ylabels(g.fig.axes[:-1])

# g.fig.savefig(f"{plot_path}rate_correlation_averaged_rates_averaged_concentrations.pdf", bbox_inches="tight")
```

When we also compute averages of concentrations over time, the relationship between concentrations and rates becomes very similar for transcripts and enzymes.
This indicates that averaged transcript concentrations would be a good proxy for enzyme concentrations.
However, as before, the relationship is non-linear and not unique – the same enzyme concentrations is related to different reaction rates when looking at different points in space.

### Comparison of the relationship between the constant and periodic scenario

```python
rates_ds_both_bc = xr.concat(
    [
        daily_averages.isel(time=-1, drop=True)[["concentration", "turnover_rate"]],
        rates_ds_constant_bc,
    ],
    dim=pd.Index(name="boundary_condition", data=["periodic", "constant"]),
)
```

```python
g = sns.relplot(
    x="concentration",
    y="turnover_rate",
    data=rates_ds_both_bc.to_dataframe().reset_index(),
    row="gene",
    col="variable",
    hue="boundary_condition",
    style="boundary_condition",
    s=60,
    height=2,
    aspect=1.5,
    alpha=0.7,
    facet_kws=dict(
        sharex="col",
        sharey="row",
    ),
    linewidth=0,
)
g.axes[1, 0].set_xlabel("$c$ [transcripts/L]")
g.axes[1, 1].set_xlabel("$c$ [mol/L]")
g.axes[0, 0].set_ylabel(
    "$\mathregular{{NO_3}^–}$ reaction rate\n [mol/L/s]", labelpad=12
)
g.axes[1, 0].set_ylabel(
    "$\mathregular{{NO_2}^–}$ reaction rate\n [mol/L/s]", labelpad=12
)
sns.despine(bottom=True, left=True)
g.fig.savefig(
    f"{plot_path}/rate_correlation_constant_vs_periodic.pdf",
    bbox_inches="tight",
    facecolor=(1, 1, 1, 0),
)
```

## “Correct” the rates for DOC limitation and oxygen inhibition

```python
inhibition_constants = xr.DataArray(
    data=[
        np.exp(model.reaction_params["K_i_o2"]["reaction"]["no3"]["log"]),
        np.exp(model.reaction_params["K_i_o2"]["reaction"]["no2"]["log"]),
    ],
    dims=("gene",),
    coords={var: rates_ds[var] for var in ["gene", "rate"]},
    name="inhibition_constant",
)
K_subs = xr.DataArray(
    data=[
        np.exp(model.reaction_params["K_subs"]["no3"]["log"]),
        np.exp(model.reaction_params["K_subs"]["no2"]["log"]),
    ],
    dims=("gene",),
    coords={var: rates_ds[var] for var in ["gene", "rate"]},
    name="half_saturation_constant",
)
```

```python
doc_limitation_term_constant_bc = reaction_model.mimen(
    solution_constant_bc_ss["doc"], model.reaction_params["K_doc"]
)
doc_correction_term_constant_bc = 1 / doc_limitation_term_constant_bc
o2_correction_term_constant_bc = (
    solution_constant_bc_ss["o2"] + inhibition_constants
) / inhibition_constants

nitrogen_concentrations_constant_bc = xr.concat(
    (
        solution_constant_bc_ss["nitrogen_no3"],
        solution_constant_bc_ss["nitrogen_no2"],
    ),
    dim=pd.Index(["nar", "nir"], name="gene"),
)
nitrogen_correction_term_constant_bc = 1 / reaction_model.mimen(
    nitrogen_concentrations_constant_bc, K_subs
)
```

```python
rates_ds_constant_bc["doc_corrected_rate"] = (
    rates_ds_constant_bc["turnover_rate"] * doc_correction_term_constant_bc
)
rates_ds_constant_bc["o2_doc_corrected_rate"] = (
    rates_ds_constant_bc["turnover_rate"]
    * o2_correction_term_constant_bc
    * doc_correction_term_constant_bc
)
rates_ds_constant_bc["o2_corrected_rate"] = (
    rates_ds_constant_bc["turnover_rate"] * o2_correction_term_constant_bc
)
rates_ds_constant_bc["all_corrections"] = (
    rates_ds_constant_bc["turnover_rate"]
    * o2_correction_term_constant_bc
    * doc_correction_term_constant_bc
    * nitrogen_correction_term_constant_bc
)
rates_ds_constant_bc["nitrogen_corrected_rate"] = (
    rates_ds_constant_bc["turnover_rate"] * nitrogen_correction_term_constant_bc
)
```

```python
rate_vars_constant_bc = [
    "doc_corrected_rate",
    "o2_doc_corrected_rate",
    "o2_corrected_rate",
    "all_corrections",
    "turnover_rate",
]
correction_names = {
    "doc_corrected_rate": "DOC",
    "o2_doc_corrected_rate": "O2 & DOC",
    "o2_corrected_rate": "O2",
    "all_corrections": "O2 & DOC & NO3/NO2",
    "turnover_rate": "none",
}
corrected_rates_constant_bc = (
    rates_ds_constant_bc[rate_vars_constant_bc]
    .rename(correction_names)
    .to_array(dim="correction", name="corrected_rates")
)
```

```python
with sns.color_palette(nitrogene.plotting.color_palettes["paul_tol_bright"]):
    g = (
        xr.merge((corrected_rates_constant_bc, rates_ds_constant_bc["concentration"]))
        .isel(
            time=-1,
        )
        .sortby("correction")
        .plot.scatter(
            x="concentration",
            y="corrected_rates",
            col="variable",
            row="gene",
            hue="correction",
            sharex="col",
            sharey="row",
            alpha=0.7,
            figsize=(6, 3.5),
            cmap="crest",
            s=20,
        )
    )
    g.figlegend.set_frame_on(False)
    g.fig.savefig(
        f"{plot_path}/rate_correlation_constant_bc_corrected_rates.pdf",
        bbox_inches="tight",
    )
```

## Integration of rates and concentrations over space

```python
integrated_rates = (rates_ds * solution_selection["bio_alive"]).integrate(coord="x")
```

```python
model.transport_params["v"] * model.transport_params["left"]["inflow_concentration"][
    "mean_val"
]["nitrogen"]["no3"] * 0.3
```

```python
integrated_rates.turnover_rate.plot(col="gene")
plt.ylim(bottom=0);
```

```python
integrated_rates.concentration.plot(col="gene", row="variable", sharey="row")
plt.ylim(bottom=0);
```

```python
g = integrated_rates.plot.scatter(
    x="concentration",
    y="turnover_rate",
    col="variable",
    row="gene",
    sharey="row",
    sharex="col",
    hue="time_of_day",
    cmap="twilight_shifted"
)
for a in g.axes.flat:
    a.set_ylim(bottom=0)
    a.set_xlim(left=0)
```

```python
daily_averages.to_netcdf(os.path.join(path_periodic, "rates_daily_averages.nc"))
```

```python
rates_ds_constant_bc.to_netcdf(os.path.join(path_constant, "rates.nc"))
```

## Time scales of production and decay for transcripts, enzymes and biomass

```python
def carbon_for_growth_factor(params, eacceptor):
    return 1 / (
        1
        - np.exp(model.reaction_params["Y"][eacceptor]["log"])
        * params["n_doc_growth"]
        * params["cell_weight"]["val"]
        / (params["M_bio"] * params["n_bio_growth"])
    )
```

```python
# Maximum doubling time for aerobic growth in hours
conversion_factor_o2 = (
    model.reaction_params["n_doc_energy"]["o2"]
    / model.reaction_params["n_energy_in"]["o2"]
    * carbon_for_growth_factor(model.reaction_params, "o2")
    * np.exp(model.reaction_params["Y"]["o2"]["log"])
)
np.log(2) / (
    np.exp(model.reaction_params["r_max"]["o2"]["log"]) * conversion_factor_o2
) / 3600
```

```python
# Maximum doubling time for growth on nitrate oxidation in hours
conversion_factor_no3 = (
    model.reaction_params["n_doc_energy"]["no3"]
    / model.reaction_params["n_energy_in"]["no3"]
    * carbon_for_growth_factor(model.reaction_params, "no3")
    * np.exp(model.reaction_params["Y"]["no3"]["log"])
)
np.log(2) / (
    np.exp(model.reaction_params["k_max"]["no3"]["log"])
    * np.exp(model.reaction_params["r_enz_log"])
    * conversion_factor_no3
    / reaction_model.AVOGADRO_CONSTANT
) / 3600
```

```python
# Maximum doubling time for growth on nitrite oxidation in hours
conversion_factor_no2 = (
    model.reaction_params["n_doc_energy"]["no2"]
    / model.reaction_params["n_energy_in"]["no2"]
    * carbon_for_growth_factor(model.reaction_params, "no2")
    * np.exp(model.reaction_params["Y"]["no2"]["log"])
)
np.log(2) / (
    np.exp(model.reaction_params["k_max"]["no2"]["log"])
    * np.exp(model.reaction_params["r_enz_log"])
    * conversion_factor_no2
    / reaction_model.AVOGADRO_CONSTANT
) / 3600
```

```python
# Half-life of biomass in days
np.log(2) / np.exp(model.reaction_params["k_dec"]["bio"]["log"]) / 3600 / 24
```

```python
conversion_factors = xr.DataArray(
    [conversion_factor_o2, conversion_factor_no3, conversion_factor_no2],
    coords={"rate": ("rate", ["o2", "no3", "no2"])},
    dims="rate",
)
doubling_times = (
    (
        np.log(2)
        / (rates.where(rates > 0) / solution_selection.bio_alive * conversion_factors)
        / 3600
    )
    .rename("doubling time")
    .assign_attrs(unit="h")
)
```

```python
# Plot the actual doubling times (assuming pseudo first-order kinetics)
doubling_times.min("x", keep_attrs=True).plot(col="rate", sharey=False)
```

## Sensitivity analysis

```python
# Parameters that will be considered in the sensitivity analysis
sens_parameters = [
    ("transport", "left", "inflow_concentration", "nitrogen", "no3"),
    ("transport", "left", "inflow_concentration", "doc"),
    ("transport", "left", "inflow_concentration", "o2"),
    ("reaction", "k_dec", "bio", "log"),
    ("reaction", "Y", "no3", "log"),
    ("reaction", "Y", "no2", "log"),
    ("reaction", "Y", "o2", "log"),
    ("reaction", "B_max", "log"),
    ("reaction", "k_max", "no3", "log"),
    ("reaction", "k_max", "no2", "log"),
    ("reaction", "r_max", "o2", "log"),
    ("reaction", "K_doc"),
    ("reaction", "K_subs", "no3", "log"),
    ("reaction", "K_subs", "no2", "log"),
    ("reaction", "K_subs", "o2", "log"),
    ("reaction", "K_i_o2", "reaction", "no3", "log"),
    ("reaction", "K_i_o2", "reaction", "no2", "log"),
    ("reaction", "K_i_o2", "transcription", "fnrp", "log"),
    ("reaction", "K_i_o2", "transcription", "nnr", "log"),
    ("reaction", "k_release", "doc"),
    ("reaction", "lambda_poc"),
    ("reaction", "c_sat_0", "doc"),
    ("reaction", "t_1_2", "enz", "log"),
    ("reaction", "r_mrna_log", "nar"),
    ("reaction", "r_mrna_log", "nir"),
    ("reaction", "r_enz_log"),
    ("reaction", "k_dissociation", "nnr", "log"),
    ("reaction", "k_dissociation", "narr", "log"),
    ("reaction", "k_bind", "narr", "no3", "log"),
    ("reaction", "k_bind", "narr", "no2", "log"),
    ("reaction", "k_bind", "nnr", "log"),
    ("reaction", "K_dna", "fnrp", "log"),
    ("reaction", "K_dna", "narr", "log"),
    ("reaction", "K_dna", "nnr", "log"),
]
```

```python
from copy import deepcopy
import nitrogene.sensitivity as sens
```

```python
sens_parameter_names = ["-".join(path) for path in sens_parameters]
default_params_dict = deepcopy(model_constant_bc.solver.get_params_dict())
```

```python
t_vals_sens = np.linspace(0, 3000 * 86400, 8000)
```

```python tags=[]
perturbed_rates_states = {}
for sens_param in sens_parameters:
    perturbed_solution = sens.compute_perturbed_ss_solution(
        model_constant_bc,
        t0=0,
        tvals=t_vals_sens,
        default_params_dict=default_params_dict,
        param_path=sens_param,
        increment=0.01,
    )
    perturbed_solution["rate_no3"] = perturbed_solution["rate_no3_norm"] * perturbed_solution["bio_alive"]
    perturbed_solution["rate_no2"] = perturbed_solution["rate_no2_norm"] * perturbed_solution["bio_alive"]
    perturbed_rates_states[sens_param] = perturbed_solution
```

```python
# Save the outputs from model runs with perturbed parameters
for i, (path, values) in enumerate(perturbed_rates_states.items()):
    mode = "w" if i == 0 else "a"
    values.to_netcdf(
        path="perturbed_rates_states_bf.nc",
        group="-".join(path),
        mode=mode
    )
```

```python
# Read in the outputs from model runs with perturbed parameters
perturbed_rates_states = {
    path: xr.open_dataset("perturbed_rates_states_bf.nc", group="-".join(path))
    for path in sens_parameters
}
```

```python
is_log_param = {path: any("log" in subpath for subpath in path) for path in sens_parameters}
```

```python
reference_rates_states = rates_states_constant_bc.assign(
    {
        "rate_no3": rates_states_constant_bc["rate_no3_norm"] * rates_states_constant_bc.bio_alive,
        "rate_no2": rates_states_constant_bc["rate_no2_norm"] * rates_states_constant_bc.bio_alive
    }
).isel(time=-1)
sens_numeric = {
    path: sens.compute_numeric_sensitivity(
        perturbed_rates_states.isel(time=-1),
        reference_rates_states,
        increment=0.01,
        is_log_param=is_log_param[path],
    ).drop_vars(["time_of_day", "travel_time"])
    for path, perturbed_rates_states in perturbed_rates_states.items()
}
```

```python
all_sens_numeric = xr.concat(sens_numeric.values(), dim=pd.Index(sens_parameter_names, name="parameter"))
```

```python
normalization_constants = reference_rates_states.integrate("x")
normalized_sensitivites = all_sens_numeric / normalization_constants
```

```python
normalized_sensitivites.to_netcdf(
    "normalized_sensitivities.nc", group="bank_filtration_constant_bc", mode="a"
)
```

```python
integrated_sensitivities = np.abs(all_sens_numeric).fillna(0).integrate("x")
integrated_sensitivities_norm = integrated_sensitivities / normalization_constants
```

```python
drop_vars = [var for var in integrated_sensitivities_norm if "norm" in var]
```

```python
integrated_sensitivities_norm.nitrogen_no2.to_series().plot.barh()
```

```python
all_perturbed_rates_states = xr.concat(perturbed_rates_states.values(), dim=pd.Index(sens_parameter_names, name="parameter"))
```

```python
sns.heatmap(
    data=integrated_sensitivities_norm.drop_vars(drop_vars)
    .to_array(name="integrated_sensitivity")
    .to_dataframe()["integrated_sensitivity"]
    .unstack(),
    xticklabels=True
)
```

### Principal Component Analysis (PCA)

```python
from sklearn.decomposition import PCA
```

```python
X = normalized_sensitivites.drop_vars(drop_vars).to_array().stack(space_state_var=("x", "variable")).fillna(0).values.T
```

```python
n_components = None
pca = PCA(n_components).fit(X)
```

```python
variance_ratio = xr.DataArray(
    pca.explained_variance_ratio_ * 100,
    dims=("principal_component",),
    name="explained_variance",
    attrs=dict(units="%", long_name="explained variance"),
    coords={
        "principal_component": (
            "principal_component",
            pd.RangeIndex(
                pca.n_components_,
            ),
            dict(long_name="principal component"),
        )
    },
)
```

```python
variance_ratio.plot.step(figsize=(5, 1.), where="mid")
plt.title("a", weight="bold")
plt.savefig(os.path.join(plot_path, "pca_explained_variance.pdf"), bbox_inches="tight")
```

```python
variance_ratio.max()
```

```python
principal_directions = xr.DataArray(
    pca.components_,
    dims=("principal_component", "parameter"),
    coords={"parameter": normalized_sensitivites.parameter},
)
```

```python
latex_labels = [
    nitrogene.plotting.get_latex_label("_".join(param_string.split("-")[1:]))
    for param_string in principal_directions.parameter.values
]
latex_labels = [
    f"$\\log({label[1:-1]})$" if not "log" in label else label for label in latex_labels
]
g = sns.catplot(
    y="weight",
    x="parameter",
    data=principal_directions.isel(principal_component=0)
    .to_dataframe(name="weight")
    .reset_index(),
    aspect=3,
    height=2,
    kind="bar",
    palette=nitrogene.plotting.color_palettes["paul_tol_bright"],
    facet_kws=dict(gridspec_kws=dict(left=0.08, right=0.98, bottom=0.4, top=0.9))
)
g.set_xticklabels(latex_labels, size=8, rotation=90)
plt.title("b", weight="bold")
g.fig.savefig(os.path.join(plot_path, "sensitivity_pca.pdf"))
```

### Compute the solution with an alternative parameter set

```python
weights = principal_directions.isel(principal_component=0)
h = -2
factors = np.exp(h * weights)
increments = np.expm1(h * weights)
# Multiplying the weights with a negative number leads to more bioavailable carbon,
# using a positive number makes the system more carbon limited
```

```python
factors
```

```python
solution_alternative_params = sens.compute_perturbed_ss_solution(
    model_constant_bc,
    t0=0,
    tvals=t_vals,
    default_params_dict=default_params_dict,
    param_path=[tuple(param.split("-")) for param in increments.parameter.values],
    increment=increments.values,
)
solution_alternative_params["rate_no3"] = (
    solution_alternative_params["rate_no3_norm"]
    * solution_alternative_params["bio_alive"]
)
solution_alternative_params["rate_no2"] = (
    solution_alternative_params["rate_no2_norm"]
    * solution_alternative_params["bio_alive"]
)
```

```python
solution_alternative_params.to_netcdf("solution_alternative_params.nc", group="bank_filtration_constant_bc", mode="a")
```

```python
solution_alternative_params_converted_units = (
    solution_alternative_params.assign(
        doc=lambda x: x.doc.assign_attrs(units="M"),
        rate_no3=lambda x: x.rate_no3.assign_attrs(units="mol/L/s"),
        rate_no2=lambda x: x.rate_no2.assign_attrs(units="mol/L/s"),
    )
).map(nitrogene.plotting.convert_concentration_units)
```

### Concentration distribution with the alternative parameters

```python
# Plot the concentration distribution at steady state
plotter = nitrogene.plotting.plot_periodic_constant_single_scenario(
    constant_solution=solution_alternative_params_converted_units.squeeze(drop=True),
    subplots_kw=dict(
        figsize=(6, 3),
        gridspec_kw=dict(hspace=0.25, wspace=0.5, top=0.9, right=0.95, bottom=0.18),
    ),
)
plotter.fig.align_ylabels()
plotter.fig.savefig(f"{plot_path}/concentration_distributions_alternative_params.pdf")
```

<!-- #region tags=[] -->
### Transcript–rate relationship with the alternative parameters
<!-- #endregion -->

```python
rates_alternative_params = xr.concat(
    [solution_alternative_params["rate_no3"], solution_alternative_params["rate_no2"]],
    dim=pd.Index(["no3", "no2"], name="rate"),
    combine_attrs="drop_conflicts"
).rename("rate")

rates_ds_alternative_params = reactive_transport_model.arange_rates_for_plotting(
    rates_alternative_params, solution_alternative_params_converted_units
)
rates_ds_alternative_params = rates_ds_alternative_params.sortby("gene")
```

```python
# Plotting
g = (
    rates_ds_alternative_params.map(nitrogene.plotting.convert_concentration_units)
    .sel(x=slice(None, 0.5))
    .isel(time=-1)
).plot.scatter(
    x="concentration",
    y="turnover_rate",
    col="variable",
    row="gene",
    hue="x",
    sharex=False,
    sharey="row",
    figsize=(6, 3.5),
    cmap="crest",
    s=20,
)
g.axes[1, 0].set_xlabel("$c$ [transcripts/L]")
g.axes[1, 1].set_xlabel("$c$ [pM]")
n_compound_labels = {
    "no3": "$\mathregular{{NO_3}^–}$",
    "no2": "$\mathregular{{NO_2}^–}$",
}
for a, row_name in zip(g.axes[:, 0], g.row_names):
    n_compound = str(rates_ds.rate.sel(gene=row_name).values)
    a.set_ylabel(
        f"{n_compound_labels[n_compound]} reaction rate\n [nmol/L/s]",
        labelpad=12,
    )
sns.despine(bottom=True, left=True)
g.fig.savefig(
    f"{plot_path}/rate_correlation_absolute_values_alternative_params.pdf",
    bbox_inches="tight",
)
```
