---
jupyter:
  jupytext:
    formats: ipynb,md
    notebook_metadata_filter: -kernelspec
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.7
---

# Monod-type denitrification model
In this model, denitrification rates are described by a Monod term:
$$r_{j} = \nu_{max}^i B \frac{C_{j}}{C_{j} + K_{j}} \frac{I_{reac}^i}{I_{reac}^i + C_{O2}} $$

```python
import os

import arviz
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pymc as pm
#import pymc3_ext as pmx
import seaborn as sns
import sunode.symode.problem
import sunode.wrappers
import aesara
import aesara.tensor as tt
import xarray as xr
from scipy import stats as stats
import nesttool

import nitrogene.plotting
import nitrogene.postprocessing as postprocessing
import nitrogene.pymc_model as pymc_model
import nitrogene.reaction_model as reaction_model
import nitrogene.utils
```

## Select ODE model options
We create a ``ModelBuilder`` object that will build the ODE right hand side function given the following model options:
* Compute fully transient equations of gas compounds:``gas_phase="transient"``
* Use a Monod-type model formulation for denitrification: ``classical_monod=True``
* Simulate denitrification as a 2-step process (nitrate to nitrite to N2): ``two_step_model=True``
* Do not consider a decay term for biomass: ``cell_decay=False``
* Consider only growth from aerobic respiration, not from nitrogen substrates: ``growth_substrates=["o2"]``
* Specify the growth yield in units of cells per mol of DOC: ``growth_yield_units="cells/mol_DOC"``
* Consider only carbon uptake for catabolism, not for anabolism: ``carbon_for_growth=False``
* Do not a Monod term for the carbon substrate in the reaction kinetics: ``doc_rate_limitaiton=False``
* Smooth out the description of gas sampling events to ease numericla integration: ``smooth_gas_sampling=True``

Furthermore we specify that we want to solve the ODE of the log concentrations (``log_scale=True``).

```python
model_args = {
    "gas_phase": "transient",
    "classical_monod": True,
    "two_step_model": True,
    "cell_decay": False,
    "growth_substrates": ["o2"],
    "growth_yield_units": "cells/mol_DOC",
    "carbon_for_growth": False,
    "doc_rate_limitation": False,
    "smooth_gas_sampling": True,
}
kwargs = {
    "log_scale": True,
}

model_builder = reaction_model.ModelBuilder(**model_args, **kwargs)
model_builder.define_model()
```

## Prepare the measurement data
We read in the experimental data from the batch experiment of Qu et al. (2015) and process it for later use in the likelihood:
We apply a Box-Cox transform to the data to improve homoscedasticity of the residuals. Transformation parameters are chosen based on the analysis in error_model.ipynb.

```python
ds = xr.open_dataset("../../experimental_data/data_Qu_2015.nc")
data = nitrogene.utils.process_data_for_likelihood(ds, transform="boxcox")
```

## Set up the statistical model in PyMC3
Defining our statistical model consists of three steps:
1. Define the prior distribution for all parameters that we want to estimate.
2. Compute the simulation result by solving the ODE.
3. Define the likelihood  based on the simulation result and our error model.

The first step also involves computing the actual model parameters from the parameters that well be estimated. We define all priors for the log parameters to account for variability/uncertianty over several orders of magnitude and to ensure positivity. For most parameters the transformation, thus, simply consists in applying an exponential function (in ``model_builder.transform_parameters``).
We also use a reparametrization for the Monod parameters of the respiration rates to avoid strong correlations in the posterior.
Instead of choosing the rate at the limit of $C_i \to \infty$, $\nu_{max}^i$, as a parameter we choose the rate $\nu_{C_{fix}}^i$ at a fixed concentration $C_{fix}^i$ as a parameter.
$\nu_{max}^i$ can then be recalculated:
$$\nu_{max}^i = \nu_{C_{fix}^i} \cdot \frac{K_s^i + C_{fix}^i}{C_{fix}^i} $$
We do not have literature values for $\nu_{C_{fix}}^i$ to choose a prior.
We therefore approximate the prior's mean and standard deviation based on samples from the original priors.

The likelihood is a Student-T distirbution that take the Box-Cox transformed simulation results as mean. For nitrite and oxygen, we add a constant background value to the concentrations before the transform. 
Variances consist of the measurement variances from triplicate measurements and an estimated constant term that accoutns for model errors.

```python
t = ds.time.values * 3600
t_dense = np.linspace(0, (ds.time.values * 3600).max(), 1000)
coords = {
    "time": t / 3600,
    "time_dense": t_dense / 3600,
    "gas_time": t[data["gas_n2"]["flat_time_indices"]],
    "nitrogen_time": t[data["nitrogen_no2"]["flat_time_indices"]],
    "bio_time": t[data["bio_alive"]["flat_time_indices"]],
}
observed_dims = {
    "bio_alive": ["bio_time"],
    "nitrogen_no2": ["nitrogen_time"],
    "gas_o2": ["gas_time"],
    "gas_n2": ["gas_time"],
}
```

```python
with pm.Model(coords=coords) as model:
    p = model_builder.default_params  # TODO rename to params
    # Define changeable variables with their prior distributions
    p["Y"]["o2"]["log"] = pm.StudentT(
        "Y_o2_log", mu=np.log(7e14), sigma=0.5, nu=10
    )
    K_i_o2_no3_log = pm.Normal("K_i_o2_no3_log", mu=-15, sigma=3)
    p["K_i_o2"]["reaction"]["no3"]["log"] = K_i_o2_no3_log
    K_i_o2_no2_log = pm.Normal("K_i_o2_no2_log", mu=-15, sigma=3)
    p["K_i_o2"]["reaction"]["no2"]["log"] = K_i_o2_no2_log

    # Reparametrization of nitrate rate parameter
    cfix = 0.6e-4
    prior_rmax_log = stats.t(loc=-44, scale=1.5, df=10).rvs(10000)
    prior_K_i_o2_no3_log = stats.norm(loc=-15, scale=3).rvs(10000)
    prior_r_no3_cfix_log = prior_rmax_log - np.log(
        (np.exp(prior_K_i_o2_no3_log) + cfix) / np.exp(prior_K_i_o2_no3_log)
    )
    r_no3_cfix_log = pm.StudentT(
        "r_cfix_no3_log",
        mu=prior_r_no3_cfix_log.mean(),
        sd=prior_r_no3_cfix_log.std(),
        nu=10,
    )
    rmax_no3_log = r_no3_cfix_log + np.log(
        (np.exp(K_i_o2_no3_log) + cfix) / np.exp(K_i_o2_no3_log)
    )
    pm.Deterministic("rmax_no3_log", rmax_no3_log)
    p["r_max"]["no3"]["log"] = rmax_no3_log

    # Reparametrization of nitrite rate parameter
    cfix2 = 2e-9
    prior_rmax_log = stats.t(loc=-44, scale=1.5, df=10).rvs(10000)
    prior_K_i_o2_no2_log = stats.norm(loc=-15, scale=3).rvs(10000)
    prior_r_no2_cfix_log = prior_rmax_log - np.log(
        (np.exp(prior_K_i_o2_no2_log) + cfix2) / np.exp(prior_K_i_o2_no2_log)
    )
    r_no2_cfix_log = pm.StudentT(
        "r_cfix_no2_log",
        mu=prior_r_no2_cfix_log.mean(),
        sd=prior_r_no2_cfix_log.std(),
        nu=10,
    )
    rmax_no2_log = r_no2_cfix_log + np.log(
        (np.exp(K_i_o2_no2_log) + cfix2) / np.exp(K_i_o2_no2_log)
    )
    pm.Deterministic("rmax_no2_log", rmax_no2_log)
    p["r_max"]["no2"]["log"] = rmax_no2_log

    # Reparametrization of oxygen rate parameters
    Ks_log = pm.StudentT("Ks_o2_log", mu=-13, sigma=2, nu=10)
    cfix = 0.6e-4
    # Approximate the prior of the reparametrized parameter based on
    # the prior of the original parameters
    prior_Ks_log = stats.t(loc=-13, scale=2, df=10).rvs(10000)
    prior_rmax_log = stats.norm(loc=-44, scale=1.5).rvs(10000)
    prior_r_cfix_log = prior_rmax_log + np.log(
        cfix / (np.exp(prior_Ks_log) + cfix)
    )
    r_cfix_log = pm.StudentT(
        "r_cfix_o2_log",
        mu=prior_r_cfix_log.mean(),
        sd=prior_r_cfix_log.std(),
        nu=10,
    )
    rmax_log = r_cfix_log + np.log((np.exp(Ks_log) + cfix) / cfix)
    pm.Deterministic("rmax_o2_log", rmax_log)
    p["K_subs"]["o2"]["log"] = Ks_log
    p["r_max"]["o2"]["log"] = rmax_log

    model_builder.transform_parameters(p, module=tt)

    y0 = model_builder.generate_initial_values()
    y0["bio"]["alive"] = (
        pm.StudentT("y0_bio_alive_log", mu=21, sigma=0.5, nu=10),
        (),
    )
    # Format parameters and initial values for use with sunode
    y0 = nesttool.apply_func_nested(y0, nitrogene.utils.as_double)
    params_with_shapes = nesttool.apply_func_nested(
        p, pymc_model.add_shapes_to_aesara_vars
    )
    params = nesttool.apply_func_nested(
        params_with_shapes, nitrogene.utils.as_double
    )

    # 2. Solve the ODE
    rhs = model_builder.generate_rhs_func()
    t0 = 0
    (
        y,
        flat_y,
        problem,
        solver,
        y0_flat,
        params_flat,
        y_dense,
        flat_y_dense,
    ) = pymc_model.solve_ode(
        t,
        t_dense,
        y0,
        params,
        rhs,
        t0,
        solver_kwargs={"checkpoint_n": 500_000},
        dense_solver_kwargs={"checkpoint_n": 10},
    )

    pm.Deterministic("y0_flat", y0_flat)
    # pm.Deterministic("params_flat", params_flat)
    # pm.Deterministic("flat_y", flat_y)
    # pm.Deterministic("flat_y_dense", flat_y_dense)

    # Unpack solution and save outpute in pymc variables
    c, c_dense, log_c = pymc_model.unpack_ode_solution(
        y, y_dense, model_builder
    )
    pymc_model.add_concentration_to_deterministics(
        c_dense, time_dim="time_dense"
    )
    pymc_model.add_concentration_to_deterministics(c, time_dim="time")

    # 3. Define the likelihood
    observed = {}
    observed_var_names = ["bio_alive", "gas_o2", "nitrogen_no2", "gas_n2"]
    minimum_measure_error = {"nitrogen_no2": 0.025, "gas_n2": 0.1}
    for data_var in observed_var_names:
        # Get the simulation results at the right time points
        simulated, sim_var_name = pymc_model.get_simulated(data_var, log_c)
        indices = data[data_var]["flat_time_indices"]

        if data_var in ["bio_alive", "gas_n2"]:
            mu = simulated[indices]
        else:
            # Simulated measurements are the ode solution plus a background value
            background = pm.Normal(f"background_{data_var}", mu=-18, sd=3)
            mu = tt.log1p(np.exp(simulated - background))[indices] + background

        # transform from log to box-cox
        lmbda = data[data_var]["lambda"]
        mu = tt.expm1(mu * lmbda) / lmbda
        pm.Deterministic(f"mu_{data_var}", mu, dims=observed_dims[data_var])

        # Estimate the data measurement error
        measure_error = pm.HalfNormal(f"measure_error_{data_var}", sd=0.1)
        if data_var in minimum_measure_error:
            measure_error += minimum_measure_error[data_var]
        data_error = data[data_var]["flat_std_vals"]
        total_error = np.sqrt(measure_error ** 2 + data_error ** 2)

        # Model measurements as the simulation results with a Student's T
        # distributed data error (of the Box-Cox transformed values)
        observed[sim_var_name] = pm.StudentT(
            "_".join([data_var, "observed"]),
            nu=7,
            mu=mu,
            #             sigma=data[data_var]["constant_std"],
            sigma=total_error,
            observed=data[data_var]["flat_measurements"],
            dims=observed_dims[data_var],
        )
```

```python
# Adjust solver tolerances and maximum step numbers
lib = sunode._cvodes.lib
lib.CVodeSStolerances(solver._ode, 1e-14, 1e-14)
lib.CVodeSStolerancesB(solver._ode, solver._odeB, 1e-12, 1e-12)
lib.CVodeQuadSStolerancesB(solver._ode, solver._odeB, 1e-12, 1e-12)
lib.CVodeSetMaxNumSteps(solver._ode, 5000)
lib.CVodeSetMaxNumStepsB(solver._ode, solver._odeB, 5000)
lib.CVodeSetMaxStep(solver._ode, 60)
lib.CVodeSetMaxStepB(solver._ode, solver._odeB, 300)
```

## Sample the prior

```python tags=[]
N_prior = 1000
with model:
    prior = pm.sample_prior_predictive(N_prior, random_seed=42)

prior_points = [
    {var.name: prior.prior[var.name].isel(draw=i, chain=0).values for var in model.free_RVs}
    for i in range(N_prior)
]
```

We create a function that computes the log probability value and its gradient with respect to parameters and apply it to all prior points.

```python
func = model.logp_dlogp_function()
func.set_extra_values({})
```

```python tags=[]
logps = []
grads = []
for point in prior_points:
    _logp, _grad = func(list(point.values()))
    logps.append(_logp)
    grads.append(_grad)
logps = np.array(logps)
```

Now we check if there are NaN values in the logp or its gradient. This can happen for extreme parameter combinations where the ODE becomes very difficult to solve numerically and the ODE solver fails.

```python
np.isfinite(logps).mean()
```

```python
sns.displot(logps[np.isfinite(logps)])
```

## Check gradients and stability numerically
Let's time the evaluation of the logp function and the gradients for 3 prior points.

```python
for point in prior_points[0:3]:
    x0 = list(point.values())
    %timeit func(x0)
```

Now we compute tthe gradient numerically and compare it to the gradient obtained from solving the adjoint sensitivity equation.

```python
eps = 5e-8
h = np.zeros_like(x0)
variable = "Y_o2_log"
idx = np.where(np.array(list(prior_points[0])) == variable)[0]
h[idx] += eps

a, grad_a = func(x0)
b, grad_b = func(x0 + h)

numerical_approximation = (b - a) / eps
gradient = grad_a[idx]
print(f"Numerical approximation of the gradient: {numerical_approximation}")
print(f"Gradient based on adjoint sensitivity equation: {gradient}")
```

```python
# Numerically approximate the second derivative to see how stable the gradient is
numercial_approx_2nd_deriv = (grad_a - grad_b) / eps
numercial_approx_2nd_deriv
```

### Check the prior predictive
We now do a check if the results of the prior predictive look reasonable. If there is too much N2 at early times or too little at late times, the reaction is too fast or too slow.

```python
too_slow = (
    prior.prior["c_nitrogen_n2"].isel(time=-1)
    < 0.2 * model_builder.default_params["c_norm"]["nitrogen"]["n2"]
)
too_fast = (
    prior.prior["c_nitrogen_n2"].isel(time=4) > 1.0 * model_builder.default_params["c_norm"]["nitrogen"]["n2"]
)
good = ~(too_slow | too_fast)
s = good.squeeze().astype(str).rename("quality")
s[good.squeeze()] = "good"
s[too_fast.squeeze()] = "too_fast"
s[too_slow.squeeze()] = "too_slow"
```

```python
print(good.sum().values, too_fast.sum().values, too_slow.sum().values)
```

```python
# Plot the parameter distributions
var_names = [var.name for var in model.free_RVs]
df = prior.prior[var_names].squeeze().to_dataframe().drop(columns="chain")
df.columns.name = "variable"
df.index.name = "draw"
df_tidy = df.unstack().rename("value").reset_index()

df_tidy = df_tidy.join(s.to_series(), on="draw")
```

```python
sns.displot(
    data=df_tidy,
    x="value",
    col="variable",
    col_wrap=4,
    hue="quality",
    kind="kde",
    facet_kws=dict(sharex=False, sharey=False),
)
```

## Sample with NUTS
The No-U-Turn sampler (NUTS) is a Hamiltonian Monte Carlo method that uses gradient information to generate new samples.
We use an adaptation of the full mass matrix to better deal with parameter correlations in the posterior.

```python
with model:
    trace_nuts = pm.sample(
        discard_tuned_samples=False,
        return_inferencedata=True,
        tune=1000,
        draws=1000,
        chains=4,
        random_seed=51,
        # idata_kwargs=dict(log_likelihood=False)
    )
```

```python
trace_nuts.extend(prior)
postprocessing.assign_units_to_inference_data(trace_nuts)
```

```python
# Save the model outputs
description = (
    "2 step classical Monod model, now with estimated initial biomass.Likelihood variance is the sum "
    "of data variance, minimum error and estimated model error (StudentT distributed likelihood)."
)
postprocessing.save_model_output(
    trace_nuts,
    path="../../traces/nuts_trace_2021_02_24_classical_monod.nc",
    description=description,
    model_builder=model_builder,
    problem=problem,
)
```

```python
trace_nuts = arviz.from_netcdf(
    "../../traces/nuts_trace_2020_08_15_classical_monod.nc"
)
```

### Convergence checks & diagnostic plots
The trace plots help us to check convergence and diagnose problems during sampling. The left column shows the posterior parameter distributions and the right column plots parameter values against sample numbers. Each chain is represented individually—if they differ considerably, convergence has not been reached.
Divergences (points in the parameter space where the sampler fails) are indicated by short black lines on the x-axis.

```python
var_names = [var.name for var in model.free_RVs if not var.name.endswith("__")]
var_names = [var.name.replace("_log__", "") for var in model.free_RVs]
```

```python
arviz.plot_trace(trace_nuts, var_names=var_names);
```

The $\hat{R}$ diagnostic test can be used to detect a lack of convergence by comparing the variance between multiple chains to the variance within each chain. Values close to 1 indicate convergence.

```python
rhat = pm.rhat(trace_nuts)[var_names]
rhat
```

```python
rhat.to_array().max()
```

We calculate an estimate of the effective sample size (ESS). It can be much smaller than the number of samples if the samples are autocorrelated.

```python
ess = pm.ess(trace_nuts)[var_names]
ess
```

### Posterior Time Series

```python
sns.set()
fig, ax = nitrogene.plotting.plot_two_step(
    trace_nuts.posterior,
    ds,
    classical_monod=True,
    n_lines=100,
)
```

### Reparametrizations
We now check if the reparametrization of the Monod rate parameters worked: $\nu_{C_{fix}}^i$ should be less correlated with $K_{O2}$ or $I_{reac}^i$ than $\nu_{max}^i$.

```python
arviz.plot_pair(
    trace_nuts, var_names=["r_cfix_o2_log", "Ks_o2_log", "rmax_o2_log"]
);
```

```python
arviz.plot_pair(
    trace_nuts, var_names=["r_cfix_no3_log", "K_i_o2_no3_log", "rmax_no3_log"]
);
```

```python
arviz.plot_pair(
    trace_nuts, var_names=["r_cfix_no2_log", "K_i_o2_no2_log", "rmax_no2_log"]
);
```
