import os
import operator
import dataclasses
import functools
import warnings
import itertools

import numpy as np
import sympy as sym
import numba
import matplotlib.pyplot as plt
from collections import namedtuple
import scipy.special
import xarray as xr
import aesara.tensor as tt
import pymc as pm

import nitrogene.utils
import nesttool as nested


try:
    import sunode

    ffi = sunode._cvodes.ffi
    lib = sunode._cvodes.lib
except ImportError:
    sunode = None


AVOGADRO_CONSTANT = 6.022e23  # Avogadro constant [molecules/mole]


def logaddexp(x1, x2):
    if any(isinstance(x, tt.TensorVariable) for x in [x1, x2]):
        return pm.math.logaddexp(x1, x2)
    else:
        return np.logaddexp(x1, x2)


def logsumexp(x, axis=None, keepdims=False):
    if isinstance(x, tt.TensorVariable):
        return pm.math.logsumexp(x, axis=axis, keepdims=keepdims)
    else:
        return scipy.special.logsumexp(x, axis=axis, keepdims=keepdims)


def expand_tuples(tup):
    upper_levels = ()
    return tuple(itertools.chain.from_iterable(_expand_tuples(upper_levels, tup)))


def _expand_tuples(upper_levels, tup):
    if isinstance(tup[-1], tuple):
        if isinstance(tup[0], str):
            assert len(tup) == 2
            return [
                _expand_tuples((*upper_levels, tup[0]), lower_level)
                for lower_level in tup[1]
            ]
        elif isinstance(tup[0], tuple):
            return [_expand_tuples(upper_levels, lower_level) for lower_level in tup]
        else:
            assert False
    else:
        return (*upper_levels, tup)

def log_transform_nested(d, keys):
    for key in keys:
        if isinstance(key, (list, tuple)):
            if isinstance(d, dict):
                nested = d.get(key[0], False)
            else:
                nested = getattr(d, key[0], False)
            if nested:
                lower_level_keys = key[1]
                log_transform_nested(nested, lower_level_keys)
            continue
        assert isinstance(key, str)
        if isinstance(d, dict):
            if key in d:
                d[key]["log"] = np.log(d[key]["val"])
        else:
            if hasattr(d, key):
                var = getattr(d, key)
                var.log = np.log(var.val)


def exp_transform_nested(d, keys, module=None):
    if module is None:
        module = sym
    for key in keys:
        if isinstance(key, (list, tuple)):
            if isinstance(d, dict):
                nested = d.get(key[0], False)
            else:
                nested = getattr(d, key[0], False)
            lower_level_keys = key[1]
            exp_transform_nested(nested, lower_level_keys, module=module)
            continue
        assert isinstance(key, str)
        if isinstance(d, dict):
            if key in d:
                d[key]["val"] = module.exp(d[key]["log"])
        else:
            if hasattr(d, key):
                var = getattr(d, key)
                var.val = module.exp(var.log)


def norm(x, mu, sigma):
    return sym.exp(-((x - mu) ** 2) / (2 * sigma ** 2)) / (sym.sqrt(2 * sym.pi) * sigma)


def mimen(c, K):
    #     return sym.Piecewise((c/(c+K),c>=0),(c/K,c<0))
    return c / (c + K)


def log_mimen(logc, logK):
    return -np.log1p(np.exp(logK - logc))


def log_inhib(logc, logK):
    return -np.log1p(np.exp(logc - logK))


def inhib(c, K):
    # return sym.Piecewise((K/(c+K),c>=0),(-c/K,c<0))
    return K / (c + K)


def hill_kinetics(x1, x2, n):
    return 1 / (1 + (x1 / x2) ** n)


def log_hill_kinetics(log_x1, log_x2, n):
    return -np.log1p(np.exp(n * (log_x1 - log_x2)))


def tox(c, K, p):
    return 1 / (1 + (c / K) ** p)


def tox_bounded(c, K, p, f_min):
    f = tox(c, K, p)
    return (1 - f_min) * f + f_min


def qss_transcription_factor_concentration_val(tf, *, params, c, nnr_activation=None):
    if tf == "nnr":
        if nnr_activation is None:
            raise ValueError("nnr_activation must be given.")
        activating_concentrations = sum(
            [c["nitrogen"][subs] for subs in nnr_activation]
        )
        return mimen(
            activating_concentrations,
            np.exp(
                params["k_dissociation"]["nnr"]["log"] - params["k_bind"]["nnr"]["log"]
            ),
        )
    elif tf == "narr":
        upper_term = (
            np.exp(params["k_bind"]["narr"]["no3"]["log"]) * c["nitrogen"]["no3"]
            + np.exp(params["k_bind"]["narr"]["no2"]["log"]) * c["nitrogen"]["no2"]
        )
        return mimen(upper_term, np.exp(params["k_dissociation"]["narr"]["log"]))
    else:
        assert False


def log_qss_transcription_factor_concentration_val(
    tf, *, params, log_c, nnr_activation=None
):
    if tf == "nnr":
        if nnr_activation is None:
            raise ValueError("nnr_activation must be given.")
        activating_concentrations = logsumexp(
            [log_c["nitrogen"][subs] for subs in nnr_activation]
        )
        return log_mimen(
            activating_concentrations,
            params["k_dissociation"]["nnr"]["log"] - params["k_bind"]["nnr"]["log"],
        )
    elif tf == "narr":
        log_upper_term = logaddexp(
            params["k_bind"]["narr"]["no3"]["log"] + log_c["nitrogen"]["no3"],
            params["k_bind"]["narr"]["no2"]["log"] + log_c["nitrogen"]["no2"],
        )
        return log_mimen(log_upper_term, params["k_dissociation"]["narr"]["log"])
    else:
        assert False


def transcriptional_activation_val(gene, c, p, log_scale=False, nir_activation=None):
    """
    Compute value of transcriptional activation factor.

    Parameters
    ----------
    gene: str
        Name of the gene, must be one of {"nar", "nir", "nor", "nos"}
    c: dict
        Concentration values in a nested dict structure
    p: dict
        Parameter values in a nested dict structure
    log_scale: bool
        if True, return the log of the expression

    """
    # IMPORTANT: If you change anything here, also change it in
    # the `transcriptional activation` method of the model builder class
    # Transcription of what gene is triggered by what substance?
    triggers = {"nar": "no3", "nir": "no2", "nor": "no", "nos": "no"}
    if gene == "nar":
        # nar transcription is induced by both nitrate and nitrite,
        # so the equation is different
        if log_scale:
            return logaddexp(
                c["nitrogen"]["no3"] - p["K_transcription"]["nar"]["no3"]["log"],
                c["nitrogen"]["no2"] - p["K_transcription"]["nar"]["no2"]["log"],
            ) - np.log1p(
                np.exp(c["nitrogen"]["no3"] - p["K_transcription"]["nar"]["no3"]["log"])
                + np.exp(
                    c["nitrogen"]["no2"] - p["K_transcription"]["nar"]["no2"]["log"]
                )
            )
        else:
            return (
                c["nitrogen"]["no3"] / np.exp(p["K_transcription"]["nar"]["no3"]["log"])
                + c["nitrogen"]["no2"]
                / np.exp(p["K_transcription"]["nar"]["no2"]["log"])
            ) / (
                1
                + c["nitrogen"]["no3"]
                / np.exp(p["K_transcription"]["nar"]["no3"]["log"])
                + c["nitrogen"]["no2"]
                / np.exp(p["K_transcription"]["nar"]["no2"]["log"])
            )
    elif gene == "nir":
        if log_scale:
            activating_concentrations = logsumexp(
                [c["nitrogen"][subs] for subs in nir_activation]
            )
            return log_mimen(
                activating_concentrations,
                p["K_transcription"]["nir"]["log"],
            )
        else:
            activating_concentrations = sum(
                [c["nitrogen"][subs] for subs in nir_activation]
            )
            return mimen(
                activating_concentrations, np.exp(p["K_transcription"]["nir"]["log"])
            )
    else:
        c_trigger = c["nitrogen"][triggers[gene]]
        if log_scale:
            return -np.log1p(np.exp(p["K_transcription"][gene]["log"] - c_trigger))
        else:
            return mimen(c_trigger, np.exp(p["K_transcription"][gene]["log"]))


def nitrite_inhibition_val(c, p, log_scale, nitrite_toxicity):
    if log_scale:
        if nitrite_toxicity == "kinetic":
            return c["tox"]
        elif nitrite_toxicity == "quasi-steady-state":
            return -np.log1p(
                np.exp(
                    p["p_tox"]
                    * (c["nitrogen"]["no2"] - p["K_i_no2_transcription"]["log"])
                )
            )
        else:
            return 0
    else:
        if nitrite_toxicity == "kinetic":
            return c["tox"]
        elif nitrite_toxicity == "quasi-steady-state":
            return tox(
                c["nitrogen"]["no2"],
                np.exp(p["K_i_no2_transcription"]["log"]),
                p["p_tox"],
            )
        else:
            return 1


def transcriptional_regulation_val(
    gene, c, p, log_scale, transcriptional_regulation_opt, nir_activation
):
    if transcriptional_regulation_opt == "substrate_regulated":
        return substrate_regulated_transcription_val(
            gene, c, p, log_scale, nir_activation
        )
    elif transcriptional_regulation_opt == "transcription_factors":
        return transcription_factor_regulation_val(gene, c, p, log_scale)
    else:
        assert False


def transcription_factor_regulation_val(gene, c, p, log_scale):
    if gene == "nar":
        if log_scale:
            log_fnrp_active = log_hill_kinetics(
                c["o2"],
                p["K_i_o2"]["transcription"]["fnrp"]["log"],
                p["n_hill"]["transcription"]["fnrp"],
            )
            log_fnrp_rel = log_fnrp_active - p["K_dna"]["fnrp"]["log"]
            log_narr_rel = c["tf"]["narr"] - p["K_dna"]["narr"]["log"]
            return (
                log_fnrp_rel
                + log_narr_rel
                - np.log1p(
                    np.exp(log_fnrp_rel)
                    + np.exp(log_narr_rel)
                    + np.exp(log_fnrp_rel + log_narr_rel)
                )
            )
        else:
            fnrp_active = hill_kinetics(
                c["o2"],
                np.exp(p["K_i_o2"]["transcription"]["fnrp"]["log"]),
                p["n_hill"]["transcription"]["fnrp"],
            )
            fnrp_rel = fnrp_active / np.exp(p["K_dna"]["fnrp"]["log"])
            narr_rel = c["tf"]["narr"] / np.exp(p["K_dna"]["narr"]["log"])
            return fnrp_rel * narr_rel / (1 + fnrp_rel + narr_rel + fnrp_rel * narr_rel)
    elif gene == "nir":
        if log_scale:
            log_active_nnr = (
                log_hill_kinetics(
                    c["o2"],
                    p["K_i_o2"]["transcription"]["nnr"]["log"],
                    p["n_hill"]["transcription"]["nnr"],
                )
                + c["tf"]["nnr"]
            )
            return log_mimen(log_active_nnr, p["K_dna"]["nnr"]["log"])
        else:
            active_nnr = (
                hill_kinetics(
                    c["o2"],
                    np.exp(p["K_i_o2"]["transcription"]["nnr"]["log"]),
                    p["n_hill"]["transcription"]["nnr"],
                )
                * c["tf"]["nnr"]
            )
            return mimen(active_nnr, np.exp(p["K_dna"]["nnr"]["log"]))
    else:
        assert False


def substrate_regulated_transcription_val(gene, c, p, log_scale, nir_activation):
    if gene == "nir" and nir_activation is None:
        raise ValueError("'nir_activation' must be specified when gene is 'nir'.")

    if log_scale:
        return transcriptional_activation_val(
            gene, c, p, log_scale, nir_activation
        ) + log_hill_kinetics(
            c["o2"],
            p["K_i_o2"]["transcription"][gene]["log"],
            p["n_hill"]["transcription"][gene],
        )
    else:
        return transcriptional_activation_val(
            gene, c, p, log_scale, nir_activation
        ) * hill_kinetics(
            c["o2"],
            np.exp(p["K_i_o2"]["transcription"][gene]["log"]),
            p["n_hill"]["transcription"][gene],
        )


def qss_mrna_concentration_val(
    gene,
    *,
    p,
    c=None,
    log_c=None,
    log_scale=False,
    enzyme_inhibition=False,
    nitrite_toxicity=False,
    transcriptional_regulation_opt="substrate_regulated",
    nir_activation=None,
):
    if log_c is None and c is None:
        raise ValueError("Either c or log_c must be given.")
    if enzyme_inhibition:
        raise ValueError(
            "Quasi steady state with enzyme inhibition currently not implemented."
        )
    if log_scale:
        if log_c is None:
            conc = nested.apply_func_nested(c, np.log)
        else:
            conc = log_c
    else:
        if c is None:
            conc = nested.apply_func_nested(log_c, np.exp)
        else:
            conc = c
    # Compute terms for transcriptional regulation and toxicity
    transcriptional_regulation = transcriptional_regulation_val(
        gene,
        conc,
        p,
        log_scale=log_scale,
        transcriptional_regulation_opt=transcriptional_regulation_opt,
        nir_activation=nir_activation,
    )
    tox_term = nitrite_inhibition_val(
        conc, p, log_scale=log_scale, nitrite_toxicity=nitrite_toxicity
    )
    # Combine the different terms
    if log_scale:
        return (
            p["r_mrna_log"][gene]
            + transcriptional_regulation
            + tox_term
            + conc["bio"]["alive"]
        )
    else:
        return (
            np.exp(p["r_mrna_log"][gene])
            * transcriptional_regulation
            * tox_term
            * conc["bio"]["alive"]
        )


def qss_enzyme_concentration_val(gene, params, conc):
    return (
        np.exp(conc["mrna"][gene] + params["r_enz_log"] - params["r_mrna_log"][gene])
        / AVOGADRO_CONSTANT
    )


class ModelBuilder:
    """
    Creates model equations based on model assumptions.

    Parameters
    ----------
    gas_phase : str
        One of {"transient", "qss", "none"} to either simulate fully transient
        gas exchange, gas phase concentrations at quasi-steady state or
        no gas phase concentrations at all, respectivley. Defaults to "transient".
    mrna_at_quasi_steady_state : bool
    classical_monod : bool
        When True, don't simulate transcript and enzyme concentrations
        and use a classsical Monod type rate law formulation.
    two_step_model : bool
        When True, simulate denitrification as a two-step reaction
        (nitrate -> nitrite -> N2) instead of four steps.
        When False, the full reaction (nitrate -> nitrite -> NO -> N2O -> N2)
        will be simulated.
    cell_decay : bool
        When True, cells undergo exponential decay. Otherwise,
        cells only grow but do not die.
    growths_substrates : list of str
        Indicates what substrates the bacteria grow on.
        Must be a subset of {"o2", "no3", "no2", "no", "n2o"}
        (but cannot contain "no" and "n2o" when `two_step_model`
        is True).
    carbon_for_growth : bool
        When True, carbon substrate is consumed for the incorporation of
        carbon into biomass. Note that this requires to make an assumption
        about the average stoichiometric composition of the biomass and an
        additional parameter relating the biomass weight to cell numbers.
        It also limits the growth yield to a maximum value.
    growth_yield_units : {"cells/mol_DOC"}
        Must be "cells/mol_doc", all unit conversions must be done separately.
    growth_model: {"exponential", "logistic"}
        Determines if microbial growth is described with an exponential or a
        logistic model.
    doc_rate_limitation : bool
        When True, a Monod term for a rate limitation by the carbon substrate is
        inserted in the electron acceptor consumption rate. It requires an
        additional parameter K_doc.
    transcription_self_inhibition : bool
        When True, enzymes inhibit the transcription of their corresponding gene
        It is set to True by default, except if classical_monod is True.
    enzymes_to_simulate : list of str
        Items must be out of {"nar", "nir", "nor", "nos"}.
    log_scale : bool
        When True, ode is defined on a log scale (i.e. the ode for log(c)
        will be solved).
    nitrite_toxicity: bool
        If True, transcription will be inhibited by nitrite (including
        a nitrite inhibition term in the transcription rate).
    transcriptional_regulation_opts: dict
        Specify for each gene how transcriptional regulation should be described.
        Options are "substrate_regulated" and "transcription_factors". For
        example: {"nar": "substrate_regulated", "nir": "transcription_factor"}
        By default, it is set to "substrate_regulated" for all genes.
    nnr_activation: list of str
        substrates that activate nnr and therefore nir transcription. Should be
        out of {"no3", "no2", "no"}. Defaults to ["no2"].
    doc_release: str
        Specify model for DOC release from the matrix. Must be one of
        {"constant", "depth_dependent"}

    Attributes
    ----------
    default_params : dict
    """

    def __init__(
        self,
        gas_phase=None,
        mrna_at_quasi_steady_state=False,
        classical_monod=False,
        two_step_model=False,
        cell_decay=True,
        growth_substrates=None,
        carbon_for_growth=True,
        growth_yield_units=None,
        growth_model="exponential",
        doc_rate_limitation=True,
        smooth_gas_sampling=False,
        enzymes_to_simulate=None,
        transcription_self_inhibition=None,
        log_scale=False,
        nitrite_toxicity=False,
        transcriptional_regulation_opts=None,
        nnr_activation=None,
        doc_release="constant",
    ):
        self.mrna_at_quasi_steady_state = mrna_at_quasi_steady_state
        self.classical_monod = classical_monod
        self.two_step_model = two_step_model
        self.cell_decay = cell_decay
        self.carbon_for_growth = carbon_for_growth
        self.doc_rate_limitation = doc_rate_limitation
        self.smooth_gas_sampling = smooth_gas_sampling
        self.doc_release = doc_release
        self.log_scale = log_scale

        self.logtrafo_names_level1 = ["cell_weight", "K_inhib_enz", "B_max"]
        self.logtrafo_names_level2 = [
            ("k_dec", ["bio"]),
            ("K_transcription", ["nir", "nor", "nos"]),
            ("K_dna", ["narr", "fnrp", "nnr"]),
            ("k_bind", ["nnr"]),
            ("k_dissociation", ["narr", "nnr"]),
            ("k_max", ["no3", "no2", "no", "n2o"]),
            ("r_max", ["no3", "no2", "no", "n2o", "o2"]),
            ("K_subs", ["no3", "no2", "no", "n2o", "o2"]),
            ("Y", ["no3", "no2", "no", "n2o", "o2"]),
        ]
        self.logtrafo_names_level3 = [
            (
                "K_i_o2",
                [
                    ("transcription", ["nar", "nir", "nor", "nos", "fnrp", "nnr"]),
                    ("reaction", ["no3", "no2", "no", "n2o"]),
                ],
            ),
            ("k_bind", [("narr", ["no3", "no2"])]),
            ("K_transcription", [("nar", ["no3", "no2"])]),
        ]

        if gas_phase is None:
            self.gas_phase = "transient"
        else:
            valid_args = {"transient", "qss", "none"}
            if gas_phase in valid_args:
                self.gas_phase = gas_phase
            else:
                raise ValueError(
                    f"Invalid keyword argument for `gas_phase`: {gas_phase}. "
                    f"It should be one of {valid_args}."
                )

        if growth_yield_units is None:
            self.growth_yield_units = "cells/mol_DOC"
            warnings.warn(
                "No units for the growth yield were specified. "
                "Assuming it is given in {}".format(self.growth_yield_units)
            )
        else:
            valid_units = ["cells/mol_DOC"]
            if growth_yield_units not in valid_units:
                raise ValueError(
                    "Invalid units for the growth yield specified: {}. "
                    "Should be one of {}".format(growth_yield_units, valid_units)
                )
            self.growth_yield_units = growth_yield_units

        if growth_substrates is None:
            self.growth_substrates = self.electron_acceptors
        else:
            self.growth_substrates = growth_substrates

        # Argument sanity checks
        if self.carbon_for_growth:
            warnings.warn(
                "Accounting for the incorporation of carbon into biomass implies "
                "an upper limit of the growth yield. The user should make sure "
                "that this limit is not exceeded. Otherwise, results will be "
                "non-sense (negative concentrations and the like)."
            )

        if mrna_at_quasi_steady_state and classical_monod:
            raise ValueError(
                "Invalid model assumption combination:"
                " mRNA is not simulated when `classical_monod` is set to True,"
                " so `mrna_at_quasi_steady_state` cannot be True."
            )
        invalid_growth_substrates = set(self.growth_substrates) - set(
            self.electron_acceptors
        )
        if invalid_growth_substrates:
            raise ValueError(
                "{} cannot be growth substrates because they are not electron acceptors. "
                "Consider removing them from the list of growth substrates or setting "
                "two_step_model to False.".format(invalid_growth_substrates)
            )

        valid_growth_models = {"exponential", "logistic"}
        if growth_model in valid_growth_models:
            self.growth_model = growth_model
        else:
            raise ValueError(
                f"Invalid growth model: {growth_model}. Must be one of {valid_growth_models}"
            )

        if transcription_self_inhibition is None:
            if classical_monod:
                self.transcription_self_inhibition = False
            else:
                self.transcription_self_inhibition = True
        else:
            if classical_monod and transcription_self_inhibition:
                raise ValueError(
                    "transcription_self_inhibition cannot be True when"
                    "classical_monod is True."
                )
            self.transcription_self_inhibition = transcription_self_inhibition

        if self.classical_monod:
            if enzymes_to_simulate is None:
                self.genes = []
            else:
                raise ValueError(
                    "Enzymes cannot be simulated when classical_monod is True."
                )

            if nitrite_toxicity:
                raise ValueError(
                    "Nitrite toxicity only applies to transcription and will not have "
                    "any effect with classical_monod set to True."
                )
            else:
                self.nitrite_toxicity = nitrite_toxicity
            if transcriptional_regulation_opts is None:
                self.transcriptional_regulation_opts = transcriptional_regulation_opts
            else:
                assert False
            if nnr_activation is None:
                self.nnr_activation = nnr_activation
            else:
                assert False

        else:
            valid_genes = [
                self.gene_to_substrate_mapping[substrate]
                for substrate in self.nitrogen_educts
            ]
            if enzymes_to_simulate is None:
                self.genes = valid_genes
            else:
                if not set(enzymes_to_simulate).issubset(set(valid_genes)):
                    invalid_genes = set(enzymes_to_simulate).difference(
                        set(valid_genes)
                    )
                    raise ValueError(
                        "Some of the specified enzymes cannot be"
                        "simulated because the corresponding substrate"
                        "is not simulated: {}".format(invalid_genes)
                    )
                else:
                    self.genes = enzymes_to_simulate
            if transcriptional_regulation_opts is None:
                self.transcriptional_regulation_opts = {
                    gene: "substrate_regulated" for gene in self.genes
                }
            else:
                if set(transcriptional_regulation_opts) != set(self.genes):
                    raise ValueError(
                        "You should specify one option for each of these genes:"
                        f"{self.genes}, but specified options for "
                        f"{transcriptional_regulation_opts}."
                    )
                else:
                    pass
                valid_options = {"substrate_regulated", "transcription_factors"}
                invalid_option = (
                    set(transcriptional_regulation_opts.values()) - valid_options
                )
                if invalid_option:
                    raise ValueError(
                        "Valid options for transcriptional regulation are"
                        f"{valid_options}, but you specified {invalid_option}."
                    )
                else:
                    self.transcriptional_regulation_opts = (
                        transcriptional_regulation_opts
                    )
            # Check/set nnr_activation option
            valid_activators = {"no3", "no2", "no"}
            if nnr_activation is None:
                self.nnr_activation = ["no2"]
            elif not set(nnr_activation) <= valid_activators:
                raise ValueError(
                    f"Invalid substrate for nnr activation: {set(nnr_activation) - valid_activators}"
                )
            if nitrite_toxicity:
                self.toxicity_model = ToxicityModel(**nitrite_toxicity)
                self.nitrite_toxicity = True
                if self.toxicity_model.inner_func_type in [
                    "toxicity",
                    "bounded_toxicity",
                ]:
                    self.logtrafo_names_level1.append("K_tox")
                if self.toxicity_model.outer_func_type in [
                    "toxicity",
                    "bounded_toxicity",
                ]:
                    self.logtrafo_names_level1.append("K_tox_2")
            else:
                self.nitrite_toxicity = False

        self.model_args = {
            "gas_phase": self.gas_phase,
            "mrna_at_quasi_steady_state": self.mrna_at_quasi_steady_state,
            "classical_monod": self.classical_monod,
            "two_step_model": self.two_step_model,
            "cell_decay": self.cell_decay,
            "growth_substrates": self.growth_substrates,
            "carbon_for_growth": self.carbon_for_growth,
            "growth_yield_units": self.growth_yield_units,
            "doc_rate_limitation": self.doc_rate_limitation,
            "smooth_gas_sampling": self.smooth_gas_sampling,
            "enzymes_to_simulate": enzymes_to_simulate,
            "transcription_self_inhibition": transcription_self_inhibition,
            "nitrite_toxicity": nitrite_toxicity,
            "transcriptional_regulation_opts": self.transcriptional_regulation_opts,
            "nnr_activation": self.nnr_activation,
        }

    def define_model(self):
        self.default_params = self.define_params()

    def define_params(self):
        """
        Define default parameters for model_qu.

        Parameters that will not be calibrated are defined on literature values.
        Parameters that will be calibrated are chosen based on a rough manual calibration.

        Parameters:
        -----------
        """

        # Read in experimental data
        dir = os.path.dirname(os.path.dirname(__file__))
        path = os.path.join(dir, "experimental_data", "data_Qu_2015.nc")
        ds = xr.open_dataset(path)
        t_samples = (
            ds.time.where(ds.isel(replicate=0)["gas sample"], drop=True)
        ).values * 3600

        # carbon source is succinic acid C4H6O4
        M_doc = 118.088  # [g/mol] molar mass of DOC
        M_bio = 246.263  # [g_bio/mol_bio] molar mass of biomass

        # transcription and translation related parameters
        t_mrna = 1.15  # mRNA half life in min
        k_dec_mrna = np.log(2) / (t_mrna * 60)  # [1/s] decay coefficient of mRNA
        t_enz = 24  # enzyme half life in h
        k_dec_enz = np.log(2) / (t_enz * 3600)  # [1/s] decay coefficient of proteins
        r_mrna = {
            gene: 5e-4 for gene in ["nar", "nir", "nor", "nos"]
        }  # [transcripts/gene] relative abundance of mRNA (at steady state)
        k_transcription_max = {
            gene: r_mrna_i * k_dec_mrna for gene, r_mrna_i in r_mrna.items()
        }  # [transcripts/gene/s] maximum transcription rate
        r_enz = 1125  # enzymes/gene
        k_translation = {
            gene: r_enz / r_mrna_i * k_dec_enz / AVOGADRO_CONSTANT
            for gene, r_mrna_i in r_mrna.items()
        }
        cell_weight = 1e-13  # g/cell
        Y_max = 1 / 3 * M_bio / cell_weight  # cells/mol
        Y_rel = {"no3": 0.6, "no2": 0.3, "no": 0.18, "n2o": 0.04, "o2": 0.27}
        for key in Y_rel:
            if key not in self.growth_substrates:
                Y_rel[key] = 0.0
        Y = {
            key: {"val": Y_rel_i * Y_max, "log": np.inf}
            for key, Y_rel_i in Y_rel.items()
        }
        # Gas exchange parameters
        T_ref = 298.15
        T = 293.15
        H_mol_m3_Pa = {
            "no": 1.9e-5,
            "n2o": 2.4e-4,
            "n2": 6.4e-6,
            "o2": 1.2e-5,
            "co2": 3.3e-4,
        }
        dH = {
            "no": 1600,
            "n2o": 2600,
            "n2": 1300,
            "o2": 1600,
            "co2": 2400,
        }
        H = {  # (mol/L) / (mol/L)
            gas: nitrogene.utils.convert_henrys_H(H, T_ref, T, dH[gas])
            for gas, H in H_mol_m3_Pa.items()
        }
        # account for other species of carbonic acid in Henry coefficient of CO2
        pK1 = 6.38
        pK2 = 10.38
        pH = 7
        H["co2"] /= 1 + 10 ** (pH - pK1) + 10 ** (2 * pH - pK1 - pK2)

        dt_sample = 1200  # s
        f = 0.013  # dimensionless
        f_dil = -np.log(1 - f)
        R = 0.082057338  # (L atm)/(K mol)
        p = 1  # atm
        conv = 1e-6 * p / (T * R)  # conversion factor from ppmv to mol/L
        r_diff_o2_ppm_h = 0.23
        r_diff_n2_ppm_h = 0.3
        r_diff_o2 = r_diff_o2_ppm_h * conv / 3600
        r_diff_n2 = r_diff_n2_ppm_h * conv / 3600
        c_leak_o2_ppm = 1.0
        c_leak_n2_ppm = 6.0
        c_leak_o2 = c_leak_o2_ppm * conv
        c_leak_n2 = c_leak_n2_ppm * conv

        # compute initial O2 concentration
        # the liquid concentration is assumed to be in equilibrium with the gas phase
        f_o2 = 0.07  # dimensionless
        p_Pa = p * 101325.0  # pressure in Pa
        R = 8.3144598  # ideal gas constant [Pa m³/(mol K)]

        # Initial mRNA concentration
        # Take the concentrations measured at time point 0
        # a_mrna       = 0.6e-15                                 # average RNA concentration per cell [g RNA/cell]
        # c_mrna_0_raw = np.array([874.+157, 36., 785., 273.])*1e9   # transcripts/g RNA

        # Default normalization constant to scale all states to the same magnitude
        default_c_norm = {
            "tf":{
                "narr": 0.1,
                "nnr": 0.1,
                "fnrp": 0.1,
            },
            "mrna": {
                "nar": 1e6,
                "nir": 1e6,
                "nor": 1e6,
                "nos": 1e6,
            },
            "enz": {
                "nar": 1e-12,
                "nir": 1e-12,
                "nor": 1e-12,
                "nos": 1e-12,
            },
            "nitrogen": {
                "no3": 1e-3,
                "no2": 1e-3,
                "no": 1e-9,
                "n2o": 1e-7,
                "n2": 1e-5,
            },
            "gas": {"no": 1e-7, "n2o": 1e-8, "n2": 1e-4, "o2": 1e-3, "co2": 1e-4},
            "bio": {"alive": 1e10, "dead": 1e9},
            "doc": 1e-3,
            "o2": 1e-4,
            "co2": 2e-3,
            "tox": 1,
        }

        params = {
            "temperature": T,  # K
            "pressure": p_Pa,  # Pa
            "ideal_gas_constant": R,  # [Pa m³/(mol K)]
            "f_o2": f_o2,  # dimensionless
            "t_1_2": {
                "mrna": {"log": np.log(t_mrna)},
                "enz": {"log": np.log(t_enz)},
            },
            "k_dec": {
                "mrna": {"val": k_dec_mrna},
                "enz": {"val": k_dec_enz},
                "bio": {  # 1/s
                    "val": 0.02 / 86400 if self.cell_decay else 0.0,
                    "log": np.inf,
                },
            },
            "k_transcription_max": k_transcription_max,
            "r_mrna_log": {gene: np.log(r_mrna_i) for gene, r_mrna_i in r_mrna.items()},
            "k_translation": k_translation,
            "r_enz_log": np.log(r_enz),
            "cell_weight": {"val": cell_weight, "log": np.inf},  # g/cell
            "Y": Y,
            "B_max": {"val": 1e12, "log": np.inf},
            "k_max": {  # mol/s/mol_enz
                "no3": {"val": 8e5, "log": np.inf},
                "no2": {"val": 8e3, "log": np.inf},
                "no": {"val": 4e4, "log": np.inf},
                "n2o": {"val": 1e5, "log": np.inf},
            },
            "r_max": {  # mol/s/cell
                "no3": {"val": 1e-18, "log": np.inf},
                "no2": {"val": 1e-18, "log": np.inf},
                "no": {"val": 1e-18, "log": np.inf},
                "n2o": {"val": 1e-18, "log": np.inf},
                "o2": {"val": 1.5e-18, "log": np.inf},
            },
            "K_subs": {  # mol/L
                "no3": {"val": 5e-6, "log": np.inf},
                "no2": {"val": 5e-6, "log": np.inf},
                "no": {"val": 1e-6, "log": np.inf},
                "n2o": {"val": 2e-9, "log": np.inf},
                "o2": {"val": 1e-4, "log": np.inf},
            },
            "K_doc": 6e-3 / M_doc if self.doc_rate_limitation else 0.0,  # mol/L
            "M_doc": M_doc,
            "M_bio": M_bio,
            "K_inhib_enz": {"val": 300.0, "log": np.inf},  # enzymes/cell
            "K_i_o2": {  # mol/L
                "reaction": {
                    "no3": {"val": 1e-6, "log": np.inf},
                    "no2": {"val": 1e-6, "log": np.inf},
                    "no": {"val": 1e-6, "log": np.inf},
                    "n2o": {"val": 1e-6, "log": np.inf},
                },
            },
            "n_energy_in": {
                "no3": 7.0,
                "no2": 14.0,
                "no": 14.0,
                "n2o": 7.0,
                "o2": 7.0,
            },
            "n_energy_out": {
                "no2": 7.0,
                "no": 14.0,
                "n2o": 7.0,
                "n2": 7.0,
            },
            "n_doc_energy": {
                "no3": 1.0,
                "no2": 3.0 if self.two_step_model else 1.0,
                "no": 1.0,
                "n2o": 1.0,
                "o2": 2.0,
            },
            "n_co2_energy": {
                "no3": 4.0,
                "no2": 12.0 if self.two_step_model else 4.0,
                "no": 4.0,
                "n2o": 4.0,
                "o2": 8.0,
            },
            "n_doc_growth": 3.0,
            "n_bio_growth": 1.0,
            "n_co2_growth": 2.0,
            "k_decomp": 0.0,
            "k_tr": {
                "no": 8e-2,  # 1/s
                "n2o": 8e-2,  # 1/s
                "n2": 8e-2,  # 1/s
                "o2": 8e-2,  # 1/s
                "co2": 8e-2,
            },
            "H": H,
            "V": {"liq": 0.05, "gas": 0.07},  # L
            "dt_sample": 1200.0,  # s
            "f_dil": f_dil,
            "r_diff": {"o2": r_diff_o2, "n2": r_diff_n2},
            "c_leak": {"o2": c_leak_o2, "n2": c_leak_n2},
            "t_sample": t_samples,
            "c_norm": nested.extract_subdict(default_c_norm, self.state_levels),
            "k_release": {"doc": 0},
            "spatial_coordinate": 0.0,
        }

        if self.doc_release == "constant":
            params["c_sat"] = {"doc": 10 / 48 * 1e-3}
        elif self.doc_release == "depth_dependent":
            params["c_sat_0"] = {"doc": 4000 / 48 * 1e-3}  # mol/L of succinate
            params["lambda_poc"] = 9  # 1/m
        else:
            assert False

        # Choose transcriptional regulation params
        transcription_params = {
            "K_transcription": {
                "nar": {
                    "no3": {"val": 1e-2, "log": np.log(1e-2)},  # mol/L
                    "no2": {"val": 1e-2, "log": np.log(1e-2)},  # mol/L
                },
                "nir": {"val": 2e-2, "log": np.log(2e-2)},  # mol/L
                "nor": {"val": 1e-7, "log": np.log(1e-7)},  # mol/L
                "nos": {"val": 5e-7, "log": np.log(5e-7)},  # mol/L
            },
            "K_i_o2": {
                "transcription": {
                    "nar": {"val": 2e-5, "log": np.inf},
                    "nir": {"val": 2e-5, "log": np.inf},
                    "nor": {"val": 2e-5, "log": np.inf},
                    "nos": {"val": 2e-5, "log": np.inf},
                    "nnr": {"val": 2e-5, "log": np.inf},
                    "fnrp": {"val": 2e-5, "log": np.inf},
                },
            },
            "n_hill": {
                "transcription": {
                    "nar": 1.0,
                    "nir": 1.0,
                    "nor": 1.0,
                    "nos": 1.0,
                    "nnr": 1.0,
                    "fnrp": 1.0,
                },
            },
            "K_dna": {
                "fnrp": {"val": 1, "log": np.log(1)},
                "narr": {"val": 1, "log": np.log(1)},
                "nnr": {"val": 1, "log": np.log(1)},
            },
            "k_bind": {
                "narr": {
                    "no3": {"val": 30, "log": np.log(30)},
                    "no2": {"val": 30, "log": np.log(30)},
                },
                "nnr": {"val": 30, "log": np.log(30)},
            },
            "k_dissociation": {
                "narr": {"val": 1e-3, "log": np.log(1e-3)},
                "nnr": {"val": 1e-3, "log": np.log(1e-3)},
            },
        }
        param_selection = {
            "substrate_regulated": {
                "nar": [
                    ("K_transcription", "nar"),
                    ("K_i_o2", "transcription", "nar"),
                    ("n_hill", "transcription", "nar"),
                ],
                "nir": [
                    ("K_transcription", "nir"),
                    ("K_i_o2", "transcription", "nir"),
                    ("n_hill", "transcription", "nir"),
                ],
                "nor": [
                    ("K_transcription", "nor"),
                    ("K_i_o2", "transcription", "nor"),
                    ("n_hill", "transcription", "nor"),
                ],
                "nos": [
                    ("K_transcription", "nos"),
                    ("K_i_o2", "transcription", "nos"),
                    ("n_hill", "transcription", "nos"),
                ],
            },
            "transcription_factors": {
                "nar": [
                    ("K_dna", "fnrp"),
                    ("K_dna", "narr"),
                    ("k_bind", "narr", "no3"),
                    ("k_bind", "narr", "no2"),
                    ("k_dissociation", "narr"),
                    ("K_i_o2", "transcription", "fnrp"),
                    ("n_hill", "transcription", "fnrp"),
                ],
                "nir": [
                    ("K_dna", "nnr"),
                    ("k_bind", "nnr"),
                    ("k_dissociation", "nnr"),
                    ("K_i_o2", "transcription", "nnr"),
                    ("n_hill", "transcription", "nnr"),
                ],
            },
        }
        if self.transcriptional_regulation_opts:
            params_flat = sunode.dtypesubset.as_flattened(params)
            for gene, regulation in self.transcriptional_regulation_opts.items():
                if gene in param_selection[regulation]:
                    selected_transcription_params = {}
                    for keys in param_selection[regulation][gene]:
                        subparams = nested.getitem_nested(transcription_params, keys)
                        if isinstance(subparams, dict):
                            flat_subparams = sunode.dtypesubset.as_flattened(subparams)
                            for subkeys, val in flat_subparams.items():
                                selected_transcription_params[keys + subkeys] = val
                        else:
                            selected_transcription_params[keys] = subparams
                    params_flat = {**params_flat, **selected_transcription_params}
                else:
                    raise ValueError(
                        f"Regulation of {gene} with option {regulation} currently not implemented."
                    )
            params = sunode.dtypesubset.as_nested(params_flat)

        # Add parameters for toxicity
        if self.nitrite_toxicity:
            params = {**params, **self.toxicity_model.generate_parameters()}
        log_transform_nested(params, self.logtrafo)
        return params

    def generate_initial_values(self):
        p = self.default_params
        c_o2_gas_0 = (
            p["pressure"]
            / (p["ideal_gas_constant"] * p["temperature"])
            * p["f_o2"]
            / 1e3
        )  # mol/L
        c_o2_0 = c_o2_gas_0 / p["H"]["o2"]  # mol/L

        c_n2_gas_0 = 5.2e-5
        c_n2_0 = c_n2_gas_0 / p["H"]["n2"]  # mol/L
        # c0_o2 = c_o2_0_liq_corrected if self.gas_phase == "qss # TODO: implement the corrected co2 concentration
        c_init = {
            "tf": {
                "narr": 0.5,
                "nnr": 0.5,
            },
            "mrna": {
                "nar": 0.0,  # transcripts/L
                "nir": 0.0,  # transcripts/L
                "nor": 0.0,  # transcripts/L
                "nos": 0.0,  # transcripts/L
            },
            "enz": {
                "nar": 0.0,  # mol/L
                "nir": 0.0,  # mol/L
                "nor": 0.0,  # mol/L
                "nos": 0.0,  # mol/L
            },
            "nitrogen": {
                "no3": 2e-3,  # mol/L
                "no2": 1e-11,  # mol/L
                # "no":  0., # mol/L
                # "n2o":  0., # mol/L
                # "n2": c_n2_0, # mol/L
            },
            "gas": {
                "no": 0.0,  # mol/L
                "n2o": 0.0,  # mol/L
                "n2": c_n2_gas_0,  # mol/L
                "o2": c_o2_gas_0,  # mol/L
                "co2": 1e-9,  # mol/L
            },
            # "o2": c_o2_0, # mol/L
            # "co2": 0., # mol/L
            "doc": 5e-3,  # mol/L
            "bio": {"alive": 3e9, "dead": 0.0},  # cells/L
        }
        if self.nitrite_toxicity:
            c_init_tox = self.toxicity_model.generate_initial_values(c_init, p)
            c_init = {**c_init, **c_init_tox}

        if self.log_scale:

            def wrapper(func):
                def inner(val):
                    if isinstance(val, tuple):
                        val, shape = val
                        return (func(val), shape)
                    return func(val)

                return inner

            y0 = nested.apply_func_nested(c_init, wrapper(np.log))

            def replace_neg_infs(x):
                if isinstance(x, tt.TensorVariable):
                    return x
                return np.where(np.isneginf(x), -35, x)

            y0 = nested.apply_func_nested(y0, wrapper(replace_neg_infs))
            # Initiate solute gases at steady state with the atmosphere
            y0["o2"] = np.log(np.exp(y0["gas"]["o2"]) / p["H"]["o2"])
            y0["co2"] = np.log(np.exp(y0["gas"]["co2"]) / p["H"]["co2"])
            y0["nitrogen"]["no"] = np.log(np.exp(y0["gas"]["no"]) / p["H"]["no"])
            y0["nitrogen"]["n2o"] = np.log(np.exp(y0["gas"]["n2o"]) / p["H"]["n2o"])
            y0["nitrogen"]["n2"] = np.log(np.exp(y0["gas"]["n2"]) / p["H"]["n2"])

        if not self.log_scale:
            # Initiate solute gases at steady state with the atmosphere
            c_init["o2"] = c_init["gas"]["o2"] / p["H"]["o2"]
            c_init["co2"] = c_init["gas"]["co2"] / p["H"]["co2"]
            c_init["nitrogen"]["no"] = c_init["gas"]["no"] / p["H"]["no"]
            c_init["nitrogen"]["n2o"] = c_init["gas"]["n2o"] / p["H"]["n2o"]
            c_init["nitrogen"]["n2"] = c_init["gas"]["n2"] / p["H"]["n2"]
            y0 = self.normalize_state(c_init)

        if self.two_step_model:
            del y0["nitrogen"]["no"]
            del y0["nitrogen"]["n2o"]
            del y0["gas"]["no"]
            del y0["gas"]["n2o"]

        for gene in ["nar", "nir", "nor", "nos"]:
            if gene not in self.genes:
                del y0["mrna"][gene]
                del y0["enz"][gene]
        if len(y0["mrna"]) == 0:
            del y0["mrna"]
        if len(y0["enz"]) == 0:
            del y0["enz"]

        tfs = list(y0["tf"].keys())
        for tf in tfs:
            if tf not in self.transcription_factors:
                del y0["tf"][tf]
        if len(y0["tf"]) == 0:
            del y0["tf"]

        if self.mrna_at_quasi_steady_state:
            del y0["mrna"]
        if self.gas_phase == "none":
            del y0["gas"]
        return y0

    def get_n_vars(self):
        return len(self.index_map)

    def transform_parameters(self, params, module=None):
        """
        Transform (where necessary) parameters from log space to normal space and do other conversions.

        This overwrites entries of dependent parameters in the params structure.

        """
        if module is None:
            module = sym
        exp_transform_nested(params, self.logtrafo, module=module)

        if isinstance(params, dict):
            params["k_dec"]["mrna"]["val"] = (
                module.log(2) * module.exp(-params["t_1_2"]["mrna"]["log"]) / 60
            )
            params["k_dec"]["enz"]["val"] = (
                module.log(2) * module.exp(-params["t_1_2"]["enz"]["log"]) / 3600
            )
            for gene in ["nar", "nir", "nor", "nos"]:
                params["k_transcription_max"][gene] = params["k_dec"]["mrna"][
                    "val"
                ] * module.exp(params["r_mrna_log"][gene])
                params["k_translation"][gene] = (
                    params["k_dec"]["enz"]["val"]
                    * module.exp(params["r_enz_log"] - params["r_mrna_log"][gene])
                    / AVOGADRO_CONSTANT
                )
            if self.nitrite_toxicity and self.toxicity_model.distribution in [
                "exponential",
                "gamma",
                "sum_of_gammas",
            ]:
                params["lambda_tox"] = (
                    module.log(2) * module.exp(-params["t_1_2_tox"]["log"]) / 3600
                )
        else:
            params.k_dec.mrna.val = (
                module.log(2) * module.exp(-params.t_1_2.mrna.log) / 60
            )
            params.k_dec.enz.val = (
                module.log(2) * module.exp(-params.t_1_2.enz.log) / 3600
            )
            for gene in ["nar", "nir", "nor", "nos"]:
                setattr(
                    params.k_transcription_max,
                    gene,
                    params.k_dec.mrna.val
                    * module.exp(getattr(params.r_mrna_log, gene)),
                )
                setattr(
                    params.k_translation,
                    gene,
                    (
                        params.k_dec.enz.val
                        * module.exp(
                            params.r_enz_log - getattr(params.r_mrna_log, gene)
                        )
                        / AVOGADRO_CONSTANT
                    ),
                )
            if self.nitrite_toxicity and self.toxicity_model.distribution in [
                "exponential",
                "gamma",
                "sum_of_gammas",
            ]:
                params.lambda_tox = (
                    module.log(2) * module.exp(-params.t_1_2_tox.log) / 3600
                )

    def generate_rhs_func(self):
        def inner(t, y, params):
            self.t = t
            self.transform_parameters(params)
            self.params = nested.apply_func_nested(params, np.array)
            if self.log_scale:
                exp = np.vectorize(sym.exp)
                self.c = nested.apply_func_nested(y, exp)
            else:
                self.c = self.unnormalize_state(y)
            return self.generate_rhs()

        return inner

    def normalize_state(self, y):
        return nested.apply_operator_nested(y, self.params.c_norm, operator.truediv)

    def unnormalize_state(self, y):
        return nested.apply_operator_nested(y, self.params.c_norm, operator.mul)

    def generate_rhs(self):
        self.rates = self.define_rates()
        return self.define_ode_system_model_qu()

    @property
    def logtrafo(self):
        return (
            self.logtrafo_names_level1
            + self.logtrafo_names_level2
            + self.logtrafo_names_level3
        )

    @property
    def transcription_factors(self):
        tf_mapping = {
            "nar": ["narr"],
            "nir": ["nnr"],
            "nor": [],
            "nos": [],
        }
        return set(
            itertools.chain(
                *[
                    tf_mapping[gene]
                    for gene in self.genes
                    if self.transcriptional_regulation_opts[gene]
                    == "transcription_factors"
                ]
            )
        )

    @property
    def state_levels_nested(self):
        levels = []
        if not self.classical_monod:
            if self.transcription_factors:
                levels.append(("tf", tuple(self.transcription_factors)))
            if not self.mrna_at_quasi_steady_state:
                levels.append(("mrna", tuple(self.genes)))
            levels.append(("enz", tuple(self.genes)))
        levels.append(("nitrogen", tuple(self.nitrogen_compounds)))
        levels.append(("o2",))
        levels.append(("doc",))
        levels.append(("co2",))
        levels.append(("bio", ("alive", "dead")))
        if self.gas_phase == "transient":
            levels.append(("gas", tuple(self.gases)))
        if self.nitrite_toxicity:
            levels.append(("X",))
        return levels

    @property
    def state_levels(self):
        return expand_tuples(self.state_levels_nested)

    @property
    def gases(self):
        if self.gas_phase != "none":
            if self.two_step_model:
                return ["o2", "n2", "co2"]
            else:
                return ["no", "n2o", "n2", "o2", "co2"]
        else:
            return None

    @property
    def electron_acceptors(self):
        if self.two_step_model:
            return ["o2", "no3", "no2"]
        else:
            return ["o2", "no3", "no2", "no", "n2o"]

    @property
    def gene_to_substrate_mapping(self):
        if self.classical_monod:
            return None
        else:
            return {
                "no3": "nar",
                "no2": "nir",
                "no": "nor",
                "n2o": "nos",
            }

    @property
    def nitrogen_educts(self):
        if self.two_step_model:
            return ["no3", "no2"]
        else:
            return ["no3", "no2", "no", "n2o"]

    @property
    def nitrogen_products(self):
        if self.two_step_model:
            return ["no2", "n2"]
        else:
            return ["no2", "no", "n2o", "n2"]

    @property
    def product_to_educt_mapping(self):
        if self.two_step_model:
            return {
                "no2": "no3",
                "n2": "no2",
            }
        else:
            return {"no2": "no3", "no": "no2", "n2o": "no", "n2": "n2o"}

    @property
    def nitrogen_compounds(self):
        return set([*self.nitrogen_educts, *self.nitrogen_products])

    def define_rates(self):
        rate_mapping = {}
        if not (self.classical_monod or self.mrna_at_quasi_steady_state):
            rate_mapping["transcription"] = self.compute_transcription_rate()
            rate_mapping["mrna_decay"] = self.compute_mrna_decay_rate()
        if not self.classical_monod:
            rate_mapping["translation"] = self.compute_translation_rate()
            rate_mapping["enzyme_decay"] = self.compute_enzyme_decay_rate()

        if self.transcription_factors:
            rate_mapping[
                "transcription_factor_production"
            ] = self.compute_transcription_factor_production_rate()
            rate_mapping[
                "transcription_factor_decay"
            ] = self.compute_transcription_factor_decay_rate()

        electron_acceptor_consumption_rate = (
            self.compute_electron_acceptor_consumption_rate()
        )
        rate_mapping[
            "electron_acceptor_consumption"
        ] = electron_acceptor_consumption_rate
        rate_mapping["nitrogen_production"] = self.compute_nitrogen_production_rate(
            electron_acceptor_consumption_rate
        )
        doc_consumption_rate = self.compute_doc_consumption_rate(
            electron_acceptor_consumption_rate
        )
        rate_mapping["doc_consumption"] = doc_consumption_rate
        rate_mapping["doc_release"] = self.doc_release_rate()
        growth_rate = self.compute_growth_rate(doc_consumption_rate)
        rate_mapping["growth"] = growth_rate
        death_rate = self.compute_death_rate()
        rate_mapping["death"] = death_rate
        biomass_decomp_rate = self.compute_biomass_decomposition_rate()
        rate_mapping["biomass_decomposition"] = biomass_decomp_rate
        doc_release_from_biomass = self.compute_doc_release_from_biomass(
            biomass_decomp_rate
        )
        rate_mapping["doc_release_from_biomass"] = doc_release_from_biomass
        rate_mapping["co2_consumption"] = self.compute_co2_consumption_rate(
            doc_release_from_biomass
        )
        rate_mapping["co2_production"] = self.compute_co2_production_rate(
            electron_acceptor_consumption_rate, growth_rate
        )

        # compute sampling reduction factor based on the current time point
        k_sample = self.sampling_factor()
        if self.gas_phase == "transient":
            rate_mapping["dilution"] = self.compute_dilution_rate(k_sample)

        if self.gas_phase != "none":
            # Leak of O2 and N2 into the system in [mol/L/s]
            # This is the sum of the contiuous leak and a pulse like leakage due to sampling
            leak_rate = self.compute_leak_rate(k_sample)
            rate_mapping["leak"] = leak_rate
            rate_mapping["mass_transfer"] = self.compute_mass_transfer_rate(
                k_sample, leak_rate
            )

        Rates = namedtuple("Rates", rate_mapping.keys())
        return Rates(**rate_mapping)

    def define_ode_system_model_qu(self):
        """Define ODE system based on rates."""
        # Initiate a dictionary with a zero value for all derivatives.
        dcdt = {}
        rates = self.rates
        p = self.params

        if self.log_scale:
            factors = nested.apply_func_nested(self.c, lambda x: 1 / x)
        else:
            factors = nested.apply_func_nested(self.c, lambda x: 1)

        if not self.classical_monod:
            if self.transcription_factors:
                dcdt["tf"] = {
                    tf: (
                        getattr(factors.tf, tf)
                        * getattr(rates.transcription_factor_production, tf)
                        - getattr(factors.tf, tf)
                        * getattr(rates.transcription_factor_decay, tf)
                    )
                    for tf in self.transcription_factors
                }
            if not self.mrna_at_quasi_steady_state:
                dcdt["mrna"] = {}
                # mRNA concentration
                for gene in rates.transcription._fields:
                    dcdt["mrna"][gene] = getattr(factors.mrna, gene) * getattr(
                        rates.transcription, gene
                    ) - getattr(factors.mrna, gene) * getattr(rates.mrna_decay, gene)

            # enzyme concentrations
            dcdt["enz"] = {}
            for gene in rates.translation._fields:
                dcdt["enz"][gene] = getattr(factors.enz, gene) * getattr(
                    rates.translation, gene
                ) - getattr(factors.enz, gene) * getattr(rates.enzyme_decay, gene)

        # CO2 concentrations
        dcdt["co2"] = (
            sum(factors.co2 * val for val in rates.co2_production.energy)
            + factors.co2 * rates.co2_production.growth
            + rates.co2_consumption
        )
        dcdt["o2"] = -factors.o2 * rates.electron_acceptor_consumption.o2

        # nitrogen substrate concentrations
        dcdt["nitrogen"] = {key: 0 for key in self.nitrogen_compounds}
        for substrate in self.nitrogen_educts:
            dcdt["nitrogen"][substrate] -= getattr(
                factors.nitrogen, substrate
            ) * getattr(rates.electron_acceptor_consumption, substrate)
        for product in self.nitrogen_products:
            dcdt["nitrogen"][product] += getattr(factors.nitrogen, product) * getattr(
                rates.nitrogen_production, product
            )

        if self.gas_phase != "none":
            for gas in self.gases:
                if gas in self.nitrogen_compounds:
                    dcdt["nitrogen"][gas] += (
                        getattr(factors.nitrogen, gas)
                        * getattr(rates.mass_transfer, gas)
                    ).expand()
                else:
                    dcdt[gas] += (
                        getattr(factors, gas) * getattr(rates.mass_transfer, gas)
                    ).expand()

        if self.gas_phase == "transient":
            dcdt["gas"] = {}
            # concentrations in the gas phase
            dcdt["gas"]["o2"] = (
                -p.V.liq / p.V.gas * factors.gas.o2 * rates.mass_transfer.o2
                - factors.gas.o2 * rates.dilution.o2
                + factors.gas.o2 * rates.leak.o2
            )
            if not self.two_step_model:
                dcdt["gas"]["no"] = (
                    -p.V.liq / p.V.gas * factors.gas.no * rates.mass_transfer.no
                    - factors.gas.no * rates.dilution.no
                )
                dcdt["gas"]["n2o"] = (
                    -p.V.liq / p.V.gas * factors.gas.n2o * rates.mass_transfer.n2o
                    - factors.gas.n2o * rates.dilution.n2o
                )
            dcdt["gas"]["n2"] = (
                -p.V.liq / p.V.gas * factors.gas.n2 * rates.mass_transfer.n2
                - factors.gas.n2 * rates.dilution.n2
                + factors.gas.n2 * rates.leak.n2
            )
            dcdt["gas"]["co2"] = (
                -p.V.liq / p.V.gas * factors.gas.co2 * rates.mass_transfer.co2
                - factors.gas.co2 * rates.dilution.co2
            )

        # dissolved organic carbon
        dcdt["doc"] = (
            -sum(factors.doc * val for val in rates.doc_consumption)
            + rates.doc_release
            + rates.doc_release_from_biomass
        )

        # number of cells
        dcdt["bio"] = {}
        dcdt["bio"]["alive"] = (
            sum(factors.bio.alive * val for val in rates.growth)
            - factors.bio.alive * rates.death
        )

        # number of dead cells
        dcdt["bio"]["dead"] = +factors.bio.dead * (
            rates.death - rates.biomass_decomposition
        )

        # Toxicity
        if hasattr(self.c, "X"):
            expand = np.vectorize(sym.expand)
            dXdt = self.toxicity_model.ode(self.t, self.c, p) * factors.X
            dcdt["X"] = expand(dXdt, deep=False)

        if self.log_scale:
            powsimp = np.vectorize(sym.powsimp)
            dydt = nested.apply_func_nested(dcdt, lambda x: powsimp(x, deep=True))
        else:
            dydt = self.normalize_state(dcdt)
        return dydt

    def sort_rhs_into_list(self, dydt):
        # Bring the variables into the right order
        inv_variable_positions = {val: key for key, val in self.index_map.items()}
        return [dydt[inv_variable_positions[i]] for i in range(self.n_vars)]

    def transcriptional_activation(self, gene):
        # IMPORTANT: If you change anything here, also change it in the
        # transcriptional_activation_val function
        # Transcription of what gene is triggered by what substance?
        triggers = {"nar": "no3", "nir": "no2", "nor": "no", "nos": "no"}
        c = self.c
        p = self.params
        if gene == "nar":
            # nar transcription is induced by both nitrate and nitrite,
            # so the equation is different
            return (
                c.nitrogen.no3 / p.K_transcription.nar.no3.val
                + c.nitrogen.no2 / p.K_transcription.nar.no2.val
            ) / (
                1
                + c.nitrogen.no3 / p.K_transcription.nar.no3.val
                + c.nitrogen.no2 / p.K_transcription.nar.no2.val
            )
        elif gene == "nir":
            activating_concentrations = sum(
                [getattr(c.nitrogen, subs) for subs in self.nnr_activation]
            )
            return mimen(activating_concentrations, p.K_transcription.nir.val)
        else:
            return mimen(
                getattr(c.nitrogen, triggers[gene]),
                getattr(p.K_transcription, gene).val,
            )

    def transcription_factor_production(self, tf):
        p = self.params
        c = self.c
        if tf == "narr":
            return (
                p.k_bind.narr.no3.val * c.nitrogen.no3
                + p.k_bind.narr.no2.val * c.nitrogen.no2
            ) * (1 - c.tf.narr)
        elif tf == "nnr":
            activating_concentrations = sum(
                [getattr(c.nitrogen, subs) for subs in self.nnr_activation]
            )
            return p.k_bind.nnr.val * (1 - c.tf.nnr) * (activating_concentrations)
        else:
            assert False

    def compute_transcription_factor_production_rate(self):
        if self.transcription_factors:
            TFProductionRate = namedtuple(
                "TFProductionRate", self.transcription_factors
            )
            return TFProductionRate(
                **{
                    tf: self.transcription_factor_production(tf)
                    for tf in TFProductionRate._fields
                }
            )
        else:
            return None

    def compute_transcription_factor_decay_rate(self):
        if self.transcription_factors:
            TFDecayRate = namedtuple("TFDecayRate", self.transcription_factors)
            return TFDecayRate(
                *[
                    getattr(self.params.k_dissociation, tf).val * getattr(self.c.tf, tf)
                    for tf in TFDecayRate._fields
                ]
            )
        else:
            return None

    def transcriptional_regulation(self, gene):
        if self.transcriptional_regulation_opts[gene] == "substrate_regulated":
            return self.substrate_regulated_transcription(gene)
        elif self.transcriptional_regulation_opts[gene] == "transcription_factors":
            return self.transcription_factor_regulation(gene)
        else:
            assert False

    def substrate_regulated_transcription(self, gene):
        return self.transcriptional_activation(gene) * hill_kinetics(
            self.c.o2,
            getattr(self.params.K_i_o2.transcription, gene).val,
            getattr(self.params.n_hill.transcription, gene),
        )

    def transcription_factor_regulation(self, gene):
        c = self.c
        p = self.params
        if gene == "nar":
            active_fnrp = hill_kinetics(
                c.o2, p.K_i_o2.transcription.fnrp.val, p.n_hill.transcription.fnrp
            )
            relative_fnrp = active_fnrp / p.K_dna.fnrp.val
            relative_narr = c.tf.narr / p.K_dna.narr.val
            return (relative_fnrp * relative_narr) / (
                1 + relative_fnrp + relative_narr + relative_fnrp * relative_narr
            )
        elif gene == "nir":
            active_nnr = (
                hill_kinetics(
                    c.o2, p.K_i_o2.transcription.nnr.val, p.n_hill.transcription.nnr
                )
                * c.tf.nnr
            )
            return mimen(active_nnr, p.K_dna.nnr.val)
        else:
            assert False

    def enzyme_inhibition(self, gene):
        if self.transcription_self_inhibition:
            return inhib(
                (getattr(self.c.enz, gene) / self.c.bio.alive * AVOGADRO_CONSTANT),
                self.params.K_inhib_enz.val,
            )
        else:
            return 1.0

    def nitrite_inhibition(self):
        if self.nitrite_toxicity:
            return self.toxicity_model.toxicity(self.c, self.params)
        else:
            return 1.0

    def compute_transcription_rate(self):
        if self.classical_monod:
            return None
        else:
            p = self.params
            c = self.c
            TranscriptionRate = namedtuple("TranscriptionRate", self.genes)
            rates = [
                getattr(p.k_transcription_max, gene)
                * self.transcriptional_regulation(gene)
                * self.nitrite_inhibition()
                * c.bio.alive
                * self.enzyme_inhibition(gene)
                for gene in TranscriptionRate._fields
            ]
            return TranscriptionRate(*rates)

    def compute_mrna_decay_rate(self):
        if self.classical_monod:
            return None
        else:
            MRnaDecayRate = namedtuple("MRnaDecayRate", self.genes)
            rate_list = [
                self.params.k_dec.mrna.val * getattr(self.c.mrna, gene)
                for gene in MRnaDecayRate._fields
            ]
            return MRnaDecayRate(*rate_list)

    def quasi_steady_state_mrna_concentrations(self):
        transcription_rate = self.compute_transcription_rate()
        Genes = namedtuple("Genes", self.genes)
        c_mrna = Genes(
            **{
                gene: getattr(transcription_rate, gene) / self.params.k_dec.mrna.val
                for gene in Genes._fields
            }
        )
        return c_mrna

    def compute_translation_rate(self):
        if self.classical_monod:
            return None
        else:
            TranslationRate = namedtuple("TranslationRate", self.genes)
            if self.mrna_at_quasi_steady_state:
                c_mrna = self.quasi_steady_state_mrna_concentrations()
            else:
                c_mrna = self.c.mrna
            rate_list = [
                getattr(self.params.k_translation, gene) * getattr(c_mrna, gene)
                for gene in TranslationRate._fields
            ]
            return TranslationRate(*rate_list)

    def compute_enzyme_decay_rate(self):
        if self.classical_monod:
            return None
        else:
            EnzymeDecayRate = namedtuple("EnzymeDecayRate", self.genes)
            rate_list = [
                self.params.k_dec.enz.val * getattr(self.c.enz, gene)
                for gene in EnzymeDecayRate._fields
            ]
            return EnzymeDecayRate(*rate_list)

    def sampling_factor(self):
        p = self.params
        if self.smooth_gas_sampling:
            # Regression to non-smooth integral of sampling_factor with 0 + time + time**2
            coefs = [5.22423574e-5, 4.27205147e-10]
            return coefs[0] + 2 * self.t * coefs[1]
        else:
            return sum(
                [
                    norm(self.t, mu=p.t_sample[i], sigma=p.dt_sample / 2)
                    for i in range(len(p.t_sample))
                ]
            )

    def compute_mass_transfer_rate(self, k_sample, leak_rates):
        if self.gas_phase == "none":
            return None
        c = self.c
        p = self.params
        MassTransferRate = namedtuple("MassTransferRate", self.gases)
        c_liq = {
            "no": None if self.two_step_model else c.nitrogen.no,
            "n2o": None if self.two_step_model else c.nitrogen.n2o,
            "n2": c.nitrogen.n2,
            "o2": c.o2,
            "co2": c.co2,
        }
        if self.gas_phase == "qss":
            # If gas concentrations are assumed to be at quasi-steady state,
            # this is not a real mass transfer rate. However, the term can be added to the
            # ode of the liquid concentration like a mass transfer rate to account for effects of the gas phase.

            mass_transfer_rates = {}
            for gas in MassTransferRate._fields:
                k_tr_gas_volume = getattr(p.k_tr, gas) * p.V.liq / p.V.gas
                leak_rate = getattr(leak_rates, gas) if gas in ["n2", "o2"] else 0
                mass_transfer_rates[gas] = getattr(p.k_tr, gas) * (
                    (leak_rate + k_tr_gas_volume * c_liq[gas])
                    / (getattr(p.H, gas) * p.f_dil * k_sample + k_tr_gas_volume)
                    - c_liq[gas]
                )
        elif self.gas_phase == "transient":
            mass_transfer_rates = {
                gas: getattr(p.k_tr, gas)
                * (getattr(c.gas, gas) / getattr(p.H, gas) - c_liq[gas])
                for gas in MassTransferRate._fields
            }
        else:
            assert False
        return MassTransferRate(**mass_transfer_rates)

    def compute_dilution_rate(self, k_sample):
        if self.gas_phase == "none":
            return None
        DilutionRate = namedtuple("DilutionRate", self.gases)
        rate = [
            k_sample * self.params.f_dil * getattr(self.c.gas, gas)
            for gas in DilutionRate._fields
        ]
        return DilutionRate(*rate)

    def compute_leak_rate(self, k_sample):
        p = self.params
        LeakRate = namedtuple("LeakRate", "o2, n2")
        leak_rate_o2 = p.r_diff.o2 + k_sample * p.c_leak.o2
        leak_rate_n2 = p.r_diff.n2 + k_sample * p.c_leak.n2
        return LeakRate(o2=leak_rate_o2, n2=leak_rate_n2)

    def compute_death_rate(self):
        return self.c.bio.alive * self.params.k_dec.bio.val

    def compute_biomass_decomposition_rate(self):
        p = self.params
        return p.k_decomp * self.c.bio.dead

    def compute_doc_release_from_biomass(self, biomass_decomposition_rate):
        p = self.params
        if self.carbon_for_growth:
            doc_per_cell = p.n_doc_growth / p.n_bio_growth / p.M_bio * p.cell_weight.val
        else:
            doc_per_cell = 0.0
        return doc_per_cell * biomass_decomposition_rate

    def compute_electron_acceptor_consumption_rate(self):
        ElectronAcceptorConsumptionRate = namedtuple(
            "ElectronAcceptorConsumptionRate", self.electron_acceptors
        )
        p = self.params
        c = self.c
        F_T = 1
        doc_limitation = mimen(c.doc, p.K_doc) if self.doc_rate_limitation else 1.0
        substrate_to_enzyme_mapping = {
            "no3": "nar",
            "no2": "nir",
            "no": "nor",
            "n2o": "nos",
        }
        r_max = {
            substrate: getattr(p.k_max, substrate).val
            * getattr(c.enz, substrate_to_enzyme_mapping[substrate])
            if substrate_to_enzyme_mapping[substrate] in self.genes
            else getattr(p.r_max, substrate).val * c.bio.alive
            for substrate in self.nitrogen_educts
        }
        rates = {
            substrate: (
                r_max[substrate]
                * F_T
                * mimen(
                    getattr(c.nitrogen, substrate), getattr(p.K_subs, substrate).val
                )
                * doc_limitation
                * inhib(c.o2, getattr(p.K_i_o2.reaction, substrate).val)
            )
            for substrate in self.nitrogen_educts
        }

        rates["o2"] = (
            p.r_max.o2.val * c.bio.alive * mimen(c.o2, p.K_subs.o2.val) * doc_limitation
        )
        return ElectronAcceptorConsumptionRate(**rates)

    def compute_nitrogen_production_rate(self, electron_acceptor_consumption_rate):
        p = self.params
        educts = self.product_to_educt_mapping
        NitrogenProductionRate = namedtuple(
            "NitrogenProductionRate", self.nitrogen_products
        )
        rate_list = {
            compound: getattr(p.n_energy_out, compound)
            / getattr(p.n_energy_in, educts[compound])
            * getattr(electron_acceptor_consumption_rate, educts[compound])
            for compound in NitrogenProductionRate._fields
        }
        return NitrogenProductionRate(**rate_list)

    def compute_doc_consumption_rate(self, electron_acceptor_consumption_rate):
        p = self.params
        DocConsumptionRate = namedtuple("DOCConsumptionRate", self.electron_acceptors)
        rate_list = []
        for electron_acceptor in DocConsumptionRate._fields:
            Y = getattr(p.Y, electron_acceptor)
            n_doc_energy = getattr(p.n_doc_energy, electron_acceptor)
            n_energy_in = getattr(p.n_energy_in, electron_acceptor)

            if self.carbon_for_growth:
                if self.growth_yield_units == "cells/mol_DOC":
                    carbon_for_growth_factor = 1 / (
                        1
                        - Y.val
                        * p.n_doc_growth
                        * p.cell_weight.val
                        / (p.M_bio * p.n_bio_growth)
                    )
                else:
                    assert False
            else:
                carbon_for_growth_factor = 1
            total_factor = n_doc_energy / n_energy_in * carbon_for_growth_factor
            rate = total_factor * getattr(
                electron_acceptor_consumption_rate, electron_acceptor
            )
            rate_list.append(rate)
        return DocConsumptionRate(*rate_list)

    def doc_release_rate(self):
        """DOC release rate from the sediment following Loschko et al. (2018)"""
        p = self.params
        if self.doc_release == "constant":
            c_sat = p.c_sat.doc
        elif self.doc_release == "depth_dependent":
            c_sat = p.c_sat_0.doc * sym.exp(-p.lambda_poc * p.spatial_coordinate)
        else:
            assert False
        return p.k_release.doc * (c_sat - self.c.doc)

    def compute_growth_rate(self, doc_consumption_rate):
        p = self.params
        GrowthRate = namedtuple("GrowthRate", self.growth_substrates)
        if self.growth_yield_units == "g_bio/mol_DOC":
            conversion_factor = 1 / p.cell_weight.val
        elif self.growth_yield_units == "cells/mol_DOC":
            conversion_factor = 1
        else:
            assert False

        # Account for reduction of the growth rate in logistic growth when
        # biomass approaches the carrying capacity
        if self.growth_model == "logistic":
            reduction_factor = 1 - self.c.bio.alive / self.params.B_max.val
        elif self.growth_model == "exponential":
            reduction_factor = 1
        else:
            assert False

        rates = {
            compound: getattr(p.Y, compound).val
            * getattr(doc_consumption_rate, compound)
            * reduction_factor
            for compound in GrowthRate._fields
        }
        return GrowthRate(**rates)

    def compute_co2_consumption_rate(self, doc_release_from_biomass):
        p = self.params
        return doc_release_from_biomass / p.n_doc_growth * p.n_co2_growth

    def compute_co2_production_rate(
        self, electron_acceptor_consumption_rate, growth_rate
    ):
        p = self.params
        # CO2 production from energy reactions
        EnergyRate = namedtuple("EnergyRate", self.electron_acceptors)
        rates = {
            compound: getattr(electron_acceptor_consumption_rate, compound)
            / getattr(p.n_energy_in, compound)
            * getattr(p.n_co2_energy, compound)
            for compound in EnergyRate._fields
        }

        co2_production_energy = EnergyRate(**rates)

        # CO2 production from biomass synthesis
        if self.carbon_for_growth:
            co2_production_growth = (
                sum(growth_rate)
                * p.cell_weight.val
                / p.M_bio
                / p.n_bio_growth
                * p.n_co2_growth
            )
        else:
            co2_production_growth = 0
        Co2ProductionRate = namedtuple("Co2ProductionRate", "energy, growth")

        return Co2ProductionRate(co2_production_energy, co2_production_growth)


def make_rate_func(rate_expressions, problem):
    return sunode.symode.lambdify.lambdify_consts(
        "_rate",
        argnames=["time", "state", "params"],
        expr=np.array(rate_expressions),
        varmap=problem._varmap,
        debug=False,
    )


def compute_rates(rate_expressions, dataset, solver, problem):
    compute_rates_inner = make_rate_func(list(rate_expressions.values()), problem)
    user_data = problem.make_user_data()
    problem.update_params(user_data, solver.get_params())

    def compute_rates_single_timepoint(t, flat_y, flat_params):
        subset_params = flat_params.view(problem.params_subset.subset_dtype)[0]
        problem.update_subset_params(user_data, subset_params)
        params = problem.extract_params(user_data)
        rates_out = np.zeros(len(rate_expressions))
        state = flat_y.view(problem.state_dtype)[0]
        compute_rates_inner(rates_out, t, state, params)
        return rates_out

    rates_vectorized = xr.apply_ufunc(
        compute_rates_single_timepoint,
        dataset.time_dense,
        dataset.flat_y_dense,
        dataset.params_flat,
        input_core_dims=[tuple(), ("variable",), ("parameter",)],
        output_core_dims=[("rate",)],
        vectorize=True,
    )
    return rates_vectorized.assign_coords(rate=list(rate_expressions))


def compute_qss_mrna(tr, solver, problem, gene):
    def qss_mrna_wrapper(flat_params, states, gene):
        states = states.view(problem.state_dtype)[..., 0]
        user_data = problem.make_user_data()
        problem.update_params(user_data, solver.get_params())
        subset_params = flat_params.view(problem.params_subset.subset_dtype)[0]
        problem.update_subset_params(user_data, subset_params)
        params = problem.extract_params(user_data)
        return qss_mrna_concentration_val(
            gene=gene,
            p=params,
            log_c=states,
            enzyme_inhibition=False,
            nitrite_toxicity=False,
            log_scale=False,
        )

    return xr.apply_ufunc(
        functools.partial(qss_mrna_wrapper, gene=gene),
        tr.params_flat,
        tr.flat_y_dense,
        input_core_dims=[
            ("parameter",),
            (
                "time_dense",
                "variable",
            ),
        ],
        output_core_dims=[("time_dense",)],
        vectorize=True,
    )


def compute_qss_enz(tr, solver, problem, gene):
    def qss_enzyme_wrapper(flat_params, states, gene):
        states = states.view(problem.state_dtype)[..., 0]
        user_data = problem.make_user_data()
        problem.update_params(user_data, solver.get_params())
        subset_params = flat_params.view(problem.params_subset.subset_dtype)[0]
        problem.update_subset_params(user_data, subset_params)
        params = problem.extract_params(user_data)
        return qss_enzyme_concentration_val(gene, params, states)

    return xr.apply_ufunc(
        functools.partial(qss_enzyme_wrapper, gene=gene),
        tr.params_flat,
        tr.flat_y_dense,
        input_core_dims=[
            ("parameter",),
            (
                "time_dense",
                "variable",
            ),
        ],
        output_core_dims=[("time_dense",)],
        vectorize=True,
    )


class ToxicityModel:
    def __init__(
        self,
        distribution="none",
        normalization=None,
        inner_func_type="identity",
        outer_func_type="toxicity",
        gamma_dist_alpha=None,
    ):
        allowed_distributions = [
            "none",
            "uniform",
            "exponential",
            "gamma",
            "sum_of_gamma",
        ]
        if not distribution in allowed_distributions:
            ValueError(f"Unknown distribution: {distribution}")
        else:
            self.distribution = distribution
        self.normalization = normalization
        self.inner_func_type = inner_func_type
        self.outer_func_type = outer_func_type
        if gamma_dist_alpha is not None:
            if self.distribution in ["gamma", "sum_of_gammas"]:
                self.gamma_dist_alpha = gamma_dist_alpha
            else:
                raise ValueError(
                    "gamma_dist_alpha is only used when using a gamma distribution."
                )
        else:
            if self.distribution in ["gamma", "sum_of_gammas"]:
                self.gamma_dist_alpha = 2
            else:
                self.gamma_dist_alpha = None
        toxicity_dims = {
            "gamma": ["lag_order"],
            "sum_of_gamma": ["lag_order"],
            "uniform": [],
            "exponential": [],
            "none": [],
        }
        toxicity_coords = {
            "gamma": {"lag_order": np.arange(self.gamma_dist_alpha)},
            "sum_of_gamma": {"lag_order": np.arange(self.gamma_dist_alpha)},
            "uniform": {},
            "exponential": {},
            "none": {},
        }
        self.toxicity_dims = toxicity_dims[distribution]
        self.toxicity_coords = toxicity_coords[distribution]

    def generate_parameters(self):
        if self.distribution == "none" or (
            self.distribution == "uniform" and not self.normalization
        ):
            dist_params = {}
        elif self.distribution == "uniform" and self.normalization:
            dist_params = {"t_before": 10}
        elif self.distribution in ["exponential", "gamma", "sum_of_gammas"]:
            t_1_2_log = np.log(5)
            dist_params = {
                "lambda_tox": np.log(2) * np.exp(-t_1_2_log) / 3600,
                "t_1_2_tox": {"log": t_1_2_log},
            }
        else:
            assert False
        if self.inner_func_type == "identity":
            inner_func_params = {}
        elif self.inner_func_type == "toxicity":
            inner_func_params = {"K_tox": {"val": 2e-3, "log": np.inf}, "p_tox": 4}
        elif self.inner_func_type == "bounded_toxicity":
            inner_func_params = {
                "K_tox": {"val": 2e-3, "log": np.inf},
                "p_tox": 4,
                "f_min": 0.2,
            }
        else:
            raise ValueError(f"Unknown function type: {self.inner_func_type}")
        if self.outer_func_type == "identity":
            outer_func_params = {}
        elif self.outer_func_type == "toxicity":
            outer_func_params = {"K_tox_2": {"val": 2e-3, "log": np.inf}, "p_tox_2": 4}
        elif self.outer_func_type == "bounded_toxicity":
            outer_func_params = {
                "K_tox_2": {"val": 2e-3, "log": np.inf},
                "p_tox_2": 4,
                "f_min_2": 0.2,
            }
        else:
            raise ValueError(f"Unknown function type: {self.outer_func_type}")
        return {**dist_params, **inner_func_params, **outer_func_params}

    def generate_initial_values(self, c0, p):
        initial = self.inner_function(c0, p)
        if self.distribution == "none":
            y0 = {}
        elif self.distribution in ["exponential", "uniform"]:
            y0 = {"X": (initial, ())}
        elif self.distribution in ["gamma", "sum_of_gammas"]:
            y0 = {
                "X": (
                    np.ones(self.gamma_dist_alpha) * initial,
                    (self.gamma_dist_alpha,),
                )
            }
        else:
            assert False
        return y0

    def ode(self, t, c, p):
        y, X = self.get_vars(c, p)
        if self.distribution == "none":
            dXdt = None
        elif self.distribution == "uniform":
            if self.normalization:
                dXdt = 1 / (t + p.t_before) * y - 1 / (t + p.t_before) * X
            else:
                dXdt = y
        elif self.distribution == "exponential":
            if self.normalization:
                dXdt = p.lambda_tox * (y - X)
            else:
                dXdt = y - p.lambda_tox * X
        elif self.distribution in ["gamma", "sum_of_gammas"]:
            if self.normalization:
                X_shifted = np.array([y, *X[:-1]])
                dXdt = p.lambda_tox * (X_shifted - X)
            else:
                raise ValueError(
                    "Gamma distribution without normalization is not implemented."
                )
        return dXdt

    def toxicity(self, c, p):
        y, X = self.get_vars(c, p)
        if self.distribution == "gamma":
            X = X[-1]
        elif self.distribution == "sum_of_gammas":
            X = 1 / self.gamma_dist_alpha * sum(X)
        else:
            X = X
        T = self.outer_function(X, p)
        return T

    def get_vars(self, c, p):
        y = self.inner_function(c, p)
        if self.distribution == "none":
            X = y
        else:
            X = c.X
        return y, X

    def inner_function(self, c, p):
        if isinstance(c, dict):
            c_nitrite = c["nitrogen"]["no2"]
        else:
            c_nitrite = c.nitrogen.no2
        if self.inner_func_type == "identity":
            f = c_nitrite
        elif self.inner_func_type == "toxicity":
            if isinstance(p, dict):
                p_tox = p["p_tox"]
                K_tox = p["K_tox"]["val"]
            else:
                p_tox = p.p_tox
                K_tox = p.K_tox.val
            f = tox(c_nitrite, K_tox, p_tox)
        elif self.inner_func_type == "bounded_toxicity":
            if isinstance(p, dict):
                p_tox = p["p_tox"]
                K_tox = p["K_tox"]["val"]
                f_min = p["f_min"]
            else:
                p_tox = p.p_tox
                K_tox = p.K_tox.val
                f_min = p.f_min
            f = tox_bounded(c_nitrite, K_tox, p_tox, f_min)
        else:
            raise ValueError(f"Function not implemented: {self.inner_func_type}")
        return f

    def outer_function(self, X, p):
        if self.outer_func_type == "identity":
            f = X
        elif self.outer_func_type == "toxicity":
            f = tox(X, p.K_tox_2.val, p.p_tox_2)
        elif self.outer_func_type == "bounded_toxicity":
            f = tox_bounded(X, p.K_tox_2.val, p.p_tox_2, p.f_min_2)
        else:
            raise ValueError(f"Function not implemented: {self.outer_func_type}")
        return f
