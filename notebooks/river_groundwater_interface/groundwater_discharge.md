---
jupyter:
  jupytext:
    formats: ipynb,md
    notebook_metadata_filter: -kernelspec
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.7
---

# Groundwater discharge into a river (GD)
In this scenario, anoxic, nitrate-rich groundwater exfiltrates into a river.
In the aquifer, particulate organic carbon concentrations are low so that reaction rates are carbon limited.
In the zone around the river higher POC concentrations lead to an increase of DOC concentration so that denitrification can take place.

```python
import os

import arviz as az
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import pandas as pd
import seaborn as sns
import xarray as xr
import pint_xarray

import nitrogene.reaction_model as reaction_model
import nitrogene.utils
import nitrogene.reactive_transport_model as reactive_transport_model
import nitrogene.plotting
```

```python
sns.set_theme(
    context="paper",
    palette=nitrogene.plotting.color_palettes["paul_tol_high_contrast"],
    rc={**nitrogene.plotting.paper_rc, "axes.formatter.use_mathtext": True},
)
```

```python
plot_path = "../../plots/groundwater_river_interface/groundwater_exfiltration"
```

```python
# Read in reaction parameters
tr = az.from_netcdf(
    "../../traces/nuts_trace_pymc3-ext_2021_02_05_nar_nir_tf_activate_nnr_by_no2_only.nc"
).posterior
```

```python
exclude_patterns = [
    "c_",
    "mu_",
    "flat",
    "measure_error",
    "background",
    "y0",
    "r_cfix_o2_log",
]
var_names = [var for var in tr if not any(exlude in var for exlude in exclude_patterns)]
name_map = nitrogene.utils.make_parameter_name_map(tr, var_names)
```

```python
model = reactive_transport_model.Model(
    calibrated_reaction_params=tr[var_names]
    .apply(lambda x: np.exp(x) if "n_hill" in x.name else x)
    .median(dim=("chain", "draw")),
    scenario="groundwater_exfiltration",
    parameter_name_map=name_map,
    solver_kw={"abstol": 1e-12, "reltol": 1e-10},
)
```

## Concentration distribution over time

```python
model.initial_values["bio"]["alive"] = (
    (np.exp(model.reaction_params["B_max"]["log"]) - 1e5)
    * np.exp(-model.reaction_params["lambda_poc"] * model.coords["x"])
    + 1e5
) / model.reaction_params["c_norm"]["bio"]["alive"]
```

```python tags=[]
%%time
# Solve the equation
# t_vals = np.linspace(0, 3000, 3000) * 86400
t_vals = np.linspace(0, 3000, 3000) * 86400
raw_output, solution = model.run_simulation(t0=0, tvals=t_vals)
```

```python
solution = solution.assign_coords(
    travel_time=(
        solution["x"] / np.abs(model.transport_params["v"]) / 86400
    ).assign_attrs(units="d", long_name="travel time")
)
solution_ss = solution.isel(time=slice(-1, None))
raw_output_ss = raw_output[-1, :]
```

```python
sns.set_theme(
    context="paper",
    rc=nitrogene.plotting.paper_rc,
    palette=nitrogene.plotting.color_palettes["paul_tol_high_contrast"],
)
```

```python
# Plot the concentration distribution at steady state
plotter = nitrogene.plotting.plot_periodic_constant_single_scenario(
    constant_solution=solution_ss.squeeze(drop=True),
    subplots_kw=dict(
        figsize=(6, 3),
        gridspec_kw=dict(hspace=0.25, wspace=0.5, top=0.85),
    ),
)
plotter.fig.savefig(f"{plot_path}/concentration_distribution.pdf")
```

```python
plot_kwargs = {"width": 400, "height": 150}
selection = solution_ss.isel(x=slice(None, None, 1))
plot_all = nitrogene.plotting.concentration_distribution_animation(
    ds=selection, plot_kwargs=plot_kwargs
)
plot_all
```

```python
# Save the simulated concentrations
path = "../../output_data/gw_river_denitrification/gw_exfiltration"
solution_ss.squeeze(drop=True).to_netcdf(os.path.join(path, "solution.nc"))
```

## Compute reaction rates

```python
rates = model.compute_reaction_rates(raw_output_ss, solution_ss)
rates_states = reactive_transport_model.combine_rates_and_states(solution_ss, rates)
rates_ds = reactive_transport_model.arange_rates_for_plotting(rates, solution_ss)
rates_ds = rates_ds.sortby("gene")
```

## Find the location of the maximum *nirS* concentration and nitrite reduction rate

```python
c_nir = rates_ds.sel(gene="nir", variable="mRNA")["concentration"]
rate_no2 = rates_ds.sel(gene="nir")["turnover_rate"]
idx_max_nir = c_nir.argmax()
x_max_nir = c_nir.x.isel(x=idx_max_nir)
c_nir.plot()
plt.axvline(x_max_nir)
ax2 = plt.twinx()
rate_no2.plot(color="C1")
idx_max_rate = rate_no2.argmax()
x_max_rate = rate_no2.x.isel(x=idx_max_rate, drop=True)
ax2.axvline(x_max_rate, color="C1")
```

```python
x_max_rate
```

## Relationship between reaction rates and transcripts/enzymes
Transcript-to-gene ratios have been suggested as an estimator for per-cell reaciton rates.
With the model we can assess if that is true for the given set-up (using nitrifier cell concentrations instead of gene counts).

```python
# Plotting
g = (rates_ds.isel(time=-1)).plot.scatter(
    x="concentration_norm",
    y="turnover_rate_norm",
    col="variable",
    row="gene",
    hue="x",
    sharex="col",
    sharey="row",
    alpha=0.7,
    figsize=(6, 3.5),
    cmap="crest",
    s=20,
)
g.axes[1, 0].set_xlabel("$c$ [transcripts/cell]")
g.axes[1, 1].set_xlabel("$c$ [enzymes/cell]")
n_compound_labels = {
    "no3": "$\mathregular{{NO_3}^–}$",
    "no2": "$\mathregular{{NO_2}^–}$",
}
for a, row_name in zip(g.axes[:, 0], g.row_names):
    n_compound = str(rates_ds.rate.sel(gene=row_name).values)
    a.set_ylabel(
        f"{n_compound_labels[n_compound]} reaction rate\n [mol/cell/s]",
        labelpad=12,
    )
sns.despine(bottom=True, left=True)
g.fig.savefig(f"{plot_path}/rate_correlation_cell_specific.pdf", bbox_inches="tight")
```

The relationship between transcripts or enzymes per cell and cell-specific reaction rates is highly non-linear. It is positive close to the river but further away from the river, reaction rates drop even though transcript levels are high.
In the model, transcription is not affected by carbon limitation but reaction rates are strongly carbon limited in the part of the aquifer further away from the river, where the organic carbon content is low.

However, absolute reaction transcript/enzyme concentrations are indirectly also affected by carbon limitation because the biomass is low in carbon-limited parts of the domain. We therefore also look at the relationship of absolute transcript/enzyme numbers and total reaction rates (following plot).

```python
# Plotting
g = (rates_ds.isel(time=-1).sel(x=slice(0, 2))).plot.scatter(
    x="concentration",
    y="turnover_rate",
    col="variable",
    row="gene",
    hue="x",
    sharex="col",
    sharey="row",
    figsize=(6, 3.5),
    cmap="crest",
    s=20,
)
g.axes[1, 0].set_xlabel("$c$ [transcripts/L]")
g.axes[1, 1].set_xlabel("$c$ [enzymes/L]")
n_compound_labels = {
    "no3": "$\mathregular{{NO_3}^–}$",
    "no2": "$\mathregular{{NO_2}^–}$",
}
for a, row_name in zip(g.axes[:, 0], g.row_names):
    n_compound = str(rates_ds.rate.sel(gene=row_name).values)
    a.set_ylabel(
        f"{n_compound_labels[n_compound]} reaction rate\n [mol/L/s]",
        labelpad=12,
    )
sns.despine(bottom=True, left=True)
g.fig.savefig(f"{plot_path}/rate_correlation_absolute_values.pdf", bbox_inches="tight")
```

The *nar* transcript concentrations now go back to zero when the rates drop moving away from the river.
However, the relationship is still highly non-linear and non-unique.

## Relationship with rates “corrected” for carbon limitation
We can test if the carbon limitaiton of the reaction rates is indeed responsible for the non-linearity of the relationship by “correcting“ the reaciton rate with the Michaelis-Menten term for carbon that we know from the rate law:

$$r_{corrected} = r \frac{c_{DOC} + K_{DOC}}{c_{DOC}}$$

```python
doc_limitation_term = reaction_model.mimen(
    solution_ss["doc"], model.reaction_params["K_doc"]
)
doc_correction_term = 1 / doc_limitation_term
rates_ds["doc_corrected_rate"] = rates_ds["turnover_rate"] * doc_correction_term
```

```python
inhibition_constants = xr.DataArray(
    data=[
        np.exp(model.reaction_params["K_i_o2"]["reaction"]["no3"]["log"]),
        np.exp(model.reaction_params["K_i_o2"]["reaction"]["no2"]["log"]),
    ],
    dims=("gene",),
    coords={var: rates_ds[var] for var in ["gene", "rate"]},
    name="inhibition_constant",
)
o2_correction_term = (solution_ss["o2"] + inhibition_constants) / inhibition_constants
```

```python
K_subs = xr.DataArray(
    data=[
        np.exp(model.reaction_params["K_subs"]["no3"]["log"]),
        np.exp(model.reaction_params["K_subs"]["no2"]["log"]),
    ],
    dims=("gene",),
    coords={var: rates_ds[var] for var in ["gene", "rate"]},
    name="half_saturation_constant",
)
```

```python
o2_correction_term = (solution_ss["o2"] + inhibition_constants) / inhibition_constants

nitrogen_concentrations = xr.concat(
    (
        solution_ss["nitrogen_no3"],
        solution_ss["nitrogen_no2"],
    ),
    dim=pd.Index(["nar", "nir"], name="gene"),
)
nitrogen_correction_term = 1 / reaction_model.mimen(nitrogen_concentrations, K_subs)
```

```python
rates_ds["o2_doc_corrected_rate"] = (
    rates_ds["turnover_rate"] * o2_correction_term * doc_correction_term
)
rates_ds["o2_corrected_rate"] = rates_ds["turnover_rate"] * o2_correction_term
rates_ds["all_corrections"] = (
    rates_ds["turnover_rate"]
    * o2_correction_term
    * doc_correction_term
    * nitrogen_correction_term
)
rates_ds["nitrogen_corrected_rate"] = (
    rates_ds["turnover_rate"] * nitrogen_correction_term
)
```

```python
# Plotting
g = (rates_ds.isel(time=-1)).plot.scatter(
    x="concentration",
    y="doc_corrected_rate",
    col="variable",
    row="gene",
    hue="x",
    sharex="col",
    sharey="row",
    alpha=0.7,
    figsize=(6, 3.5),
    cmap="crest",
    s=20,
)
g.axes[1, 0].set_xlabel("transcripts/L")
g.axes[1, 1].set_xlabel("enzymes/L")
n_compound_labels = {
    "no3": "$\mathregular{{NO_3}^–}$",
    "no2": "$\mathregular{{NO_2}^–}$",
}
for a, row_name in zip(g.axes[:, 0], g.row_names):
    n_compound = str(rates_ds.rate.sel(gene=row_name).values)
    a.set_ylabel(
        f"{n_compound_labels[n_compound]} reaction rate\n [mol/L/s]",
        labelpad=12,
    )
g.fig.align_ylabels(g.axes[:4])
# sns.despine(bottom=True, left=True)
g.fig.savefig(f"{plot_path}/rate_correlation_doc_corrected.pdf", bbox_inches="tight")
```

The corrected rate show a nearly perfectly linear relationship with transcript and enzyme concentrations, showing that the non-linearities indeed mostly stem form carbon limitation of the rates.

Only a slight x-offset of the relationship can be observed for *nar* transcripts and enzymes. This is because transcription of the *nar* gene is triggered by both nitrate and nitrite. Close to the river, nitrate is already consumed so that the reaction rate is zero, but transripts are present because there is still nitrite left.

The close to linear relationship means that in this scenario, the transcript are a good proxy for the *potential*, not carbon-limited rates. If the DOC concentration and half-saturation constant are know, we could obtain actual reaciton rates from the transcript concentrations accounting for the DOC limitation term.

```python
file_name = "rates.nc"
rates_ds.to_netcdf(os.path.join(path, file_name))
```

<!-- #region tags=[] -->
## Sensitivity analysis
<!-- #endregion -->

```python
from copy import deepcopy
import nitrogene.sensitivity as sens
```

```python
# Parameters that will be considered in the sensitivity analysis
sens_parameters = [
    ("transport", "right", "inflow_concentration", "nitrogen", "no3"),
    ("reaction", "k_dec", "bio", "log"),
    ("reaction", "Y", "no3", "log"),
    ("reaction", "Y", "no2", "log"),
    ("reaction", "Y", "o2", "log"),
    ("reaction", "B_max", "log"),
    ("reaction", "k_max", "no3", "log"),
    ("reaction", "k_max", "no2", "log"),
    ("reaction", "r_max", "o2", "log"),
    ("reaction", "K_doc"),
    ("reaction", "K_subs", "no3", "log"),
    ("reaction", "K_subs", "no2", "log"),
    ("reaction", "K_subs", "o2", "log"),
    ("reaction", "K_i_o2", "reaction", "no3", "log"),
    ("reaction", "K_i_o2", "reaction", "no2", "log"),
    ("reaction", "K_i_o2", "transcription", "fnrp", "log"),
    ("reaction", "K_i_o2", "transcription", "nnr", "log"),
    ("reaction", "k_release", "doc"),
    ("reaction", "lambda_poc"),
    ("reaction", "c_sat_0", "doc"),
    ("reaction", "t_1_2", "enz", "log"),
    ("reaction", "r_mrna_log", "nar"),
    ("reaction", "r_mrna_log", "nir"),
    ("reaction", "r_enz_log"),
    ("reaction", "k_dissociation", "nnr", "log"),
    ("reaction", "k_dissociation", "narr", "log"),
    ("reaction", "k_bind", "narr", "no3", "log"),
    ("reaction", "k_bind", "narr", "no2", "log"),
    ("reaction", "k_bind", "nnr", "log"),
    ("reaction", "K_dna", "fnrp", "log"),
    ("reaction", "K_dna", "narr", "log"),
    ("reaction", "K_dna", "nnr", "log"),
]
```

```python
default_params_dict = deepcopy(model.solver.get_params_dict())
```

```python
sens_parameter_names = ["-".join(path) for path in sens_parameters]
```

```python tags=[]
perturbed_rates_states = {}
for sens_param in sens_parameters:
    perturbed_solution = sens.compute_perturbed_ss_solution(
        model,
        t0=0,
        tvals=t_vals,
        default_params_dict=default_params_dict,
        param_path=sens_param,
        increment=0.01,
    )
    perturbed_solution["rate_no3"] = (
        perturbed_solution["rate_no3_norm"] * perturbed_solution["bio_alive"]
    )
    perturbed_solution["rate_no2"] = (
        perturbed_solution["rate_no2_norm"] * perturbed_solution["bio_alive"]
    )
    perturbed_rates_states[sens_param] = perturbed_solution
```

```python
len(perturbed_rates_states)
```

```python
sens_path = "../../output_data/gw_river_denitrification/"
```

```python
# Save the outputs from model runs with perturbed parameters
for i, (path, values) in enumerate(perturbed_rates_states.items()):
    mode = "w" if i == 0 else "a"
    values.to_netcdf(
        path=os.path.join(sens_path, "perturbed_rates_states_gd.nc"),
        group="-".join(path),
        mode=mode,
    )
```

```python
# Read in the outputs from model runs with perturbed parameters
perturbed_rates_states = {
    path: xr.open_dataset(
        os.path.join(sens_path, "perturbed_rates_states_gd.nc"), group="-".join(path)
    )
    for path in sens_parameters
}
```

```python
is_log_param = {
    path: any("log" in subpath for subpath in path) for path in sens_parameters
}
```

```python
reference_rates_states = rates_states.assign(
    {
        "rate_no3": rates_states["rate_no3_norm"] * rates_states.bio_alive,
        "rate_no2": rates_states["rate_no2_norm"] * rates_states.bio_alive,
    }
).isel(time=-1)
sens_numeric = {
    path: sens.compute_numeric_sensitivity(
        perturbed_rates_states.isel(time=-1),
        reference_rates_states,
        increment=0.01,
        is_log_param=is_log_param[path],
    ).drop_vars(["time", "time_of_day", "travel_time"])
    for path, perturbed_rates_states in perturbed_rates_states.items()
}
```

```python
all_sens_numeric = xr.concat(
    sens_numeric.values(), dim=pd.Index(sens_parameter_names, name="parameter")
)
```

```python
normalization_constants = reference_rates_states.integrate("x")
normalized_sensitivites = all_sens_numeric / normalization_constants
```

```python
integrated_sensitivities = np.abs(all_sens_numeric).fillna(0).integrate("x")
integrated_sensitivities_norm = integrated_sensitivities / normalization_constants
```

```python
normalized_sensitivites.to_netcdf(
    "normalized_sensitivities.nc", group="groundwater_discharge", mode="a"
)
```

```python
drop_vars = ["o2"] + [var for var in integrated_sensitivities_norm if "norm" in var]
```

```python
sns.heatmap(
    data=integrated_sensitivities_norm.drop_vars(drop_vars)
    .to_array(name="integrated_sensitivity")
    .to_dataframe()["integrated_sensitivity"]
    .unstack()
)
```

### Principal Component Analysis (PCA)

```python
from scipy import linalg
```

```python
X = (
    normalized_sensitivites.drop_vars(drop_vars)
    .to_array()
    .stack(space_state_var=("x", "variable"))
    .fillna(0)
    .values.T
)
```

```python
from sklearn.decomposition import PCA
```

```python
n_components = None
pca = PCA(n_components).fit(X)
```

```python
cov_mat = np.cov(X.T)
sns.heatmap(cov_mat, center=0)
```

```python
cov_no_center = X.T @ X / len(X)
```

```python
eigvals, eigvecs = linalg.eigh(cov_no_center)
```

```python
X_scaled = X / np.sqrt(len(X))
U, S, Vh = linalg.svd(X_scaled.T, full_matrices=False)
```

The eigenvalues of the data covariance matrix (that is, the covariance in the parameter sensitivities) represent the data variance explained by the respective principal compenent.
The first eigenvalue is much larger than all the others, indicating that there is mainly one important parameter combination.

```python
plt.plot(S**2)
plt.plot(eigvals[::-1])
plt.plot(pca.explained_variance_)
```

```python
variance_ratio = xr.DataArray(
    pca.explained_variance_ratio_ * 100,
    dims=("principal_component",),
    name="explained_variance",
    attrs=dict(units="%", long_name="explained variance"),
    coords={
        "principal_component": (
            "principal_component",
            pd.RangeIndex(
                pca.n_components_,
            ),
            dict(long_name="principal component"),
        )
    },
)
```

```python
variance_ratio.plot.step(figsize=(5, 1.0), where="mid")
plt.title("a", weight="bold")
plt.savefig(os.path.join(plot_path, "pca_explained_variance.pdf"), bbox_inches="tight")
```

The first principal component mostly contains contributions from four parameters:
the nitrate inflow concentration and the three parameters related to the release of organic carbon from the matrix.

```python
principal_directions = xr.DataArray(
    pca.components_,
    dims=("principal_component", "parameter"),
    coords={"parameter": normalized_sensitivites.parameter},
)
```

```python
plt.plot(-U[:, 0])
plt.plot(eigvecs[:, -1])
plt.plot(principal_directions.isel(principal_component=0).values)
plt.xticks(
    ticks=np.arange(len(principal_directions.parameter.values)),
    labels=principal_directions.parameter.values,
    rotation=90,
);
```

```python
latex_labels = [
    nitrogene.plotting.get_latex_label("_".join(param_string.split("-")[1:]))
    for param_string in principal_directions.parameter.values
]
latex_labels = [
    f"$\\log({label[1:-1]})$" if not "log" in label else label for label in latex_labels
]
g = sns.catplot(
    y="weight",
    x="parameter",
    data=principal_directions.isel(principal_component=0)
    .to_dataframe(name="weight")
    .reset_index(),
    aspect=3,
    height=2,
    kind="bar",
    palette=nitrogene.plotting.color_palettes["paul_tol_bright"],
    facet_kws=dict(gridspec_kws=dict(left=0.08, right=0.98, bottom=0.4, top=0.9)),
)
g.set_xticklabels(latex_labels, size=8, rotation=90)
plt.title("b", weight="bold")
g.fig.savefig(os.path.join(plot_path, "sensitivity_pca.pdf"))
```

```python
sns.heatmap(principal_directions.to_series().unstack().T, yticklabels=True, center=0)
```

### Spatial distribution of the sensitivities

```python
with sns.color_palette("tab20"):
    normalized_sensitivites.drop_vars(drop_vars).to_array().plot.line(
        col="variable", col_wrap=3, sharey=False, hue="parameter"
    )
    plt.savefig("../../plots/sensitivity_distribution.pdf")
```

### Compute the solution with an alternative parameter set


```python
weights = principal_directions.isel(principal_component=0)
factors = np.exp(-2 * weights)
increments = np.expm1(-2 * weights)
# Multiplying the weights with a negative number leads to more bioavailable carbon,
# using a positive number makes the system more carbon limited
```

```python
lib = sunode._cvodes.lib
lib.CVodeSStolerances(model.solver._ode, 1e-12, 1e-8)
```

```python
import nitrogene.sensitivity as sens
```

```python tags=[]
solution_alternative_params = sens.compute_perturbed_ss_solution(
    model,
    t0=0,
    tvals=t_vals,
    default_params_dict=default_params_dict,
    param_path=[tuple(param.split("-")) for param in increments.parameter.values],
    increment=increments.values,
)
solution_alternative_params["rate_no3"] = (
    solution_alternative_params["rate_no3_norm"]
    * solution_alternative_params["bio_alive"]
)
solution_alternative_params["rate_no2"] = (
    solution_alternative_params["rate_no2_norm"]
    * solution_alternative_params["bio_alive"]
)
```

```python
solution_alternative_params.to_netcdf(
    "solution_alternative_params.nc", group="groundwater_discharge", mode="a"
)
```

```python
solution_alternative_params_converted_units = (
    solution_alternative_params.assign(
        doc=lambda x: x.doc.assign_attrs(units="M"),
        rate_no3=lambda x: x.rate_no3.assign_attrs(units="mol/L/s"),
        rate_no2=lambda x: x.rate_no2.assign_attrs(units="mol/L/s"),
    )
).map(nitrogene.plotting.convert_concentration_units)
```

### Concentration distribution with the alternative parameters

```python
# Plot the concentration distribution at steady state
plotter = nitrogene.plotting.plot_periodic_constant_single_scenario(
    constant_solution=solution_alternative_params_converted_units.squeeze(drop=True),
    subplots_kw=dict(
        figsize=(6, 3),
        gridspec_kw=dict(hspace=0.25, wspace=0.5, top=0.9, right=0.95, bottom=0.18),
    ),
)
plotter.fig.align_ylabels()
plotter.fig.savefig(f"{plot_path}/concentration_distributions_alternative_params.pdf")
```

<!-- #region tags=[] -->
### Transcript–rate relationship with the alternative parameters
<!-- #endregion -->

```python
rates_alternative_params = xr.concat(
    [solution_alternative_params["rate_no3"], solution_alternative_params["rate_no2"]],
    dim=pd.Index(["no3", "no2"], name="rate"),
    combine_attrs="drop_conflicts",
).rename("rate")
```

```python
rates_ds_alternative_params = reactive_transport_model.arange_rates_for_plotting(
    rates_alternative_params, solution_alternative_params_converted_units
)
rates_ds_alternative_params = rates_ds_alternative_params.sortby("gene")
```

```python
# Plotting
g = (
    rates_ds_alternative_params.map(nitrogene.plotting.convert_concentration_units)
    .sel(x=slice(2.0, None))
    .isel(time=-1)
).plot.scatter(
    x="concentration",
    y="turnover_rate",
    col="variable",
    row="gene",
    hue="x",
    sharex=False,
    sharey="row",
    figsize=(6, 3.5),
    cmap="crest",
    s=20,
)
g.axes[1, 0].set_xlabel("$c$ [transcripts/L]")
g.axes[1, 1].set_xlabel("$c$ [pM]")
n_compound_labels = {
    "no3": "$\mathregular{{NO_3}^–}$",
    "no2": "$\mathregular{{NO_2}^–}$",
}
for a, row_name in zip(g.axes[:, 0], g.row_names):
    n_compound = str(rates_ds.rate.sel(gene=row_name).values)
    a.set_ylabel(
        f"{n_compound_labels[n_compound]} reaction rate\n [nmol/L/s]",
        labelpad=12,
    )
sns.despine(bottom=True, left=True)
g.fig.savefig(
    f"{plot_path}/rate_correlation_absolute_values_alternative_params.pdf",
    bbox_inches="tight",
)
```
