---
jupyter:
  jupytext:
    formats: ipynb,md
    notebook_metadata_filter: -kernelspec
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.11.5
---

# Gas measurement and gene expression data from Qu et al. (2015)
This notebook reads in and plots the measurement data that I got from Linda Bergaust. 
It is the data behind figure 1 in of the paper by Qu et al. (2015). 

### Literature
Qu, Z., Bakken, L. R., Molstad, L., Frostegård, Å., & Bergaust, L. L. (2015). Transcriptional and metabolic regulation of denitrification in Paracoccus denitrificans allows low but significant activity of nitrous oxide reductase under oxic conditions. *Environmental Microbiology, 18(9),* 2951–2963. https://doi.org/10.1111/1462-2920.13128


```python
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
import xarray as xr

%matplotlib inline
sns.set()
sns.set_context("talk")
# sns.set_context('notebook')

path = "~/Documents/phd/experiments/Qu/Data figure 1 Qu et al 2016.xlsx"
path_ndata = "~/Documents/phd/experiments/Qu/2013_05_gases for transcription_succinate_butyrate_Newrobotsort for sing.xlsx"
```

## Nitrogen compound concentrations
The data file *Data figure 1 Qu et al 2016.xlsx* contains post-processed data for the nitrogen concentrations. Although concentrations have been measured in the gas phase, liquid or bulk concentrations are given. 
For the model I want to use the gas phase concentrations. These are given in the file *2013_05_gases for transcription_succinate_butyrate_Newrobotsort for sing.xlsx*.
So use

- *2013_05_gases for transcription_succinate_butyrate_Newrobotsort for sing.xlsx* for all nitroge compound data
- *Data figure 1 Qu et al 2016.xlsx* to read in qPCR data and cell densities

### Read in data

```python
# Read in the gas and nitrite measurements
df_dict = pd.read_excel(
    path_ndata,
    sheet_name=["25", "26", "27"],
    header=None,
    usecols="B,H:L,AT",
    skiprows=25,
    nrows=38,
    names=["time", "O2", "CO2", "N2O", "N2", "NO", "NO2-"],
)

# add column indicating the replicate number
for i, key in enumerate(df_dict.keys()):
    df_dict[key]["replicate"] = i


# Concatenate the data from the different samples
df = (
    pd.concat(df_dict, names=["sample", "sampling point"])
    .swaplevel("sample", "sampling point")
    .sort_index(level="sampling point")
)

# Indicate that those are gas samples
df["gas sample"] = True

# Reset unnecessary index
df.reset_index(level="sample", inplace=True)

# Compute the mean time for the sampling across the replicate vials
time_mean = df.groupby("sampling point").mean()["time"]
df.rename(columns={"time": "time_precise"}, inplace=True)
# Merge mean time into the dataframe
df = pd.merge(
    df,
    time_mean.to_frame(),
    how="outer",
    on=["sampling point"],
    suffixes=("_precise", "_"),
)

# Choose mean sampling time and replicate as index
df.set_index(["time", "replicate"], inplace=True)

df.head()
```

### Convert concentration units from ppmv to mol/L
The concentration in ppmv is
\begin{equation}
c_i^{ppmv} = \frac{V_i}{V_{tot}} = \frac{n_i}{n_{tot}}
\end{equation}
whereas the concentration in mol/L=M is
\begin{equation}
c_i^{M} = \frac{n_i}{V_{tot}}\,.
\end{equation}
The conversion from the former to the latter is
\begin{equation}
c_i^M = c_i^{ppmv} \frac{n_{tot}}{V_{tot}}
\end{equation}
The ratio $\frac{n_{tot}}{V_{tot}}$ can be obtained from the ideal gas law:
\begin{equation}
\frac{n_{tot}}{V_{tot}} = \frac{p_{tot}}{RT}
\end{equation}

Specifying the units of each conversion factor yields
\begin{equation}
c_i \left[\frac{\text{mol}}{\text{L}}\right] = c_i \left[\text{ppmv}\right]\cdot 10^{-6} \cdot \frac{p\left[\text{atm}\right]}{T\left[\text{K}\right] R\left[\frac{\text{L atm}}{\text{ K mol}}\right]}
\end{equation}

```python
# Convert the gas concentrations
R = 0.082057338  # (L atm)/(K mol)
T = 293.15  # K
p = 1  # atm
conversion_factor = 1e-6 * p / (T * R)
for gas in ["O2", "CO2", "N2O", "N2", "NO"]:
    df[gas] = df[gas] * conversion_factor

# Convert nitrite concentrations from mM to mol/L
df["NO2-"] = df["NO2-"] * 1e-3
df.head()
```

### Put the data into a dataset

```python
# Save the data to a dataset which can be easily saved later
ds = xr.Dataset.from_dataframe(df)
# Rename the variables
# Assign units
ds["time"].attrs = {"units": "h"}
for key in ["O2", "CO2", "N2O", "N2", "NO", "NO2-"]:
    ds[key].attrs = {"units": "mol/L"}
```

## qPCR Data
### Read in the data

```python
# Read in qPCR data
df2 = pd.read_excel(
    path,
    sheet_name=1,
    header=None,
    skiprows=3,
    names=["sample", "time", "OD660", "cells", "napA", "narG", "nirS", "norB", "nosZ"],
    usecols="B,C,F:G,Q:U",
    nrows=40,
)

# create a column for the replicate number
n_reps = df2.groupby(["time"]).count()["napA"]
replicate = [np.arange(n_rep) for n_rep in n_reps]
df2["replicate"] = np.concatenate(replicate).astype("U")
# Indicate that those samples are no gas samples
df2["gas sample"] = False

df2.set_index(keys=["time", "replicate"])
df2.head()
```

### Unit conversions
Cell concentrations are converted from cells/mL to cells/L.

qPCR data of functional genes are given in transcripts/(ng mRNA). This needs to be converted to transcripts/cell, because this is the output of the model.
Qu et al. (2015) use the average mRNA yield for conversion:
> For various reasons, the estimated number of
transcripts per cell is of interest. We can obtain an estimate of this based on the amounts of RNA per cell. The measured RNA per cell showed considerable variation, suggesting a variable extraction efficiency (Fig. S2). Thus, rather than using the measured RNA per cell in each sample to calculate the number of transcripts per cell, we used the average of all measurements, which was 0.6 fg RNA per cell and report the estimated number of transcripts per 1000 cells

Linda Bergaust explains in a email that this conversion ist tricky:
> We didn’t see any correlations or trends between sampling times and RNA yields, and so we decided to rather use the average RNA yield per cell across samples. Per cell estimations are problematic because there are so many steps from sampling to gene expression profile (sampling -> RNAprotect -> lysis -> RNA purification -> DNase treatment -> reverse transcription -> qPCR).

However, I do not have any other information so I will follow their approach.
The conversion then is
\begin{equation}
c_{mRNA}\left[\frac{\text{transcripts}}{\text{cell}}\right] = c_{mRNA}\left[\frac{\text{transcripts}}{\text{ng}_{\text{RNA}}}\right] \cdot 0.6\frac{\text{fg RNA}}{\text{cell}} \cdot 10^{-6} \frac{\text{ng RNA}}{\text{fg RNA}}
\end{equation}

**Update:**
I will normalize each mRNA concentration series by its maximum. 
That is, for replicate $i$ and gene $j$, the normalized concentration is:
\begin{equation}
c_{j,i}^{norm}(t) = \frac{c_{j,i}(t)}{\max_t(c_{j,i}(t))}
\end{equation}

The reason is that the previous normalization does not give consistent results.
As Qu et al. (2015) describe in the paper, one of the replicates showed the same timing as the other two replicates but an overall higher magnitude. 
This indicates that the measuements can only provide relative but not absolute values.
This could be caused by a different (but unknown) extraction efficiency between the replicates. 
We can also circumvent overestimating the data standard deviation du to different magniutes if we normalize the time series.

**Update 2 (October 29, 2019)**
By normalizing to the maximum I loose all the information about absolute mRNA time series.
To me, it seems to be a better approach to normalize all the replicate time series to a common mean value

$$
c_{norm}^j(t, i) = c_j(t, i) \cdot \frac{\text{mean}_{t,i}(c_j)}{\text{mean}_t(c_j)}\,,
$$
where $\text{mean}_{t,i}$ is the mean value over all timepoints and all replicates whereas $\text{mean}_{t}$ is the time average of a single replicate time series.

```python
# Convert the cell data
df2["cells"] = df2["cells"] * 1e3

# Convert the transcription data
conversion_factor = 0.6 * 1e-6
for key in ["napA", "narG", "nirS", "norB", "nosZ"]:
    df2[key] = df2[key] * conversion_factor
df2.head()
```

```python
# Arange data in a dataset for saving it later
ds2 = xr.Dataset.from_dataframe(df2.set_index(keys=["time", "replicate"]))
for key in ["napA", "narG", "nirS", "norB", "nosZ"]:
    ds2[key].attrs= {"units": "transcripts/cell"}
```

```python
# Copy original values before normalizing
ds2 = ds2.assign(
    {key + "_raw": ds2[key] for key in ["napA", "narG", "nirS", "norB", "nosZ"]}
)


def normalize_to_common_mean(x):
    return x.mean() / x.mean(dim="time") * x


ds2.update(
    ds2[["napA", "narG", "nirS", "norB", "nosZ"]].apply(
        normalize_to_common_mean, keep_attrs=True
    )
)

# Keeping the attributes somehow does not work, so reassign them
for key in ["napA", "narG", "nirS", "norB", "nosZ"]:
    ds2[key].attrs = {"units": "transcripts/cell"}
ds2["time"].attrs = {"units": "h"}
ds2["cells"].attrs = {"units": "cells/L"}
```

```python
# Compare the data before and after normalization
not_normalized_mrna_data = (
    ds2[["napA_raw", "narG_raw", "nirS_raw", "norB_raw", "nosZ_raw"]]
    .to_dataframe()
    .stack()
    .reset_index()
    .rename(columns={"level_2": "gene", 0: "concentration"})
)
g = sns.relplot(
    x="time",
    y="concentration",
    data=not_normalized_mrna_data,
    kind="line",
    #     hue="replicate",
    col="gene",
)
plt.subplots_adjust(top=0.82)
g.fig.suptitle("Data Before Normalization", fontsize=22)

normalized_mrna_data = (
    ds2[["napA", "narG", "nirS", "norB", "nosZ"]]
    .to_dataframe()
    .stack()
    .reset_index()
    .rename(columns={"level_2": "gene", 0: "concentration"})
)
g = sns.relplot(
    x="time",
    y="concentration",
    data=normalized_mrna_data,
    kind="line",
    #     hue="replicate",
    col="gene",
)
plt.subplots_adjust(top=0.82)
g.fig.suptitle("Normalized Data", fontsize=22)
```

```python
# Adjust data types in the two datasets and save it to an HDF5 file
ds2["replicate"] = ds2["replicate"].astype("int")
ds2["sample"] = ds2["sample"].astype("str")
# Merge the data sets
ds_all = xr.merge([ds, ds2])
ds_all.attrs["Reference"] = ("Qu, Zhi, Lars R. Bakken, Lars Molstad, Åsa Frostegård, and Linda L. Bergaust."
    "“Transcriptional and Metabolic Regulation of Denitrification in  Paracoccus denitrificans Allows Low but Significant "
    "Activity of Nitrous Oxide Reductase under Oxic Conditions.” Environmental Microbiology 18, no. 9 (November 16, 2015):"
    " 2951–63. https://doi.org/10.1111/1462-2920.13128.")
ds_all.attrs["Authors"] = "Qu, Zhi, Lars R. Bakken, Lars Molstad, Åsa Frostegård, and Linda L. Bergaust"
ds_all.attrs["File Creator"] = "Anna Störiko <anna.stoeriko@uni-tuebingen.de>"
ds_all.attrs["Note"] = "This dataset originates from a batch experiment with the denitrifier *Paracoccus denitrificans* conducted by Qu et al. (2015). For details regarding the experimental methods, see the referenced publicaton."
```

```python
ds_all['NO2-'].attrs["description"] = "aqueous nitrite concentration"
ds_all['NO2-'].attrs["long_name"] = "nitrite concentration"
ds_all['OD660'].attrs["description"] = "optical density at 660 nm"
ds_all['cells'].attrs["description"] = "cell density in the liquid phase"
ds_all['cells'].attrs["long_name"] = "cell density"

for subs in ['O2', 'CO2', 'N2O', 'NO', 'N2']:
    ds_all[subs].attrs['long_name'] = f"{subs} concentration"
    ds_all[subs].attrs["description"] = f"gas phase {subs} concentration"
for gene in ['napA', 'narG', 'nirS', 'norB', 'nosZ']:
    ds_all[gene].attrs["long_name"] = f"{gene} concentration"
    ds_all[gene].attrs["description"] = (f"{gene} transcript concentration normalized to the cell density; "
                                         "replicate time series have been normalized to a common mean.")
    gene_raw = f"{gene}_raw"
    ds_all[gene_raw].attrs["long_name"] = f"{gene} concentration"
    ds_all[gene_raw].attrs["description"] = f"{gene} transcript concentration normalized to the cell density; "
```

```python
# Show a summary of the data set
ds_all
ds_all.to_netcdf(path="./data_Qu_2015.nc")
```

## Plots

### Nitrogen compound concentrations

```python
# ds.NO.plot(hue='replicate', marker='x',linestyle="None")
# ds.NO.mean(dim='replicate').plot()
# plt.errorbar(ds['time'],ds['NO'].mean(dim='replicate'),yerr=ds.NO.std(dim='replicate'))
# sns.lineplot(x='time',y='NO2-',data = ds.to_dataframe().reset_index(),
#              ci = "sd",
#              err_style="bars")
```

```python
# Reorganize data into tidy form for plotting
df_N = df.reset_index().melt(value_vars=['O2', 'NO', 'N2O','N2'],
       id_vars = ['time','replicate'])

with sns.axes_style('darkgrid'):
    fig,ax = plt.subplots(figsize=(12,5))
    sns.lineplot(x='time',y='value',
                 data = df_N,
                 hue='variable',
                 markers = True,
                 dashes = False,
                 #style = 'replicate')
                 ci = "sd",
                 err_style="bars")
    plt.xlabel('time [h]')
    plt.ylabel('concentration [mol/L]')
    plt.tight_layout()

```

### Cell densities

```python
fig,ax = plt.subplots(figsize=(12,5))
ax = sns.lineplot(x='time', y='cells', data=df2.reset_index(),
                  markers = True,
                  dashes = False,
                  #style = 'replicate')
                  ci = "sd",
                  err_style="bars")
plt.ylabel('cell density [cells/L]')
```

### mRNA time series 

```python
fig,ax = plt.subplots(figsize=(12,5))
sns.lineplot(
    x="time",
    y="concentration",
    data=normalized_mrna_data,
    hue="gene",
    ci="sd",
    err_style="bars",
    ax=ax
)
plt.xlabel('time [h]')
plt.ylabel('mRNA concentration\n (transcripts/cell)')
fig.savefig('mRNA_timeseries.pdf')

```
