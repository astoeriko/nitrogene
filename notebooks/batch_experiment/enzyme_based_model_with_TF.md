---
jupyter:
  jupytext:
    formats: ipynb,md
    notebook_metadata_filter: -kernelspec
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.7
---

<!-- #region -->
# Enzyme based denitrification model
## Model summary
The model explicitly simulates transcription factor, transcript and enzyme concentrations.
We consider the transcription factor NarR, FnrP and NNR. Inactivation of transcription factors by oxygen is described at quasi-steady state whereas activation by nitrous oxides is considered to be transient:
$$\frac{X_{\text{NarR}}}{dt} = \left(a_{\text{NarR}}^{{\text{NO}_3}^-} C_{{\text{NO}_3}^-} + a_{\text{NarR}}^{{\text{NO}_2}^-} C_{{\text{NO}_2}^-} \right) \left(1 - X_{\text{NarR}}\right) - k_{\text{dec}}^{\text{NarR}} X_{\text{NarR}}$$

$$X_{\text{FnrP}} = \frac{{I_{\text{FnrP}}}^p}{{I_{\text{FnrP}}}^p + {C_{\text{O}_2}}^p}$$

$$\frac{\hat{X}_{\text{NNR}}}{dt} = a_{\text{NNR}} C_{{\text{NO}_2}^-} \left(1 - \hat{X}_{\text{NNR}}\right) - k_{\text{dec}}^{\text{NNR}} \hat{X}_{\text{NNR}}$$

$$X_{\text{NNR}} = \frac{
	    {I_{\text{NNR}}}^q
    }{
	    {I_{\text{NNR}}}^q + {C_{\text{O}_2}}^q
    } \hat{X}_{\text{NNR}}$$
Transcript concentrations $T_i$ for gene $i$ are assumed to be at quasi-steady state and their concentrations are given by
$$T_i =  \alpha^i f_{act}^i B$$
with
$$f_{act}^{\mathit{nar}} = \frac{
		\frac{
			X_{\text{FnrP}}X_{\text{NarR}}
		}{
		K_{\text{FnrP}}K_{\text{NarR}}}
	}
	{
		1
		+ \frac{X_{\text{FnrP}}}{K_{\text{FnrP}}}
		+ \frac{X_{\text{NarR}}}{K_{\text{NarR}}}
		+ \frac{
			X_{\text{FnrP}}X_{\text{NarR}}
		}{
			K_{\text{FnrP}}K_{\text{NarR}}
		}
	}$$
for transcription of *nar* and 
$$f_{act}^{\mathit{nir}} = \frac{X_{\text{NNR}}}{X_{\text{NNR}} + K_{\text{NNR}}}$$
for transcription of *nir*.


Enzyme concentrations follow
$$\frac{d E_i}{dt} = k_{trl} T_i - d_E E_i$$

The denitrificaiton rate of substrate $j$ is then given by
$$r_j = E_i k_{max}^i \frac{C_{j}}{C_{j} + K_{j}} \frac{I_{reac}^i}{I_{reac}^i + C_{\text{O}_2}}$$
<!-- #endregion -->

## Import libraries

```python
import os

os.environ["OMP_NUM_THREADS"] = "1"
os.environ["MKL_NUM_THREADS"] = "1"
os.environ["NUMBA_NUM_THREADS"] = "1"
os.environ["NUMBA_THREADING_LAYER"] = "tbb"

import arviz
import matplotlib.pyplot as plt
import nesttool
import numpy as np
import pandas as pd
import pymc as pm
#import pymc3_ext as pmx
import seaborn as sns
import sunode.symode.problem
import sympy as sym
import aesara
import aesara.tensor as tt
import xarray as xr
from scipy import stats as stats

import nitrogene.plotting as plotting
import nitrogene.postprocessing as postprocessing
import nitrogene.pymc_model as pymc_model
import nitrogene.reaction_model as reaction_model
import nitrogene.utils

```

<!-- #region -->
## Select ODE model options
We create a ``ModelBuilder`` object that will build the ODE right hand side function given the following model options:
* Compute fully transient equations of gas compounds:``gas_phase="transient"`` 
* Simulate transcript concentrations at quasi-steady state: ``mrna_at_quasi_steady_state=False``
* Use the enzyme based model formulation for denitrification, not a Monod-type model: ``classical_monod=False``
* Simulate denitrification as a 2-step process (nitrate to nitrite to N2): ``two_step_model=True``
* Do not consider a decay term for biomass: ``cell_decay=False``
* Consider only growth from aerobic respiration, not from nitrogen substrates: ``growth_substrates=["o2"]``
* Specify the growth yield in units of cells per mol of DOC: ``growth_yield_units="cells/mol_DOC"``
* Consider only carbon uptake for catabolism, not for anabolism: ``carbon_for_growth=False``
* Do not a use a Monod term for the carbon substrate in the reaction kinetics: ``doc_rate_limitaiton=False``
* Smooth out the description of gas sampling events to ease numerical integration: ``smooth_gas_sampling=True``
* Use the enzyme based formulation for both denitrification steps, i.e. simulate NAR and NIR concentrations: ``enzymes_to_simulate=["nar", "nir"]``
* Do not consider downregulation of transcription by high enzyme concentrations: ``transcription_self_inhibition=False``
* Do not consider downregulation of transcription by nitrite toxicity: ``nitrite_toxicity=False``
* Use the transcription factor based formulation for transcriptional activation: 
```python
transcription_regulation_opts={
    "nar": "transcription_factors",
    "nir": "transcription_factors",
}
```

Furthermore we specify that we want to solve the ODE of the log concentrations (``log_scale=True``).
<!-- #endregion -->

```python
model_args = {
    "gas_phase": "transient",
    "mrna_at_quasi_steady_state": True,
    "classical_monod": False,
    "two_step_model": True,
    "cell_decay": False,
    "growth_substrates": ["o2"],
    "growth_yield_units": "cells/mol_DOC",
    "carbon_for_growth": False,
    "doc_rate_limitation": False,
    "smooth_gas_sampling": True,
    "enzymes_to_simulate": ["nir", "nar"],
    "transcription_self_inhibition": False,
    "nitrite_toxicity": False,
    "transcriptional_regulation_opts": {
        "nar": "transcription_factors",
        "nir": "transcription_factors",
    },
}
kwargs = {"log_scale": True}

model_builder = reaction_model.ModelBuilder(**model_args, **kwargs)
model_builder.define_model()
```

## Prepare the measurement data
We read in the experimental data from the batch experiment of Qu et al. (2015) and process it for later use in the likelihood: We apply a Box-Cox transform to the data to improve homoscedasticity of the residuals. Transformation parameters are chosen based on the analysis in error_model.ipynb.

```python
ds = xr.open_dataset("../../experimental_data/data_Qu_2015.nc")
data = nitrogene.utils.process_data_for_likelihood(ds, transform="boxcox")
```

## Set up the statistical model in PyMC3
Defining our statistical model consists of three steps:
1. Define the prior distribution for all parameters that we want to estimate.
2. Compute the simulation result by solving the ODE.
3. Define the likelihood  based on the simulation result and our error model.

The first step also involves computing the actual model parameters from the parameters that will be estimated. We define all priors for the log parameters to account for variability/uncertianty over several orders of magnitude and to ensure positivity. For most parameters the transformation, thus, simply consists in applying an exponential function.
Furthermore, we compute $r_{trsc}^{max}$ and $k_{trsl}$ based on the mRNA and enzyme half-lives, $T_{intra}^{max}$ and $E_{intra}^{max}$ (in ``model_builder.transform_parameters``).
We also use a reparametrization for the Monod parameters of the aerobic respiration rate to avoid strong correlations in the posterior.
Instead of choosing the rate at the limit of $C_{O2} \to \infty$, $r_{max}^{O2}$, as a parameter we choose the rate $r_{C_{fix}}^{O2}$ at a fixed oxygen concentration $C_{fix}^{O2}$ as a parameter.
$r_{max}^{O2}$ can then be recalculated:
$$r_{max}^{O2} = r_{C_{fix}^{O2}} \cdot \frac{K_s^{O2} + C_{fix}^{O2}}{C_{fix}^{O2}} $$
We do not have literature values for $r_{C_{fix}}^{O2}$ to choose a prior.
We therefore approximate the prior's mean and standard deviation based on samples from the original priors.

The likelihood is a Student-T distribution that take the Box-Cox transformed simulation results as mean. For nitrite and oxygen, we add a constant background value to the concentrations before the transform.
Variances consist of the measurement variances from triplicate measurements and an estimated constant term that accoutns for model errors.

```python
variable_error = False

t = ds.time.values * 3600
t_dense = np.linspace(0, (ds.time.values * 3600).max(), 1000)
coords = {
    "time": t / 3600,
    "time_dense": t_dense / 3600,
    "gas_time": t[data["gas_n2"]["flat_time_indices"]],
    "nitrogen_time": t[data["nitrogen_no2"]["flat_time_indices"]],
    "bio_time": t[data["bio_alive"]["flat_time_indices"]],
    "mrna_time": t[data["mrna_norm_nir"]["flat_time_indices"]],
}
observed_dims = {
    "bio_alive": ["bio_time"],
    "mrna_norm_nir": ["mrna_time"],
    "mrna_norm_nar": ["mrna_time"],
    "nitrogen_no2": ["nitrogen_time"],
    "gas_o2": ["gas_time"],
    "gas_n2": ["gas_time"],
}

with pm.Model(coords=coords) as model:
    p = model_builder.default_params  # TODO rename to params

    # 1. Define prior distribution of parameters that we want to estimate
    p["Y"]["o2"]["log"] = pm.StudentT(
        "Y_o2_log", mu=np.log(7e14), sigma=0.5, nu=10
    )  # Hassan (2016)
    p["k_max"]["no3"]["log"] = pm.StudentT(
        "kmax_no3_log", mu=7, sigma=2, nu=10
    )
    p["k_max"]["no2"]["log"] = pm.StudentT(
        "kmax_no2_log", mu=7, sigma=2, nu=10
    )
    p["t_1_2"]["enz"]["log"] = pm.Normal("t12_enz_log", mu=3.0, sigma=0.8)

    p["r_mrna_log"]["nar"] = pm.StudentT(
        "r_mrna_log_nar", mu=-2, sigma=0.7, nu=10
    )
    p["r_mrna_log"]["nir"] = pm.StudentT(
        "r_mrna_log_nir", mu=-2, sigma=0.7, nu=10
    )
    p["K_i_o2"]["transcription"]["fnrp"]["log"] = pm.StudentT(
        "K_i_o2_transcription_fnrp_log", mu=-11, sigma=2, nu=10
    )
    p["K_i_o2"]["transcription"]["nnr"]["log"] = pm.StudentT(
        "K_i_o2_transcription_nnr_log", mu=-11, sigma=2, nu=10
    )
    p["n_hill"]["transcription"]["fnrp"] = np.exp(
        pm.Normal("n_hill_transcription_fnrp", mu=0, sigma=0.5)
    )
    p["n_hill"]["transcription"]["nnr"] = np.exp(
        pm.Normal("n_hill_transcription_nnr", mu=0, sigma=0.5)
    )
    p["k_bind"]["narr"]["no3"]["log"] = pm.Normal(
        "k_bind_narr_no3_log", mu=0, sigma=2
    )
    p["k_bind"]["narr"]["no2"]["log"] = pm.Normal(
        "k_bind_narr_no2_log", mu=0, sigma=2
    )
    p["k_bind"]["nnr"]["log"] = pm.Normal("k_bind_nnr_log", mu=0, sigma=2)
    p["k_dissociation"]["narr"]["log"] = pm.Normal(
        "k_dissociation_narr_log", mu=-9, sigma=0.5
    )
    p["k_dissociation"]["nnr"]["log"] = pm.Normal(
        "k_dissociation_nnr_log", mu=-9, sigma=0.5
    )
    p["K_dna"]["narr"]["log"] = pm.Normal("K_dna_narr_log", mu=-0.8, sigma=0.5)
    p["K_dna"]["fnrp"]["log"] = pm.Normal("K_dna_fnrp_log", mu=-0.8, sigma=0.5)
    p["K_dna"]["nnr"]["log"] = pm.Normal("K_dna_nnr_log", mu=-0.8, sigma=0.5)
    # Reparametrization of oxygen rate parameters
    Ks_log = pm.StudentT("Ks_o2_log", mu=-13, sigma=2, nu=10)
    cfix = 0.6e-4
    # Approximate the prior of the reparametrized parameter based on
    # the prior of the original parameters
    prior_Ks_log = stats.t(loc=-13, scale=2, df=10).rvs(10000)
    prior_rmax_log = stats.norm(loc=-44, scale=1.5).rvs(10000)
    prior_r_cfix_log = prior_rmax_log + np.log(
        cfix / (np.exp(prior_Ks_log) + cfix)
    )
    r_cfix_log = pm.StudentT(
        "r_cfix_o2_log",
        mu=prior_r_cfix_log.mean(),
        sd=prior_r_cfix_log.std(),
        nu=10,
    )
    rmax_log = r_cfix_log + np.log((np.exp(Ks_log) + cfix) / cfix)
    pm.Deterministic("rmax_o2_log", rmax_log)
    p["K_subs"]["o2"]["log"] = Ks_log
    p["r_max"]["o2"]["log"] = rmax_log

    K_i_o2_no3_log = pm.Normal("K_i_o2_no3_log", mu=-15, sigma=3)
    p["K_i_o2"]["reaction"]["no3"]["log"] = K_i_o2_no3_log
    K_i_o2_no2_log = pm.Normal("K_i_o2_no2_log", mu=-15, sigma=3)
    p["K_i_o2"]["reaction"]["no2"]["log"] = K_i_o2_no2_log

    model_builder.transform_parameters(p, module=tt)

    y0 = model_builder.generate_initial_values()
    y0["bio"]["alive"] = pm.StudentT(
        "y0_bio_alive_log", mu=21, sigma=0.5, nu=10
    )
    y0["tf"][
        "narr"
    ] = reaction_model.log_qss_transcription_factor_concentration_val(
        "narr",
        params=p,
        log_c=y0,
    )
    y0["tf"][
        "nnr"
    ] = reaction_model.log_qss_transcription_factor_concentration_val(
        "nnr", params=p, log_c=y0, nnr_activation=model_builder.nnr_activation
    )

    # Format parameters and initial values for use with sunode
    y0 = nesttool.apply_func_nested(y0, nitrogene.utils.as_double)
    y0_with_shapes = nesttool.apply_func_nested(
        y0, pymc_model.add_shapes_to_aesara_vars
    )
    params_with_shapes = nesttool.apply_func_nested(
        p, pymc_model.add_shapes_to_aesara_vars
    )
    params = nesttool.apply_func_nested(
        params_with_shapes, nitrogene.utils.as_double
    )

    # 2. Solve the ODE
    rhs = model_builder.generate_rhs_func()
    t0 = 0
    (
        y,
        flat_y,
        problem,
        solver,
        y0_flat,
        params_flat,
        y_dense,
        flat_y_dense,
    ) = pymc_model.solve_ode(
        t,
        t_dense,
        y0_with_shapes,
        params,
        rhs,
        t0,
        solver_kwargs={"checkpoint_n": 500_000},
        dense_solver_kwargs={"checkpoint_n": 10},
    )
    pm.Deterministic("y0_flat", y0_flat)
    # pm.Deterministic("params_flat", params_flat, dims="params_flat")
    # pm.Deterministic("flat_y", flat_y, dims=["time", "variable"])
    # pm.Deterministic("flat_y_dense", flat_y_dense, dims=["time_dense", "variable"])

    # Compute qss mRNA concentrations
    pymc_model.compute_qss_mrna_concentrations(
        y,
        p,
        model_builder.genes,
        model_builder.log_scale,
        model_builder.transcriptional_regulation_opts,
    )
    pymc_model.compute_qss_mrna_concentrations(
        y_dense,
        p,
        model_builder.genes,
        model_builder.log_scale,
        model_builder.transcriptional_regulation_opts,
    )

    c, c_dense, log_c = pymc_model.unpack_ode_solution(
        y, y_dense, model_builder
    )

    # Normalize mRNA concentrations with biomass
    for conc, log_scale in [(c, False), (log_c, True), (c_dense, False)]:
        pymc_model.compute_normalized_mrna_concentrations(conc, log_scale)

    pymc_model.add_concentration_to_deterministics(
        c_dense, time_dim="time_dense"
    )
    pymc_model.add_concentration_to_deterministics(c, time_dim="time")

    # 3. Define the likelihood
    observed = {}
    observed_var_names = [
        "bio_alive",
        "gas_o2",
        "nitrogen_no2",
        "gas_n2",
        "mrna_norm_nir",
        "mrna_norm_nar",
    ]
    minimum_measure_error = {"nitrogen_no2": 0.025, "gas_n2": 0.1}
    for data_var in observed_var_names:
        # Get the simulation results at the right time points
        simulated, sim_var_name = pymc_model.get_simulated(data_var, log_c)
        indices = data[data_var]["flat_time_indices"]

        # Get the simulated mean log concentrations by adding a background
        if data_var in [
            "bio_alive",
            "gas_n2",
            "mrna_norm_nir",
            "mrna_norm_nar",
        ]:
            mu = simulated[indices]
        else:
            background = pm.Normal(f"background_{data_var}", mu=-18, sd=3)
            mu = tt.log1p(np.exp(simulated - background))[indices] + background

        # transform from log to box-cox
        lmbda = data[data_var]["lambda"]
        mu = tt.expm1(mu * lmbda) / lmbda
        pm.Deterministic(f"mu_{data_var}", mu, dims=observed_dims[data_var])

        # Estimate the data measurement error
        if variable_error:  # variable error
            valid_time_indices = pd.unique(indices)
            valid_time_indices.sort()
            valid_times = t[valid_time_indices]

            error_mu = pm.Normal(f"measure_error_log_mu_{data_var}", mu=-2)
            std = pm.HalfNormal(f"measure_error_log_std_{data_var}", sd=0.2)
            raw = tt.zeros(len(t))
            valid_raw = pm.Normal(
                f"measure_error_log_raw_{data_var}", shape=len(valid_times)
            )
            raw = tt.set_subtensor(raw[valid_time_indices], valid_raw)
            measure_error = tt.exp(error_mu + std * raw)[indices]
            pm.Deterministic(
                f"measure_error_{data_var}",
                measure_error,
                dims=observed_dims[data_var],
            )
        else:  # constant error
            measure_error = pm.HalfNormal(f"measure_error_{data_var}", sd=0.1)
            if data_var in minimum_measure_error:
                measure_error += minimum_measure_error[data_var]
            data_error = data[data_var]["flat_std_vals"]
            total_error = np.sqrt(measure_error ** 2 + data_error ** 2)

        # Model measurements as the simulation results with a Student's T
        # distributed data error (of the Box-Cox transformed values)
        observed[sim_var_name] = pm.StudentT(
            "_".join([data_var, "observed"]),
            nu=7,
            mu=mu,
            sigma=total_error,
            observed=data[data_var]["flat_measurements"],
            dims=observed_dims[data_var],
        )
```

```python
# Set solver tolerances and options
lib = sunode._cvodes.lib
lib.CVodeSStolerances(solver._ode, 1e-12, 1e-12)
lib.CVodeSStolerancesB(solver._ode, solver._odeB, 1e-8, 1e-8)
lib.CVodeQuadSStolerancesB(solver._ode, solver._odeB, 1e-8, 1e-8)
lib.CVodeSetMaxNumSteps(solver._ode, 5000)
lib.CVodeSetMaxNumStepsB(solver._ode, solver._odeB, 5000)
lib.CVodeSetMaxStep(solver._ode, 60)
lib.CVodeSetMaxStepB(solver._ode, solver._odeB, 300)
```

Sunode returns a symbolic representation of the ODE's rhs so that we can check if our implementation of the model is correct.

```python
problem._sym_dydt[1]
```

## Sample the prior

```python
N_prior = 1000
with model:
    prior = pm.sample_prior_predictive(N_prior, random_seed=0)

prior_points = [
    {var.name: prior.prior[var.name].isel(draw=i, chain=0).values for var in model.free_RVs}
    for i in range(N_prior)
]
```

We create a function that computes the log probability value and its gradient with respect to parameters and apply it to all prior points.

```python
func = model.logp_dlogp_function()
func.set_extra_values({})
```

```python
logps = []
grads = []
for point in prior_points:
    _logp, _grad = func(list(point.values()))
    logps.append(_logp)
    grads.append(_grad)
logps = np.array(logps)
```

Now we check if there are NaN values in the logp or its gradient. This can happen for extreme parameter combinations where the ODE becomes very difficult to solve numerically and the ODE solver fails.

```python
np.isfinite(logps).mean()
```

```python
prior_nan_idxs = np.isnan(logps).nonzero()[0]
```

```python
np.isnan(np.array(grads)).any(1).nonzero()[0]
```

## Check gradients and stability numerically


Let's time the evaluation of the logp function and the gradients for 3 prior points.

```python
for point in prior_points[0:3]:
    x0 = list(point.values())
    %timeit func(x0)
```

Now we compute the gradient numerically and compare it to the gradient obtained from solving the adjoint sensitivity equation.

```python
eps = 5e-8
h = np.zeros_like(x0)
variable = "r_mrna_log_nar"
idx = np.where(np.array(list(prior_points[0])) == variable)[0]
h[idx] += eps

a, grad_a = func(x0)
b, grad_b = func(x0 + h)

numerical_approximation = (b - a) / eps
gradient = grad_a[idx]
print(f"Numerical approximation of the gradient: {numerical_approximation}")
print(f"Gradient based on adjoint sensitivity equation: {gradient}")
```

```python
# Numerically approximate the second derivative to see how stable the gradient is
numercial_approx_2nd_deriv = (grad_a - grad_b) / eps
numercial_approx_2nd_deriv
```

## Check the prior predictive
We now do a check if the results of the prior predictive look reasonable. If there is too much N2 at early times or too little at late times, the reaction is too fast or too slow.

```python
too_slow = (
    prior.prior["c_nitrogen_n2"].isel(time=-1)
    < 0.2 * model_builder.default_params["c_norm"]["nitrogen"]["n2"]
)
too_fast = (
    prior.prior["c_nitrogen_n2"].isel(time=4) > 1.0 * model_builder.default_params["c_norm"]["nitrogen"]["n2"]
)
good = ~(too_slow | too_fast)
s = good.squeeze().astype(str).rename("quality")
s[good.squeeze()] = "good"
s[too_fast.squeeze()] = "too_fast"
s[too_slow.squeeze()] = "too_slow"
```

```python
print(good.sum().values, too_fast.sum().values, too_slow.sum().values)
```

```python
# Plot the time series
fig, ax = plotting.plot_two_step(
    prior.prior, ds, classical_monod=False, genes=["nar", "nir"]
)
```

```python
# Plot the parameter distributions
var_names = [var.name for var in model.free_RVs]
df = prior.prior[var_names].squeeze().to_dataframe().drop(columns="chain")
df.columns.name = "variable"
df.index.name = "draw"
df_tidy = df.unstack().rename("value").reset_index()

df_tidy = df_tidy.join(s.to_series(), on="draw")
```

```python
sns.displot(
    data=df_tidy,
    x="value",
    col="variable",
    col_wrap=4,
    hue="quality",
    kind="kde",
    facet_kws=dict(sharex=False, sharey=False),
)
```

## Sample with NUTS
The No-U-Turn sampler (NUTS) is a Hamiltonian Monte Carlo method that uses gradient information to generate new samples.
We use an adaptation of the full mass matrix to better deal with parameter correlations in the posterior.

```python
with model:
    trace_nuts = pm.sample(
        # discard_tuned_samples=False,
        return_inferencedata=True,
        tune=1000,
        draws=1000,
        chains=4,
        random_seed=51,
        # idata_kwargs=dict(log_likelihood=False)
    )
```

## Postprocessing
### Saving the data

```python
trace_nuts.extend(prior)
postprocessing.assign_units_to_inference_data(trace_nuts)
```

```python
trace_nuts.sample_stats.diverging.sum()
```

```python
# Save the results
model_description = (
    "Two-step model with enzyme based formulation for nar and nir, "
    "transcription regulated by NarR, FnrP and NNR and mRNA at quasi-steady state. "
    "Activation of NNR by nitrite only (not by nitrate). "
    "Hill function for oxygen inhibition of transcription factors. "
    "Sampled with NUTS base on the pymc3-ext version (4 chains). "
    "No background values for nar and nir. Likelihood variance is the sum "
    "of data variance, minimum error and estimated model error (StudentT distributed likelihood)."
    "Used all replicates of the transcript data. Estimated initial biomass concentration. "
    "Separate parameters for the maximum mRNA concentration of nar and nir."
)
postprocessing.save_model_output(
    trace_nuts,
    "../../traces/nuts_trace_pymc3-ext_2021_02_05_nar_nir_tf_activate_nnr_by_no2_only.nc",
    model_description,
    model_builder,
    problem,
)
```

```python
tr = arviz.from_netcdf(
    "../../traces/nuts_trace_2020_08_21_nar_nir_var_error_fixed_prior_tempered_nuts_fix_mrna_fix_lamda.nc"
)
```

### Convergence checks & diagnostic plots
The trace plots help us to check convergence and diagnose problems during sampling. The left column shows the posterior parameter distributions and the right column plots parameter values against sample numbers. Each chain is represented individually—if they differ considerably, convergence has not been reached.
Divergences (points in the parameter space where the sampler fails) are indicated by short black lines on the x-axis.

```python
var_names = [var.name.replace("_log__", "") for var in model.free_RVs]
```

```python
with arviz.rc_context(rc={"plot.max_subplots": 50}):
    arviz.plot_trace(trace_nuts, var_names=var_names);
```

The $\hat{R}$ diagnostic test can be used to detect a lack of convergence by comparing the variance between multiple chains to the variance within each chain. Values close to 1 indicate convergence.

```python
rhat = pm.rhat(trace_nuts)[var_names]
rhat
```

We compute the effective sample size (ESS) that takes into account that MCMC samples can be autocorrelated. ESS and $\hat{R}$ diagnostics are computed based on the approach of [Vehtari (2021)]( https://doi.org/10.1214/20-BA1221).

```python
ess = pm.ess(trace_nuts)[var_names]
ess
```

## Posterior time series

```python
sns.set()
fig, ax = plotting.plot_two_step(
    trace_nuts.posterior,
    ds,
    classical_monod=False,
    genes=["nar", "nir"],
    n_lines=100,
)
```

## Residual plots
We plot the normalized residuals $r_ij$ to see how influential single data points are.
The residuals for time point $i$ and replicate $j$ are given by 
$$r_{ij} = \frac{y_i - \hat{y}_{ij}}{\sigma_i}\,,$$
where  $y_i$ is the simulated value, $\hat{y}_{ij}$ is the measured value and $\sigma_i$ is the total (model + measurement) error.

```python
def compute_residuals(posterior, data, var):
    mu = trace_nuts.posterior[f"mu_{var}"]
    time_dim = mu.dims[-1]
    model_error = trace_nuts.posterior[f"measure_error_{var}"]
    if var in minimum_measure_error:
        model_error += minimum_measure_error[var]
    data_errors = xr.DataArray(
        data=data[var]["flat_std_vals"],
        dims=time_dim,
        coords={time_dim: mu[time_dim]},
    )
    total_errors = np.sqrt(model_error ** 2 + data_errors ** 2)
    measurements = xr.DataArray(
        data=data[var]["flat_measurements"],
        dims=time_dim,
        coords={time_dim: mu[time_dim]},
    )
    return (mu - measurements) / total_errors, measurements
```

```python
sns.set(style="darkgrid")
resids, measurements = compute_residuals(trace_nuts, data, "mrna_norm_nar")
resids.sel(chain=0, draw=0).plot(marker="o", lw=0, label="residuals")
ax2 = plt.gca().twinx()
measurements.plot(marker="o", lw=0, ax=ax2, color="C1", label="data")
ax2.grid(False)
```
