# Enzyme-Based Denitrification Modeling
This repository contains modelling code related to two connected projects.

In the first study, we model batch experiments with the denitrifier *Paracoccus denitrificans* performed by [Qu et al. (2015)](README.md#References).
We compare an enzyme based formulation that simulates transcription factors, transcripts and enzymes of denitrification genes to a Monod-type formulation.
We build the model using the Python libraries [*sunode*](https://github.com/aseyboldt/sunode) and [*PyMC*](https://docs.pymc.io/en/stable/).
Sunode creates a symbolic representation of our ODE, generates C-code and solves the ODE using the library SUNDIALS.
PyMC allows us to build a Bayesian model of the data generating process (defining prior distributions and an error model) and to sample the posterior distribution using different methods.
More details on the methods and results can be found in the related publication ([Störiko et al., 2021](README.md#References)).

In the second study, we extend the model to include advective-dispersive transport and additional reaction processes.
Based on the extended model we simulate denitrification, and the production of transcripts and enzymes at the river–groundwater interface.
The transport part of the model is implemented in a separate module, [*adrpy*](https://gitlab.com/astoeriko/adrpy).


## Overview of the files
The `nitrogene` directory contains python files that define the models.
The reaction part of the model is constructed with code contained in the file `reaction_model.py`.
It provides the `ModelBuilder` class that allows to flexibly build models with different features.
Code in the file `reactive_transport_model.py` sets up the reaction model for the groundwater–river interface and couples it to transport.
The python files `plotting.py`, `postprocessing.py`, `pymc_model.py` and `utils.py` provide helper functions for building the model, formatting, plotting and saving model results.
The file `sensitivity.py` provides functions for the numerical computation of local parameter sensitivities for a sensitivity analysis in the second study.

Model outputs and experimental data are not present in the git repository but are uploaded in the [corresponding zenodo repository](https://doi.org/10.5281/zenodo.4058392).

### Notebooks for the development of an enzyme-based model and calibration to a batch experiment
Notebooks for the first study related to the development of the enzyme-based model and its calibration can be found in the directory `notebooks/batch_experiment`.
The notebooks `enzyme_based_model_with_TF.md` and `monod_type_model.md` set up and sample the enzyme-based and Monod-type model, respectively. An enzyme-based model with simplified description of transcriptional regulation is set up in `enzyme_based_model_without_TF.md`.
In the notebook `analyze_model_results.md` we read in and analyze the results from the different models.
The notebook `error_model.md` provides some background and reasoning for our error model and how we choose the parameters for the Box-Cox transform of data and modelling results.
`smooth_gas_sampling.md` explains our approach for smoothing out the effect of gas sampling.

Experimental data originating from the batch experiment described by Qu et al. (2015) are needed to run the modelling code. They are stored as a netcdf file in the subdirectory `experimental_data`.

Modeling results can be found as netcdf files in the folder `traces`. To read them in, using `arviz.from_netcdf` is recommended.

### Notebooks to simulate denitrification at the river–groundwater interface
Notebooks related to the reactive transport model for the river–groundwater interface are in the directory `notebooks/river_groundwater_interface`.
We set up three different scenarios.
In the notebooks `groundwater_discharge.md`, `bank_filtration.md` and `bank_storage.md` we set up and run the simulation for the three scenarios and analyze the generated model outputs. We also conduct a sensitivity analysis for each of the scenarios in these notebooks.
The notebook `plot_all_scenarios.md` creates additional plots to compare the scenarios with each other.
The notebook `plot_sensitivity_analysis.md` summarizes and compares the results of the sensitivity analysis for all three scenarios.

Model outputs are saved in the directory `output_data/gw_river_denitrification`.

## Installation
First, you need to install dependencies, which can be done with the conda package manager.
A list of the required packages can be found in the setup.py.
To create an new conda environment and install the packages, do
```
conda create -n nitrogene-env
conda activate nitrogene-env
conda config --add channels conda-forge
conda config --set channel_priority strict

conda install numpy sympy numba matplotlib scipy xarray pandas pymc arviz holoviews hvplot seaborn sunode
```
Note that the calibration of the enzyme-based model with the batch experiment data was originally (i.e. for the publication of the model) done with PyMC3.
By now, there has been a new release of PyMC (version 4) with some major changes that are not backwards-compatible.
I have tried to update the python modules and notebooks to the new PyMC version but I cannot promise that everything works.

The packages [*nesttool*](https://gitlab.com/astoeriko/nesttool) and [*adrpy*](https://gitlab.com/astoeriko/adrpy) are not available via conda and need to be installed using pip. They should be downloaded and installed automatically during the installation of the *nitrogene* package.
You can install the repo directly from gitlab as follows:
```
pip install git+https://gitlab.com/astoeriko/nitrogene.git
```
Alternatively, you can clone or download the repository and run `pip install -e .` inside this directory.

A list of packages and their version used to produced the simulation results can be found in the files `environment_parameter_estimation.yml` (for the environment used in the first project related to calibration of the model) and `environmen_reactive_transport_model.yml` (for the environment used in the second project related to simulating denitrification at the river–groundwater interface), respectively.

All jupyer notebooks are saved in the format of markdown files using jupytext.
In order to run them, you need to install [jupytext](https://jupytext.readthedocs.io) and pair them with an ipynb file.

## License
The modelling code and its documentation itself are licensed under an MIT license (see LICENSE file).
The datasets containing the model outputs (in the `traces` subdirectory) are licensed under a [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).
The dataset containing experimental data (in the `experimental data` subdirectory) is provided by Linda Bergaust and may be used to reproduce the modelling results.

## References
- Paper with the original experiment and data set:

  Qu, Z., Bakken, L. R., Molstad, L., Frostegård, Å., & Bergaust, L. L. (2015). Transcriptional and metabolic regulation of denitrification in  *Paracoccus denitrificans* allows low but significant activity of nitrous oxide reductase under oxic conditions. *Environmental Microbiology*, *18*(9), 2951–2963. https://doi.org/10.1111/1462-2920.13128

- Paper describing the development and calibration of the enzyme-explicit model:

  Störiko, A., Pagel, H., Mellage, A., & Cirpka, O. A. (2021). Does it pay off to explicitly link functional gene expression to denitrification rates in reaction models? *Frontiers in Microbiology*, *12*. https://doi.org/10.3389/fmicb.2021.684146

- Preprint about simulations with the enzyme-based model for denitrification at the river–groundwater interface:

    Störiko, A., Pagel, H., Mellage, A., Van Cappellen, P., & Cirpka, O. A. (2021). *Denitrification-driven transcription and enzyme production at the river–groundwater interface: Insights from reactive-transport modeling* [Preprint]. Earth and Space Science Open Archive. https://doi.org/10.1002/essoar.10508487.2

## Contact
If you have any questions or concerns regarding the repository you can [file an issue](https://gitlab.com/astoeriko/nitrogene/-/issues/new) or contact [Anna Störiko](mailto:anna.stoeriko@uni-tuebingen.de).
