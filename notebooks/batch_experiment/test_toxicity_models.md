---
jupyter:
  jupytext:
    formats: ipynb,md
    main_language: python
    notebook_metadata_filter: -kernelspec
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.11.5
---

# Toxicity model with delay distribution

```python
import os

os.environ["OMP_NUM_THREADS"] = "1"
os.environ["MKL_NUM_THREADS"] = "1"
os.environ["NUMBA_NUM_THREADS"] = "1"
os.environ["NUMBA_THREADING_LAYER"] = "tbb"
```

```python
import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
import seaborn as sns
import pymc3 as pm
import xarray as xr
import sympy as sym
import theano
import theano.tensor as tt
import sunode.symode.problem
import sunode.wrappers.as_theano
import arviz

import nitrogene.plotting as plotting
import nitrogene.postprocessing as postprocessing
import nitrogene.pymc_model as pymc_model
import nitrogene.reaction_model as reaction_model
import nitrogene.utils
```

```python
# Choose the distribution type
# It must be one of `exp' (for exponential), 'gamma', 'sum_of_gamma', 'uniform', or 'none'
distribution = "gamma"
# Choose if the delay should be applied to concentrations or to the toxicity (choose 'concentration', 'toxicity')
apply_delay_to = "concentration"
```

```python
tox_args = {
    "distribution": distribution,
    "normalization": True,
}
if "gamma" in distribution:
    tox_args["gamma_dist_alpha"] = 4

if apply_delay_to == "concentration":
    tox_args["inner_func_type"] = "identity"
    tox_args["outer_func_type"] = "bounded_toxicity"
elif apply_delay_to == "toxicity":
    tox_args["inner_func_type"] = "bounded_toxicity"
    tox_args["outer_func_type"] = "identity"
else:
    assert False

model_args = {
    "gas_phase": "transient",
    "mrna_at_quasi_steady_state": False,
    "classical_monod": False,
    "two_step_model": True,
    "cell_decay": False,
    "growth_substrates": ["o2"],
    "growth_yield_units": "cells/mol_DOC",
    "carbon_for_growth": False,
    "doc_rate_limitation": False,
    "smooth_gas_sampling": True,
    "enzymes_to_simulate": ["nir", "nar"],
    "transcription_self_inhibition": False,
    "nitrite_toxicity": tox_args,
}
kwargs = {"log_scale": True}

model_builder = reaction_model.ModelBuilder(**model_args, **kwargs)
model_builder.define_model()
```

```python
ds = xr.open_dataset("../../experimental_data/data_Qu_2015.nc")
# Exclude third replicate during the peak as it is a bit delayed compared to the others.
exclude_times = ds.time[(ds.time > 23) & (ds.time < 30)]
ds["nirS"].loc[dict(time=exclude_times, replicate=2)] = np.nan
ds["narG"].loc[dict(time=exclude_times, replicate=2)] = np.nan
data = nitrogene.utils.process_data_for_likelihood(ds, transform="boxcox")
```

```python
time = ds.time.values * 3600
t_dense = np.linspace(0, (ds.time.values * 3600).max(), 1000)
```

```python
model, problem, solver = pymc_model.make_model(model_builder, data, time, t_dense)
```

```python
lib = sunode._cvodes.lib
lib.CVodeSStolerances(solver._ode, 1e-14, 1e-14)
lib.CVodeSStolerancesB(solver._ode, solver._odeB, 1e-12, 1e-12)
lib.CVodeQuadSStolerancesB(solver._ode, solver._odeB, 1e-12, 1e-12)
lib.CVodeSetMaxNumSteps(solver._ode, 5000)
lib.CVodeSetMaxNumStepsB(solver._ode, solver._odeB, 5000)
lib.CVodeSetMaxStep(solver._ode, 60)
lib.CVodeSetMaxStepB(solver._ode, solver._odeB, 300)
```

```python
problem._sym_dydt[0]
```

```python
func = model.logp_dlogp_function()
func.set_extra_values({})
```

```python
N_prior = 20
with model:
    prior = pm.sample_prior_predictive(N_prior, random_seed=0)
```

```python
prior_points = [
    {var.name: prior[var.name][i] for var in model.free_RVs} for i in range(N_prior)
]
```

```python
logps = []
for point in prior_points:
    logps.append(func(func.dict_to_array(point))[0])
logps = np.array(logps)
```

```python
np.isfinite(logps).mean()
```

```python
sns.distplot(logps[np.isfinite(logps)])
```

### Sample with SMC

```python
with model:
    trace = pm.sample_smc(draws=1000, progressbar=True)
```

```python
tr = postprocessing.to_inference_data(
    time=time, time_dense=t_dense, data=data, trace=trace, model=model, prior=prior
)
```

```python
model_description = (
    "Two-step model with enzyme based formulation for nar and nir and nitrite toxicity. "
    "Delayed toxicity with gamma distribution (alpha=4). "
    "No background for mRNA."
    "Constant error (StudentT distributed likelihood). "
    "Used the transcript data with correct unit conversion. "
    "Sampled with SMC."
)
postprocessing.save_model_output(
    tr,
    "traces/smc_trace_2020_06_25_enzyme_based_gamma_tox_conc_const_error_studentT.nc",
    model_description,
    model_builder,
    problem,
)
```

```python
fig, ax = plotting.plot_two_step(
    tr.posterior,
    ds,
    classical_monod=False,
    genes=["nar", "nir"],
    n_lines=100,
)
```

```python
var_names = [var.name for var in model.free_RVs if not var.name.endswith("__")]
```

```python
arviz.plot_density(tr, var_names=var_names, bw=6, transform=lambda x: x / np.log(10));
```
