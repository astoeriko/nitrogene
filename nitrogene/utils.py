import numpy as np
import pandas as pd
from collections import namedtuple
import os
import sympy as sym
import xarray as xr
from scipy import special


def convert_henrys_H(H_ref, T_ref, T, deriv):
    """Correct Henry's coefficient for temperature and convert it to dimensionless units.
    Temperature correction is done according to equation (19) in Sander (2015).

    Parameters
    ----------

    H_ref: Henry coefficient in [mol/(m^3 Pa)] at reference temperature, i.e. H_ref = c_w/p
    T_ref: reference temperate in [K]
    T    : temperature to convert to in K
    deriv: temperature dependence of H_cp d(ln H_cp)/d(1/T) [K] at reference temperature

    H    : Henry's coefficient at temperature T in [mol*L_liquid/(mol*L_gas)], i.e. H = c_g/c_w

    References:
    Sander,nitrite., 2015. Compilation of Henry’s law constants (version 4.0) for water as solvent.
    Atmospheric Chemistry and Physics 15, 4399–4981. https://doi.org/10.5194/acp-15-4399-2015

    """
    # Define gas constant in [J/(mol K)]
    R = 8.315

    H_cp = H_ref * np.exp(deriv * (1 / T - 1 / T_ref))

    H = 1 / (H_cp * R * T)

    return H


def process_data_for_likelihood(ds, *, set_minimum_std=False, transform="none"):
    """Compute average and standard deviation of the data.


    Parameters
    ----------
    set_minimum_std : bool
        If True, data standard deviations are replaced by a minimum value
        if they are smaller than that value.
    transform: str (one of {'none', 'log', 'boxcox'})
        Transformation to apply to the data.
    """

    names = [
        "bio_alive",
        "nitrogen_no2",
        "gas_n2",
        "gas_o2",
        "gas_co2",
        "gas_no",
        "gas_n2o",
        "mrna_norm_nar",
        "mrna_norm_nir",
        "mrna_norm_nor",
        "mrna_norm_nos",
    ]
    name_map = {
        "nitrogen_no2": "NO2-",
        "gas_no": "NO",
        "gas_n2o": "N2O",
        "gas_n2": "N2",
        "gas_o2": "O2",
        "gas_co2": "CO2",
        "mrna_norm_nar": "narG",
        "mrna_norm_nir": "nirS",
        "mrna_norm_nor": "norB",
        "mrna_norm_nos": "nosZ",
        "bio_alive": "cells",
    }
    inv_name_map = {ds_name: model_name for model_name, ds_name in name_map.items()}

    if transform == "none":
        ds = ds.copy()[[name_map[var] for var in names]]
    elif transform == "log":
        ds = np.log(ds.copy()[[name_map[var] for var in names]])
    elif transform == "boxcox":
        lmbdas = {
            "gas_o2": 0.18,
            "gas_co2": 0.18,
            "nitrogen_no2": 0.4,
            "gas_no": 0.3,
            "gas_n2o": 0.18,
            "gas_n2": 0.18,
            "gas_co2": 0.18,
            "mrna_norm_nir": 0.4,
            "mrna_norm_nar": 0.1,
            "mrna_norm_nor": 0.15,
            "mrna_norm_nos": 0.2,
            "bio_alive": 0.05,
        }
        ds = ds.copy()[[name_map[var] for var in names]]
        for var in names:
            name_in_ds = name_map[var]
            ds[name_in_ds] = xr.apply_ufunc(
                special.boxcox, ds[name_in_ds].where(ds[name_in_ds] > 0), lmbdas[var]
            )
    else:
        raise ValueError(f"Not a valid transformation: {transform}")

    if set_minimum_std and transform != "none":
        raise ValueError(
            "set_minimum_std is only a valid option for non-transformed data."
        )
    minimum_std = {
        "mrna_norm_nar": 1e-6,
        "mrna_norm_nir": 1e-6,
        "mrna_norm_nor": 1e-6,
        "mrna_norm_nos": 1e-6,
        "bio_alive": 5e9,
        "nitrogen_no2": 1e-6,
        "gas_no": 1e-9,
        "gas_n2o": 1e-9,
        "gas_n2": 1e-10,
        "gas_o2": 1e-6,
        "gas_co2": 1e-6,
    }

    # Don't use first cell concentration as it obviously does not make sense
    # for the model – it is far too high.
    # Probably, a large part of the inoculum died/was dead.
    ds["cells"].loc[dict(time=0, replicate=0)] = np.nan
    # Also don't use the first point for transcript levels.
    # These transcript levels are still influenced by a fully oxic atmosphere,
    # which I do not account for in the model.
    ds["narG"].loc[dict(time=0, replicate=0)] = np.nan
    ds["nirS"].loc[dict(time=0, replicate=0)] = np.nan
    ds["norB"].loc[dict(time=0, replicate=0)] = np.nan
    ds["nosZ"].loc[dict(time=0, replicate=0)] = np.nan

    output = {}
    for name in names:
        name_in_ds = name_map[name]
        dat = ds[name_in_ds]
        dat = dat.where(np.isfinite(dat))
        # Make sure that all variables have the same order of dimensions
        dat = dat.transpose("time", "replicate")
        # Choose only timepoint where the time series actually has data
        indices = dat.notnull().any(dim="replicate")
        # If one of the replicates has only NaNs, drop it
        dat = dat.dropna(dim="replicate", how="all")
        # Compute mean and standard deviation
        mean = (dat.where(indices, drop=True).mean(dim="replicate")).values
        std = (dat.where(indices, drop=True).std(dim="replicate")).values
        measurements = (
            dat.where(indices, drop=True).transpose("time", "replicate").values
        )
        idx_time, idx_rep = dat.notnull().data.nonzero()
        idx_time = xr.DataArray(idx_time, dims=("measurement",))
        idx_rep = xr.DataArray(idx_rep, dims=("measurement",))
        flat_measurements = dat.isel(time=idx_time, replicate=idx_rep)
        flat_std_vals = dat.isel(time=idx_time).std("replicate")
        if set_minimum_std:
            # If the standard deviation is too small, set it to a minimum value
            std[std < minimum_std[name]] = minimum_std[name]

        # Compute the standard deviation of the centered variable over all time points
        centered_vals = dat - dat.mean(dim="replicate")
        if name == "nitrogen_no2":
            time_below_quantification = dat.isel(time=20).time
            mean_below_quantification = dat.isel(time=slice(20, None)).mean()
            centered_vals.loc[dict(time=slice(time_below_quantification, None))] = (
                dat.isel(time=slice(20, None)) - mean_below_quantification
            )
        n_means = (~centered_vals.isnull().any(dim="replicate")).sum().data
        const_std = centered_vals.std(ddof=n_means)
        # Don't include outliers when calculating the standard deviation
        is_not_outlier_std = np.abs(centered_vals) < 2 * const_std
        const_std = centered_vals.where(is_not_outlier_std).std(ddof=n_means).data

        output[name] = {
            "mean_vals": mean,
            "std_vals": std,
            "indices": indices.values,
            "measurements": measurements,
            "flat_measurements": flat_measurements.data,
            "flat_time_indices": idx_time.data,
            "constant_std": const_std,
            "flat_std_vals": flat_std_vals.values,
        }
        if transform == "boxcox":
            output[name]["lambda"] = lmbdas[name]
    return output


def as_double(val):
    """Convert values (in a tuple) to doubles"""
    if isinstance(val, tuple):
        val, shape = val
        if hasattr(val, "astype"):
            return (val.astype("float64"), shape)
        return (np.array(val, dtype=np.float64), shape)

    if hasattr(val, "astype"):
        return val.astype("float64")
    return np.array(val, dtype=np.float64)


def get_first_tuple_elem(val):
    """Return first element of a tuple or the input if input is not a tuple."""
    if isinstance(val, tuple):
        return val[0]
    else:
        return val


def make_parameter_name_map(tr, var_names=None):
    """Generate map from parameter names in the model to names in the trace."""
    if var_names is None:
        var_names = list(tr)
    replacements = {
        "kmax": "k_max",
        "t12": "t_1_2",
        "rmax": "r_max",
        "Ks": "K_subs",
        "K_i_o2": "K_i_o2_reaction",
    }

    name_map = {}
    for var in var_names:
        new_var = var
        for to_replace, replacement in replacements.items():
            new_var = new_var.replace(to_replace, replacement)
        name_map[new_var] = var
    return name_map