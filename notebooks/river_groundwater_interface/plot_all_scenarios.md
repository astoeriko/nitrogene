---
jupyter:
  jupytext:
    formats: ipynb,md
    notebook_metadata_filter: -kernelspec
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.7
---

```python
import os
import string
import textwrap

import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import numpy as np
import pandas as pd
import seaborn as sns
import xarray as xr

import nitrogene.plotting
```

```python
sns.set_theme(
    style="darkgrid",
    context="paper",
    palette=nitrogene.plotting.color_palettes["paul_tol_bright"],
    rc=nitrogene.plotting.paper_rc,
)
```

## Read in the model outputs

```python
data_path = "../../output_data/gw_river_denitrification/"
plot_path = "../../plots/groundwater_river_interface"
```

```python
rate_filenames = {
    "groundwater_exfiltration": "rates.nc",
    "bank_filtration_fluctuating_o2": "rates_daily_averages.nc",
    "bank_filtration_constant_o2": "rates.nc",
    "bank_storage": "rates_daily_averages.nc",
}
directory_names = {
    "groundwater_exfiltration": "gw_exfiltration",
    "bank_filtration_fluctuating_o2": "bank_filtration_fluctuating_oxygen",
    "bank_filtration_constant_o2": "bank_filtration_constant_oxygen",
    "bank_storage": "bank_storage",
}
```

```python
# Open the files
solutions = {}
rates = {}
for scenario, directory in directory_names.items():
    rate_path = os.path.join(data_path, directory, rate_filenames[scenario])
    solution_path = os.path.join(data_path, directory, "solution.nc")
    solutions[scenario] = xr.open_dataset(solution_path)
    rates[scenario] = xr.open_dataset(rate_path)
```

## Relationship between average transcript concentrations and reaction rates

```python
# Drop the travel time coordinate because it is not present/the same in all scenarios
rates_without_travel_time = {
    scenario: vals if not ("travel_time" in vals) else vals.drop_vars("travel_time")
    for scenario, vals in rates.items()
}

all_rates = {
    scenario: vals.isel(time=-1, drop=True).expand_dims(scenario=[scenario])
    for scenario, vals in rates_without_travel_time.items()
}
all_rates = xr.concat(all_rates.values(), dim="scenario")
```

```python
# Define scenario names to use for plotting
scenario_names = {
    "groundwater_exfiltration": "groundwater discharge",
    "bank_filtration_constant_o2": "bank filtration (constant O₂)",
    "bank_filtration_fluctuating_o2": "bank filtration (periodic O₂)",
    "bank_storage": "bank storage",
}
wrapped_scenario_names = {
    key: "\n".join(textwrap.wrap(val, width=20)) for key, val in scenario_names.items()
}
```

```python
def annotate_location(x_val, rate_data, ax, xytext, **annot_kw):
    ax.annotate(
        text=f"x = {x_val.values:1.2f} m",
        xy=(
            rate_data.concentration.sel(x=x_val),
            rate_data.turnover_rate.sel(x=x_val),
        ),
        xytext=xytext,
        **annot_kw,
    )
```

```python
gene_labels = {"nar": "$\\mathit{narG}$", "nir": "$\\mathit{nirS}$"}
row_labels = {
    "nar": "$\\mathit{narG}$\n"
    + r"$\mathregular{{NO_3}^–}\, \to\, \mathregular{{NO_2}^–}$",
    "nir": "$\\mathit{nirS}$\n" + r"$\mathregular{{NO_2}^–}\, \to\, \mathregular{N_2}$",
}
selection = all_rates.sel(variable="mRNA", x=slice(0, 2)).assign(
    turnover_rate=lambda x: x.turnover_rate * 1e9,
    concentration=lambda x: x.concentration * 1e-9
)

nar_data_ge = selection.sel(gene="nar", scenario="groundwater_exfiltration")

reversal_x = nar_data_ge.x.isel(x=nar_data_ge.concentration.argmax("x"))
max_rate_x = nar_data_ge.x.isel(x=nar_data_ge.turnover_rate.argmax("x"))

g = (
    selection.assign_coords(
        scenario=all_rates.scenario.indexes["scenario"].map(wrapped_scenario_names),
        gene=all_rates.gene.indexes["gene"].map(row_labels),
    )
).plot.scatter(
    x="concentration",
    y="turnover_rate",
    col="scenario",
    row="gene",
    sharex=False,
    sharey=False,
    hue="x",
    cmap="crest",
    figsize=(7, 3.5),
    add_guide=False,
)
g.set_titles(maxchar=100, template="{value}")
g.set_axis_labels(y_var="reaction rate [nM/s]", x_var="")
g.fig.supxlabel("transcript concentration [$10^9\,$transcripts/L]")
for a, label in zip(g.axes.T.flat, "abcdefgh"):
    a.annotate(text=label, xy=(0.08, 0.85), xycoords="axes fraction")
    #a.xaxis.set_major_formatter(nitrogene.plotting.MathTextSciFormatter(fmt="%.1e"))

# Customize the position of the row labels
for label, a in zip(g.row_labels, g.axes[:, 0]):
    left, lower, width, height = a.axes.get_position().bounds
    label.set(
        anncoords="figure fraction",
        rotation="horizontal",
        horizontalalignment="center",
        linespacing=1.7,
        position=(0.05, lower + height / 2),
    )
# Make space for the new row labels
g.fig.subplots_adjust(left=0.17, right=1.08, hspace=0.4)
g.add_colorbar(label="x [m]", anchor=(-0.25, 0.5), aspect=30)
annot_kw = dict(
    arrowprops={
        "arrowstyle": "-",
        "shrinkA": 0,
        "shrinkB": 3,
        "color": "k",
    },
    size=8,
)
annotate_location(
    reversal_x,
    nar_data_ge,
    g.axes[0, 0],
    xytext=(-45, -5),
    textcoords="offset points",
    **annot_kw
)
annotate_location(
    max_rate_x,
    nar_data_ge,
    g.axes[0, 0],
    xytext=(-5, -25),
    textcoords="offset points",
    **annot_kw
)

# Limit the number of grid lines to make the plot less crowded
for a in g.axes.flat:
#    a.xaxis.set_major_locator(mtick.MaxNLocator(nbins=2))
    a.yaxis.set_major_locator(mtick.MaxNLocator(nbins=3))
x_tick_locations = {
    (0, 0): [0, 4, 8],
    (0, 1): [0, 2, 4],
    (0, 2): [0, 2, 4],
    (0, 3): [0, 2, 4],
    (1, 0): [0, 1, 2],
    (1, 1): [0, .5, 1],
    (1, 2): [0, .5, 1],
    (1, 3): [0, .5, 1],
}
# Manually set the same axis limits and ticks for the last three scenarios
for row in [0, 1]:
    x_lim = np.max([a.get_xlim()[1] for a in g.axes[row, 1:]])
    y_lim = np.max([a.get_ylim()[1] for a in g.axes[row, 1:]])
    ytick_locations = g.axes[row, 1].get_yticks()[1:]
    for a in g.axes[row, 1:]:
        a.set_xlim(right=x_lim)
        a.set_ylim(top=y_lim)
        a.yaxis.set_ticks(ytick_locations)
for (row, col), ticks in x_tick_locations.items():
    a = g.axes[row, col]
    a.xaxis.set_ticks(ticks)
g.fig.savefig(os.path.join(plot_path, "rate_correlations.pdf"))
```

## Relationship between average enzyme concentrations and reaction rates

```python
gene_labels = {"nar": "narG", "nir": "nirS"}
row_labels_enz = {
    "nar": "NAR\n" + r"$\mathregular{{NO_3}^–}\, \to\, \mathregular{{NO_2}^–}$",
    "nir": "NIR\n" + r"$\mathregular{{NO_2}^–}\, \to\, \mathregular{N_2}$",
}
selection = all_rates.sel(variable="enzymes", x=slice(0, 2)).assign(
    turnover_rate=lambda x: x.turnover_rate * 1e9,
    concentration=lambda x: x.concentration * 1e12,
)

nar_data_ge = selection.sel(gene="nar", scenario="groundwater_exfiltration")

reversal_x = nar_data_ge.x.isel(x=nar_data_ge.concentration.argmax("x"))
max_rate_x = nar_data_ge.x.isel(x=nar_data_ge.turnover_rate.argmax("x"))

g = (
    selection.assign_coords(
        scenario=all_rates.scenario.indexes["scenario"].map(wrapped_scenario_names),
        gene=all_rates.gene.indexes["gene"].map(row_labels_enz),
    )
).plot.scatter(
    x="concentration",
    y="turnover_rate",
    col="scenario",
    row="gene",
    sharex=False,
    sharey=False,
    hue="x",
    cmap="crest",
    figsize=(7, 3.5),
    add_guide=False,
)
g.set_titles(maxchar=100, template="{value}")
g.set_axis_labels(y_var="reaction rate [nM/s]", x_var="")
g.fig.supxlabel("enzyme concentration [pM]")
for a, label in zip(g.axes.T.flat, "abcdefgh"):
    a.annotate(text=label, xy=(0.08, 0.85), xycoords="axes fraction")
    a.get_xaxis().get_offset_text().set_x(1.05)
# Customize the position of the row labels
for label, a in zip(g.row_labels, g.axes[:, 0]):
    left, lower, width, height = a.axes.get_position().bounds
    label.set(
        anncoords="figure fraction",
        rotation="horizontal",
        horizontalalignment="center",
        linespacing=1.7,
        position=(0.05, lower + height / 2),
    )
# Make space for the new row labels
g.fig.subplots_adjust(left=0.17, right=1.08, hspace=0.4)
g.add_colorbar(label="x [m]", anchor=(-0.25, 0.5), aspect=30)
g.fig.savefig(os.path.join(plot_path, "rate_correlations_enzymes.pdf"))
```

## Relationship between transcript concentrations and “corrected” rates

```python
rate_vars = [
    "turnover_rate",
    "o2_corrected_rate",
    "doc_corrected_rate",
    "o2_doc_corrected_rate",
    "all_corrections",
]
corrected_rates = all_rates[rate_vars].to_array(
    dim="correction", name="corrected_rates"
)
```

```python
exclude_corrections = xr.apply_ufunc(
    np.isin,
    corrected_rates.correction,
    ["o2_corrected_rate", "o2_doc_corrected_rate", "all_corrections"],
)
mask = ~((corrected_rates.scenario == "groundwater_exfiltration") & exclude_corrections)
```

```python
correction_names = {
    "turnover_rate": "effective rate",
    "o2_corrected_rate": "without O₂\ninhibition",
    "doc_corrected_rate": "without DOC\nlimitation",
    "o2_doc_corrected_rate": "without O₂\ninhibition and\nDOC limitation",
    "all_corrections": "without O₂\ninhibition, DOC\nand N limitation",
}
```

```python
with sns.axes_style(
    style="ticks",
    rc={
        **nitrogene.plotting.paper_rc,
        "axes.edgecolor": "dimgrey",
        "xtick.color": "dimgrey",
        "ytick.color": "dimgrey",
    },
):

    g = sns.relplot(
        data=(
            xr.merge((corrected_rates.where(mask), all_rates["concentration"]))
            .sel(
                variable="mRNA",
                x=slice(0, 1.5),
                scenario=["groundwater_exfiltration", "bank_filtration_constant_o2"],
            )
            .assign(corrected_rates=lambda x: x.corrected_rates * 1e6)
            .assign_coords(
                scenario=lambda x: x.scenario.indexes["scenario"].map(
                    wrapped_scenario_names
                )
            )
            .assign_coords(
                correction=lambda x: [
                    correction_names[key] for key in x.correction.values
                ]
            )
            .to_dataframe()
            .reset_index()
        ),
        x="concentration",
        y="corrected_rates",
        col="scenario",
        row="gene",
        facet_kws=dict(
            sharex=False,
            sharey=False,
            margin_titles=True,
        ),
        height=1.5,
        aspect=1.3,
        hue="correction",
        style="correction",
        s=20,
        edgecolor="face",
        alpha=0.75,
    )
    g.set_titles(col_template="{col_name}", row_template="")
    g.set_axis_labels(y_var="reaction rate [µM/s]", x_var="$c$ [transcripts/L]")
    g.set_xlabels(labelpad=12)
    g.fig.align_ylabels()
    for a in g.axes[0, :]:
        text = a.get_title()
        a.set_title(
            text,
            pad=8,
        )
    for a, label in zip(g.axes.flat, "abcdefgh"):
        # Label the panels
        a.annotate(text=label, xy=(0.08, 0.85), xycoords="axes fraction")
        # Set the axis formatter
        # a.xaxis.set_major_formatter(nitrogene.plotting.MathTextSciFormatter(fmt="%.0e"))
    # Customize the position of the row labels
    for row, a in zip(g.row_names, g.axes[:, 0]):
        left, lower, width, height = a.axes.get_position().bounds
        plt.annotate(
            text=row_labels[row],
            xy=(0.07, lower + height / 2),
            xycoords="figure fraction",
            rotation="horizontal",
            horizontalalignment="center",
            verticalalignment="center",
            linespacing=1.7,
        )
    g.fig.subplots_adjust(
        left=0.24,
        wspace=0.35,
        right=0.77,
        hspace=0.7,
    )
    for a in g.axes.flat:
        a.tick_params(color="dimgray", labelcolor="black")
    g.legend.get_title().set_text(None)
    g.legend.labelspacing = 3
    g.legend.set_bbox_to_anchor((0.8, 0.25, 0.21, 0.6))
    g.fig.savefig(os.path.join(plot_path, "corrected_rates.pdf"))
```

To quantify the degree of linearity in the relationship between transcript concentrations and reaction rates, we compute the coefficient of determination $R^2$ of the linear regression line. Values close to 1 will indicate an approximately linear relationship.

```python
r_squared = (
    xr.corr(corrected_rates, all_rates["concentration"].sel(variable="mRNA"), dim="x")
    ** 2
)
```

```python
sns.barplot(
    data=r_squared.sel(
        gene="nar", scenario=["groundwater_exfiltration", "bank_filtration_constant_o2"]
    )
    .to_dataframe("r_squared")
    .reset_index(),
    x="scenario",
    y="r_squared",
    hue="correction",
)
```

# Concentration distribution in all scenarios

```python
def convert_units(ds):
    assert ds.o2.attrs["units"] == "mol/L"
    assert ds.doc.attrs["units"] == "mol-C/L"
    assert ds.nitrogen_no3.attrs["units"] == "mol/L"
    assert ds.nitrogen_no2.attrs["units"] in {"mol/L", "mol/l"}
    assert ds.nitrogen_n2.attrs["units"] == "mol/L"
    assert ds.enz_nar.attrs["units"] == "mol/L"
    assert ds.enz_nir.attrs["units"] == "mol/L"
    return ds.assign(
        o2=lambda x: (x.o2 * 1e6).assign_attrs(units="µmol/L"),
        doc=lambda x: (x.doc * 1e3).assign_attrs(units="mmol-C/L"),
        nitrogen_no3=lambda x: (x.nitrogen_no3 * 1e6).assign_attrs(units="µmol/L"),
        nitrogen_no2=lambda x: (x.nitrogen_no2 * 1e6).assign_attrs(units="µmol/L"),
        nitrogen_n2=lambda x: (x.nitrogen_n2 * 1e6).assign_attrs(units="µmol/L"),
        enz_nar=lambda x: (x.enz_nar * 1e12).assign_attrs(units="pmol/L"),
        enz_nir=lambda x: (x.enz_nir * 1e12).assign_attrs(units="pmol/L"),
    )
```

```python
with sns.color_palette(palette=nitrogene.plotting.color_palettes["paul_tol_high_contrast"]):
    x_selection = slice(0, 2.5)
    plotter = nitrogene.plotting.MultipleScenarioConcentrationDistributionPlot(
        scenario_names=[
            "groundwater discharge",
            "bank filtration",
            "bank storage",
        ],
        flow_directions=["<-", "->", "<->"],
        periodic_solutions=[
            None,
            convert_units(solutions["bank_filtration_fluctuating_o2"].sel(x=x_selection)),
            convert_units(solutions["bank_storage"].sel(x=x_selection)),
        ],
        constant_solutions=[
            convert_units(solutions["groundwater_exfiltration"].sel(x=x_selection)),
            convert_units(solutions["bank_filtration_constant_o2"].sel(x=x_selection)),
            None,
        ],
        subplots_kw=dict(
            sharey="row",
            figsize=(7, 7),
            gridspec_kw=dict(
                hspace=0.2,
                wspace=0.1,
                top=0.9,
                height_ratios=[0.5, 2, 2, 2, 2, 2, 2],
                right=0.8,
            ),
        ),
        title_kw=dict(size="large", pad=10),
        add_fig_legend=[False, False, False],
        subplot_legend_kw={
            "loc": "upper left",
            "bbox_to_anchor": (1.05, 0, 1, 1),
            "frameon": False,
        },
    )
    plotter.plot()
    plotter.fig.align_labels()
    # sns.despine(bottom=True, left=True)
    legends = [a.get_legend() for a in plotter.axes[1:, -1].flat]
    for legend, row_label in zip(legends, string.ascii_lowercase):
        legend.set_title(f"{row_label}) " + legend.get_title().get_text())

    formatter = nitrogene.plotting.MathTextSciFormatter(fmt="%.0e")
    for scenario, axes_map in plotter.axes_mapping.items():
        axes_map["mrna"].yaxis.set_major_formatter(formatter)
        axes_map["bio"].yaxis.set_major_formatter(formatter)
    sns.despine()
    #for a in plotter.axes[0,:]:
    #    sns.despine(ax=a, left=True, bottom=True)
    #    a.set_facecolor("gainsboro")
    #for a in plotter.axes[1:, :].flat:
    #    a.tick_params(labelcolor="black")
    plotter.fig.savefig(
        os.path.join(plot_path, "concentration_distributions_all_scenarios.pdf"),
        bbox_inches="tight",
    )
```

## Transcript and enzyme concentrations normalized to biomass

```python
with sns.color_palette(palette=nitrogene.plotting.color_palettes["paul_tol_high_contrast"]):
    plotter_enz_mrna = nitrogene.plotting.MultipleScenarioConcentrationDistributionPlot(
        scenario_names=[
            "groundwater discharge",
            "bank filtration",
            "bank storage",
        ],
        flow_directions=["<-", "->", "<->"],
        periodic_solutions=[
            None,
            convert_units(solutions["bank_filtration_fluctuating_o2"].sel(x=x_selection)),
            convert_units(solutions["bank_storage"].sel(x=x_selection)),
        ],
        constant_solutions=[
            convert_units(solutions["groundwater_exfiltration"].sel(x=x_selection)),
            convert_units(solutions["bank_filtration_constant_o2"].sel(x=x_selection)),
            None,
        ],
        subplots_kw=dict(
            sharey="row",
            figsize=(6.6, 3),
            gridspec_kw=dict(
                hspace=0.35,
                wspace=0.1,
                top=0.9,
                height_ratios=[0.5, 2, 2],
                right=0.8,
            ),
        ),
        title_kw=dict(size="large", pad=10),
        add_fig_legend=[False, False, False],
        subplot_legend_kw={
            "loc": "upper left",
            "bbox_to_anchor": (1.05, 0, 1, 1),
            "frameon": False,
        },
        groups_to_plot=["mrna_norm", "enz_norm"],
    )
    plotter_enz_mrna.plot()
    plotter_enz_mrna.fig.align_labels()
    # sns.despine(bottom=True, left=True)
    legends = [a.get_legend() for a in plotter_enz_mrna.axes[1:, -1].flat]
    for legend, row_label in zip(legends, string.ascii_lowercase):
        legend.set_title(f"{row_label}) " + legend.get_title().get_text())
    sns.despine()
    #for a in plotter_enz_mrna.axes[0,:]:
    #    sns.despine(ax=a, left=True, bottom=True)
    #    a.set_facecolor("gainsboro")
    #for a in plotter_enz_mrna.axes[1:, :].flat:
    #    a.tick_params(labelcolor="black")
    plotter_enz_mrna.fig.savefig(
        os.path.join(plot_path, "normalized_mrna_enz.pdf"),
        bbox_inches="tight",
    )
```

## Relationship between transcript and enzyme concentrations

```python
time_slices = {
    "groundwater_exfiltration": {},
    "bank_filtration_constant_o2": {},
    "bank_filtration_fluctuating_o2": {"time": slice(-239, None)},
    "bank_storage": {"time": slice(None, 239)},
}
solutions_1day = {
    scenario: solution.isel(**time_slices[scenario])
    for scenario, solution in solutions.items()
}
solutions_same_time = {
    scenario: solution.assign_coords(time=solutions_1day["bank_storage"].time)
    if "time" in solution
    else solution
    for scenario, solution in solutions_1day.items()
}
```

```python
# Combine the solutions into a single data array
all_solutions = xr.concat(
    solutions_same_time.values(),
    dim=pd.Index(solutions_same_time.keys(), name="scenario"),
    coords="minimal",
)

genes = ["nar", "nir"]
mrna_enz_by_gene = xr.Dataset(
    {
        variable: xr.concat(
            [all_solutions[f"{variable}_{gene}"] for gene in genes],
            dim=pd.Index(genes, name="gene"),
        )
        for variable in ["mrna", "enz"]
    }
)

mrna_enz_by_gene["mrna"].attrs.update(
    units="transcripts/L", long_name="transcript concentration"
)
mrna_enz_by_gene["enz"].attrs.update(units="mol/L", long_name="enzyme concentration")
```

```python
legend_rc_params = {
    "legend.borderpad": 0.2,
    "legend.labelspacing": 0.3,
    "legend.handletextpad": 0.1,
}
with sns.axes_style(
    style="ticks",
    rc={
        **nitrogene.plotting.paper_rc,
        "axes.edgecolor": "grey",
        "xtick.color": "grey",
        "ytick.color": "grey",
    },
):
    with sns.color_palette(nitrogene.plotting.color_palettes["paul_tol_high_contrast"]):
        g = (
            mrna_enz_by_gene.assign(
                mrna=lambda x: x.mrna.mean("time"),
                enz=lambda x: (x.enz * 1e12).assign_attrs(
                    units="pM", long_name="enzyme concentration"
                ),
            )
            .assign_coords(
                scenario=all_rates.scenario.indexes["scenario"].map(wrapped_scenario_names),
                gene=all_rates.gene.indexes["gene"].map(gene_labels),
            )
            .isel(time=slice(None, None, 40), x=slice(None, None, 1))
            .plot.scatter(
                x="mrna",
                y="enz",
                col="scenario",
                hue="gene",
                figsize=(5.1, 2.2),
                s=10,
                alpha=0.5
            )
        )
        g.set_xlabels(label="")
        g.fig.supxlabel(t="transcript concentration [transcripts/L]")
        g.set_titles(template="{value}", maxchar=50, pad=10)
        g.fig.subplots_adjust(bottom=0.33, left=0.08, right=0.98)
        g.fig.legends[0].set_bbox_to_anchor(
            (0.55, 0.55, 0.45, 0.45), transform=g.axes[0, -1].transAxes
        )
        g.fig.legends[0].set_frame_on(False)
        sns.despine()
        for a in g.axes.flat:
            a.tick_params(labelcolor="black")
        g.fig.savefig(os.path.join(plot_path, "mrna_vs_enz.pdf"), facecolor=(1, 1, 1, 0))
```

# Simulated transcript measurements

```python
def repeat_data(data, n_days):
    return xr.concat(
        [data] * n_days,
        xr.concat(
            [data.time + i for i in range(n_days)],
            dim="time",
        ).assign_attrs(units=data.time.attrs["units"]),
    )
```

```python
times_per_day = len(solutions["bank_filtration_fluctuating_o2"]["mrna_nar"])
```

```python
sns.set_theme(
    style="ticks",
    context="paper",
    palette=nitrogene.plotting.color_palettes["paul_tol_bright"],
        rc={
        **nitrogene.plotting.paper_rc,
        "axes.edgecolor": "dimgrey",
        "xtick.color": "dimgrey",
        "ytick.color": "dimgrey",
    },
)
```

```python
def saturation_curve(x, limit=1, scale=1):
    return np.tanh(x / scale) * limit
```

```python
with sns.axes_style(
    style="ticks",
        rc={
        **nitrogene.plotting.paper_rc,
        "axes.edgecolor": "grey",
        "xtick.color": "grey",
        "ytick.color": "grey",
    },
):
    fig, ax = plt.subplots(
        nrows=2,
        sharex=False,
        figsize=(5, 3),
        gridspec_kw=dict(hspace=0.6, right=0.8, bottom=0.15, left=0.11),
    )
    plot_data = solutions["bank_filtration_fluctuating_o2"]["mrna_nir"].assign_coords(
        time=lambda x: x.time - 99
    )
    plot_data.time.attrs.update(
        units=solutions["bank_filtration_fluctuating_o2"].time.attrs["units"]
    )
    multiday_data = repeat_data(plot_data, n_days=14)

    # Time series sampled at different frequencies
    frequency = np.array([10, 3, 1, 1 / 7])
    idx_interval = times_per_day / frequency
    time_idx = [
        np.arange(0, multiday_data.sizes["time"], interval) for interval in idx_interval
    ]
    rng = np.random.default_rng(seed=42)
    random_time_offsets = [
        rng.normal(scale=saturation_curve(1 / freq, scale=1.0, limit=1.5), size=len(idx))
        for idx, freq in zip(time_idx, frequency)
    ]  # time offsets in h
    perturbed_indices = [
        np.mod(
            np.round(idx + offset * times_per_day / 24 + 0.7 * times_per_day).astype("int"),
            multiday_data.sizes["time"],
        )
        for idx, offset in zip(time_idx, random_time_offsets)
    ]

    data_different_freqencies = [multiday_data.isel(time=idx) for idx in perturbed_indices]
    markers = [".", "d", "<", "v"]
    x_sel = 0.17
    for data, freq, marker in zip(data_different_freqencies, frequency, markers):
        data.sortby("time").sel(x=x_sel, method="nearest").sel(time=slice(0, 10)).plot.line(
            marker=marker, label=f"{freq:.2g}", ax=ax[0]
        )
    ax[0].legend(
        loc="center left",
        bbox_to_anchor=(1.0, 0.5),
        title="sampling\nfrequency [day$^{-1}$]",
        frameon=False,
    )
    ax[0].set_title(f"x = {x_sel:1.2g} m")
    ax[0].annotate("a", xy=(0.03, 0.86), xycoords="axes fraction", weight="bold")

    # Profiles at two time points
    plot_data.isel(time=[-180, -40], x=slice(None, 40, 3)).assign_coords(
        time=lambda x: np.round(x.time, decimals=2)
    ).plot.line(marker="o", hue="time", ax=ax[1])
    legend = ax[1].get_legend()
    legend.set_bbox_to_anchor((1.03, 0.7))
    legend.set_frame_on(False)
    ax[1].annotate("b", xy=(0.03, 0.86), xycoords="axes fraction", weight="bold")
    for a in ax.flat:
        a.tick_params(labelcolor="black")
    sns.despine()
    fig.savefig(os.path.join(plot_path, "simulated_samples.pdf"))
```
