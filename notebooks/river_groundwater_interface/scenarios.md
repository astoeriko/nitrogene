---
title: "Simulation scenarios for the second work package"
bibliography: ../../../../Documents/phd/phd.bib
...


# 1 Bank filtration
- Water from a nitrate contaminated, oxic river infiltrates into an aquifer, induced by pumping.
- Common scenario in drinking water production
- Nitrate removal in the aquifer is relevant for drinking water production in the well.
- Nitrate concentration in the river: 10 mg/L
- Flow velocity of 1e-5 m/s
- DOC concentration in the river of 2 mg/L

## 1a With constant oxygen concentration
- shaded river where diurnal cycles in oxygen saturation through photosynthesis do not play a role.
- oxygen concentration of 8 mg/L

## 1b With fluctuating oxygen concentration
- diurnal cylces in the photosynthesis activity cause fluctuating oxygen concentrations
- oxygen signal approximated by a sine function with a frequency of 1/day
- mean oxygen concentration of 8 mg/L with an amplitude of 3 mg/L


# 2 Bank storage in a regulated river
- during baseflow, water exfiltrates from the aquifer into the river
- high river stage during a flood event/release of water from a reservoir reverses the flow so that river water infiltrates the aquifer
- flow patterns in a regulated river approximated by a sine wave for the infiltration velocity
- amplitude of 5e-6 m/s, mean velocity slightly negative so that the river is overall gaining
- groundwater is anoxic, contains no DOC
- constant oxygen concentration in the river of 8 mg/L
- DOC concentration in the river: 2 mg/L
- nitrate concentration: a) 5 mg/L, b) 30 mg/L

# 3 Gaining stream in an agricultural area
- high nitrate load (30 mg/L) in an anoxic groundwater without DOC
- flow of groundwater towards the river
- carbon rich sediments around the river enable denitrifaction that has not taken place in the aquifer due to lack of electron donors  

# Parameter references

## Nutrient concentrations in river and groundwater

| Reference | scenario | GW nitrate | river O2 | river nitrate | DOC river | GW O2 |
| --------- | --------- | :--------: | :------: | :-----------: | :-------: | :---: |
| @shuaiDenitrificationBanksFluctuating2017 | bankstorage through hydropeaking in the Lower Colorado River | 3 mg/L | 9 mg/L | 20 mg/L | 6 mg/L |
| @liuEffectWaterChemistry2017 | column experiments mimicking situation in Columbia river (bank storage in a regulated river)| 28.5 mg/L | 7 mg/L | 2.8 mg/L | |
| @guNitrateReductionStreambed2007 | column experiments mimicking nitrate loaded groundwater discharging into a lowland stream | 38 mg/L | – | 8.9 mg/L |
| @kennedySpatialTemporalDynamics2009 | GW from an agricultural area discharging into stream | 27 mg/L | – | – | – | 0.6 mg/L |
| @kunzHighFrequencyMeasurements2017 | river with oxygen fluctuations | – | mean 11 mg/L, amplitude 2–5 mg/L | 9 mg/L | – | – |
| @hayashiDiurnalFluctuationsElectrical2012 | alpine river with oxygen flucuations | – | fluctuations between 8 and 20 mg/L | ~10 mg/L | ~2 mg/L |


## Flow velocities at the river-groundwater interface
| Reference | scenario | method | infiltration velocity | exfiltration velocity |
| --------- | -------- | ------ | :-------------------: | :-------------------: |
| @shuaiDenitrificationBanksFluctuating2017 | hydropeaking in the Lower Colorado River | 2-D flow model | 3.3e-6 m/s | 2.6e-6 m/s|
| @liuCoupledHydrobiogeochemicalProcesses2017 | hydropeaking in the Columbia River | estimate based on Darcy's law and head measurements | 2.7e-6 m/s | 4.2e-6 m/s|
| @gerechtDynamicsHyporheicFlow2011 | hydropeaking in the Lower Colorado River | estimate based on Darcy's law and head measurements | 2e-5 to 1.2e-4 m/s | 8e-5 to 1.2e-4 m/s|
| @vogtFluctuationsElectricalConductivity2010 | bank filtration (River Thur) | 1-D transport of electrical conductivity | 1e-4 m/s | – |
| @sheetsLagTimesBank2002 | bank filtration (Great Miami River) | travel times from EC data and distances between wells | 3e-6 to 6e-5 m/s | – |
| @bertinRadon222ChlorideNatural1994 | bank filtration at river Lot | radon tracer | 7e-6 to 3.8e-5 m/s| – |
| @kennedySpatialTemporalDynamics2009 | GW exfiltration into a river (West Bear Creek) | estimate based on Darcy's law | – | 1.97e-5 m/s |

## Carrying capacity $B_{max}$
- Literature values range from 0.9 × 10<sup>8</sup> to 9.4 × 10<sup>8</sup> cells/mL of porous medium.
- I need to convert that to the corresponding cell density in the liquid phase (in cells/mL liquid) by dividing by the porosity.
- Assuming a porosity of 30%, 10<sup>9</sup> cells/mL (1e12 cells/L) is a reasonable value for $B_{max}$.

## Biomass degradation constant $k_{dec}^{bio}$
- Literature values range from 10<sup>-9</sup> s<sup>-1</sup> (Ding, 2010) to 2.7 × 10<sup>-6</sup> s<sup>-1</sup> (Li et al., 2017) with more values being closer to 10<sup>-6</sup> s<sup>-1</sup>.

## Inflowing oxygen concentration
- oxygen saturation at 20°C is 9.1 mg/L but concentrations can become supersaturated due to photosynthesis.
- Diurnal fluctuations have been observed in many rivers, ranging between about 3 mg/L to 20 mg/L (concentrations can probably even drop below that but I don't have in exmaple in the literature I screened)
- mean concentration of 8 mg/L with an amplitude of 3 mg/L lies well within that range

## Inflowing nitrate concentration
- [Kunz et al. (2017)](https://agupubs.onlinelibrary.wiley.com/doi/full/10.1002/2016WR019355) and [Hayashi et al. (2012)](https://www.sciencedirect.com/science/article/pii/S0022169412003964#f0010) both report about 10 mg/L of nitrate

## Inflowing DOC concentration
- Literature values indicate that DOC in rivers mostly is in the range of 1–5 mg-C/L (hihger values have been observed in boreal sites, e.g. in skandinavian countries).
- 2 mg-C/L correspond to 0.16 mmol-C/L. Assuming that the DOC consists of succinic acid (C4H6O4), this corresponds to $2 \frac{\text{mg-C}}{\text{L}} \frac{1}{12}\frac{\text{mol-C}}{\text{g-C}} \frac{1}{4}\frac{\text{mol}}{\text{mol-C}} = 0.042 \frac{\text{mmol}}{\text{L}}$

## Advective velocity
Literature values of seepage velocities range from 1.2e-7 m/s (Schmidt et al., 2006; for a mostly gaining river) to about 5e-5 m/s (Vogt et al. 2010; in a losing river).
Accounting for a porosity of about 30%, an advective velocity of 1e-5 m/s seems to be reasonable.

## Dispersion coefficient
A common parametric model for the longitudinal dispersion coefficient is
$$D_l = \alpha_l v + D_e$$
with longitudinal dispersivity $\alpha_l$ which is on the order of magnitude of the mean grain size and the effective diffusion coefficient $D_e$.
The dispersivity for sandy material on a scale of meters is around 0.1 m [@gelharCriticalReviewData1992].



## DOC half-saturation constant
Literature values have a rather broad range from 1e-5 mol/L to 5e-4 mol/L. Assuming that DOC consists of succinate, this corresponds to succinate concentrations of 2.5e-6 to 1.25e-4 mol/L. A concentraition of 2e-5 mol/L of succinate is well in that range.

## DOC release rate
kinzelbachNumericalModelingNatural1991 suggest the following parametrization:
$$r_{release} = k_{release} * (c_{DOC}^{sat} - c_{DOC})$$

An alternative, but equivalent parametrization is used by guNitrateReductionStreambed2007, sawyerEnhancedRemovalGroundwaterborne2015, and knightsTidalControlsRiverbed2017:
$$r_{release} = \frac{\rho_b}{\theta} \alpha \left(c_{\text{POC}} - K_d c_{\text{DOC}}\right)$$

If we set $k_{release} = \frac{\rho_b}{\theta} \alpha K_d$ and $c_{DOC}^{sat} = \frac{c_{\text{POC}}}{K_d}$, and assume that the POC concentration is constant this is becomes the same equation as the one used by kinzelbachNumericalModelingNatural1991.

Literature values for the DOC saturation concentration range from 1.3e-4 to 8.3e-4 mol/L. A value in the middle (4e-4mol/L) corresponds to 1 mol/L of succinate.

Literature values for the release rate constant range from 7.8e-9 / s to 2.3e-5/s.

# References
