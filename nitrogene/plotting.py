import re
import holoviews as hv
import hvplot.xarray
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.patches as patches
import matplotlib.ticker as mticker
import seaborn as sns
import numpy as np
import xarray as xr
import pandas as pd
import pint_xarray


AVOGADRO_CONSTANT = 6.022e23  # Avogadro constant [molecules/mole]


color_palettes = {
    "uni_tuebingen": [
        "#326295",
        "#CC8A00",
        "#4A7729",
        "#BE3A34",
        "#A76389",
        "#816040",
        "#85B09A",
        "#A39382",
        "#E1B87F",
        "#69B3E7",
    ],
    # The following three color schemes are based on:
    # Tol, Paul (2018). Color schemes (Technical Note SRON/EPS/TN/09-002). SRON. https://personal.sron.nl/~pault
    "paul_tol_bright": [
        "#4477AA",
        "#EE6677",
        "#228833",
        "#CCBB44",
        "#66CCEE",
        "#AA3377",
        "#BBBBBB",
    ],
    "paul_tol_muted": [
        "#332288",
        "#CC6677",
        "#117733",
        "#DDCC77",
        "#88CCEE",
        "#882255",
        "#44AA99",
        "#999933",
        "#AA4499",
        "#DDDDDD",
    ],
    "paul_tol_vibrant": [
        "#0077BB",
        "#EE7733",
        "#33BBEE",
        "#EE3377",
        "#CC3311",
        "#009988",
        "#BBBBBB",
    ],
    "paul_tol_high_contrast": ["#004488", "#BB5566", "#DDAA33"],
}
uni_tue_anthrazit = "#32414B"
uni_tue_light_grey = "#e7e6e7"
uni_tue_rc = {
    "axes.facecolor": uni_tue_light_grey,
    "text.color": uni_tue_anthrazit,
    "axes.labelcolor": uni_tue_anthrazit,
    "xtick.color": uni_tue_anthrazit,
    "ytick.color": uni_tue_anthrazit,
}
paper_rc = {
    "font.size": 8,
    "axes.titlesize": "medium",
    "axes.labelsize": "medium",
    "xtick.labelsize": "medium",
    "ytick.labelsize": "medium",
    "legend.fontsize": "medium",
    "legend.title_fontsize": "medium",
    "font.sans-serif": "Libertinus Sans",
    "font.serif": "Libertinus Serif",
    "mathtext.fontset": "custom",
    "mathtext.bf": "sans:bold",
    # "mathtext.cal": "cursive",
    "mathtext.it": "sans:italic",
    "mathtext.rm": "sans",
    "mathtext.sf": "sans",
    "mathtext.tt": "monospace",
    "mathtext.fallback": "stixsans",
    "axes.formatter.limits": (-3, 4),
    "axes.formatter.use_mathtext": True,
}


class MathTextSciFormatter(mticker.Formatter):
    """Format tick labels with exponent on each tick.

    Notes
    -----
    The source for this formatter comes from an answer on stackoverflow[1]_.

    References
    ----------
    [1] https://stackoverflow.com/a/49330649/10201982
    """
    def __init__(self, fmt="%1.2e"):
        self.fmt = fmt
    def __call__(self, x, pos=None):
        s = self.fmt % x
        decimal_point = '.'
        positive_sign = '+'
        tup = s.split('e')
        significand = tup[0].rstrip(decimal_point)
        sign = tup[1][0].replace(positive_sign, '')
        exponent = tup[1][1:].lstrip('0')
        if exponent:
            exponent = '10^{%s%s}' % (sign, exponent)
        if significand and exponent:
            s =  r'%s{\times}%s' % (significand, exponent)
        else:
            s =  r'%s%s' % (significand, exponent)
        return "${}$".format(s)


def colored_line(x, y, z, *, ax=None, vmin=None, vmax=None, cmap=None, **kwargs):
    linewidth = kwargs.pop("linewidth", kwargs.pop("lw", 2))
    if ax is None:
        ax = plt.gca()
    points = np.array([x, y]).T.reshape(-1, 1, 2)
    # segments = np.concatenate([points[:-1], points[1:]], axis=1)
    segments = np.concatenate([points[:-2], points[1:-1], points[2:]], axis=1)
    if vmin is None:
        vmin = z.min()
    if vmax is None:
        vmax = z.max()
    norm = plt.Normalize(vmin, vmax)
    lc = mpl.collections.LineCollection(segments, cmap=cmap, norm=norm, **kwargs)
    # Set the values used for colormapping
    lc.set_array(z)
    lc.set_linewidth(linewidth)
    line = ax.add_collection(lc)
    ax.autoscale_view()
    return line


def colored_line_xarray(
    ds,
    x,
    y,
    hue,
    *,
    label=None,
    hue_style=None,
    ax=None,
    vmin=None,
    vmax=None,
    cmap=None,
    extend=None,
    levels=None,
    norm=None,
    add_guide=False,
    _is_facetgrid=None,
    meta_data=None,
    **kwargs,
):
    return colored_line(
        x=ds[x].values,
        y=ds[y].values,
        z=ds[hue].values,
        ax=ax,
        vmin=vmin,
        vmax=vmax,
        cmap=cmap,
        **kwargs,
    )


def multiple_colored_lines_xarray(
    ds, x, y, hue, *, cmap=None, vmin=None, vmax=None, norm=None, **kwargs
):
    remaining_dims = list(ds.dims)
    remaining_dims.remove(hue)
    if remaining_dims:
        flat_ds = ds.stack(line=remaining_dims)
        lines = []
        if vmin is None:
            vmin = flat_ds[hue].min()
        if vmax is None:
            vmax = flat_ds[hue].max()
        norm = mpl.colors.Normalize(vmin=vmin, vmax=vmax)
        mappable = mpl.cm.ScalarMappable(norm=norm, cmap=cmap)
        for i in range(flat_ds.sizes["line"]):
            line = colored_line_xarray(
                flat_ds.isel(line=i), x, y, hue, norm=norm, cmap=cmap, **kwargs
            )
            lines.append(line)
        return mappable
    else:
        return colored_line_xarray(ds, x, y, hue, **kwargs)


def inv_boxcox(x, lmbda):
    if lmbda == 0:
        return np.exp(x)
    else:
        return (x * lmbda + 1) ** (1 / lmbda)


def arrange_rate_data(rates, states, time_resolution="dense"):
    if time_resolution == "dense":
        base_name = "c_dense"
    else:
        base_name = "c"
    rates = rates / states[f"{base_name}_bio_alive"]

    # Arange rate data
    substrate_to_gene_mapping = {
        "o2": None,
        "no3": "nar",
        "no2": "nir",
        #     "no": "nor",
        #     "n2o": "nos"
    }
    idx = rates.rate.to_index()
    genes = idx.map(substrate_to_gene_mapping)

    return (
        rates.assign_coords(gene=genes)
        .swap_dims({"rate": "gene"})
        .rename("turnover_rate")
    )


def arrange_concentration_data(states):
    gene_to_substrate_mapping = {
        "nar": "no3",
        "nir": "no2",
    }

    # Arrange mRNA data
    var_names = [var for var in list(states) if "c_dense_mrna_norm_" in var]
    genes = [var.rsplit("_", 1)[-1] for var in var_names]
    c_mrna = (
        xr.concat([states[var] for var in var_names], dim=pd.Index(genes, name="gene"))
        # .transpose()
        .rename("c_mrna_norm")
    )

    # Arrange enzyme data
    var_names = [var for var in list(states) if "c_dense_enz_" in var]
    genes = [var.rsplit("_", 1)[-1] for var in var_names]
    c_enz = (
        xr.concat(
            [states[var] / states.c_dense_bio_alive * 6.2e23 for var in var_names],
            dim=pd.Index(genes, name="gene"),
        )
        # .transpose()
        .rename("c_enz_norm")
    )

    # Compute rmax
    k_max = xr.concat(
        [
            np.exp(states[f"kmax_{gene_to_substrate_mapping[gene]}_log"])
            for gene in genes
        ],
        dim=pd.Index(genes, name="gene"),
    )
    r_max = c_enz * k_max / 6.2e23

    concentrations = xr.concat(
        [c_mrna, c_enz, r_max],
        dim=pd.Index(["mRNA", "enzymes", "maximum rate"], name="variable"),
    ).rename("concentration")

    return concentrations


def arrange_rates_and_concentrations(rates, states):
    e_acceptor_consumption_rates = arrange_rate_data(rates, states)
    concentrations = arrange_concentration_data(states)
    return xr.merge(
        [concentrations, e_acceptor_consumption_rates.sel(gene=concentrations.gene)]
    )


def arrange_experimental_data_and_monod_rates(monod_rates, monod_states, data):
    # arrange rates from the Monod model
    monod_rates_array = arrange_rate_data(
        monod_rates, monod_states, time_resolution="coarse"
    )

    # arrange experimental mRNA data
    gene_names = {"nirS": "nir", "narG": "nar"}
    genes_data = ["narG", "nirS"]
    genes = [gene_names[var] for var in genes_data]
    c_mrna = (
        xr.concat(
            [data[var].rename() for var in genes_data], dim=pd.Index(genes, name="gene")
        )
        .transpose()
        .rename("concentration")
        .assign_coords(variable="mrna")
    )
    # Merge mRNA and rate data
    return xr.merge([c_mrna, monod_rates_array.mean(["chain", "draw"])])


def plot_rate_correlations(
    rates,
    states,
    monod_rates,
    monod_states,
    data,
    n_times=50,
    data_kws=None,
    plot_style="lines",
    figsize=None,
    **plot_kws,
):
    rates_ds = arrange_rates_and_concentrations(rates, states)
    monod_ds = arrange_experimental_data_and_monod_rates(
        monod_rates, monod_states, data
    )

    if plot_style == "scatter":
        # Plot scatter points
        time_selector = slice(None, None, rates_ds.sizes["time_dense"] // n_times)
        g = rates_ds.sel(time_dense=time_selector).plot.scatter(
            x="concentration",
            y="turnover_rate",
            hue="time_dense",
            col="variable",
            row="gene",
            zorder=3,
            marker=".",
            sharex="none",
            sharey="row",
            figsize=figsize,
            **plot_kws,
        )

        # Plot the connecting lines
        for var, ax_row in zip(rates_ds.variable.values, g.axes.T):
            for gene, ax in zip(rates_ds.gene.values, ax_row):
                rates_ds.sel(gene=gene, variable=var, drop=True).stack(
                    sample=("chain", "draw")
                ).set_coords("concentration")["turnover_rate"].plot.line(
                    x="concentration",
                    alpha=0.5,
                    add_legend=False,
                    ax=ax,
                    # hue="sample",
                    color="grey",
                    _labels=False,
                )
    elif plot_style == "lines":
        g = xr.plot.FacetGrid(
            rates_ds,
            row="gene",
            col="variable",
            sharex="none",
            sharey="row",
            figsize=figsize,
        )
        kws = {
            "vmin": None,
            "vmax": None,
            "cmap": "crest",
            "extend": "neither",
            "levels": None,
            "norm": None,
        }
        if plot_kws is None:
            plot_kws = {}
        plot_kws = {**kws, **plot_kws}
        g.map_dataset(
            multiple_colored_lines_xarray,
            x="concentration",
            y="turnover_rate",
            hue="time_dense",
            # add_guide=False,
            **plot_kws,
        )
    else:
        assert False

    # Order axes into a dict for easier access
    axes_dict = {
        (variable, gene): ax
        for gene, ax_row in zip(g.row_names, g.axes)
        for variable, ax in zip(g.col_names, ax_row)
    }

    # Plot the data
    default_data_kws = {
        "marker": "*",
        "color": "C1",
    }
    if data_kws is None:
        data_kws = {}
    data_kws = {**default_data_kws, **data_kws}
    for gene, ax in zip(g.row_names, g.axes[:, 0]):
        monod_ds.sel(gene=gene).mean("replicate").dropna("time").set_coords(
            "concentration"
        )["turnover_rate"].plot.line(
            x="concentration", ax=ax, _labels=False, **data_kws
        )

    # Set the axis labels
    axes_dict[("mRNA", "nir")].set_xlabel("$T$ [transcripts/cell]")
    axes_dict[("enzymes", "nir")].set_xlabel("$E$ [enzymes/cell]")
    axes_dict[("maximum rate", "nir")].set_xlabel("$r_{\mathrm{max}}$ [mol/cell/s]")
    g.set_ylabels("rate [mol/cell/s]")
    g.fig.align_labels(g.axes)
    g.cbar.set_label("time [h]")

    # Manually enable sharing the x-axis for mRNA and enzyme concentrations
    g.axes[1, 0].get_shared_x_axes().join(g.axes[1, 0], g.axes[0, 0])
    g.axes[1, 1].get_shared_x_axes().join(g.axes[1, 1], g.axes[0, 1])

    # Add arrows
    style = "Simple, tail_width=0.2, head_width=2, head_length=3"
    arrow_kw = dict(arrowstyle=style, color="0.25", zorder=3)
    arrow_data = {
        ("mRNA", "nar"): [
            ((0.01, 2e-19), (0.02, 6e-19), "arc3,rad=.2"),
            ((0.038, 6e-19), (0.038, 2e-19), "arc3,rad=.0"),
            ((0.03, -1e-19), (0.01, -1e-19), "arc3,rad=.0"),
        ],
        ("mRNA", "nir"): [
            ((0.025, 3e-20), (0.021, 7.5e-20), "arc3,rad=.1"),
            ((0.003, 7.5e-20), (0.003, 3.5e-20), "arc3,rad=.0"),
            ((0.005, -1e-20), (0.02, -1e-20), "arc3,rad=.0"),
        ],
        ("maximum rate", "nar"): [
            ((2e-18, 2e-19), (3e-18, 5e-19), "arc3,rad=.1"),
            ((6e-18, 6e-19), (6e-18, 2e-19), "arc3,rad=.0"),
            ((7e-18, 1e-19), (12e-18, 1e-19), "arc3,rad=.0"),
            ((1e-17, -1e-19), (2e-18, -1e-19), "arc3,rad=.0"),
        ],
        ("maximum rate", "nir"): [
            ((1e-20, 2e-20), (5e-20, 7e-20), "arc3,rad=-.1"),
            ((7.3e-20, 6e-20), (7.3e-20, 1e-20), "arc3,rad=.0"),
            ((7e-20, -1e-20), (2e-20, -1e-20), "arc3,rad=.0"),
        ],
    }
    arrows = {
        panel: [
            mpl.patches.FancyArrowPatch(
                start_point, end_point, **arrow_kw, connectionstyle=style
            )
            for (start_point, end_point, style) in arrow_list
        ]
        for panel, arrow_list in arrow_data.items()
    }
    for panel, arrow_list in arrows.items():
        for arrow in arrow_list:
            axes_dict[panel].add_patch(arrow)

    g.axes[0, 0].set_ylim(bottom=-1.5e-19, top=8e-19)
    g.axes[1, 0].set_ylim(bottom=-2e-20, top=0.95e-19)
    g.axes[0, 0].set_xlim(right=4.5e-2)
    g.axes[0, 1].set_xlim(right=270)
    g.axes[0, 2].set_xlim(left=-1e-18, right=2.2e-17)
    g.axes[1, 2].set_xlim(left=-0.5e-20, right=1e-19)

    return g, rates_ds


def round_x_max(x_max):
    order_of_magnitude = np.floor(np.log10(x_max))
    scaling_factor = 10 ** (order_of_magnitude)
    return np.floor(x_max / scaling_factor) * scaling_factor


def compute_thinning_factor(tr, n_lines):
    n_draws = len(tr.draw)
    n_chains = len(tr.chain)
    thin = n_draws * n_chains // n_lines
    thin = np.maximum(thin, 1)
    return {"sample": slice(None, None, thin)}


def plot_quantiles(dataarray, dims, ax, quantiles=[0.05, 0.95], **plt_kwargs):
    a = dataarray.quantile(quantiles, dim=dims)
    lines_median = dataarray.median(dim=dims).plot.line(
        x="time_dense", ax=ax, **plt_kwargs
    )
    line_quantiles = ax.fill_between(
        x=a.time_dense,
        y1=a.sel(quantile=quantiles[1]),
        y2=a.sel(quantile=quantiles[0]),
        alpha=0.3,
        **plt_kwargs,
    )
    return lines_median[0], line_quantiles


def make_legend(leg_data, ax, **leg_kwargs):
    leg = ax.legend(tuple(leg_data.values()), tuple(leg_data.keys()), **leg_kwargs)
    # Make lines in the legend thicker than in the plot
    leg = adjust_linewidth(leg)
    return leg


def adjust_linewidth(leg):
    for line in leg.get_lines():
        line.set_linewidth(plt.rcParams["lines.linewidth"])
        line.set_alpha(1.0)
    return leg


def create_leg_data_for_model_comparison(leg_data_with_genes, leg_data_monod):
    leg_data = {
        **leg_data_with_genes,
        "Monod\nmodel": next(iter(leg_data_monod.values())),
    }
    return leg_data


def make_color_mapping(color_map_name, uniform_color="#32414B"):
    """Map subtances to colors.

    Parameters:
    -----------
    color_map_name: str
        Should be one of {"bright", "muted"}

    Returns:
    -------
    color_mapping: dict
    """
    paul_tol_bright_colors = {
        "blue": "#4477AA",
        "red": "#EE6677",
        "green": "#228833",
        "cyan": "#66CCEE",
        "yellow": "#CCBB44",
        "purple": "#AA3377",
        "grey": "#BBBBBB",
    }

    paul_tol_muted_colors = {
        "blue": "#332288",
        "red": "#CC6677",
        "green": "#117733",
        "cyan": "#88CCEE",
        "yellow": "#DDCC77",
        "purple": "#882255",
        "grey": "#555555",
    }
    paul_tol_vibrant_colors = {
        "blue": "#0077BB",
        "red": "#CC3311",
        "green": "#009988",
        "cyan": "#33BBEE",
        "yellow": "#EE7733",
        "purple": "#EE3377",
        "grey": "#BBBBBB",
    }
    color_name_mapping = {
        "no3": "red",
        "no2": "blue",
        "n2": "yellow",
        "o2": "green",
        "cells": "purple",
        "rmax_nir": "grey",
    }
    if color_map_name == "uniform":
        color_mapping = {key: uniform_color for key in color_name_mapping}
    else:
        color_maps = {
            "bright": paul_tol_bright_colors,
            "muted": paul_tol_muted_colors,
            "vibrant": paul_tol_vibrant_colors,
        }
        color_mapping = {
            variable: color_maps[color_map_name][color_name]
            for variable, color_name in color_name_mapping.items()
        }
    return color_mapping


class ModelPlotter:
    def __init__(self, tr, model_type, n_lines, color_mapping):
        self.model_type = model_type
        self.is_single_realization = "draw" not in tr.dims
        self.n_lines = 1 if self.is_single_realization else n_lines
        if self.is_single_realization:
            self.plot_kwargs = {}
            self.selector = None
            self.tr = tr
        else:
            self.selector = compute_thinning_factor(tr, self.n_lines)
            self.tr = tr.stack(sample=("chain", "draw"))
            linewidth = 0.8 * plt.rcParams["lines.linewidth"]
            self.plot_kwargs = {
                "alpha": 0.05,
                "linewidth": linewidth,
            }
            if self.model_type == "gene_based":
                self.enz_plot_kwargs = {
                    "alpha": 0.15,
                    "linewidth": linewidth,
                }
        self.color_mapping = color_mapping

    def plot_nitrogen_concentrations(self, ax, ax_n2, **plot_kwargs):
        # Plot the nitrogen concentrations
        c_no3 = self.tr.c_dense_nitrogen_no3.sel(self.selector)
        l_no3, *_ = c_no3.plot.line(
            x="time_dense",
            add_legend=False,
            ax=ax,
            color=self.color_mapping["no3"],
            **self.plot_kwargs,
            **plot_kwargs,
        )
        c_no2 = self.tr.c_dense_nitrogen_no2.sel(self.selector)
        l_no2, *_ = c_no2.plot.line(
            x="time_dense",
            add_legend=False,
            ax=ax,
            color=self.color_mapping["no2"],
            **self.plot_kwargs,
            **plot_kwargs,
        )
        c_n2 = self.tr.c_dense_gas_n2.sel(self.selector)
        ax_n2.ticklabel_format(scilimits=(-2, 3))
        l_n2, *_ = c_n2.plot.line(
            x="time_dense",
            add_legend=False,
            ax=ax_n2,
            color=self.color_mapping["n2"],
            **self.plot_kwargs,
            **plot_kwargs,
        )
        ax_n2.set_ylim(
            top=ax.get_ylim()[1] * 8e-4 / 2e-3, bottom=ax.get_ylim()[0] * 8e-4 / 2e-3
        )
        ax_n2.grid(False)
        ax_n2.yaxis.set_ticks_position("none")
        ax.yaxis.set_ticks_position("none")
        # Create a legend from the existing lines
        leg_data = {"nitrate": l_no3, "nitrite": l_no2, "$\mathregular{N_2}$ gas": l_n2}
        return leg_data

    def plot_o2_and_cells(self, ax, ax_cells, **plot_kwargs):
        c_o2 = self.tr.c_dense_gas_o2.sel(self.selector)
        l_o2, *_ = c_o2.plot.line(
            x="time_dense",
            add_legend=False,
            ax=ax,
            color=self.color_mapping["o2"],
            **self.plot_kwargs,
            **plot_kwargs,
        )
        ax_cells.ticklabel_format(scilimits=(-2, 3))
        c_cells = self.tr.c_dense_bio_alive.sel(self.selector)
        l_cells, *_ = c_cells.plot.line(
            x="time_dense",
            add_legend=False,
            ax=ax_cells,
            color=self.color_mapping["cells"],
            **self.plot_kwargs,
            **plot_kwargs,
        )
        ax_cells.set_ylim(
            top=ax.get_ylim()[1] * 6e11 / 3e-3, bottom=ax.get_ylim()[0] * 6e11 / 3e-3
        )
        ax_cells.grid(False)
        ax_cells.yaxis.set_ticks_position("none")
        ax.yaxis.set_ticks_position("none")
        # Store legend information
        leg_data = {"$\mathregular{O_2}$ gas": l_o2, "cells": l_cells}
        return leg_data

    def plot_mrna_concentrations(self, genes, ax, **plot_kwargs):
        # Plot the mRNA concentrations
        gene_to_subtrate_mapping = {
            "nar": "no3",
            "nir": "no2",
            "nor": "no",
            "nos": "n2o",
        }
        name_in_data = {"nar": "narG", "nir": "nirS", "nor": "norB", "nos": "nosZ"}
        lines = {}
        for gene in genes:
            var_names = self.tr.data_vars.keys()
            if f"c_dense_mrna_norm_{gene}" in var_names:
                c_mrna = self.tr[f"c_dense_mrna_norm_{gene}"]
            elif f"c_dense_mrna_{gene}_qss" in var_names:
                c_mrna = self.tr[f"c_dense_mrna_{gene}_qss"]
                c_mrna = (c_mrna / self.tr.c_dense_bio_alive).sel(self.selector)
            elif f"c_dense_mrna_{gene}" in var_names:
                c_mrna = self.tr[f"c_dense_mrna_{gene}"]
                c_mrna = (c_mrna / self.tr.c_dense_bio_alive).sel(self.selector)
            else:
                raise ValueError(f"No mRNA data for {gene} available.")
            color = self.color_mapping[gene_to_subtrate_mapping[gene]]
            l_mrna, *_ = c_mrna.plot.line(
                x="time_dense",
                add_legend=False,
                ax=ax,
                color=color,
                **self.plot_kwargs,
                **plot_kwargs,
            )
            lines[gene] = l_mrna
        ax.set_ylabel("mRNA [transcripts/cell]")
        leg_data = {name_in_data[key]: line for key, line in lines.items()}
        return leg_data

    def plot_enzymes(self, genes, ax, quantiles=False, **plot_kwargs):
        gene_to_subtrate_mapping = {
            "nar": "no3",
            "nir": "no2",
            "nor": "no",
            "nos": "n2o",
        }
        lines_enz = {}
        lines_rmax = {}
        if not self.is_single_realization:
            ax_rmax = ax.twinx()
        for gene in genes:
            c_enz = self.tr[f"c_dense_enz_{gene}"].sel(self.selector)
            color = self.color_mapping[gene_to_subtrate_mapping[gene]]
            if quantiles:
                l_enz, _ = plot_quantiles(c_enz, dims="sample", ax=ax, color=color)
            else:
                l_enz, *_ = c_enz.plot.line(
                    x="time_dense",
                    add_legend=False,
                    ax=ax,
                    color=color,
                    **self.enz_plot_kwargs,
                    **plot_kwargs,
                )
            lines_enz[gene] = l_enz
            if not self.is_single_realization:
                # Plot maximum rate
                if gene == "nir":
                    k_max = np.exp(
                        self.tr[f"kmax_{gene_to_subtrate_mapping[gene]}_log"]
                    ).sel(self.selector)
                    r_max = c_enz * k_max
                    if quantiles:
                        l_rmax, _ = plot_quantiles(
                            r_max,
                            dims="sample",
                            ax=ax_rmax,
                            color=self.color_mapping["rmax_nir"],
                        )
                    else:
                        l_rmax, *_ = r_max.plot.line(
                            x="time_dense",
                            add_legend=False,
                            ax=ax_rmax,
                            color=self.color_mapping["rmax_nir"],
                            **self.enz_plot_kwargs,
                            **plot_kwargs,
                        )
                    lines_rmax[gene] = l_rmax
        ax.set_xlabel("time [h]")
        ax.set_ylabel("enzymes [mol/L]")
        if not self.is_single_realization:
            ax_rmax.set_ylabel("max. rate [mol/L/s]")
            x_min_l, x_max_l = ax.get_ylim()
            x_min_r, x_max_r = ax_rmax.get_ylim()
            left_tick = round_x_max(x_max_l)
            right_tick = round_x_max(x_max_r)
            ax_rmax.set_ylim(
                top=x_max_l * right_tick / left_tick,
                bottom=x_min_l * right_tick / left_tick,
            )
            ax_rmax.set_yticks(ax.get_yticks()[1:-1] * right_tick / left_tick)
            ax_rmax.grid(False)
            ax_rmax.yaxis.set_ticks_position("none")
            ax.yaxis.set_ticks_position("none")
        leg_data_enz = {key.upper(): line for key, line in lines_enz.items()}
        leg_data_rmax = {
            f"$r_{{max}}$ {key.upper()}": line for key, line in lines_rmax.items()
        }
        return {**leg_data_enz, **leg_data_rmax}


class DataPlotter:
    def __init__(self, ds, color_mapping, marker_mapping=None):
        self.ds = ds
        marker_size = plt.rcParams["lines.markersize"]
        marker_kwargs = {
            "markerfacecolor": plt.rcParams["axes.facecolor"],
            "markersize": 0.9 * marker_size,
            "markeredgewidth": 0.7 * plt.rcParams["lines.linewidth"],
        }
        if marker_mapping is None:
            marker_mapping = {
                "no3": "<",
                "no2": "o",
                "n2": "v",
                "nar": "<",
                "nir": "o",
                "o2": "^",
                "cells": "p",
            }

        self.marker_kwargs = marker_kwargs
        self.color_mapping = color_mapping
        self.marker_mapping = marker_mapping

    def plot_mrna_data(self, genes, ax):
        gene_to_subtrate_mapping = {
            "nar": "no3",
            "nir": "no2",
            "nor": "no",
            "nos": "n2o",
        }
        name_in_data = {"nar": "narG", "nir": "nirS", "nor": "norB", "nos": "nosZ"}
        for gene in genes:
            color = self.color_mapping[gene_to_subtrate_mapping[gene]]
            self.ds[name_in_data[gene]].dropna(dim="time", how="all").plot.line(
                x="time",
                color=color,
                linewidth=0,
                add_legend=False,
                ax=ax,
                zorder=3,
                marker=self.marker_mapping[gene],
                **self.marker_kwargs,
            )
        ax.set_ylabel("mRNA [transcripts/cell]")

    def plot_nitrogen_data(self, ax, ax_n2):
        self.ds["NO2-"].dropna(dim="time").plot.line(
            x="time",
            color=self.color_mapping["no2"],
            linewidth=0,
            add_legend=False,
            ax=ax,
            zorder=0,
            marker=self.marker_mapping["no2"],
            **self.marker_kwargs,
        )
        self.ds["N2"].dropna(dim="time").plot.line(
            x="time",
            color=self.color_mapping["n2"],
            linewidth=0,
            add_legend=False,
            ax=ax_n2,
            zorder=0,
            marker=self.marker_mapping["n2"],
            **self.marker_kwargs,
        )
        ax.set_ylabel("solutes [mol/L]")
        ax_n2.set_ylabel("$\mathregular{N_2}$ gas [mol/L]")
        ax.xaxis.label.set_visible(False)
        ax_n2.xaxis.label.set_visible(False)

    def plot_o2_and_cell_data(self, ax, ax_cells):
        self.ds["O2"].dropna(dim="time").plot.line(
            x="time",
            color=self.color_mapping["o2"],
            linewidth=0,
            add_legend=False,
            ax=ax,
            zorder=0,
            marker=self.marker_mapping["o2"],
            **self.marker_kwargs,
        )
        self.ds["cells"].dropna(dim="time").plot.line(
            x="time",
            color=self.color_mapping["cells"],
            linewidth=0,
            add_legend=False,
            ax=ax_cells,
            zorder=0,
            marker=self.marker_mapping["cells"],
            **self.marker_kwargs,
        )
        ax.set_ylabel("$\mathregular{O_2}$ gas [mol/L]")
        ax.xaxis.label.set_visible(False)
        ax_cells.xaxis.label.set_visible(False)


def plot_two_step(
    tr,
    ds,
    classical_monod=True,
    genes=[],
    mrna_at_quasi_steady_state=False,
    n_lines=300,
    color_map="bright",
    dark_palette=None,
    figsize=None,
    quantiles=False,
):
    color_mapping = make_color_mapping(color_map)
    model_type = "monod" if classical_monod else "gene_based"

    model_plotter = ModelPlotter(
        tr=tr, model_type=model_type, n_lines=n_lines, color_mapping=color_mapping
    )
    data_plotter = DataPlotter(ds, color_mapping)

    n_rows = 1 if model_plotter.model_type == "monod" else 2
    if figsize == None:
        figsize = (15, 4 * n_rows)

    fig, axes = plt.subplots(ncols=2, nrows=n_rows, figsize=figsize, sharex=True)
    for ax in axes.flat:
        ax.ticklabel_format(scilimits=(-2, 3))

    flat_ax = axes.flat

    # Plot the nitrogen concentrations
    ax_n2 = plt.twinx(ax=flat_ax[0])
    leg_data = model_plotter.plot_nitrogen_concentrations(flat_ax[0], ax_n2)
    data_plotter.plot_nitrogen_data(flat_ax[0], ax_n2)
    leg = make_legend(leg_data, flat_ax[0], loc="center left", handlelength=2, fontsize=8, bbox_to_anchor=(-0.05, 0, 0.4, 1))

    # Plot the O2 concentration
    ax_cells = flat_ax[1].twinx()
    leg_data = model_plotter.plot_o2_and_cells(flat_ax[1], ax_cells)
    data_plotter.plot_o2_and_cell_data(flat_ax[1], ax_cells)
    leg = make_legend(leg_data, flat_ax[1], loc="center right", handlelength=2, fontsize=8)

    if model_plotter.model_type == "gene_based":
        # Plot the mRNA concentrations
        leg_data = model_plotter.plot_mrna_concentrations(genes, flat_ax[2])
        data_plotter.plot_mrna_data(genes, flat_ax[2])
        leg = make_legend(
            leg_data, flat_ax[2], loc="upper right", prop={"style": "italic"}, handlelength=2, fontsize=8
        )

        # Plot the enzyme concentrations
        leg_data = model_plotter.plot_enzymes(genes, flat_ax[3], quantiles=quantiles)
        leg = make_legend(leg_data, flat_ax[3], loc="upper left", handlelength=2, fontsize=8)
    return fig, axes


def plot_two_step_model_comparison(
    tr_genes,
    tr_monod,
    ds,
    genes=[],
    mrna_at_quasi_steady_state=False,
    n_lines_genes=50,
    n_lines_monod=20,
    color_map="bright",
    monod_model_color="#32414B",
    figsize=None,
    quantiles=False,
):
    color_mapping = make_color_mapping(color_map)
    color_mapping_monod = make_color_mapping("uniform", monod_model_color)

    plotter_monod = ModelPlotter(
        tr=tr_monod,
        model_type="monod",
        n_lines=n_lines_monod,
        color_mapping=color_mapping_monod,
    )
    plotter_genes = ModelPlotter(
        tr=tr_genes,
        model_type="gene_based",
        n_lines=n_lines_genes,
        color_mapping=color_mapping,
    )
    data_plotter = DataPlotter(ds, color_mapping)

    n_rows = 2
    if figsize == None:
        figsize = (15, 4 * n_rows)

    fig, axes = plt.subplots(ncols=2, nrows=n_rows, figsize=figsize, sharex=True)
    for ax in axes.flat:
        ax.ticklabel_format(scilimits=(-2, 3))

    flat_ax = axes.flat

    # Plot the nitrogen concentrations
    ax_n2 = plt.twinx(ax=flat_ax[0])
    leg_data = plotter_genes.plot_nitrogen_concentrations(flat_ax[0], ax_n2)
    leg_data_monod = plotter_monod.plot_nitrogen_concentrations(
        flat_ax[0], ax_n2, linestyle="--", dashes=(3, 5)
    )
    data_plotter.plot_nitrogen_data(flat_ax[0], ax_n2)
    leg_data = create_leg_data_for_model_comparison(leg_data, leg_data_monod)
    leg = make_legend(leg_data, flat_ax[0], loc="center left", handlelength=2, fontsize=8, bbox_to_anchor=(-0.05, 0, 0.4, 1))

    # Plot the O2 concentration
    ax_cells = flat_ax[1].twinx()
    leg_data = plotter_genes.plot_o2_and_cells(flat_ax[1], ax_cells)
    leg_data_monod = plotter_monod.plot_o2_and_cells(
        flat_ax[1], ax_cells, linestyle="--", dashes=(3, 5)
    )
    data_plotter.plot_o2_and_cell_data(flat_ax[1], ax_cells)
    leg_data = create_leg_data_for_model_comparison(leg_data, leg_data_monod)
    leg = make_legend(leg_data, flat_ax[1], loc="center right", handlelength=2, fontsize=8)

    # Plot the mRNA concentrations
    leg_data = plotter_genes.plot_mrna_concentrations(genes, flat_ax[2])
    data_plotter.plot_mrna_data(genes, flat_ax[2])
    leg = make_legend(leg_data, flat_ax[2], loc="upper right", prop={"style": "italic"}, handlelength=2, fontsize=8)

    # Plot the enzyme concentrations
    leg_data = plotter_genes.plot_enzymes(genes, flat_ax[3], quantiles=quantiles)
    leg = make_legend(leg_data, flat_ax[3], loc="upper left", handlelength=2, fontsize=8)
    return fig, axes


def make_qss_array(c_qss, tr, *, var, normalization="biomass"):
    var_base_name = f"c_dense_{var}"
    var_names = [var for var in tr if var_base_name in var and "norm" not in var]
    genes = [var.rsplit("_", 1)[-1] for var in var_names]
    c_transient = xr.concat(
        [tr[var] for var in var_names], dim=pd.Index(genes, name="gene")
    ).rename(f"{var}_transient")

    if normalization == "biomass":
        c_qss_norm = c_qss.copy() / tr.c_dense_bio_alive
        c_transient_norm = c_transient.copy() / tr.c_dense_bio_alive
    elif normalization == "none":
        c_transient_norm = c_transient.copy()
        c_qss_norm = c_qss.copy()
    else:
        raise (ValueError(f"Invalid normalization method: {normalization}"))

    if var == "enz":
        c_transient_norm *= AVOGADRO_CONSTANT
        c_qss_norm *= AVOGADRO_CONSTANT

    return xr.concat(
        [c_qss_norm, c_transient_norm],
        dim=pd.Index(["quasi-steady state", "transient"], name=f"{var}_model"),
    )


def plot_qss(
    c_qss,
    tr,
    *,
    var,
    normalization="biomass",
    n_samples=10,
    time_thinning=5,
    figsize=(5, 2.5),
    scatter_size=10,
    colormap="cividis",
    subplot_kws=None,
    sharex=True,
    sharey=True,
    faceting="col",
    x_var="transient",
    y_var="quasi-steady state",
    cbar_kws=None,
    plot_style="line",
):
    qss_array = make_qss_array(c_qss, tr, var=var, normalization=normalization)
    qss_ds = qss_array.to_dataset(dim=f"{var}_model")
    long_var_names = {
        "enz": "enzyme",
        "mrna": "transcript",
    }
    qss_ds = qss_ds.assign_coords(
        enzyme=qss_ds.gene.str.upper(),
        transcript=("$\mathit{" + qss_ds.gene + "}$"),
    ).swap_dims({"gene": long_var_names[var]})

    thinning_factor = compute_thinning_factor(tr, n_samples)
    facet_kw = {faceting: long_var_names[var]}
    if plot_style == "scatter":
        g = (
            qss_ds.stack(sample=("chain", "draw"))
            .sel(**thinning_factor, time_dense=slice(0, None, time_thinning))
            .plot.scatter(
                x=x_var,
                y=y_var,
                **facet_kw,
                hue="time_dense",
                cmap=colormap,
                s=scatter_size,
                add_guide=False,
                alpha=0.6,
                figsize=figsize,
                subplot_kws=subplot_kws,
                sharex=sharex,
                sharey=sharey,
            )
        )
    elif plot_style == "line":
        g = xr.plot.FacetGrid(
            qss_ds.stack(sample=("chain", "draw"))
            .sel(**thinning_factor, time_dense=slice(0, None, time_thinning))
            .unstack("sample"),
            **facet_kw,
            figsize=figsize,
            sharex=sharex,
            sharey=sharey,
        )
        kws = {
            "vmin": None,
            "vmax": None,
            "cmap": colormap,
            "extend": "neither",
            "levels": None,
            "norm": None,
        }
        g.map_dataset(
            multiple_colored_lines_xarray,
            x="quasi-steady state",
            y="transient",
            hue="time_dense",
            # hue_style="continuous",
            add_guide=False,
            **kws,
        )
    else:
        assert False

    if cbar_kws is None:
        cbar_kws = {}
    g.add_colorbar(label="time [h]", **cbar_kws)
    return g, qss_ds


def plot_mrna_enz_qss(
    c_qss_mrna,
    c_qss_enz,
    tr,
    *,
    normalization=None,
    n_samples=10,
    time_thinning=5,
    figsize=(5, 3.2),
    scatter_size=10,
    colormap="cividis",
):
    if normalization is None:
        normalization = {"mrna": "none", "enz": "none"}
    mrna_array = make_qss_array(
        c_qss_mrna, tr, var="mrna", normalization=normalization["mrna"]
    )
    enz_array = make_qss_array(
        c_qss_enz, tr, var="enz", normalization=normalization["enz"]
    )

    # Join mRNA and enzyme data for generating a single plot
    da_mrna_enz = [
        (mrna_array).rename({"mrna_model": "model"}),
        enz_array.rename({"enz_model": "model"}),
    ]
    da_mrna_enz = xr.concat(
        da_mrna_enz, dim=pd.Index(["mRNA", "enzymes"], name="molecule_type")
    )
    ds_mrna_enz = da_mrna_enz.to_dataset(dim="model")
    thinning_factor = compute_thinning_factor(tr, n_samples)
    selection = ds_mrna_enz.stack(sample=("chain", "draw")).sel(
        **thinning_factor, gene=["nar", "nir"], time_dense=slice(0, None, time_thinning)
    )
    g = selection.plot.scatter(
        x="transient",
        y="quasi-steady state",
        col="gene",
        row="molecule_type",
        sharex="row",
        sharey="row",
        add_guide=False,
        hue="time_dense",
        s=scatter_size,
        alpha=0.8,
        figsize=figsize,
        cmap=colormap,
    )
    for a in g.axes.flat:
        m = np.maximum(a.get_ylim()[1], a.get_xlim()[1])
        a.set_xlim(right=m)
        a.set_ylim(top=m)
        a.tick_params(axis="both", which="major", pad=-1)
    sns.despine(left=True, bottom=True)
    plt.subplots_adjust(wspace=0.15, hspace=0.45)
    g.set_titles(template="{coord}={value}")
    for label in g.row_labels:
        label.set_text(label.get_text().split("=")[-1])
    g.add_colorbar(label="time [h]", shrink=0.8, pad=0.08)
    handles, labels = g.axes[0, 0].collections[0].legend_elements(num=4)
    for h in handles:
        h.set_markersize(5)
    h = tuple(handles)
    g.axes[1, 0].legend(
        handles=[h, h, h],
        numpoints=1,
        handlelength=2.3,
        handleheight=0.8,
        labels=["", "10 draws\nfrom the\nposterior", ""],
        labelspacing=-1.1,
        handler_map={
            h: mpl.legend_handler.HandlerTuple(ndivide=None),
        },
    )
    g.fig.align_ylabels(g.axes[:, 0])
    return g


def concentration_subplot(ds, var_names, x_var, label, **plot_kwargs):
    if isinstance(var_names, str):
        max_val = ds[var_names].max().values
    elif isinstance(var_names, list):
        max_val = ds[var_names].to_array().max().values
    else:
        assert False
    return ds.hvplot.line(
        x=x_var,
        y=var_names,
        value_label=label,
        ylim=(0, max_val * 1.1),
        **plot_kwargs
    )


def concentration_distribution_animation(ds, *, x_var="x", plot_kwargs=None):
    if plot_kwargs is None:
        plot_kwargs = {}
    mrna_plot = concentration_subplot(
        ds=ds,
        var_names=["mrna_nar", "mrna_nir"],
        x_var=x_var,
        label="mRNA concentration",
        **plot_kwargs
    )
    enz_plot = concentration_subplot(
        ds=ds,
        var_names=["enz_nar", "enz_nir"],
        x_var=x_var,
        label="enzyme concentration",
        **plot_kwargs
    )
    nitrogen_plot = concentration_subplot(
        ds=ds,
        var_names=["nitrogen_no3", "nitrogen_no2", "nitrogen_n2"],
        x_var=x_var,
        label="concentration",
        **plot_kwargs
    )
    doc_plot = concentration_subplot(
        ds=ds, var_names="doc", x_var=x_var, label="DOC concentration", **plot_kwargs
    )
    oxygen_plot = concentration_subplot(
        ds=ds, var_names="o2", x_var=x_var, label="O2 concentration", **plot_kwargs
    )
    bio_plot = concentration_subplot(
        ds=ds,
        var_names=["bio_alive", "bio_dead"],
        x_var=x_var,
        label="cell density",
        **plot_kwargs
    )
    plot_all = (
        nitrogen_plot
        + doc_plot
        + oxygen_plot
        + bio_plot
        + mrna_plot
        + enz_plot
    ).cols(2)
    return plot_all


class PeriodicConstantPlot:
    """Plot the constant and/or periodic concentration distribution.


    Parameters:
    -----------
    periodic_solution: xarray Dataset
       Contains the periodic solution and must have a "time" dimension over which
       values will be aggregated.
    constant_solution: xarray Dataset
       Containst the constant solution.
    subplots_kw: dict
        Keyword arguments that will be passed to plt.subplots.
    add_fig_legend: Boolean
        Switch to enable/disable a figure-level legend that distinguishes periodic and
        constant solution. By default, it is only drawn if `periodic_solution` is given.
    subplot_legend: Boolean
        Add a legend indicating the plotted variable to each subplot.
    periodic_label: str
        Label to use for the periodic solution in the figure legend.
    constant_label: str
        Label to use for the constant solution in the figure legend.
    periodic_plot_kw: dict
        Keyword arguments passed to pot function in the periodic plot.
    constant_plot_kw: dict
        Keyword arguments passed to pot function in the constant plot.
    legend_kw: dict
        Keyword arguments for the figure legend.
    subpot_legend_kw: dict
        Keyword arguments for the subplot legend.
    subplot_legend_kw_dict: dict
        Dictionary tat has the group names as keys and a dict with subplot_legend_kw
        for each group individually as values.
    xvar: str
        Coordinate to plot on the xaxis.
    """

    def __init__(
        self,
        figure,
        axes_mapping,
        *,
        groups_to_plot,
        periodic_solution=None,
        constant_solution=None,
        add_fig_legend=None,
        subplot_legends=True,
        periodic_label="periodic",
        constant_label="constant",
        periodic_plot_kw=None,
        constant_plot_kw=None,
        legend_kw=None,
        subplot_legend_kw=None,
        subplot_legend_kw_dict=None,
        xvar="x",
    ):
        if periodic_solution is None and constant_solution is None:
            raise ValueError(
                "Either periodic_solution or constant_solution must be given."
            )
        self.periodic_solution = periodic_solution
        self.constant_solution = constant_solution
        self.fig = figure
        self.axes_mapping = axes_mapping
        if add_fig_legend is None:
            if periodic_solution is None:
                self.add_fig_legend = False
            else:
                self.add_fig_legend = True
        else:
            self.add_fig_legend = add_fig_legend

        self.subplot_legends = subplot_legends
        if subplot_legend_kw is None and subplot_legend_kw_dict is not None:
            pass
        elif subplot_legend_kw_dict is None and subplot_legend_kw is not None:
            pass
        elif subplot_legend_kw_dict is None and subplot_legend_kw is None:
            subplot_legend_kw = {}
        else:
            assert False
        self.subplot_legend_kw = subplot_legend_kw
        self.subplot_legend_kw_dict = subplot_legend_kw_dict

        self.periodic_label = periodic_label
        self.constant_label = constant_label
        self.xvar = xvar

        if periodic_plot_kw is None:
            periodic_plot_kw = {}
        self.periodic_plot_kw = periodic_plot_kw
        if constant_plot_kw is None:
            if self.periodic_solution is None:
                constant_plot_kw = dict(ls="-")
            else:
                constant_plot_kw = dict(ls="--")
        self.constant_plot_kw = constant_plot_kw

        self.groups_to_plot = groups_to_plot
        self.groups = self._make_groups()
        self.plot(legend_kw=legend_kw)

    def _make_groups(self):
        var_names = pd.Series(
            [
                "nitrogen_no3",
                "nitrogen_no2",
                "nitrogen_n2",
                "o2",
                "doc",
                "bio_alive",
                "bio_dead",
                "mrna_nar",
                "mrna_nir",
                "enz_nar",
                "enz_nir",
                "mrna_norm_nar",
                "mrna_norm_nir",
                "enz_norm_nar",
                "enz_norm_nir",
            ],
            name="variable",
        )
        var_labels = var_names.str.rsplit(pat="_", n=1, expand=True)
        var_labels.columns = ["group", "name"]
        var_names = var_labels.join(var_names)
        return var_names.loc[var_names["group"].isin(self.groups_to_plot)].groupby("group")

    @property
    def colors(self):
        return plt.rcParams["axes.prop_cycle"].by_key()["color"]

    def plot(self, legend_kw=None):
        for group, labels in self.groups:
            ax = self.axes_mapping[group]
            for i, (_, var_data) in enumerate(labels.iterrows()):
                self._subplot_periodic_and_constant(
                    var_data["variable"],
                    group,
                    color=self.colors[i % len(self.colors)],
                    ax=ax,
                )
        if self.add_fig_legend:
            self._make_legend(legend_kw=legend_kw)
        return

    def _make_legend(self, legend_kw=None):
        if legend_kw is None:
            legend_kw = {}
        handles = []
        labels = []
        if self.periodic_solution is not None:
            handles.append(self.axes_mapping["o2"].lines[0])
            labels.append(f"{self.periodic_label} (mean)")
            handles.append(self.axes_mapping["o2"].collections[0])
            labels.append(f"{self.periodic_label} (min and max)")
        if self.constant_solution is not None:
            handles.append(self.axes_mapping["o2"].lines[-1])
            labels.append(self.constant_label)
        default_legend_kw = dict(ncol=len(labels), loc="upper center")
        legend_kw = {**default_legend_kw, **legend_kw}
        self.fig.legend(handles, labels, **legend_kw)

    def _subplot_periodic(self, var, color, ax):
        assert self.periodic_solution is not None
        poly = ax.fill_between(
            self.periodic_solution[self.xvar],
            self.periodic_solution[var].min("time"),
            self.periodic_solution[var].max("time"),
            alpha=0.5,
            color=color,
        )
        label = self._get_var_label(self.periodic_solution[var])
        self.periodic_solution[var].mean("time").plot(
            x=self.xvar, label=label, ax=ax, color=color, **self.periodic_plot_kw
        )

    def _get_var_label(self, var):
        label_names = {
            "mrna_nar": "narG",
            "mrna_nir": "nirS",
            "enz_nar": "NAR",
            "enz_nir": "NIR",
            "mrna_norm_nar": "narG",
            "mrna_norm_nir": "nirS",
            "enz_norm_nar": "NAR",
            "enz_norm_nir": "NIR",
            "nitrogen_no3": r"${\mathrm{NO}_3}^-$",
            "nitrogen_no2": r"${\mathrm{NO}_2}^-$",
            "nitrogen_n2": r"$\mathrm{N}_2$",
            "bio_alive": "alive",
            "bio_dead": "dead",
            "o2": None, #"$\mathrm{O}_2$",
            "doc": None, #"DOC",
        }
        return label_names[var.name]

    def _subplot_constant(self, var, color, ax):
        assert self.constant_solution is not None
        if self.periodic_solution is None:
            label = self._get_var_label(self.constant_solution[var])
        else:
            label = None
        self.constant_solution[var].plot(
            x=self.xvar, label=label, ax=ax, color=color, **self.constant_plot_kw
        )

    def _make_y_label(self, var, ax):
        ylabel = "$c$"
        if "units" in var.attrs:
            ylabel += f" [{var.attrs['units']}]"
        ax.set_ylabel(ylabel)

    def _get_group_label(self, group):
        group_labels = {
            "mrna": "transcripts",
            "enz": "enzymes",
            "mrna_norm": "transcripts",
            "enz_norm": "enzymes",
            "nitrogen": "N species",
            "bio": "cell density",
            "o2": "oxygen",
            "doc": "DOC",
        }
        return group_labels[group]

    def _subplot_periodic_and_constant(self, var, group, color=None, ax=None):
        if ax is None:
            ax = plt.axes()

        if self.periodic_solution is not None:
            self._subplot_periodic(var, color, ax)
        if self.constant_solution is not None:
            self._subplot_constant(var, color, ax)

        if self.periodic_solution is not None:
            self._make_y_label(self.periodic_solution[var], ax)
        else:
            self._make_y_label(self.constant_solution[var], ax)
        if self.subplot_legends:
            if self.subplot_legend_kw_dict is not None:
                kw = self.subplot_legend_kw_dict[group]
            else:
                kw = self.subplot_legend_kw
            ax.legend(title=self._get_group_label(group), **kw)
        return ax


def plot_periodic_constant_single_scenario(
    *,
    periodic_solution=None,
    constant_solution=None,
    subplots_kw=None,
    add_fig_legend=None,
    periodic_label="periodic",
    constant_label="constant",
    legend_kw=None,
    xvar="x",
    groups_to_plot=None,
):
    def _make_figure_single_scenario(subplots_kw, groups):
        n_groups = len(groups)
        n_cols = 3
        n_rows = n_groups // n_cols + int(n_groups % n_cols > 0)
        fig, axes = plt.subplots(ncols=n_cols, nrows=n_rows, sharex=True, **subplots_kw)
        axes_mapping = {group: ax for group, ax in zip(groups, axes.flat)}
        return fig, axes, axes_mapping

    if subplots_kw is None:
        subplots_kw = {}
    if groups_to_plot is None:
        groups_to_plot = ["nitrogen", "mrna", "bio", "o2", "enz", "doc"]
    fig, ax, axes_mapping = _make_figure_single_scenario(subplots_kw, groups_to_plot)
    plotter = PeriodicConstantPlot(
        fig,
        axes_mapping,
        groups_to_plot=groups_to_plot,
        periodic_solution=periodic_solution,
        constant_solution=constant_solution,
        add_fig_legend=add_fig_legend,
        periodic_label=periodic_label,
        constant_label=constant_label,
        legend_kw=legend_kw,
        xvar=xvar,
    )
    for a in ax[:-1, :].flat:
        a.set_xlabel(None)
    return plotter


class MultipleScenarioConcentrationDistributionPlot:
    def __init__(
        self,
        scenario_names,
        flow_directions,
        *,
        periodic_solutions=None,
        constant_solutions=None,
        subplots_kw=None,
        add_fig_legend=None,
        periodic_label="periodic",
        constant_label="constant",
        legend_kw=None,
        subplot_legend_kw=None,
        title_kw=None,
        xvar="x",
        groups_to_plot=None,
    ):
        if subplots_kw is None:
            subplots_kw = {}
        self.subplots_kw = subplots_kw

        if subplot_legend_kw is None:
            subplot_legend_kw = {}
        self.subplot_legend_kw = subplot_legend_kw

        if title_kw is None:
            title_kw = {}
        self.title_kw = title_kw

        self.xvar = xvar
        self.legend_kw = legend_kw
        self.constant_label = constant_label
        self.periodic_label = periodic_label
        self.add_fig_legend = add_fig_legend
        self.scenario_names = scenario_names
        self.n_scenarios = len(scenario_names)

        # Define solution in a consistent format
        if periodic_solutions is None:
            periodic_solutions = [None] * self.n_scenarios
        else:
            assert len(periodic_solutions) == self.n_scenarios
        self.periodic_solutions = periodic_solutions
        if constant_solutions is None:
            constant_solutions = [None] * self.n_scenarios
        else:
            assert len(constant_solutions) == self.n_scenarios
        self.constant_solutions = constant_solutions

        assert len(flow_directions) == self.n_scenarios
        self.flow_directions = flow_directions

        if add_fig_legend is None:
            add_fig_legend = [None] * self.n_scenarios
        else:
            assert len(add_fig_legend) == self.n_scenarios
        self.add_fig_legend = add_fig_legend

        if groups_to_plot is None:
            self.groups_to_plot = ["nitrogen", "mrna", "bio", "o2", "enz", "doc"]
        else:
            self.groups_to_plot = groups_to_plot

    def _make_figure_multiple_scenarios(self):
        n_groups = len(self.groups_to_plot)
        self.fig, self.axes = plt.subplots(
            ncols=self.n_scenarios, nrows=n_groups + 1, sharex=True, **self.subplots_kw
        )
        self.axes_mapping = {
            scenario: {group: ax for group, ax in zip(self.groups_to_plot, self.axes[1:, i])}
            for i, scenario in enumerate(self.scenario_names)
        }
        return

    def _get_axes_y(self, group):
        ax = self.axes_mapping[self.scenario_names[-1]][group]
        (x0, y0), (x1, y1) = ax.get_position().get_points()
        mid_y = (y0 + y1) / 2
        return mid_y

    def plot(self):
        self._make_figure_multiple_scenarios()
        self.plotters = {}
        for i, (
            scenario,
            periodic_solution,
            constant_solution,
            add_fig_legend,
        ) in enumerate(
            zip(
                self.scenario_names,
                self.periodic_solutions,
                self.constant_solutions,
                self.add_fig_legend,
            )
        ):
            self.plotters[scenario] = PeriodicConstantPlot(
                self.fig,
                self.axes_mapping[scenario],
                groups_to_plot=self.groups_to_plot,
                periodic_solution=periodic_solution,
                constant_solution=constant_solution,
                add_fig_legend=add_fig_legend,
                subplot_legends=(i == self.n_scenarios - 1),
                periodic_label=self.periodic_label,
                constant_label=self.constant_label,
                legend_kw=self.legend_kw,
                subplot_legend_kw=self.subplot_legend_kw,
                xvar=self.xvar,
            )
        self._remove_x_labels()
        self._remove_y_labels()
        self._set_titles()
        self._annotate_flow_direction()
        return self.plotters

    def _remove_x_labels(self):
        for a in self.axes[:-1, :].flat:
            a.set_xlabel(None)

    def _remove_y_labels(self):
        for a in self.axes[:, 1:].flat:
            a.set_ylabel(None)

    def _set_titles(self):
        for a, scenario in zip(self.axes[0, :].flat, self.scenario_names):
            a.set_title(scenario, **self.title_kw)

    def _annotate_flow_direction(self):
        for a, direction in zip(self.axes[0, :].flat, self.flow_directions):
            a.annotate(
                "river",
                xy=(0.05, 0.5),
                xycoords="axes fraction",
                verticalalignment="center",
            )
            a.annotate(
                "groundwater",
                xy=(0.57, 0.5),
                xycoords="axes fraction",
                verticalalignment="center",
            )
            a.annotate(
                text="",
                xy=(0.53, 0.5),
                xytext=(0.25, 0.5),
                arrowprops=dict(arrowstyle=direction, color="k"),
                xycoords="axes fraction",
            )
            a.xaxis.set_visible(False)
            a.yaxis.set_visible(False)


def get_latex_label(var_name, log_version="log"):
    """
    Get the LaTeX-formatted label for a variable.

    Parameters
    ----------
    var_name: str
        Name of the variable.
    log_version: str
        Specify which version of log to use in the label.
        Must be one of {"log", "log10"}.

    Returns
    -------
    label: str
        The LaTeX-formatted label.
    """
    plot_labels = {
        "rmax_o2_log": r"$\log\left(\nu_{\mathdefault{max}}^{\mathregular{O_2}}\right)$",
        "Ks_o2_log": r"$\log\left(K_{\mathregular{O_2}}\right)$",
        "K_subs_o2_log": r"$\log\left(K_{\mathregular{O_2}}\right)$",
        "K_subs_no2_log": r"$\log\left(K_{\mathregular{{NO_2}^-}}\right)$",
        "K_subs_no3_log": r"$\log\left(K_{\mathregular{{NO_3}^-}}\right)$",
        "t12_enz_log": r"$\log\left(t_{1/2}^{E}\right)$",
        "t_1_2_enz_log": r"$\log\left(t_{1/2}^{E}\right)$",
        "t12_mrna_log": r"$\log\left(t_{1/2}^{T}\right)$",
        "kmax_no2_log": r"$\log\left(k_{\mathdefault{max}}^{\mathregular{{NO_2}^-}}\right)$",
        "k_max_no2_log": r"$\log\left(k_{\mathdefault{max}}^{\mathregular{{NO_2}^-}}\right)$",
        "kmax_no3_log": r"$\log\left(k_{\mathdefault{max}}^{\mathregular{{NO_3}^-}}\right)$",
        "k_max_no3_log": r"$\log\left(k_{\mathdefault{max}}^{\mathregular{{NO_3}^-}}\right)$",
        "rmax_o2_log": r"$\log\left(\nu_{\mathdefault{max}}^{\mathregular{{O_2}}}\right)$",
        "r_max_o2_log": r"$\log\left(\nu_{\mathdefault{max}}^{\mathregular{{O_2}}}\right)$",
        "rmax_no2_log": r"$\log\left(\nu_{\mathdefault{max}}^{\mathregular{{NO_2}^-}}\right)$",
        "rmax_no3_log": r"$\log\left(\nu_{\mathdefault{max}}^{\mathregular{{NO_3}^-}}\right)$",
        "Y_o2_log": r"$\log\left(Y_{\mathregular{O_2}}\right)$",
        "Y_no3_log": r"$\log\left(Y_{\mathregular{{NO_3}^-}}\right)$",
        "Y_no2_log": r"$\log\left(Y_{\mathregular{{NO_2}^-}}\right)$",
        "K_i_o2_transcription_nar_log": r"$\log\left(I_{\mathregular{transcr}}^{\mathit{nar}}\right)$",
        "K_i_o2_transcription_nir_log": r"$\log\left(I_{\mathregular{transcr}}^{\mathit{nir}}\right)$",
        "K_transcription_nir_log": r"$\log\left(A_{\mathit{nir}}\right)$",
        "K_transcription_nar_no3_log": r"$\log\left(A_{\mathit{nar}}^{\mathregular{{NO_3}^-}}\right)$",
        "K_transcription_nar_no2_log": r"$\log\left(A_{\mathit{nar}}^{\mathregular{{NO_2}^-}}\right)$",
        "K_transcription_nir_log": r"$\log\left(A_{\mathit{nir}}^{\mathregular{{NO_2}^-}}\right)$",
        "K_i_o2_no3_log": r"$\log\left(I_{\mathregular{react}}^{\mathregular{NAR}}\right)$",
        "K_i_o2_no2_log": r"$\log\left(I_{\mathregular{react}}^{\mathregular{NIR}}\right)$",
        "K_i_o2_reaction_no3_log": r"$\log\left(I_{\mathregular{react}}^{\mathregular{NAR}}\right)$",
        "K_i_o2_reaction_no2_log": r"$\log\left(I_{\mathregular{react}}^{\mathregular{NIR}}\right)$",
        "r_mrna_log_nir": r"$\log\left(\beta_T^{\mathit{nir}}\right)$",
        "r_mrna_log_nar": r"$\log\left(\beta_T^{\mathit{nar}}\right)$",
        "r_enz_log": r"$\log\left(\beta_E\right)$",
        "r_cfix_no3_log": r"$\log\left(\nu_{\mathdefault{fix}}^{\mathregular{{NO_3}^-}}\right)$",
        "r_cfix_no2_log": r"$\log\left(\nu_{\mathdefault{fix}}^{\mathregular{{NO_2}^-}}\right)$",
        "r_cfix_o2_log": r"$\log\left(\nu_{\mathdefault{fix}}^{\mathregular{O_2}}\right)$",
        "y0_bio_alive_log": r"$\log\left(B_0\right)$",
        "measure_error_bio_alive": r"$\sigma_B$",
        "measure_error_gas_o2": r"$\sigma_{O_2}$",
        "measure_error_gas_n2": r"$\sigma_{N_2}$",
        "measure_error_nitrogen_no2": r"$\sigma_{{NO_2}^-}$",
        "measure_error_mrna_norm_nir": r"$\sigma_{\mathit{nir}}$",
        "measure_error_mrna_norm_nar": r"$\sigma_{\mathit{nar}}$",
        "background_gas_o2": r"$b_{O_2}$",
        "background_nitrogen_no2": r"$b_{{NO_2}^-}$",
        "K_i_o2_transcription_nnr_log": r"$\log\left(I_{\mathregular{NNR}}\right)$",
        "K_i_o2_transcription_fnrp_log": r"$\log\left(I_{\mathregular{FnrP}}\right)$",
        "k_bind_narr_no3_log": r"$\log\left(a_{\mathregular{NarR}}^{\mathregular{{NO_3}^-}}\right)$",
        "k_bind_narr_no2_log": r"$\log\left(a_{\mathregular{NarR}}^{\mathregular{{NO_2}^-}}\right)$",
        "k_bind_nnr_log": r"$\log\left(a_{\mathregular{NNR}}\right)$",
        "k_dissociation_narr_log": r"$\log\left(k_{\mathregular{dec}}^{\mathregular{NarR}}\right)$",
        "k_dissociation_nnr_log": r"$\log\left(k_{\mathregular{dec}}^{\mathregular{NNR}}\right)$",
        "K_dna_fnrp_log": r"$\log\left(K_{\mathregular{FnrP}}\right)$",
        "K_dna_narr_log": r"$\log\left(K_{\mathregular{NarR}}\right)$",
        "K_dna_nnr_log": r"$\log\left(K_{\mathregular{NNR}}\right)$",
        "n_hill_transcription_fnrp": r"$\log\left(p\right)$",
        "n_hill_transcription_nnr": r"$\log\left(q\right)$",
        "right_inflow_concentration_nitrogen_no3": r"$c_{\mathregular{{NO_3}^-}}^{\mathrm{GW}}$",
        "left_inflow_concentration_nitrogen_no3": r"$c_{\mathregular{{NO_3}^-}}^{\mathrm{river}}$",
        "left_inflow_concentration_doc": r"$c_{\mathregular{O_2}}^{\mathrm{river}}$",
        "left_inflow_concentration_o2": r"$c_{\mathregular{DOC}}^{\mathrm{river}}$",
        "k_dec_bio_log": r"$k_{\mathrm{dec}}^{\mathrm{bio}}$",
        "B_max_log": r"$\log\left(B_{\mathrm{max}}\right)$",
        "K_doc": r"$K_{\mathrm{DOC}}$",
        "k_release_doc": r"$k_{\mathrm{release}}$",
        "lambda_poc": r"$\lambda_{\mathrm{POC}}$",
        "c_sat_0_doc": r"$c_{\mathrm{sat}}^0$",
        "v_amplitude": r"$\hat{v}$",
    }
    if log_version == "log":
        return plot_labels[var_name]
    elif log_version == "log10":
        return re.sub("(log)", r"\1_{10}", plot_labels[var_name])
    else:
        assert False


def convert_concentration_units(x):
    target_units = {
        "nitrogen_no3": "mM",
        "nitrogen_no2": "mM",
        "nitrogen_n2": "mM",
        "doc": "mM",
        "o2": "µM",
        "enz_nar": "pM",
        "enz_nir": "pM",
        "rate_no3": "nmol/L/s",
        "rate_no2": "nmol/L/s",
        "turnover_rate": "nmol/L/s",
    }
    if x.name in target_units:
        unit = target_units[x.name]
        converted =  x.pint.quantify().pint.to(unit).pint.dequantify(format="~P")
    else:
        converted = x
    return converted