---
jupyter:
  jupytext:
    formats: ipynb,md
    main_language: python
    notebook_metadata_filter: -kernelspec
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.11.5
---

```python
import string
import os

import arviz
import matplotlib.pyplot as plt
import matplotlib.ticker
import numpy as np
import pandas as pd
import nitrogene.plotting as plotting
import pymc as pm
import seaborn as sns
import xarray as xr

import nitrogene.utils
```

```python
ds = xr.open_dataset("../../experimental_data/data_Qu_2015.nc")
data = nitrogene.utils.process_data_for_likelihood(
    ds,
)
```

```python
rc = {
    "font.size": 11,
    "legend.fontsize": 8,
    "legend.handlelength": 1.5,
    "legend.labelspacing": 0.1,
    "mathtext.fontset": "custom",
    "mathtext.rm": "Source Sans Pro",
    "mathtext.it": "Source Sans Pro:italic",
    "mathtext.bf": "Source Sans Pro:bold",
    "savefig.dpi": 400
}
sns.set(
    style="whitegrid",
    context="paper",
    rc=nitrogene.plotting.paper_rc,
    palette=nitrogene.plotting.color_palettes["paul_tol_bright"],
)
pdf_metadata = {"Author": "Anna Störiko"}
```

## Read in simulation results

```python
# Read in the data from the Monod type model
inference_data = arviz.from_netcdf(
    "../../traces/nuts_trace_2021_02_24_classical_monod.nc"
)
tr = inference_data.posterior
prior = inference_data.prior
```

```python
# Read in the data of the TF based model
inference_data_with_genes = arviz.from_netcdf(
    "../../traces/nuts_trace_pymc3-ext_2021_02_05_nar_nir_tf_activate_nnr_by_no2_only.nc"
)
tr_with_genes_all_chains = inference_data_with_genes.posterior
tr_with_genes = tr_with_genes_all_chains.drop_sel(chain=3)
prior_with_genes = inference_data_with_genes.prior
all_priors = prior.update(prior_with_genes)
```

```python
# Data from the enzyme-based model with a simplified description of transcriptional regulation
idata_simplified_model = arviz.from_netcdf(
    "../../traces/nuts_trace_pymc3-ext_2021_02_08_nar_nir_hill_func_larger_error.nc"
)
```

## Concentration time series of the Monod-type and the enzyme-based model

```python
plot_path = "../../plots/manuscript"
```

```python
# with sns.axes_style("whitegrid", rc=rc):
fig, ax = nitrogene.plotting.plot_two_step_model_comparison(
    tr_with_genes,
    tr.median(dim=["draw", "chain"]),
    ds,
    genes=["nar", "nir"],
    n_lines_genes=100,
    #     n_lines_monod=30,
    figsize=(6.5, 3.2),
    quantiles=False,
    color_map="bright",
)
sns.despine(bottom=True, left=True)
plt.subplots_adjust(wspace=0.4, hspace=0.2, right=0.9)
fig.align_labels()
for a, label in zip(ax.flat, string.ascii_lowercase):
    a.set_title(label, pad=3, weight="bold")
fig.savefig(os.path.join(plot_path, "model_comparison.pdf"), bbox_inches="tight")
```

```python
fig.savefig(os.path.join(plot_path, "model_comparison.png"), bbox_inches="tight", dpi=400)
```

## Results of the model with a simplified description of transcriptional regulation

```python
from matplotlib.ticker import ScalarFormatter


class ScalarFormatterForceFormat(ScalarFormatter):
    def _set_format(self):  # Override function that finds format to use.
        self.format = "%1.1f"  # Give format here
```

```python
fig, ax = plotting.plot_two_step(
    idata_simplified_model.posterior,
    ds,
    classical_monod=False,
    genes=["nar", "nir"],
    n_lines=50,
    figsize=(5.78, 3.2),
    quantiles=False,
)
yfmt = ScalarFormatterForceFormat()
yfmt.set_powerlimits((0, 0))
ax[1, 1].get_shared_x_axes().get_siblings(ax[1, 1])[
    -1
].yaxis.set_major_formatter(yfmt)
sns.despine(bottom=True, left=True)
plt.subplots_adjust(wspace=0.47, hspace=0.25, left=0.08)
fig.align_labels()
for a, label in zip(ax.flat, string.ascii_lowercase):
    a.set_title(label, pad=3, weight="bold")
plt.savefig(
    os.path.join(plot_path, "time_series_simplified_model.pdf"),
    metadata=pdf_metadata,
)
```

## Relationship between transcripts/enzymes and reaction rates

```python
def enzyme_based_denitrification_rates(posterior, fixed_params):
    substrates = ["no3", "no2"]
    substrate_to_gene_mapping = {"no3": "nar", "no2": "nir"}
    rates = {}
    for key in substrates:
        gene = substrate_to_gene_mapping[key]
        k = np.exp(posterior[f"kmax_{key}_log"])
        K_i = np.exp(posterior[f"K_i_o2_{key}_log"])
        K = fixed_params["K_subs"][key]["val"]
        c_enz = posterior[f"c_dense_enz_{gene}"]
        c_subs = posterior[f"c_dense_nitrogen_{key}"]
        c_o2 = posterior[f"c_dense_o2"]
        rates[key] = k * c_enz * c_subs / (c_subs + K) * K_i / (K_i + c_o2)
    return xr.concat(
        rates.values(), dim=pd.Index(list(rates.keys()), name="rate")
    )


def monod_denitrification_rates(
    posterior, fixed_params, time_resolution="dense"
):
    substrates = ["no3", "no2"]
    rates = {}
    if time_resolution == "dense":
        base_name = "c_dense"
    else:
        base_name = "c"
    for key in substrates:
        r_max = np.exp(posterior[f"rmax_{key}_log"])
        K_i = np.exp(posterior[f"K_i_o2_{key}_log"])
        K = fixed_params["K_subs"][key]["val"]
        c_subs = posterior[f"{base_name}_nitrogen_{key}"]
        c_o2 = posterior[f"{base_name}_o2"]
        B = posterior[f"{base_name}_bio_alive"]
        rates[key] = r_max * B * c_subs / (c_subs + K) * K_i / (K_i + c_o2)
    return xr.concat(
        rates.values(), dim=pd.Index(list(rates.keys()), name="rate")
    )
```

```python
fixed_params = {
    "K_subs": {  # mol/L
        "no3": {"val": 5e-6, "log": np.inf},
        "no2": {"val": 5e-6, "log": np.inf},
        "no": {"val": 1e-6, "log": np.inf},
        "n2o": {"val": 2e-9, "log": np.inf},
        "o2": {"val": 1e-4, "log": np.inf},
    },
}
rates = enzyme_based_denitrification_rates(tr_with_genes, fixed_params)
monod_rates = monod_denitrification_rates(
    tr, fixed_params, time_resolution="coarse"
)
```

```python
sns.set(
    style="darkgrid",
    context="paper",
    rc=plotting.paper_rc,
    palette=plotting.color_palettes["paul_tol_bright"],
)
```

```python
# selector = {"draw": np.random.choice(rates.sizes["draw"], 2), "chain": np.arange(3)}
selector = {"draw": [759, 436], "chain": [0, 1, 2]}
# selector = {"draw": [680, 553], "chain": [0, 1, 2]}
# selector = {'draw': [178, 171], 'chain': [0, 1, 2] }
g, rates_ds = plotting.plot_rate_correlations(
    rates.isel(**selector),
    tr_with_genes.isel(**selector),
    monod_rates,
    tr,
    ds,
    n_times=40,
    cmap="crest",
    figsize=(5.2, 3.5),
    lw=1,
    data_kws={
        "color": "0.3",
        "marker": "D",
        "zorder": 3,
        "markerfacecolor": "none",
        "alpha": 0.9,
    },
)
g.axes[0, 0].get_yaxis().get_offset_text().set_x(-0.1)
g.axes[1, 0].get_yaxis().get_offset_text().set_x(-0.1)
g.axes[1, 2].get_xaxis().get_offset_text().set_x(1.05)
g.axes[0, 2].get_xaxis().get_offset_text().set_x(1.05)


for a, label in zip(g.axes.flat, string.ascii_lowercase):
    formatted_label = f"{label}"
    a.annotate(
        formatted_label, (0.05, 0.86), xycoords="axes fraction", size=9, weight="bold"
    )
g.fig.savefig(
    os.path.join(plot_path, "rate_correlations_new.pdf"), metadata=pdf_metadata
)
```

## Quasi-steady state of enzymes

```python
from nitrogene.plotting import AVOGADRO_CONSTANT
```

```python
r_enz_log = np.log(1125)
genes = ["nar", "nir"]
c_enz_qss = (
    xr.concat(
        [
            tr_with_genes[f"c_dense_mrna_{gene}"]
            * np.exp(r_enz_log - tr_with_genes[f"r_mrna_log_{gene}"])
            for gene in genes
        ],
        dim=pd.Index(genes, name="gene"),
    )
    / AVOGADRO_CONSTANT
)
```

```python
g, enz_ds = plotting.plot_qss(
    c_enz_qss,
    tr_with_genes,
    var="enz",
    n_samples=5,
    time_thinning=5,
    normalization="none",
    colormap="crest",
    figsize=(3.0, 3.4),
    scatter_size=5,
    subplot_kws=dict(aspect="equal", adjustable="datalim"),
    sharex=False,
    sharey=False,
    faceting="row",
    x_var="quasi-steady state",
    y_var="transient",
    # cbar_kws={"location": "bottom"}
    cbar_kws={"aspect": 30, "pad": 0.03},
    plot_style="line",
)
g.fig.savefig(os.path.join(plot_path, "qss_enzymes.pdf"), metadata=pdf_metadata)
```

## Transcription factor concentrations

```python
def hill_kinetics(x1, x2, n):
    return 1 / (1 + (x1 / x2) ** n)


# Compute fractions of active TFs
active_narr = tr_with_genes.c_dense_tf_narr
active_fnrp = hill_kinetics(
    tr_with_genes.c_dense_o2,
    np.exp(tr_with_genes.K_i_o2_transcription_fnrp_log),
    np.exp(tr_with_genes.n_hill_transcription_fnrp),
)
nnr_inhibition = hill_kinetics(
    tr_with_genes.c_dense_o2,
    np.exp(tr_with_genes.K_i_o2_transcription_nnr_log),
    np.exp(tr_with_genes.n_hill_transcription_nnr),
)
active_nnr = tr_with_genes.c_dense_tf_nnr * nnr_inhibition

tf_data = xr.concat(
    [active_fnrp, active_narr, active_nnr],
    dim=pd.Index(["FnrP", "NarR", "NNR"], name="transcription factor"),
)
```

```python
# Plot the transcription factor concentrations
g = (
    tf_data.stack(sample=("chain", "draw"))
    .isel(sample=slice(None, None, 50))
    .plot.line(
        x="time_dense",
        col="transcription factor",
        figsize=(5, 5/3),
        add_legend=False,
    )
)
g.set_axis_labels(x_var="time [h]", y_var="active fraction [–]")
plt.subplots_adjust(left=0.08)
g.fig.savefig(
    os.path.join(plot_path, "tf_concentrations.pdf"), metadata=pdf_metadata
)
```

## Posterior parameter distributions

```python
sns.set(
    style="ticks",
    context="paper",
    palette=plotting.color_palettes["paul_tol_high_contrast"],
    rc={
        **plotting.paper_rc,
        "axes.edgecolor": "dimgrey",
        "xtick.color": "dimgrey",
        "ytick.color": "dimgrey",
    }
)
```

```python
fig = plt.figure(constrained_layout=False, figsize=(3.346, 4))
spec_a = fig.add_gridspec(ncols=3, nrows=2, bottom=0.45, top=0.82, hspace=2, left=0.05)
axs_rows = []
for i in range(2):
    axs_cols = []
    for j in range(3):
        ax_a_ij = fig.add_subplot(spec_a[i, j])
        axs_cols.append(ax_a_ij)
    axs_rows.append(axs_cols)

ax = np.array(axs_rows)
ax = arviz.plot_density(
    [tr, tr_with_genes, all_priors],
    var_names=[
        "kmax_no2_log",
        "K_i_o2_no3_log",
        "Y_o2_log",
        "t12_enz_log",
        "K_i_o2_transcription_nnr_log",
        "k_bind_narr_no3_log",
    ],
    data_labels=["Monod\n model", "enzyme-based\n model", "prior"],
    shade=0.2,
    transform=lambda x: x / np.log(10),
    ax=ax[:2, :],
)

plt.ticklabel_format(scilimits=(-2, 3), style="sci")
for a in ax.flat:
    a.set_title(plotting.get_latex_label(a.get_title(), log_version="log10"))
    sns.despine(ax=a, left=True)
    a.tick_params(labelcolor="black", labelsize="small")
plt.figlegend(
    *ax[0, 0].get_legend_handles_labels(),
    loc="upper center",
    # loc="center left",
    # bbox_to_anchor=(0.1, 0.94),
    bbox_transform=fig.transFigure,
    ncol=3,
    fontsize=8,
    frameon=False
)
ax[0, 0].get_legend().remove()

ax[0, 1].set_xlim((14.6, 14.9))

spec_b = fig.add_gridspec(nrows=1, ncols=2, bottom=0.15, top=0.32, left=0.2, wspace=0.7)
ax_b0 = fig.add_subplot(spec_b[0, 0])
var_names = ["rmax_o2_log", "Ks_o2_log"]
g = arviz.plot_pair(
    tr / np.log(10),
    var_names=var_names,
    scatter_kwargs={"zorder": 3, "alpha": 0.3, "color": "C0"},
    ax=ax_b0,
)
g = arviz.plot_pair(
    tr_with_genes / np.log(10),
    var_names=var_names,
    scatter_kwargs={"zorder": 3, "alpha": 0.3, "color": "C1"},
    ax=ax_b0,
)
ax_b0.xaxis.set_major_locator(matplotlib.ticker.MaxNLocator(2))
ax_b1 = fig.add_subplot(spec_b[0, 1])
var_names = ["t12_enz_log", "kmax_no2_log"]
g = arviz.plot_pair(
    tr_with_genes / np.log(10),
    var_names=var_names,
    scatter_kwargs={"zorder": 3, "alpha": 0.3, "color": "C1"},
    ax=ax_b1,
)
for a in [ax_b0, ax_b1]:
    a.set_xlabel(plotting.get_latex_label(a.get_xlabel(), log_version="log10"), fontsize="medium")
    a.set_ylabel(plotting.get_latex_label(a.get_ylabel(), log_version="log10"), fontsize="medium")
    sns.despine(ax=a)
    a.tick_params(labelcolor="black", labelsize="small")

plt.annotate(
    "a",
    (0.03, 0.94),
    xycoords="figure fraction",
    horizontalalignment="left",
    verticalalignment="center",
    weight="bold",
)
plt.annotate("b", (0.03, 0.35), xycoords=fig, weight="bold")
fig.savefig(os.path.join(plot_path, "posterior_dists.pdf"), metadata=pdf_metadata)
```

```python
var_names = [
    "kmax_no3_log",
    "kmax_no2_log",
    "rmax_o2_log",
    "rmax_no3_log",
    "rmax_no2_log",
    "K_i_o2_no3_log",
    "K_i_o2_no2_log",
    "Y_o2_log",
    "Ks_o2_log",
    "t12_enz_log",
    "r_mrna_log_nar",
    "r_mrna_log_nir",
    "K_i_o2_transcription_fnrp_log",
    "K_i_o2_transcription_nnr_log",
    "k_bind_narr_no3_log",
    "k_bind_narr_no2_log",
    "k_bind_nnr_log",
    "k_dissociation_narr_log",
    "k_dissociation_nnr_log",
    "K_dna_fnrp_log",
    "K_dna_narr_log",
    "K_dna_nnr_log",
    "n_hill_transcription_fnrp",
    "n_hill_transcription_nnr",
]
```

```python
# Pre-select data so that we do not load all of the data when doing the transform
traces = [tr, tr_with_genes, all_priors]
var_name_lists = [
    [var for var in var_names if var in trace] for trace in traces
]
parameter_plotting_data = [
    trace[variables] for trace, variables in zip(traces, var_name_lists)
]
n_vars = len(var_names)
# Plot all denisites
ax = arviz.plot_density(
    parameter_plotting_data,
    var_names=var_names,
    data_labels=["Monod model", "enzyme-based\nmodel", "prior"],
    shade=0.2,
    figsize=(5.78, 5),
    grid=(n_vars // 5 + 1, 5),
    transform=lambda x: x / np.log(10),
)
for a, ax_count in zip(ax.flat[:n_vars], string.ascii_lowercase):
    a.set_title(plotting.get_latex_label(a.get_title(), log_version="log10"))
    a.tick_params(labelcolor="black", labelsize="small")
# Move legend to a place where there is some space (the empty last axis)
ax[0, 0].get_legend().remove()
plt.figlegend(
    loc="center",
    bbox_to_anchor=(0.55, 0.5),
    bbox_transform=ax.flat[-1].transAxes,
    fontsize=8,
    frameon=False
)
plt.subplots_adjust(hspace=1.5)
plt.savefig(os.path.join(plot_path, "all_posterior_dists.pdf"))
```

### Compute and print quantiles of posterior variables

```python
def apply_exp_to_log_vars(x):
    substrings = ["log", "n_hill", "background"]
    if any(s in x.name for s in substrings):
        return np.exp(x)
    else:
        return x
```

```python
# For the Monod-type model
var_names = [
    name for name in tr if not ("c_" in name or "mu" in name or "flat" in name)
]

quantiles = tr[var_names].map(apply_exp_to_log_vars).quantile([0.1, 0.5, 0.9])
print(
    quantiles.to_array().to_pandas().to_string(float_format="{:.1e} &".format)
)
```

```python
# For the enzyme-based model
var_names = [
    name
    for name in tr_with_genes
    if not ("c_" in name or "mu" in name or "flat" in name)
]

quantiles = (
    tr_with_genes[var_names].map(apply_exp_to_log_vars)
).quantile([0.1, 0.5, 0.9])
print(
    quantiles.to_array().to_pandas().to_string(float_format="{:.1e} &".format)
)
```

```python
# For the enzyme-based model
var_names = [
    name
    for name in idata_simplified_model.posterior
    if not ("c_" in name or "mu" in name or "flat" in name)
]

quantiles = (
    idata_simplified_model.posterior[var_names].map(apply_exp_to_log_vars)
).quantile([0.1, 0.5, 0.9])
print(
    quantiles.to_array().to_pandas().to_string(float_format="{:.1e} &".format)
)
```

```python
var_names = [
    #     "Y_o2_log", "rmax_o2_log", "Ks_o2_log", "kmax_no3_log", "kmax_no2_log",
    "K_i_o2_transcription_fnrp_log",
    "K_i_o2_transcription_nnr_log",
    "k_bind_narr_no3_log",
    "k_bind_narr_no2_log",
    "k_bind_nnr_log",
    "k_dissociation_narr_log",
    "k_dissociation_nnr_log",
    "t12_enz_log",
    "r_mrna_log_nar",
    "r_mrna_log_nir",
]
g = arviz.plot_pair(
    tr_with_genes,
    var_names=var_names,
    scatter_kwargs={"zorder": 3, "alpha": 0.3},
    figsize=(11, 8),
)
sns.despine()
for a in g.flat:
    xlabel = a.get_xlabel()
    ylabel = a.get_ylabel()
    if xlabel != "":
        a.set_xlabel(plotting.get_latex_label(xlabel))
    if ylabel != "":
        a.set_ylabel(plotting.get_latex_label(ylabel))
```

#### Compare the estimated model error between the models

```python
with sns.axes_style(style="ticks"):
    ax = arviz.plot_density(
        [tr, tr_with_genes],
        var_names=[var for var in tr if "measure_error" in var],
        data_labels=["classical Monod model", "enzyme based model"],
        shade=0.2,
        figsize=(7, 2),
        grid=(1, 4),
    )
plt.show()
```

## Posterior correlations
We compute pairwise correlation coefficients of posterior parameters with numpy's `corrcoeff` function.
### … in the enzyme-based model

```python
exclude_vars = [
    "c_",
    "mu_",
    "y0_flat",
    "measure_error",
    "background",
    "t12_mrna_log",
    "rmax_o2_log",
]
var_names = [
    var
    for var in tr_with_genes.data_vars
    if not any(var.startswith(start) for start in exclude_vars)
]

parameter_array = (
    tr_with_genes[var_names].to_array().stack(sample=("chain", "draw"))
)
corr_matrix = np.corrcoef(parameter_array)
corr_matrix = xr.DataArray(
    data=corr_matrix,
    coords={"variable": parameter_array["variable"]},
    dims=("variable", "variable"),
)
```

```python
labels = [plotting.get_latex_label(var) for var in var_names]
g = sns.clustermap(
    data=corr_matrix.to_pandas(),
    center=0,
    vmin=-1,
    vmax=1,
    # metric="cosine",
    annot=True,
    fmt=".2f",
    cmap="vlag",
    xticklabels=labels,
    yticklabels=labels,
    figsize=(5.1, 6.5),
    annot_kws={"fontsize": 6},
    dendrogram_ratio=(1e-5, 0.1),
    cbar_pos=(0.85, 0.03, 0.03, 0.1),
    cbar_kws={"label": "correlation\ncoefficient"},
)
g.ax_row_dendrogram.remove()

g.savefig(
    os.path.join(plot_path, "posterior_correlation_TF_based_model.pdf"),
    metadata=pdf_metadata,
)
```

### … in the Monod-type model

```python
exclude_vars = ["c_", "mu_", "y0_flat"]
var_names_monod = [
    var
    for var in tr.data_vars
    if not any(var.startswith(start) for start in exclude_vars)
]
parameter_array = (
    tr[var_names_monod].to_array().stack(sample=("chain", "draw"))
)
corr_matrix = np.corrcoef(parameter_array)
corr_matrix = xr.DataArray(
    data=corr_matrix,
    coords={"variable": parameter_array["variable"]},
    dims=("variable", "variable"),
)
labels = [plotting.get_latex_label(var) for var in var_names_monod]
df = corr_matrix.to_pandas()
g = sns.clustermap(
    data=df,
    center=0,
    vmin=-1,
    vmax=1,
    # metric="cosine",
    annot=True,
    fmt=".2f",
    cmap="vlag",
    xticklabels=labels,
    yticklabels=labels,
    figsize=(5.1, 6.5),
    annot_kws={"fontsize": 7},
    dendrogram_ratio=(1e-5, 0.1),
    cbar_pos=(0.85, 0.03, 0.03, 0.1),
    cbar_kws={"label": "correlation\ncoefficient"},
)
g.ax_row_dendrogram.remove()
g.savefig(os.path.join(plot_path, "posterior_correlation_monod_model.pdf"))
```

### Reparametrization in the Monod-type model
The maximum rate parameter for nitrate reduction is strongly correlated with the oxygen inhibition constant of nitrate reduction (left panel).
The parameter we use instead after reparametrization, the nitrate reduction rate at a fixed oxygen concentration, is not correlated with the inhibition constant (right panel).

```python
# Show the effect of reparametrization
fig, axes = plt.subplots(ncols=2, sharex=False, sharey=False, figsize=(5, 2.))
tr.K_i_o2_no3_log.attrs["long_name"] = "$\\log(I_{react}^{NAR})$"
tr.rmax_no3_log.attrs["long_name"] = "$\\log(r_{max}^{{NO_3}^-})$"
tr.r_cfix_no3_log.attrs["long_name"] = "$\\log(r_{fix}^{{NO_3}^-})$"
tr[["rmax_no3_log", "K_i_o2_no3_log"]].plot.scatter(
    x="rmax_no3_log", y="K_i_o2_no3_log", alpha=0.05, ax=axes[0]
)
tr[["r_cfix_no3_log", "K_i_o2_no3_log"]].plot.scatter(
    x="r_cfix_no3_log", y="K_i_o2_no3_log", alpha=0.05, ax=axes[1]
)
axes[0].set_title("without reparametrization")
axes[1].set_title("after reparametrization")
for a in axes.flat:
    a.tick_params(labelcolor="black")
plt.tight_layout()
sns.despine()
```
