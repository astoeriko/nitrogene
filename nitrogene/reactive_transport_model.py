import operator
import os
import re
import warnings

import adrpy as transport
import adrpy.velocity_funcs as velo
import arviz as az
import matplotlib.pyplot as plt
import nesttool as nested
import numpy as np
import pandas as pd
import seaborn as sns
import sunode
import sympy as sym
import xarray as xr

import nitrogene.reaction_model as reaction_model


def mg_to_mol(concentration, compound):
    """Convert concentrations from mg/L to mol/L."""
    conversion_factor = {
        "DO": 1 / 32,
        "C4H6O4": 1 / (4 * 12),
        "DOC": 1 / 12,
        "nitrate": 1 / 62,
    }
    if compound not in conversion_factor:
        raise ValueError(
            f"Compound not supported: {compound}; must be one of {set(conversion_factor.keys())}"
        )
    return concentration * conversion_factor[compound] * 1e-3


def get_shape(obj):
    if hasattr(obj, "shape"):
        return obj.shape
    return ()


def set_initial_values(y0_dict, y0_array):
    for key, val in y0_dict.items():
        if isinstance(val, dict):
            set_initial_values(val, y0_array[key])
        else:
            y0_array[key] = val


class Model:
    """The class represents my model set-up for a coupled reactive transport model."""

    def __init__(
        self,
        calibrated_reaction_params,
        scenario="bank_filtration_fluctuating_oxygen",
        *,
        parameter_name_map=None,
        reaction_model_args=None,
        solver_kw=None,
    ):
        self.scenario = scenario
        self.make_reaction_model(model_args=reaction_model_args)
        self.set_reaction_params(
            calibrated_reaction_params, name_map=parameter_name_map
        )
        self.set_transport_params()
        self.set_transport_param_dims()
        self.set_initial_values()
        self.make_reaction_problem()
        self.make_transport_problem()
        if solver_kw is None:
            solver_kw = {}
        self.make_solver(**solver_kw)

    def make_reaction_model(self, model_args=None):
        default_model_args = {
            "gas_phase": "none",
            "mrna_at_quasi_steady_state": True,
            "classical_monod": False,
            "two_step_model": True,
            "cell_decay": True,
            "growth_substrates": ["o2", "no3", "no2"],
            "growth_yield_units": "cells/mol_DOC",
            "growth_model": "logistic",
            "carbon_for_growth": True,
            "doc_rate_limitation": True,
            "smooth_gas_sampling": True,
            "enzymes_to_simulate": ["nir", "nar"],
            "transcription_self_inhibition": False,
            "nitrite_toxicity": False,
            "transcriptional_regulation_opts": {
                "nar": "transcription_factors",
                "nir": "transcription_factors",
            },
            "doc_release": "depth_dependent",
        }
        if model_args is None:
            model_args = default_model_args
        else:
            model_args = {**default_model_args, **model_args}
        kwargs = {"log_scale": False}

        model_builder = reaction_model.ModelBuilder(**model_args, **kwargs)
        model_builder.define_model()
        self.reaction_model = model_builder

    def correct_growth_yield(self, params):
        """
        Correct growth yield for incorporation of carbon into biomass.

        Parameters
        ----------
        params : dict
            Dictionary holding reaction parameters with uncorrected growth yield.

        Returns
        -------
        float
            Corrected growth yield.

        Notes
        -----
        In the original model, DOC consumption and growth were described by

        .. math::

            r_{DOC}^* = \frac{n_{DOC}^{energy}}{n_{O_2}^{energy}} r_{O_2}\\
            r_{growth} = Y^* r_{DOC}^*

        and only cell densities, but not carbon substrate concentrations, were measured.
        :math:`Y^*` is the apparent, estimated growth yield based on the modeled DOC
        consumption rate :math:`r_{DOC}^*` which does not account for carbon uptake for
        biomass and therefore underestimates the true DOC consumption rate :math:`r_{DOC}`.
        However, if I account for the incorporation of carbon into biomass, the rate laws are

        .. math:

            r_{DOC} = \frac{n_{DOC}^{energy}}{n_{O_2}^{energy}} \frac{1}{1 - f_{DOC}Y} r_{O_2}\\
            r_{growth} = Y^* r_{DOC}^*

        where :math:`f_{DOC} = \frac{n_{DOC}^{growth}}{n_{bio}^{growth}}\frac{w_{bio}}{M_{bio}}`
        is the amount of DOC needed to build one cell.
        However, growth rates have been correctly estimated based on cell counts, so I know that

        .. math:

            Y^* r_{DOC}^* = Y r_{DOC}\\
            Y^* \frac{n_{DOC}^{energy}}{n_{O_2}^{energy}} r_{O_2} = Y \frac{n_{DOC}^{energy}}{n_{O_2}^{energy}} \frac{1}{1 - f_{DOC}Y} r_{O_2}\\
            \Rightarrow Y^* = Y \frac{1}{1 - f_{DOC}Y}\\
            \Rightarrow (1 - f_{DOC}Y) Y^* = Y\\
            \Rightarrow Y^* - Y^* f_{DOC}Y = Y\\
            \Rightarrow Y^* = Y + Y^* f_{DOC}Y = Y (1 + Y^* f_{DOC})\\
            \Rightarrow Y = \frac{Y^*}{1 + Y^* f_{DOC}}

        """
        carbon_per_cell = (
            params["n_doc_growth"]
            / params["n_bio_growth"]
            * params["cell_weight"]["val"]
            / params["M_bio"]
        )
        Y_o2 = np.exp(params["Y"]["o2"]["log"])
        return Y_o2 / (carbon_per_cell * Y_o2 + 1)

    def set_reaction_params(
        self,
        calibrated_reaction_params,
        name_map=None,
    ):
        """Set the reaction parameter using default values and provided parameter values

        Parameters:
        -----------

        calibrated_reaction_params: xarray.Dataset
            dataset containing the parameters that should be used
        name_map: dict
            map of parameter names in the model to names in calibrated_reaction_params
        """
        if name_map is None:
            name_map = {}
        params = self.reaction_model.default_params
        n_carbon_per_succinate = 4
        self.porosity = 0.3
        params["k_dec"]["bio"]["log"] = np.log(1e-7)
        params["k_decomp"] = 0.2 / 86400
        params["B_max"]["log"] = np.log(1e11 / self.porosity)
        params["K_doc"] = 1e-5 * n_carbon_per_succinate
        params["k_release"]["doc"] = 0.2 / 86400
        params["c_norm"]["doc"] = 5e-5 * n_carbon_per_succinate
        params["c_norm"]["nitrogen"]["no3"] = 1e-4
        params["c_norm"]["bio"]["alive"] = np.exp(params["B_max"]["log"])
        # Use mol-C based reaction stoichiometry (instead of mol succinate)
        params["n_doc_energy"] = {
            "no3": 1.0 * n_carbon_per_succinate,
            "no2": 3.0 * n_carbon_per_succinate,
            "o2": 2.0 * n_carbon_per_succinate,
        }
        params["n_doc_growth"] = 3.0 * n_carbon_per_succinate
        if self.reaction_model.doc_release == "depth_dependent":
            params["c_sat_0"]["doc"] = mg_to_mol(250, "DOC")
            params["lambda_poc"] = 5
        elif self.reaction_model.doc_release == "constant":
            params["c_sat"]["doc"] = mg_to_mol(20, "DOC")
        else:
            assert False

        # Set reaction params based on the calibration results
        flat_params = sunode.dtypesubset.as_flattened(params)
        # Make a list of the calibrated parameters to keep track if all are used
        calibrated_param_names = list(calibrated_reaction_params)
        for keys, param in flat_params.items():
            # Get the correct parameter name
            key = "_".join(keys)
            key = name_map.get(key, key)
            # Fill in the values
            if key in calibrated_reaction_params:
                flat_params[keys] = float(calibrated_reaction_params[key].values)
                if key in calibrated_param_names:
                    calibrated_param_names.remove(key)
        if calibrated_param_names:
            warnings.warn(
                f"The following parameters were provided but not used: {calibrated_param_names}. "
                "Make sure that their names match the names in the model if they should be used."
            )
        params = sunode.dtypesubset.as_nested(flat_params)
        # Convert growth yield to cells / mol-c (instead of cells/mol succinate)
        params["Y"]["o2"]["log"] = params["Y"]["o2"]["log"] - np.log(n_carbon_per_succinate)
        Y_o2_corrected = self.correct_growth_yield(params)
        params["Y"]["o2"]["log"] = np.log(Y_o2_corrected)
        params["Y"]["no3"]["log"] = 32.3 - np.log(n_carbon_per_succinate)
        params["Y"]["no2"]["log"] = 31.8 - np.log(n_carbon_per_succinate)
        self.reaction_params = params


    def set_transport_param_dims(self):
        if self.scenario == "bank_filtration_fluctuating_oxygen":
            boundary_conditions = {
                "left": {
                    "inflow_concentration": {
                        "amplitude": self.reaction_state_shapes,
                        "frequency": self.reaction_state_shapes,
                        "phase_shift": self.reaction_state_shapes,
                        "mean_val": self.reaction_state_shapes,
                    },
                },
            }
        elif self.scenario == "bank_filtration_constant_oxygen":
            boundary_conditions = {
                "left": {
                    "inflow_concentration": self.reaction_state_shapes,
                }
            }
        elif (
            self.scenario == "bank_storage"
            or self.scenario == "groundwater_exfiltration"
        ):
            boundary_conditions = {
                "left": {
                    "inflow_concentration": self.reaction_state_shapes,
                },
                "right": {
                    "inflow_concentration": self.reaction_state_shapes,
                },
            }
        else:
            assert False
        if self.scenario == "bank_storage":
            velocity_params = {
                "v": {
                    "amplitude": (),
                    "frequency": (),
                    "phase_shift": (),
                    "mean_val": (),
                }
            }
        elif (
            self.scenario == "bank_filtration_fluctuating_oxygen"
            or self.scenario == "bank_filtration_constant_oxygen"
            or self.scenario == "groundwater_exfiltration"
        ):
            velocity_params = {"v": ()}
        else:
            assert False

        transport_params = {
            "dx": (),
            "length": (),
            "transported": self.reaction_state_shapes,
            "dispersivity": (),
            "porosity": (),
            "molecular_diffusion": (),
            **velocity_params,
            **boundary_conditions,
        }
        self.transport_param_dims = transport_params

    def set_transport_params(self):
        if self.scenario == "bank_filtration_fluctuating_oxygen":
            boundary_conditions = {
                "left": {
                    "inflow_concentration": {
                        "amplitude": {
                            "tf": {"narr": 0.0, "nnr": 0.0, "fnrp": 0.0},
                            "mrna": {"nar": 0.0, "nir": 0.0},
                            "enz": {"nar": 0.0, "nir": 0.0},
                            "nitrogen": {
                                "no3": 0,
                                "no2": 0,
                                "n2": 0,
                            },
                            "doc": 0,
                            "bio": {"alive": 0.0, "dead": 0.0},
                            "o2": 3.0 / 32 * 1e-3,
                            "co2": 0,
                        },
                        "frequency": {
                            "tf": {"narr": 0.0, "nnr": 0.0, "fnrp": 0.0},
                            "mrna": {"nar": 0.0, "nir": 0.0},
                            "enz": {"nar": 0.0, "nir": 0.0},
                            "nitrogen": {
                                "no3": 0.0,
                                "no2": 0.0,
                                "n2": 0.0,
                            },
                            "doc": 0.0,
                            "bio": {"alive": 0.0, "dead": 0.0},
                            "o2": 1.0 / 86400,
                            "co2": 0.0,
                        },
                        "phase_shift": {
                            "tf": {"narr": 0.0, "nnr": 0.0, "fnrp": 0.0},
                            "mrna": {"nar": 0.0, "nir": 0.0},
                            "enz": {"nar": 0.0, "nir": 0.0},
                            "nitrogen": {
                                "no3": 0.0,
                                "no2": 0.0,
                                "n2": 0.0,
                            },
                            "doc": 0.0,
                            "bio": {"alive": 0.0, "dead": 0.0},
                            "o2": 0,
                            "co2": 0.0,
                        },
                        "mean_val": {
                            "tf": {"narr": 0.0, "nnr": 0.0, "fnrp": 0.0},
                            "mrna": {"nar": 0.0, "nir": 0.0},
                            "enz": {"nar": 0.0, "nir": 0.0},
                            "nitrogen": {
                                "no3": mg_to_mol(10, "nitrate"),
                                "no2": 0.0,
                                "n2": 0.0,
                            },
                            "doc": mg_to_mol(2, "DOC"),
                            "bio": {"alive": 0.0, "dead": 0.0},
                            "o2": mg_to_mol(8.0, "DO"),
                            "co2": 0.0,
                        },
                    },
                },
            }
        elif self.scenario == "bank_filtration_constant_oxygen":
            boundary_conditions = {
                "left": {
                    "inflow_concentration": {
                        "tf": {"narr": 0.0, "nnr": 0.0, "fnrp": 0.0},
                        "mrna": {"nar": 0.0, "nir": 0.0},
                        "enz": {"nar": 0.0, "nir": 0.0},
                        "nitrogen": {
                            "no3": mg_to_mol(10, "nitrate"),
                            "no2": 0.0,
                            "n2": 0.0,
                        },
                        "doc": mg_to_mol(2, "DOC"),
                        "bio": {"alive": 0.0, "dead": 0.0},
                        "o2": mg_to_mol(8.0, "DO"),
                        "co2": 0.0,
                    }
                }
            }
        elif (
            self.scenario == "bank_storage"
            or self.scenario == "groundwater_exfiltration"
        ):
            boundary_conditions = {
                "left": {
                    "inflow_concentration": {
                        "tf": {"narr": 0.0, "nnr": 0.0, "fnrp": 0.0},
                        "mrna": {"nar": 0.0, "nir": 0.0},
                        "enz": {"nar": 0.0, "nir": 0.0},
                        "nitrogen": {
                            "no3": mg_to_mol(10, "nitrate"),
                            "no2": 0.0,
                            "n2": 0.0,
                        },
                        "doc": mg_to_mol(2, "DOC"),
                        "bio": {"alive": 0.0, "dead": 0.0},
                        "o2": mg_to_mol(8.0, "DO"),
                        "co2": 0.0,
                    }
                },
                "right": {
                    "inflow_concentration": {
                        "tf": {"narr": 0.0, "nnr": 0.0, "fnrp": 0.0},
                        "mrna": {"nar": 0.0, "nir": 0.0},
                        "enz": {"nar": 0.0, "nir": 0.0},
                        "nitrogen": {
                            "no3": mg_to_mol(30, "nitrate"),
                            "no2": 0.0,
                            "n2": 0.0,
                        },
                        "doc": 0.0,
                        "bio": {"alive": 0.0, "dead": 0.0},
                        "o2": 0.0,
                        "co2": 0.0,
                    }
                },
            }
        else:
            assert False
        if self.scenario == "bank_storage":
            velocity_params = {
                "v": {
                    "amplitude": 1e-5,
                    "frequency": 1 / 86400,
                    "phase_shift": 0.0,
                    "mean_val": -1e-6,
                }
            }
            v = 1e-5
            dx = 0.02
            length = 4.0
        elif (
            self.scenario == "bank_filtration_fluctuating_oxygen"
            or self.scenario == "bank_filtration_constant_oxygen"
        ):
            velocity_params = {"v": 1e-5}
        elif self.scenario == "groundwater_exfiltration":
            velocity_params = {"v": -1e-5}
        else:
            assert False

        transport_params = {
            "dx": 0.02,
            "length": 4.0,
            "transported": {
                "tf": {"narr": 0.0, "nnr": 0.0, "fnrp": 0.0},
                "mrna": {"nar": 0.0, "nir": 0.0},
                "enz": {"nar": 0.0, "nir": 0.0},
                "nitrogen": {
                    "no3": 1.0,
                    "no2": 1.0,
                    "n2": 1.0,
                },
                "doc": 1.0,
                "bio": {"alive": 0.0, "dead": 0.0},
                "o2": 1.0,
                "co2": 1.0,
            },
            "dispersivity": 0.1,
            "porosity": self.porosity,
            "molecular_diffusion": 1e-9,
            **velocity_params,
            **boundary_conditions,
        }
        self.transport_params = transport_params

    @property
    def coords(self):
        return {
            "x": np.arange(
                self.transport_params["dx"] / 2,
                self.transport_params["length"],
                self.transport_params["dx"],
            )
        }

    def set_initial_values(self):
        if self.reaction_model.doc_release == "depth_dependent":
            B_0 = (
                np.exp(self.reaction_params["B_max"]["log"]) * np.exp(
                    -self.reaction_params["lambda_poc"] * self.coords["x"]
                )
            )
        elif self.reaction_model.doc_release == "constant":
            B_0 = 1e8
        else:
            assert False
        if (
            self.scenario == "bank_filtration_fluctuating_oxygen"
            or self.scenario == "bank_filtration_constant_oxygen"
        ):
            initial_values = {
                "tf": {"narr": 0.0, "nnr": 0.0},
                #"mrna": {"nar": 0.0, "nir": 0.0},
                "enz": {"nar": 0.0, "nir": 0.0},
                "nitrogen": {
                    "no3": mg_to_mol(0, "nitrate"),
                    "no2": 0.0,
                    "n2": 0.0,
                },
                "doc": mg_to_mol(2, "DOC"),
                "bio": {"alive": B_0, "dead": B_0 / 5},
                "o2": mg_to_mol(0, "DO"),
                "co2": 0.0,
            }
        elif (
            self.scenario == "groundwater_exfiltration"
            or self.scenario == "bank_storage"
        ):
            initial_values = {
                "tf": {"narr": 0.0, "nnr": 0.0},
                "enz": {"nar": 0.0, "nir": 0.0},
                "nitrogen": {
                    "no3": mg_to_mol(30, "nitrate"),
                    "no2": 0.0,
                    "n2": 0.0,
                },
                "doc": mg_to_mol(0, "DOC"),
                "bio": {"alive": B_0, "dead": B_0 / 5},
                "o2": mg_to_mol(0, "DO"),
                "co2": 0.0,
            }
        else:
            assert False
        initial_values = nested.extract_subdict(initial_values, self.reaction_model.state_levels)
        initial_values = nested.apply_operator_nested(
            initial_values,
            factors=self.reaction_params["c_norm"],
            operator=operator.truediv,
        )
        self.initial_values = initial_values

    @property
    def reaction_state_shapes(self):
        return {
            "tf": {"narr": (), "nnr": ()},
            "enz": {"nar": (), "nir": ()},
            "nitrogen": {
                "no3": (),
                "no2": (),
                "n2": (),
            },
            "doc": (),
            "bio": {"alive": (), "dead": ()},
            "o2": (),
            "co2": (),
        }

    def make_reaction_problem(self):
        rhs = self.reaction_model.generate_rhs_func()
        state_shapes = self.reaction_state_shapes
        param_shapes = nested.apply_func_nested(self.reaction_params, get_shape)
        self.reaction_problem = sunode.SympyProblem(
            params=param_shapes,
            states=state_shapes,
            rhs_sympy=rhs,
            derivative_params=(),
        )

    def make_transport_problem(self):
        if self.scenario == "bank_filtration_fluctuating_oxygen":
            kwargs = {
                "left_boundary": "sinusoidal",
                "velocity_func": velo.constant_velocity,
            }
        elif self.scenario == "bank_filtration_constant_oxygen":
            kwargs = {
                "left_boundary": "constant_concentration",
                "velocity_func": velo.constant_velocity,
            }
        elif self.scenario == "bank_storage":
            kwargs = {
                "left_boundary": "constant_concentration",
                "right_boundary": "constant_concentration",
                "velocity_func": velo.sinusoidal_velocity,
            }
        elif self.scenario == "groundwater_exfiltration":
            kwargs = {
                "left_boundary": "constant_concentration",
                "right_boundary": "constant_concentration",
                "velocity_func": velo.constant_velocity,
            }
        else:
            assert False

        self.transport_problem = transport.TransportProblem(
            reaction_problem=self.reaction_problem,
            transport_param_dims=self.transport_param_dims,
            coords=self.coords,
            **kwargs,
        )

    def make_solver(self, abstol=1e-8, reltol=1e-6):
        """Create ODE solver object.

        Parameters
        ----------
        abstol: float or numpy array, optional
            Absolute tolerance, it can be given as a single value for all
            states or as a numpy array of length <number of states>, specifying
            the tolerance for each state individually.
        reltol: float or numpy array, optional
            Relative tolerance, it can be given as a single value for all
            states or as a numpy array of length <number of states>, specifying
            the tolerance for each state individually.
        """
        self.solver = sunode.solver.Solver(
            self.transport_problem,
            sens_mode=None,
            solver="BDF",
            linear_solver="spgmr_finitediff",
            constraints=np.ones(self.transport_problem.n_states),
            abstol=abstol,
            reltol=reltol,
        )

    def run_simulation(self, t0, tvals):
        y0 = np.ones((), dtype=self.transport_problem.state_dtype)
        set_initial_values(self.initial_values, y0)
        self.solver.set_params_dict(
            {
                "transport": self.transport_params,
                "reaction": self.reaction_params,
            }
        )
        output = self.solver.make_output_buffers(tvals)
        self.solver.solve(t0=t0, tvals=tvals, y0=y0, y_out=output)
        return output, self.output_to_xarray(output, tvals)

    def output_to_xarray(self, output, time):
        solution = self.solver.as_xarray(time, output, unstack_params=False).copy(
            deep=True
        )
        solution["time"] = solution.time / 86400
        solution["time"].attrs["units"] = "d"
        solution["x"].attrs["units"] = "m"

        # strip the 'solution' prefix from the variable names
        name_mapping = {
            key: re.compile("solution_(.*)").search(key).group(1)
            for key in solution
            if "solution" in key
        }
        solution = solution[list(name_mapping.keys())].rename_vars(name_mapping)

        # unnormalize concentrations
        c_norm_flat = sunode.dtypesubset.as_flattened(
            self.reaction_model.default_params["c_norm"]
        )
        for keys, val in c_norm_flat.items():
            flat_key = "_".join(keys)
            solution[flat_key] *= val

        # Compute mRNA concentrations for the quasi-steady state case
        if self.reaction_model.model_args["mrna_at_quasi_steady_state"]:
            nested_solution = self.solver.as_xarray(time, output, unstack_params=False, unstack_state=False).copy(
                deep=True
            )
            rescaled_concentrations = sunode.dtypesubset.as_nested({
                keys: nested.getitem_nested(nested_solution["solution"].values, keys)
                * factor
                for keys, factor in c_norm_flat.items()
            })
            for gene in self.reaction_model.genes:
                qss_mrna_conc = reaction_model.qss_mrna_concentration_val(
                    gene,
                    p=self.reaction_params,
                    c=rescaled_concentrations,
                    nir_activation=self.reaction_model.model_args["nnr_activation"],
                    nitrite_toxicity=self.reaction_model.model_args["nitrite_toxicity"],
                    transcriptional_regulation_opt=self.reaction_model.model_args[
                        "transcriptional_regulation_opts"
                    ][gene],
                )
                solution = solution.assign(
                    {
                        f"mrna_{gene}": solution[f"enz_{gene}"].copy(data=qss_mrna_conc)
                    }
                )

        # Normalize mRNA and enzyme concentrations
        mrnas_enzs = [
            var_name for var_name in solution if "mrna" in var_name or "enz" in var_name
        ]
        for label in mrnas_enzs:
            variable, gene = re.match("(mrna|enz)_(nar|nir)", label).groups()
            norm_label = f"{variable}_norm_{gene}"
            solution[norm_label] = solution[label] / solution.bio_alive
            if variable == "enz":
                solution[norm_label] *= reaction_model.AVOGADRO_CONSTANT

        long_names = {
            "tf_narr": "NarR transcription factor",
            "tf_nnr": "NNR transcription factor",
            "tf_fnrp": "FnrP transcription factor",
            "nitrogen_no3": "nitrate",
            "nitrogen_no2": "nitrite",
            "nitrogen_n2": "N2 gas",
            "mrna_nar": "nar transcripts",
            "mrna_nir": "nir transcripts",
            "enz_nar": "NIR enzymes",
            "enz_nir": "NAR enzymes",
            "doc": "DOC",
            "o2": "oxygen",
            "co2": "CO2",
            "bio_alive": "living cells",
            "bio_dead": "dead cells",
            "mrna_norm_nar": "nar transcripts",
            "mrna_norm_nir": "nir transcripts",
            "enz_norm_nar": "NAR enzymes",
            "enz_norm_nir": "NIR enzymes",
        }
        units = {
            "tf_narr": "–",
            "tf_nnr": "–",
            "tf_fnrp": "–",
            "nitrogen_no3": "mol/L",
            "nitrogen_no2": "mol/L",
            "nitrogen_n2": "mol/L",
            "mrna_nar": "transcripts/L",
            "mrna_nir": "transcripts/L",
            "enz_nar": "mol/L",
            "enz_nir": "mol/L",
            "doc": "mol-C/L",
            "o2": "mol/L",
            "co2": "mol/L",
            "bio_alive": "cells/L",
            "bio_dead": "cells/L",
            "mrna_norm_nar": "transcripts/cell",
            "mrna_norm_nir": "transcripts/cell",
            "enz_norm_nar": "enzymes/cell",
            "enz_norm_nir": "enzymes/cell",
        }
        for label, name in long_names.items():
            if label in solution:
                solution[label].attrs["long_name"] = name
        for label, unit in units.items():
            if label in solution:
                solution[label].attrs["units"] = unit
        return solution

    def make_rate_expressions(self):
        e_accceptor_rates = (
            self.reaction_model.compute_electron_acceptor_consumption_rate()
        )
        return e_accceptor_rates._asdict()

    def make_rate_func(self, rate_expressions):
        return sunode.symode.lambdify.lambdify_consts(
            "_rate",
            argnames=["time", "state", "params"],
            expr=np.array(rate_expressions),
            varmap=self.reaction_problem._varmap,
            debug=False,
        )

    def get_flat_states(self, raw_output, solution):
        var_names = [
            "_".join(keys) for keys in self.reaction_problem.state_subset.paths
        ]
        y_flat = raw_output.reshape(
            (
                -1,
                self.transport_problem.n_reaction_states,
                self.transport_problem.n_cells,
            )
        )
        return xr.DataArray(
            data=y_flat,
            coords={"time": solution.time, "variable": var_names, "x": solution.x},
            dims=["time", "variable", "x"],
        )

    def get_flat_params(self):
        flat_reaction_params = self.solver.get_params()["reaction"][None].view(
            np.float64
        ).copy()
        return xr.DataArray(flat_reaction_params, dims=("parameter",))

    def compute_reaction_rates(self, raw_output, solution):
        def compute_rates_single_timepoint(t, flat_y, flat_params):
            params = flat_params.view(
                self.reaction_problem.params_subset.dtype
            )[0]
            rates_out = np.zeros(len(rate_expressions))
            state = flat_y.copy().view(self.reaction_problem.state_dtype)[0]
            compute_rates_inner(rates_out, t, state, params)
            return rates_out

        rate_expressions = self.make_rate_expressions()
        compute_rates_inner = self.make_rate_func(list(rate_expressions.values()))
        user_data = self.reaction_problem.make_user_data()
        self.reaction_problem.update_params(
            user_data, self.solver.get_params()["reaction"]
        )
        y_flat = self.get_flat_states(raw_output, solution)
        params_flat = self.get_flat_params()
        rates_vectorized = xr.apply_ufunc(
            compute_rates_single_timepoint,
            y_flat.time * 86400,
            y_flat,
            params_flat,
            input_core_dims=[tuple(), ("variable",), ("parameter",)],
            output_core_dims=[("rate",)],
            vectorize=True,
        )
        return rates_vectorized.assign_coords(rate=list(rate_expressions))


def combine_rates_and_states(solution, rates):
    rates_ds = (
        rates.to_dataset(dim="rate").rename_vars(
            {state: f"rate_{state}_norm" for state in rates.rate.values}
        )
        / solution.bio_alive
    )

    rates_states = xr.merge([solution, rates_ds])

    rates_states = rates_states.assign_coords(
        {"time_of_day": lambda x: (x.time * 24 + 8) % 24}
    )
    rates_states.time_of_day.attrs["units"] = "h"
    rates_states.time_of_day.attrs["long_name"] = "time of the day"
    return rates_states


def arange_rates_for_plotting(rates, solution):
    """Merge rates and transcript/enzyme concentrations into one dataset for plotting."""
    # Arange rate data
    substrate_to_gene_mapping = {
        "o2": None,
        "no3": "nar",
        "no2": "nir",
    }
    idx = rates.rate.to_index()
    genes = idx.map(substrate_to_gene_mapping)
    e_acceptor_consumption_rates = (
        rates.assign_coords(gene=genes)
        .swap_dims({"rate": "gene"})
        .rename("turnover_rate")
    )
    e_acceptor_consumption_rates.attrs["units"] = "mol/L/s"
    rates_norm = (e_acceptor_consumption_rates / solution.bio_alive).rename(
        "turnover_rate_norm"
    )
    rates_norm.attrs["units"] = "mol/cell/s"
    # Arrange mRNA and enzyme data
    mrna_enz_data = {}
    for var in ["mrna", "enz"]:
        var_names = []
        genes = []
        for var_name in list(solution):
            pattern = re.compile(f"{var}_([a-z]*)$")
            m = pattern.match(var_name)
            if m:
                var_names.append(var_name)
                genes.append(m.groups()[0])
        mrna_enz_data[var] = xr.concat(
            [solution[var] for var in var_names], dim=pd.Index(genes, name="gene")
        ).transpose()
    concentrations = xr.concat(
        mrna_enz_data.values(),
        dim=pd.Index(["mRNA", "enzymes"], name="variable"),
        combine_attrs="drop_conflicts",
    ).rename("concentration")
    rates_ds = xr.merge(
        [
            concentrations,
            (concentrations / solution.bio_alive).rename("concentration_norm"),
            e_acceptor_consumption_rates.sel(gene=mrna_enz_data["mrna"].gene),
            rates_norm.sel(gene=mrna_enz_data["mrna"].gene),
        ]
    )
    rates_ds = rates_ds.assign_coords({"time_of_day": lambda x: (x.time * 24 + 8) % 24})
    rates_ds.time_of_day.attrs["units"] = "h"
    rates_ds.time_of_day.attrs["long_name"] = "time of the day"
    return rates_ds
