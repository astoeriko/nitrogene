---
jupyter:
  jupytext:
    formats: ipynb,md
    main_language: python
    notebook_metadata_filter: -kernelspec
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.11.5
---

```python
from scipy import stats
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr

import nitrogene.utils
```

# Choosing an appropriate Data Error Model for the Data of Qu et al. (2015)

## The Problem of Modeling Data Errors
In order to compute the liklihood of the ode model with respect to the data, we need to come up with a model for the error of the measured concentrations.
We therefore need an estimate of the variance of the data around the simulated mean concentrations.

### Absolute Data Errors
When applying errors on the non-transformed data, we get absolute data errors.
An absolute error means that for a concentration of 2 it is equally likely to measure a concentration of 1 and of 3.
This is problematic for several reasons:
- An absolute error on concentration data implies that we can measure negative concentrations. From a physical point of view that does not really make sense as concentrations cannot be negative.
- If we choose a constant data error on the non-transformed data, large and small concentrations will have the same uncertainty, which is not what we usually observe.
  Usually, we cannot measure large concentrations with the same absolute accuracy as small concentrations.
- One way to deal with that would be to estimate a different absolute data error for each measurement timepoint. However, how do we estimate the time varying data errors?
  We could use the triplicate measurements but will likely underestimate the error. It might also create a high variability of the data error that cannot be explained by different errror mechanisms but simply by not having enough samples to estimate the error. (Imagine the replicates being close together in one measurement but having replicates with a further spread in the next one.) It also does not solve the problem of negative measurements.

### Relative Data Errors
If we apply a constant error on the log scale instead, it will be a relative error in the measurement space.
An example: If we have a true concentration of 2, it would be equally likely to measure a concentration of 3 and 1.5.
This makes more sense for most measurements, but not when concentrations approach the limit of quantification.
At a certain point the error does not become smaller, when concentrations become smaller.

That is, the actual data error is neither completely relative nor absolute.
Let's look at some example data.

```python
# Read in data
ds = xr.open_dataset("../../experimental_data/data_Qu_2015.nc")
data = nitrogene.utils.process_data_for_likelihood(ds, transform="log")
```

If we look a the non-transformed N2O concentratins we see that the spread in the data is much smaller when concentrations are small. That is, the error is not absolute, there is a strong relative component.

```python
ds["N2O"].dropna("time").plot.line("x", x="time");
```

If we look at log transformed data, we notice that the spread is larger for small concentrations. That shows us that the data error is not purely relative.

So both, the log transformation and the identitiy transformation can explain some aspects of the data error, but neither yields a good model for all data.
So maybe we would need something “in between” those two transformations.

```python
np.log(ds["N2O"]).dropna("time").plot.line("x", x="time")
plt.ylabel("log N2O concentration");
```

## The Box-Cox Transformation
The Box-Cox transformation is defined as [1]
$$
y = 
\begin{cases}
\frac{x^\lambda - 1}{\lambda} \text{ if } \lambda > 0, \\
\log(x), \text{ if } \lambda = 0,
\end{cases}
$$
It requires data to be positive.
When, $\lambda$ is 1, the transform corresponds to the identity transformation (i.e. no transform, the data stay the same), whereas for $\lambda=0$, it is the log transform.
For values of $\lambda$ between 0 and 1, it is something “in between” identity and log transformation.

When we have data that is already log transformed ($z=\log(x)$), we can compute the Box-Cox transformed data as follows:
$$
y = 
\begin{cases}
\frac{\exp(z\lambda) - 1}{\lambda} \text{ if } \lambda > 0, \\
z, \text{ if } \lambda = 0,
\end{cases}
$$

```python
def inv_boxcox(x, lmbda):
    if lmbda == 0:
        return np.exp(x)
    else:
        return (x * lmbda + 1) ** (1 / lmbda)
```

```python
transformed_data = stats.norm.rvs(loc=-3, scale=0.15, size=100_000)
non_transformed_data = inv_boxcox(transformed_data, lmbda=0.1)
```

When data are normally distributed in the Box-Cox “space”...

```python
transformed_data = sns.distplot(transformed_data)
```

...they have a Box-Cox distribution in “normal” space. It looks a bit like something in between normal distribution and lognormal distribution, depending on the value of $\lambda$.

```python
sns.distplot(non_transformed_data)
```

```python
# Lambda can be estimated from the data with scipy (but it does not seem to be very robust, particularly not for my data).
lmbdas, ps = stats.boxcox_normplot(non_transformed_data, 0, 1, plot=plt)
```

```python
lmbdas[ps.argmax()]
```

```python
stats.boxcox_normmax(non_transformed_data)
```

## Estimating the Data Error of the Box-Cox Transformed Data

We want to choose lambda in a way that the data standard deviation does not depend on the concentration value anymore, but is constant.
This is done manually by trying different $\lambda$ values and verifying the results with plots.

After the Box-Cox transformation, the data should still have different means but the same standard deviation.
The standard deviation of the transformed measurement data is therefore estimated after subtracting the mean value.
$$\sigma = std\left(boxcox(c) - mean(boxcox(c)\right)$$
When computing this standard deviation we have to account for the fact that we already spend several degrees of freedom to estimate the mean values.
Therefore, the normalization constant is not the number of measurements but the number of measurements (as it would be if we subtracted the true mean) but the number of measurements minus the number of mean values we already calculated (which corresponds to the number of measurement time points).

```python
var = "mrna_norm_nir"
orig = data[var]["measurements"]
orig[np.isneginf(orig)] = np.nan
lmbda_all = {
    "gas_o2": 0.18,
    "gas_co2": 0.18,
    "nitrogen_no2": 0.4,
    "gas_no": 0.3,
    "gas_n2o": 0.18,
    "gas_n2": 0.18,
    "mrna_norm_nir": 0.01,
    "mrna_norm_nar": 0.1,
    "mrna_norm_nor": 0.15,
    "mrna_norm_nos": 0.2,
    "bio_alive": 0.05,
}
lmbda = lmbda_all[var]

dat = np.expm1(lmbda * orig) / lmbda
if var == "nitrogen_no2":
    dat_norm = dat - np.nanmean(dat, 1)[:, None]
    # Nitrite data have this strange exponential decay behaviour
    # in a concentration range that is most likely below the quantification limit.
    # If I keep the data, I don't want to subtract the mean of this strange trend
    # because that leads to super small standard deviations, suggesting that we are absolutely
    # sure about the quantitative values of these concentrations.
    # I would rather take their mean value as a background concentration and treat the deviation
    # as an error which likely arises from the measurements. E.g. samples have been thawed
    # all at the same time and then processes sequentially, leading to an exponential decay...
    # Subtracting the mean over all these concentrations below the quantification limit
    # gives me a more realistic estimate of the errors.
    dat_norm[20:, :] = dat[20:, :] - np.nanmean(dat[20:, :])
else:
    dat_norm = dat - np.nanmean(dat, 1)[:, None]
dat_std = np.nanstd(dat_norm, ddof=len(dat_norm))
# Don't consider outliers for calculating the standard deviation.
# They are mostly related to an error in timing
dat_std = dat_norm[(np.abs(dat_norm) < 2 * dat_std)].std(ddof=len(dat_norm))
```

This is the original (log transformed) data. The spread in the data is often larger for small concentartions than for large concentrations. That means that the relative error of the data is larger for small concentrations.

```python
plt.plot(orig)
plt.xlabel("timepoint")
plt.ylabel("log(c)");
```

This is the Box-Cox transformed data. Ideally, now the spread in the data should be constant.

```python
plt.plot(dat)
plt.xlabel("timepoint")
plt.ylabel("boxcox(c)");
```

This is easier to see if we plot the data after substracting their mean value (over the replicates, i.e. the mean still varies in time).

```python
plt.plot(dat_norm)
plt.xlabel("timepoint")
plt.ylabel("boxcox(c) - mean(boxcox(c))");
```

Let's check how the distribution of these centered, transformed data looks like.

```python
sns.distplot(dat_norm.ravel(), rug=True)
plt.xlabel("boxcox(c) - mean(boxcox(c))");
```

Now let's plot the standard deviations against the mean values to see if there is a correlation. It should be constant.

```python
sns.regplot(dat.mean(1), dat.std(1))
plt.xlabel("mean(boxcox(c))")
plt.ylabel("std(boxcox(c))");
```

For comparison, that is what the plot looks like with the log transformed data. (Honestly, the picture is not that clear from this plot, but the later plots help to see the difference.)

```python
sns.regplot(orig.mean(1), orig.std(1))
plt.xlabel("mean(log(c))")
plt.ylabel("std(log(c))");
```

### What would the data look like with the chosen $\lambda$ and calculated error?
Let's look first at how simulated measurement data look like for different concentartions.
Measured concentartions are assumed to be the sum of the ODE solution and a background value:
$$c = c_{ode} + c_{background}$$
which both are defined on a log scale.
Let's define $y$ as the Box-Cox transform of $c$.
Then, the measurements in Box-Cox “space” follow a normal distribution around $y$ with the a standard deviation $\sigma$.
Actually, it is not a normal distribution but a truncated normal distirbution that is cut off at $-\frac{1}{\lambda}$ as that is the minimum value that Box-Cox transformed data can take.

```python
mu_all = {
    "gas_co2": -12,
    "gas_o2": -17,
    "nitrogen_no2": -18,
    "gas_no": -24,
    "gas_n2o": -24,
    "gas_n2": -24,
    "mrna_norm_nar": -17,
    "mrna_norm_nir": -17,
    "mrna_norm_nor": -17,
    "mrna_norm_nos": -17,
    "bio_alive": 1,
}
mu = mu_all[var]

# log concentration values
x_lin = np.linspace(np.maximum(np.nanmin(orig), -30) - 5, np.nanmax(orig), 1000)
# log concentrations with background
y_lin = np.logaddexp(mu, x_lin)
# box-cox concentartions without error
y_lin_box = np.expm1(lmbda * y_lin) / lmbda
# box-cox concentrations with error
data_error = dat_std * stats.truncnorm(a=-1 / lmbda, b=np.inf).rvs(
    size=(200, len(y_lin_box))
)
y_lin_box_sim = data_error + y_lin_box
# concentrations with error in log space
y_lin_sim = np.log1p(lmbda * y_lin_box_sim) / lmbda
```

```python
# Plot the distribution of the data error
sns.distplot(data_error)
plt.axvline(-1 / lmbda)
```

In the following plot we can see that the relative error is larger for small concentartions than for large concentrations.

```python
plt.plot(x_lin, y_lin_sim.T)
plt.xlabel("ODE concentration")
plt.ylabel("Measured log concentrations");
```

Now let's check how the simulated measurements would look like with this error model (taking the data mean values as the simulated concentration with background):

```python
y = np.nanmean(orig, 1)
y_box = np.expm1(lmbda * y) / lmbda
y_box_sim = (
    dat_std * stats.truncnorm(a=-1 / lmbda, b=np.inf).rvs(size=(200, len(y_box)))
    + y_box
)
y_sim = np.log1p(lmbda * y_box_sim) / lmbda
```

```python
plt.plot(y_sim.T, alpha=0.1, color="grey")
plt.plot(orig, "d", color="black")
plt.xlabel("timepoint")
plt.ylabel("log(c)");
```

```python
plt.plot(y_box_sim.T, alpha=0.1, color="grey")
plt.plot(dat, "d", color="black")
plt.xlabel("timepoint")
plt.ylabel("boxcox(c)");
```

```python
plt.plot(np.exp(y_sim.T), alpha=0.1, color="grey")
plt.plot(np.exp(orig), "d", color="black")
plt.xlabel("timepoint")
plt.ylabel("concentration");
```

Compare the real with the simulated Box-Cox transformed, centered concentrations.

```python
plt.plot((y_box_sim - np.nanmean(dat, 1)).T, color="grey", alpha=0.1)
plt.plot(dat_norm, "d")
plt.xlabel("timepoint")
plt.ylabel("boxcox(c) - mean(boxcox(c))");
```

## Discussion
Overall, assuming a constant data error for the Box-Cox transformed data seems to yield a reasonable error model.

Of course, it can not explain all patterns in observed errors:
- For example, one commonly observed pattern in the dataset is that the error becomes large when there are large changes in concentration.
  This happens when one of the replicates reacts a bit earlier or later than the others (time shift). In other words, there is actually an error in the timing.
- A similar, but slightly different patterns can be observed in the N2 data. Here, the concentartion increase seems to be overall faster in one of the replicates.
  This leads to a systematic increase of the standard deviations between and .
  The pattern would be difficult to explain by mere measurement errors but seems to be caused by the natural variability of the biological replicates.
  
The current error model cannot account for such effects so that it will likely underestimate errors in such situations.


## References
1. Box, G. E. P., and D. R. Cox. “An Analysis of Transformations.” Journal of the Royal Statistical Society. Series B (Methodological) 26, no. 2 (1964): 211–52.

2. Qu, Zhi, Lars R. Bakken, Lars Molstad, Åsa Frostegård, and Linda L. Bergaust. “Transcriptional and Metabolic Regulation of Denitrification in  Paracoccus denitrificans Allows Low but Significant Activity of Nitrous Oxide Reductase under Oxic Conditions.” Environmental Microbiology 18, no. 9 (November 16, 2015): 2951–63. https://doi.org/10.1111/1462-2920.13128.

