---
jupyter:
  jupytext:
    notebook_metadata_filter: -kernelspec
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.7
---

```python
import os
import string
import textwrap
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import xarray as xr
import nitrogene.plotting
import pint_xarray
import netCDF4
import nesttool

import nitrogene.reactive_transport_model as reactive_transport_model
```

```python
sns.set_theme(
    context="paper",
    palette=nitrogene.plotting.color_palettes["paul_tol_high_contrast"],
    rc=nitrogene.plotting.paper_rc,
)
```

<!-- #region tags=[] -->
# Sensitivity Analysis

The sensitivity analysis consists of three basic steps:

1. Computing local sensitivities by numerical differentiation,
2. conducting a principal component analysis (PCA) in order to identify influential parameters, and
3. a scenario analysis where we run simulations with an alternative parameter set, in which influential parameters were perturbed.

## Read in the simulation data
Sensitivities are computed in the notebook for the individual scenarios, so here we only need to read in the results.
<!-- #endregion -->

```python
plot_path = "../../plots/groundwater_river_interface/"
data_path = "../../output_data/gw_river_denitrification/"
```

```python
sens_path = os.path.join(data_path, "normalized_sensitivities.nc")
sensitivities_dataset = netCDF4.Dataset(sens_path)
```

```python
normalized_sensitivities = {
    scenario: xr.open_dataset(sens_path, group=scenario)
    for scenario in sensitivities_dataset.groups
}
```

## Spatially integrated sensitivities
The sensitivities are (1-D) spatial field because concentrations and reaction rates depend on the distance from the river. In order to agggregate the sensitivities, we integrate their absolute value in space.

```python
def integrate_sensitivity(sens):
    return np.abs(sens).fillna(0).integrate("x")
```

```python
integrated_sensitivities_norm = {
    scenario: integrate_sensitivity(sens)
    for scenario, sens in normalized_sensitivities.items()
}
```

```python
drop_vars = [var for var in integrated_sensitivities_norm if "norm" in var]
```

```python
time_averaged_integrated_sensitivities = xr.concat(
    [
        sens.mean("time") if "time" in sens.dims else sens
        for sens in integrated_sensitivities_norm.values()
    ],
    dim=pd.Index(integrated_sensitivities_norm.keys(), name="scenario"),
    coords="minimal",
    compat="override",
)
```

```python
import matplotlib.cm
from matplotlib.colors import Normalize
```

```python
data = (
    time_averaged_integrated_sensitivities.fillna(0)
    .drop_vars(drop_vars)
    .to_array(name="integrated_sensitivity")
    .to_dataframe()["integrated_sensitivity"]
)
fg = sns.FacetGrid(data.reset_index(), col="scenario", gridspec_kws=dict(right=0.9, wspace=0.02))
cbar_ax = fg.fig.add_axes([0.92, 0.15, 0.03, 0.7])


def draw_heatmap(columns, rows, values, **kwargs):
    data = kwargs.pop("data")
    d = data.pivot(index=rows, columns=columns, values=values)
    sns.heatmap(d, **kwargs)


vmax = fg.data.integrated_sensitivity.max()
fg.map_dataframe(
    draw_heatmap,
    columns="variable",
    rows="parameter",
    values="integrated_sensitivity",
    cbar=True,
    cbar_ax=cbar_ax,
    vmin=0,
    vmax=vmax,
    xticklabels=False,
    yticklabels=False,
    cbar_kws={'label': 'normalized sensitivity'}
)
```

## Principal Component Analysis

```python
from sklearn.decomposition import PCA
```

```python
n_components = None
pca_all_scenarios = {}
for scenario, sens_norm in normalized_sensitivities.items():
    stacked_vars = sens_norm.drop_vars(drop_vars).to_array()
    state_dims = list(set(stacked_vars.dims) - {"parameter"})
    X = stacked_vars.stack(space_state_var=state_dims).fillna(0).values.T
    pca_all_scenarios[scenario] = PCA(n_components).fit(X)
```

### Explained variance

```python
def extract_variance_ratio(pca):
    return xr.DataArray(
        pca.explained_variance_ratio_ * 100,
        dims=("principal_component",),
        name="explained_variance",
        attrs=dict(units="%", long_name="explained variance"),
        coords={
            "principal_component": (
                "principal_component",
                pd.RangeIndex(
                    pca.n_components_,
                ),
                dict(long_name="principal component"),
            )
        },
    )
```

```python
variance_ratios = xr.concat(
    [
        extract_variance_ratio(pca).assign_coords(scenario=scenario)
        for scenario, pca in pca_all_scenarios.items()
    ],
    dim="scenario",
)
```

```python
scenario_names = {
    "groundwater_discharge": "groundwater discharge",
    "bank_filtration_constant_bc": "bank filtration (constant O₂)",
    "bank_filtration_fluctuating_bc": "bank filtration (periodic O₂)",
    "bank_storage": "bank storage",
}
wrapped_scenario_names = {
    key: "\n".join(textwrap.wrap(val, width=20)) for key, val in scenario_names.items()
}
```

```python
g = variance_ratios.plot.step(where="mid", col="scenario", size=1.8)
for scenario, a in zip(g.col_names, g.axes.flat):
    a.set_title(scenario_names[scenario])
g.fig.savefig(os.path.join(plot_path, "sensitivity_pca_explained_variance.pdf"))
```

### Principal directions
We identify influential parameters by checking which parameters have large contributions to the first principal component (which explains nearly all of the variance).

```python
def extract_principal_directions(pca, parameter_names=None):
    coords = {"principal_component": pd.RangeIndex(pca.n_components_)}
    if parameter_names is not None:
        coords["parameter"] = parameter_names
    return xr.DataArray(
        pca.components_,
        dims=("principal_component", "parameter"),
        coords=coords,
    )
```

```python
principal_directions = xr.concat(
    [
        extract_principal_directions(
            pca, parameter_names=normalized_sensitivities[scenario].parameter
        ).assign_coords(scenario=scenario)
        for scenario, pca in pca_all_scenarios.items()
    ],
    dim="scenario",
)
```

```python tags=[]
g = sns.catplot(
    x="weight",
    y="parameter",
    data=principal_directions.isel(principal_component=0)
    .to_dataframe(name="weight")
    .reset_index(),
    aspect=0.1,
    col="scenario",
    height=11,
    kind="bar",
    palette=nitrogene.plotting.color_palettes["paul_tol_bright"],
    facet_kws=dict(
        gridspec_kws=dict(left=0.08, right=0.98, bottom=0.4, top=0.9, wspace=0.3)
    ),
)
latex_labels = [
    nitrogene.plotting.get_latex_label("_".join(param_string.split("-")[1:]))
    for param_string in g.data.set_index("scenario")
    .loc["bank_storage"]
    .parameter.values
]
latex_labels = [
    f"$\\log({label[1:-1]})$" if not "log" in label else label for label in latex_labels
]
g.set_yticklabels(latex_labels, size=8, rotation=0)
for scenario, a in g.axes_dict.items():
    a.set_title(wrapped_scenario_names[scenario])
g.fig.savefig(
    os.path.join(plot_path, "sensitivity_pca_principal_component.pdf"),
    bbox_inches="tight",
)
```

## Scenario Analysis


### Parameter scaling factors

```python
weights = principal_directions.isel(principal_component=0)
h = -2
factors = np.exp(h * weights)
```

### Read in the solutions with perturbed parameters

```python
perturbed_solution_dataset = netCDF4.Dataset(os.path.join(data_path, "solution_alternative_params.nc"))
```

```python
solutions_alternative_params = {
    scenario: xr.open_dataset(
        os.path.join(data_path, "solution_alternative_params.nc"), group=scenario
    )
    for scenario in perturbed_solution_dataset.groups
}
```

```python
def convert_solution_units(solution):
    return (
        solution.assign(
            doc=lambda x: x.doc.assign_attrs(units="M"),
            rate_no3=lambda x: x.rate_no3.assign_attrs(units="mol/L/s"),
            rate_no2=lambda x: x.rate_no2.assign_attrs(units="mol/L/s"),
        )
    ).map(nitrogene.plotting.convert_concentration_units)
```

```python
solution_alternative_params_converted_units = {
    scenario: convert_solution_units(solution)
    for scenario, solution, in solutions_alternative_params.items()
}
```

### Concentration distributions with the alternative parameters

```python
x_selection = slice(0, 4)
plotter = nitrogene.plotting.MultipleScenarioConcentrationDistributionPlot(
    scenario_names=[
        "groundwater discharge",
        "bank filtration",
        "bank storage",
    ],
    flow_directions=[
        "<-",
        "->",
        "<->",
    ],
    periodic_solutions=[
        None,
        None,
        solution_alternative_params_converted_units["bank_storage"].sel(x=x_selection),
    ],
    constant_solutions=[
        solution_alternative_params_converted_units["groundwater_discharge"]
        .isel(time=0, drop=True)
        .sel(x=x_selection),
        solution_alternative_params_converted_units["bank_filtration_constant_bc"]
        .isel(time=0, drop=True)
        .sel(x=x_selection),
        None,
    ],
    subplots_kw=dict(
        sharey="row",
        figsize=(7, 8),
        gridspec_kw=dict(
            hspace=0.35,
            wspace=0.1,
            top=0.9,
            height_ratios=[0.5, 2, 2, 2, 2, 2, 2],
            right=0.8,
        ),
    ),
    title_kw=dict(size="large", pad=10),
    add_fig_legend=[False, False, False],
    subplot_legend_kw={
        "loc": "upper left",
        "bbox_to_anchor": (1.05, 0, 1, 1),
        "frameon": False,
    },
)
plotter.plot()
plotter.fig.align_labels()
# sns.despine(bottom=True, left=True)
legends = [a.get_legend() for a in plotter.axes[1:, -1].flat]
for legend, row_label in zip(legends, string.ascii_lowercase):
    legend.set_title(f"{row_label}) " + legend.get_title().get_text())

plotter.fig.savefig(
    os.path.join(
        plot_path, "concentration_distributions_all_scenarios_alternative_params.pdf"
    ),
    bbox_inches="tight",
)
```

### Analyze the transcript–rate relationship for the perturbed parameter set

```python
# Arange the data for plotting
rates_alternative_params = {}
rates_ds_alternative_params = {}
for scenario, solution in solutions_alternative_params.items():
    rates_alternative_params[scenario] = xr.concat(
        [solution["rate_no3"], solution["rate_no2"]],
        dim=pd.Index(["no3", "no2"], name="rate"),
        combine_attrs="drop_conflicts",
    ).rename("rate")
    rates_ds = reactive_transport_model.arange_rates_for_plotting(
        rates_alternative_params[scenario],
        solution_alternative_params_converted_units[scenario],
    )
    rates_ds_alternative_params[scenario] = rates_ds.sortby("gene")

average_rates_ds_all_scenarios = xr.concat(
    [r.mean("time") for r in rates_ds_alternative_params.values()],
    dim=pd.Index(rates_ds_alternative_params.keys(), name="scenario"),
)
average_rates_ds_all_scenarios["turnover_rate"].attrs.update(
    units="mol/L/s", long_name="reaction rate"
)

average_rates_ds_all_scenarios = average_rates_ds_all_scenarios.assign_coords(
    scenario_long_name=average_rates_ds_all_scenarios.scenario.to_series().map(
        scenario_names
    )
)
```

```python
# Scatter plot of reaction rates against transcript concentrations
gene_labels = {"nar": "narG", "nir": "nirS"}
row_labels_enz = {
    "nar": "NAR\n" + r"$\mathregular{{NO_3}^–}\, \to\, \mathregular{{NO_2}^–}$",
    "nir": "NIR\n" + r"$\mathregular{{NO_2}^–}\, \to\, \mathregular{N_2}$",
}
g = (
    average_rates_ds_all_scenarios.assign_coords(
        scenario=average_rates_ds_all_scenarios.scenario.indexes["scenario"].map(
            wrapped_scenario_names
        ),
        gene=average_rates_ds_all_scenarios.gene.indexes["gene"].map(row_labels_enz),
    )
    .sel(variable="mRNA")
    .assign(concentration=lambda x: x.concentration * 1e-9)
    .map(nitrogene.plotting.convert_concentration_units)
).plot.scatter(
    x="concentration",
    y="turnover_rate",
    col="scenario",
    row="gene",
    hue="x",
    sharex="row",
    sharey="row",
    figsize=(6, 3.5),
    cmap="crest",
    s=20,
    add_guide=False,
)
g.set_titles(maxchar=100, template="{value}")
g.set_xlabels("")
g.fig.supxlabel("transcript concentration [$10^9\,$transcripts/L]")

# Customize the position of the row labels
for label, a in zip(g.row_labels, g.axes[:, 0]):
    left, lower, width, height = a.axes.get_position().bounds
    label.set(
        anncoords="figure fraction",
        rotation="horizontal",
        horizontalalignment="center",
        linespacing=1.7,
        position=(0.075, lower + height / 2),
    )
# Make space for the new row labels
g.fig.subplots_adjust(left=0.25, right=1.05)
g.add_colorbar(label="x [m]", anchor=(-0.1, 0.5))
# sns.despine(bottom=True, left=True)
g.fig.savefig(
    f"{plot_path}/rate_correlation_absolute_values_alternative_params.pdf",
    bbox_inches="tight",
)
```
