---
jupyter:
  jupytext:
    formats: ipynb,md
    notebook_metadata_filter: -kernelspec
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.7
---

# Bank Storage (BS)
In this scenario, the direction of groundwater flow reverses in a daily cycle. As a result, groundwater discharge and river water infiltration alternate, and the interface to the river is alternately oxic and anoxic.

```python
import operator
import os
import re
import textwrap

import arviz as az
import holoviews as hv
import hvplot.xarray
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import sunode
import sympy as sym
import xarray as xr

import nitrogene.utils
import nitrogene.plotting
import nitrogene.reactive_transport_model as reactive_transport_model
import nesttool
```

```python
sns.set_theme(
    context="paper",
    palette=nitrogene.plotting.color_palettes["paul_tol_high_contrast"],
    rc=nitrogene.plotting.paper_rc,
)
```

```python
%matplotlib inline
```

```python
plot_path = "../../plots/groundwater_river_interface/bank_storage"
```

## Model setup

```python
tr = az.from_netcdf(
    "../../traces/nuts_trace_pymc3-ext_2021_02_05_nar_nir_tf_activate_nnr_by_no2_only.nc"
).posterior
```

```python
exclude_patterns = [
    "c_",
    "mu_",
    "flat",
    "measure_error",
    "background",
    "y0",
    "r_cfix_o2_log",
]
var_names = [var for var in tr if not any(exlude in var for exlude in exclude_patterns)]
name_map = nitrogene.utils.make_parameter_name_map(tr, var_names)
```

```python
reaction_model_args = {
    "gas_phase": "none",
    "mrna_at_quasi_steady_state": True,
    "classical_monod": False,
    "two_step_model": True,
    "cell_decay": True,
    "growth_substrates": ["o2", "no3", "no2"],
    "growth_yield_units": "cells/mol_DOC",
    "growth_model": "logistic",
    "carbon_for_growth": True,
    "doc_rate_limitation": True,
    "enzymes_to_simulate": ["nir", "nar"],
    "transcription_self_inhibition": False,
    "nitrite_toxicity": False,
    "transcriptional_regulation_opts": {
        "nar": "transcription_factors",
        "nir": "transcription_factors",
    },
    "doc_release": "depth_dependent",
}
```

```python
model = reactive_transport_model.Model(
    calibrated_reaction_params=tr[var_names]
    .apply(lambda x: np.exp(x) if "n_hill" in x.name else x)
    .median(dim=("chain", "draw")),
    scenario="bank_storage",
    parameter_name_map=name_map,
    reaction_model_args=reaction_model_args,
    solver_kw={"abstol": 1e-10, "reltol": 1e-8},
)
```

```python
%%time
# Run the equation without fluctuations to steady state first
model.transport_params["v"]["amplitude"] = 0
time = np.linspace(0, 1000, 1000) * 86400
raw_output_constant_v, solution_constant_v = model.run_simulation(t0=0, tvals=time)
```

```python
lib = sunode._cvodes.lib
ffi = sunode._cvodes.ffi
```

```python
lib.CVodeSetMaxNumSteps(model.solver._ode, 2000)
```

```python
%%time
# Solve the equation with velocity fluctuations
y0 = sunode.dtypesubset._as_dict(
    raw_output_constant_v[-1, :].view(model.transport_problem.state_dtype)
)
y0 = nesttool.apply_func_nested(y0, np.squeeze)
model.initial_values = y0
model.transport_params["v"]["amplitude"] = 1e-5
time = (
    np.concatenate(
        (
            np.linspace(0, 500, 500, endpoint=False),
            np.arange(500, 503, 1 / 240),
        )
    )
    * 86400
)
raw_output, solution = model.run_simulation(t0=0, tvals=time)
```

```python
times_per_day = int(1 // solution.time.diff("time").values[-1])
```

```python
times_per_day
```

## Concentration distribution over time

```python
stable_slice = slice(-3 * times_per_day, None)
stable_solution = solution.assign_coords(
    time=solution.time - solution.time.isel(time=-3 * times_per_day)
).isel(time=stable_slice)
```

```python
stable_solution.nitrogen_no2.plot(cmap="cividis", vmin=0)
```

```python
stable_solution.mrna_norm_nir.plot(cmap="cividis")
```

```python
plot_kwargs = {"width": 400, "height": 150}
selection = stable_solution.isel(x=slice(None, None, 4))
plot_all = nitrogene.plotting.concentration_distribution_animation(
    ds=selection, plot_kwargs=plot_kwargs
)
plot_all
```

```python
hv.save(
    plot_all,
    plot_path + "periodic_denitrification_no_carbon_limitation.html",
    fmt="scrubber",
    title="Concentration distributions",
)
```

```python
plotter = nitrogene.plotting.plot_periodic_constant_single_scenario(
    periodic_solution=stable_solution,
    subplots_kw=dict(
        figsize=(6, 3),
        gridspec_kw=dict(hspace=0.25, wspace=0.6, top=0.9),
    ),
    legend_kw=dict(frameon=False),
    periodic_label="solution",
)
plotter.fig.savefig(os.path.join(plot_path, "concentration_distribution.pdf"))
```

```python
path = "../../output_data/gw_river_denitrification/bank_storage/"
stable_solution.to_netcdf(os.path.join(path, "solution.nc"))
```

## Shift in the location of the denitrification zone
Compute the location of the denitrification zone based on the peak nitrite concentrations.

```python
idx = stable_solution.sel(x=slice(0.9, None)).nitrogen_no2.argmax("x")
peak_location = (
    stable_solution.sel(x=slice(0.9, None))
    .x.isel(x=idx)
    .rename("nitrite peak location")
)
peak_location.plot(label="peak location")
plt.axhline(y=peak_location.mean(), label="mean value", linestyle="--")
plt.legend()
```

## Compute the maximum advective depth of penetration

In order to know how far the river maximally penetrates the aquifer by advection, we integrate the positive part of the velocity function over time.

```python
velocity_func = model.transport_problem.velocity_func
v = velocity_func(
    solution.time.values * 86400, model.solver._user_data.transport_params
)
velocity = (
    (xr.DataArray(v, dims="time", coords={"time": solution.time}) * 86400)
    .rename("v")
    .assign_attrs(units="m/d")
)
```

```python
last_period = velocity.isel(time=slice(-times_per_day, None))
positive_part = last_period.where(lambda x: x > 0, drop=True)
maximum_penetration_depth = (positive_part).integrate("time").assign_attrs(units="m")
maximum_penetration_depth
```

## Compute the depth of penetration of the river water

```python
velocity.isel(time=slice(-3 * times_per_day, None)).plot()
plt.fill_between(positive_part.time, positive_part.data, alpha=0.5)
```

## Compute reaction rates

```python
rates = model.compute_reaction_rates(raw_output[stable_slice, :], stable_solution)
rates_states = reactive_transport_model.combine_rates_and_states(stable_solution, rates)
```

```python
rates_ds = reactive_transport_model.arange_rates_for_plotting(rates, stable_solution)
rates_ds = rates_ds.assign_coords(
    datetime=(
        "time",
        pd.to_datetime(
            rates_ds.time.values,
            unit="d",
        ),
    )
)
rates_ds = rates_ds.sortby("gene")
```

## Relationship between reaction rates and transcripts/enzymes

```python
def annotate_location(x, rates_ds, ax, text_xy=None, **annot_kw):
    annot_data = (
        rates_ds.sel(variable="enzymes", gene="nir")
        .sel(x=x, method="nearest")
        .isel(time=slice(-4 * times_per_day, None, 3))
    )
    actual_x = annot_data.x
    annot_x = annot_data["concentration"].mean("time")
    annot_y = annot_data["turnover_rate"].mean("time")
    return ax.annotate(
        text=f"x = {actual_x.values:1.2f} m",
        xy=(annot_x.values, annot_y.values),
        xytext=text_xy,
        **annot_kw,
    )
```

```python
# Plotting
row_labels = {
    "nar": "$\\mathit{narG}$\n"
    + r"$\mathregular{{NO_3}^–}\, \to\, \mathregular{{NO_2}^–}$",
    "nir": "$\\mathit{nirS}$\n" + r"$\mathregular{{NO_2}^–}\, \to\, \mathregular{N_2}$",
}
selection = (
    rates_ds.isel(time=slice(-times_per_day, None, 5))
    .sel(x=slice(None, 1.5, 4))
    .assign(turnover_rate=lambda x: x.turnover_rate * 1e9)
)
selection = selection.assign(
    concentration=selection.where(
        lambda x: x.variable != "enzymes",
        selection["concentration"].sel(variable="enzymes") * 1e12,
    )["concentration"]
).assign_coords(variable=["transcripts", "enzymes"])
g = selection.plot.scatter(
    x="concentration",
    y="turnover_rate",
    col="variable",
    row="gene",
    hue="time_of_day",
    sharex=False,
    sharey="row",
    alpha=0.85,
    figsize=(5.5, 3.5),
    cmap="twilight_shifted",
    s=10,
)
# Set axis labels and titles
g.axes[1, 0].set_xlabel("$c$ [transcripts/L]")
g.axes[1, 1].set_xlabel("$c$ [pmol/L]")
g.set_ylabels("reaction rate [nM/s]")
g.set_titles(template="{value}")
# Customize the position of the row labels
for row, label, a in zip(g.row_names, g.row_labels, g.axes[:, 0]):
    left, lower, width, height = a.axes.get_position().bounds
    label.set(
        text=row_labels[row],
        anncoords="figure fraction",
        rotation="horizontal",
        horizontalalignment="center",
        linespacing=1.7,
        position=(0.08, lower + height / 2),
    )
# Make the colorbar a bit thinner
cbar = g.fig.axes[-1]
cbar.set_aspect(1.8)
cbar.axes.set_facecolor((1, 1, 1, 0))

# Make space for the new row labels
g.fig.subplots_adjust(left=0.25, right=0.83)
cbar_pos = g.fig.axes[-1].get_position().bounds
g.fig.axes[-1].set_position((0.88, cbar_pos[1], cbar_pos[2], cbar_pos[3]))

# Align labels and remove spines
g.fig.align_ylabels(g.axes)
sns.despine(bottom=True, left=True)

# Add continuous alphabetic subplot labels
for a, label in zip(g.axes.flat, "abcd"):
    a.annotate(text=label, xy=(0.1, 0.8), xycoords="axes fraction")

# Add labels for specific locations
annot_kw = dict(
    arrowprops={
        "arrowstyle": "-",
        "shrinkA": 0,
        "shrinkB": 3,
        "color": "k",
    },
    size=8,
)
x_values = (0.01, 0.9, 1.05)
offsets = ((-20, 40), (-17, 15), (-15, 35))
for x_val, offset in zip(x_values, offsets):
    annotate_location(
        x=x_val,
        rates_ds=selection,
        text_xy=offset,
        ax=g.axes[1, 1],
        textcoords="offset points",
        **annot_kw,
    )
g.fig.savefig(
    f"{plot_path}/rate_correlation_time_variable.pdf",
    facecolor=(1, 1, 1, 0),
)
```

## Time averaged reaction rates

```python
rates_states_average = rates_states.isel(time=slice(-times_per_day, None)).integrate(
    "time"
)
```

```python
dt = rates_ds.time.diff("time").mean()
daily_averages = (
    rates_ds.swap_dims({"time": "datetime"})
    .reset_coords("time")
    .groupby("datetime.day")
    .reduce(np.trapz, dim="datetime", dx=dt)
    .swap_dims(day="time")
)
```

```python
rates_ds["turnover_rate_daily_average"] = daily_averages["turnover_rate"].reindex_like(
    rates_ds.time, method="pad"
)
```

```python
# Plotting
g = rates_ds.plot.scatter(
    x="concentration",
    y="turnover_rate_daily_average",
    row="gene",
    col="variable",
    hue="x",
    sharex="col",
    sharey="row",
    alpha=0.7,
    figsize=(6, 3.5),
    cmap="crest",
)
g.cbar.formatter.set_powerlimits((-3, 3))
g.axes[1, 0].set_xlabel("transcripts/cell")
g.axes[1, 1].set_xlabel("enzymes/cell")
n_compound_labels = {
    "no3": "$\mathregular{{NO_3}^–}$",
    "no2": "$\mathregular{{NO_2}^–}$",
}
for a, row_name in zip(g.axes[:, 0], g.row_names):
    n_compound = str(rates_ds.rate.sel(gene=row_name).values)
    a.set_ylabel(
        "\n".join(
            textwrap.wrap(
                f"average {n_compound_labels[n_compound]} reaction rate\n [mol/cell/s]",
                width=45,
            )
        ),
        labelpad=12,
    )
# sns.despine(bottom=True, left=True)
g.fig.align_ylabels(g.axes)
# g.fig.savefig(
#   f"{plot_path}/rate_correlation_averaged_rates.pdf", bbox_inches="tight"
# )
```

```python
solution_daily_averages = (
    stable_solution.rename_vars({"time": "numeric_time"})
    .assign(
        time=(
            "time",
            pd.to_datetime(
                stable_solution.time.values,
                unit="d",
            ),
        )
    )
    .groupby("time.day")
    .reduce(np.trapz, dim="time", dx=dt)
    .swap_dims(day="time")
    .isel(time=slice(1, -1))
)
```

```python
# Plotting
g = (daily_averages.isel(time=-1)).plot.scatter(
    x="concentration",
    y="turnover_rate",
    row="gene",
    col="variable",
    sharex="col",
    sharey="row",
    hue="x",
    alpha=0.7,
    figsize=(6, 3.5),
    cmap="crest",
)
g.axes[1, 0].set_xlabel("transcripts/cell")
g.axes[1, 1].set_xlabel("enzymes/cell")
for a, row_name in zip(g.axes[:, 0], g.row_names):
    n_compound = str(rates_ds.rate.sel(gene=row_name).values)
    a.set_ylabel(
        "\n".join(
            textwrap.wrap(
                f"average {n_compound_labels[n_compound]} reaction rate\n [mol/cell/s]",
                width=45,
            )
        ),
        labelpad=12,
    )
sns.despine(bottom=True, left=True)
g.fig.align_ylabels(g.fig.axes[:-1])
g.fig.savefig(
    f"{plot_path}/rate_correlation_averaged_rates_averaged_concentrations.pdf",
    bbox_inches="tight",
)
```

```python
daily_averages.to_netcdf(os.path.join(path, "rates_daily_averages.nc"))
```

## Spatially integrated rates

```python
integrated_rates = rates_ds.integrate(coord="x")
```

```python
integrated_rates.turnover_rate.plot(col="gene")
plt.ylim(bottom=0);
```

```python
integrated_rates.concentration.plot(col="gene", row="variable", sharey="row")
plt.ylim(bottom=0);
```

```python
g = integrated_rates.plot.scatter(
    x="concentration",
    y="turnover_rate",
    col="variable",
    row="gene",
    sharey="row",
    sharex="col",
    hue="time_of_day",
    cmap="twilight_shifted"
)
for a in g.axes.flat:
    a.set_ylim(bottom=0)
    a.set_xlim(left=0)
```

```python
integrated_rates_left = rates_ds.sel(x=slice(0, 0.5)).integrate(coord="x")
```

```python
g = integrated_rates_left.plot.scatter(
    x="concentration",
    y="turnover_rate",
    col="variable",
    row="gene",
    sharey="row",
    sharex="col",
    hue="time_of_day",
    cmap="twilight_shifted"
)
for a in g.axes.flat:
    a.set_ylim(bottom=0)
    a.set_xlim(left=0)
```

```python
idx_min_no2 = stable_solution["nitrogen_no2"].sel(x=slice(0, 1.5)).argmin("x")
```

```python
stable_solution.sel(x=slice(0, 1.5)).x[idx_min_no2].plot()
```

## Sensitivity analysis

```python
# Parameters that will be considered in the sensitivity analysis
sens_parameters = [
    ("transport", "left", "inflow_concentration", "nitrogen", "no3"),
    ("transport", "right", "inflow_concentration", "nitrogen", "no3"),
    ("transport", "left", "inflow_concentration",  "doc"),
    ("transport", "left", "inflow_concentration",  "o2"),
    ("transport", "v", "amplitude"),
    ("reaction", "k_dec", "bio", "log"),
    ("reaction", "Y", "no3", "log"),
    ("reaction", "Y", "no2", "log"),
    ("reaction", "Y", "o2", "log"),
    ("reaction", "B_max", "log"),
    ("reaction", "k_max", "no3", "log"),
    ("reaction", "k_max", "no2", "log"),
    ("reaction", "r_max", "o2", "log"),
    ("reaction", "K_doc"),
    ("reaction", "K_subs", "no3", "log"),
    ("reaction", "K_subs", "no2", "log"),
    ("reaction", "K_subs", "o2", "log"),
    ("reaction", "K_i_o2", "reaction", "no3", "log"),
    ("reaction", "K_i_o2", "reaction", "no2", "log"),
    ("reaction", "K_i_o2", "transcription", "fnrp", "log"),
    ("reaction", "K_i_o2", "transcription", "nnr", "log"),
    ("reaction", "k_release", "doc"),
    ("reaction", "lambda_poc"),
    ("reaction", "c_sat_0", "doc"),
    ("reaction", "t_1_2", "enz", "log"),
    ("reaction", "r_mrna_log", "nar"),
    ("reaction", "r_mrna_log", "nir"),
    ("reaction", "r_enz_log"),
    ("reaction", "k_dissociation", "nnr", "log"),
    ("reaction", "k_dissociation", "narr", "log"),
    ("reaction", "k_bind", "narr", "no3", "log"),
    ("reaction", "k_bind", "narr", "no2", "log"),
    ("reaction", "k_bind", "nnr", "log"),
    ("reaction", "K_dna", "fnrp", "log"),
    ("reaction", "K_dna", "narr", "log"),
    ("reaction", "K_dna", "nnr", "log"),
]
```

```python
from copy import deepcopy
import nitrogene.sensitivity as sens
```

```python
sens_parameter_names = ["-".join(path) for path in sens_parameters]
default_params_dict = deepcopy(model.solver.get_params_dict())
```

```python
t_vals_sens = (
    np.concatenate(
        (
            np.linspace(0, 500, 500, endpoint=False),
            np.arange(500, 503, 1 / 240),
        )
    )
    * 86400
)
```

```python
lib.CVodeSStolerances(model.solver._ode, 1e-12, 1e-8)
```

```python tags=[]
perturbed_rates_states = {}
for i, sens_param in enumerate(sens_parameters):
    print(f"{i}: {sens_param}")
    try:
        perturbed_solution = sens.compute_perturbed_solution(
            model,
            t0=0,
            tvals=t_vals_sens,
            default_params_dict=default_params_dict,
            param_path=sens_param,
            increment=0.005,
            time_slice=stable_slice
        )
        perturbed_solution["rate_no3"] = perturbed_solution["rate_no3_norm"] * perturbed_solution["bio_alive"]
        perturbed_solution["rate_no2"] = perturbed_solution["rate_no2_norm"] * perturbed_solution["bio_alive"]
        perturbed_rates_states[sens_param] = perturbed_solution
    except Exception as e:
        print(e)
```

```python
error_params = list(set(sens_parameters) - set(perturbed_rates_states.keys()))
```

```python
error_params
```

```python
lib.CVodeSStolerances(model.solver._ode, 1e-12, 1e-10)
```

```python tags=[]
for i, sens_param in enumerate(error_params):
    print(f"{i}: {sens_param}")
    try:
        perturbed_solution = sens.compute_perturbed_solution(
            model,
            t0=0,
            tvals=t_vals_sens,
            default_params_dict=default_params_dict,
            param_path=sens_param,
            increment=0.005,
            time_slice=stable_slice
        )
        perturbed_solution["rate_no3"] = perturbed_solution["rate_no3_norm"] * perturbed_solution["bio_alive"]
        perturbed_solution["rate_no2"] = perturbed_solution["rate_no2_norm"] * perturbed_solution["bio_alive"]
        perturbed_rates_states[sens_param] = perturbed_solution
    except Exception as e:
        print(e)
```

```python
perturbed_rates_states_sorted = {key: perturbed_rates_states[key] for key in sens_parameters}
```

```python
perturbed_rates_states = perturbed_rates_states_sorted
```

```python
# Save the outputs from model runs with perturbed parameters
for i, (path, values) in enumerate(perturbed_rates_states.items()):
    mode = "w" if i == 0 else "a"
    values.to_netcdf(
        path="perturbed_rates_states_bs.nc",
        group="-".join(path),
        mode=mode
    )
```

```python
# Read in the outputs from model runs with perturbed parameters
perturbed_rates_states = {
    path: xr.open_dataset("perturbed_rates_states_bs.nc", group="-".join(path))
    for path in sens_parameters
}
```

```python
is_log_param = {path: any("log" in subpath for subpath in path) for path in sens_parameters}
```

```python
reference_rates_states = rates_states.assign(
    {
        "rate_no3": rates_states["rate_no3_norm"] * rates_states.bio_alive,
        "rate_no2": rates_states["rate_no2_norm"] * rates_states.bio_alive,
    }
)
sens_numeric = {
    path: sens.compute_numeric_sensitivity(
        perturbed_solution.assign_coords(
            time=perturbed_solution.time
            - perturbed_solution.time.isel(time=-3 * times_per_day)
        ),
        reference_rates_states,
        increment=0.01,
        is_log_param=is_log_param[path],
    )
    for path, perturbed_solution in perturbed_rates_states.items()
}
```

```python
all_sens_numeric = xr.concat(sens_numeric.values(), dim=pd.Index(sens_parameter_names, name="parameter"))
```

```python
normalization_constants = reference_rates_states.integrate("x")
normalized_sensitivites = all_sens_numeric / normalization_constants
```

```python
normalized_sensitivites.to_netcdf("normalized_sensitivities.nc", group="bank_storage", mode="a")
```

```python
integrated_sensitivities = np.abs(all_sens_numeric).fillna(0).integrate("x")
integrated_sensitivities_norm = integrated_sensitivities / normalization_constants
```

```python
drop_vars = [var for var in integrated_sensitivities_norm if "norm" in var]
```

```python
integrated_sensitivities_norm.mean("time").rate_no3.to_series().plot.barh()
```

```python
sns.heatmap(
    data=integrated_sensitivities_norm.drop_vars(drop_vars).mean("time")
    .to_array(name="integrated_sensitivity")
    .to_dataframe()["integrated_sensitivity"]
    .unstack(),
    xticklabels=True
)
```

```python
from sklearn.decomposition import PCA
```

```python
X = normalized_sensitivites.drop_vars(drop_vars).to_array().stack(space_state_var=("x", "time",  "variable")).fillna(0).values.T
```

```python
n_components = None
pca = PCA(n_components).fit(X)
```

```python
variance_ratio = xr.DataArray(
    pca.explained_variance_ratio_ * 100,
    dims=("principal_component",),
    name="explained_variance",
    attrs=dict(units="%", long_name="explained variance"),
    coords={
        "principal_component": (
            "principal_component",
            pd.RangeIndex(
                pca.n_components_,
            ),
            dict(long_name="principal component"),
        )
    },
)
```

```python
variance_ratio.plot.step(figsize=(5, 1.), where="mid")
plt.title("a", weight="bold")
plt.savefig(os.path.join(plot_path, "pca_explained_variance.pdf"), bbox_inches="tight")
```

```python
variance_ratio.max()
```

```python
principal_directions = xr.DataArray(
    pca.components_,
    dims=("principal_component", "parameter"),
    coords={"parameter": normalized_sensitivites.parameter},
)
```

```python
latex_labels = [
    nitrogene.plotting.get_latex_label("_".join(param_string.split("-")[1:]))
    for param_string in principal_directions.parameter.values
]
latex_labels = [
    f"$\\log({label[1:-1]})$" if not "log" in label else label for label in latex_labels
]
g = sns.catplot(
    y="weight",
    x="parameter",
    data=principal_directions.isel(principal_component=0)
    .to_dataframe(name="weight")
    .reset_index(),
    aspect=3,
    height=2,
    kind="bar",
    palette=nitrogene.plotting.color_palettes["paul_tol_bright"],
    facet_kws=dict(gridspec_kws=dict(left=0.08, right=0.98, bottom=0.4, top=0.9))
)
g.set_xticklabels(latex_labels, size=8, rotation=90)
plt.title(r"$\mathbf{c}$ bank storage")
g.fig.savefig(os.path.join(plot_path, "sensitivity_pca_bs.pdf"))
```

```python
g = sns.catplot(
    x="weight",
    y="parameter",
    data=principal_directions.isel(principal_component=0)
    .to_dataframe(name="weight")
    .reset_index(),
    aspect=0.2,
    #height=2,
    kind="bar",
    palette=nitrogene.plotting.color_palettes["paul_tol_bright"],
    facet_kws=dict(gridspec_kws=dict(left=0.08, right=0.98, bottom=0.4, top=0.9))
)
g.set_yticklabels(latex_labels, size=8, rotation=0)
plt.title(r"$\mathbf{c}$ bank storage")
#g.fig.savefig(os.path.join(plot_path, "sensitivity_pca_bs.pdf"))
```

### Compute the solution with an alternative parameter set


```python
weights = principal_directions.isel(principal_component=0)
h = -2
factors = np.exp(h * weights)
increments = np.expm1(h * weights)
# Multiplying the weights with a negative number leads to more bioavailable carbon,
# using a positive number makes the system more carbon limited
```

```python
factors
```

```python tags=[]
solution_alternative_params = sens.compute_perturbed_solution(
    model,
    t0=0,
    tvals=t_vals_sens,
    default_params_dict=default_params_dict,
    param_path=[tuple(param.split("-")) for param in increments.parameter.values],
    increment=increments.values,
    time_slice=stable_slice
)
solution_alternative_params["rate_no3"] = (
    solution_alternative_params["rate_no3_norm"]
    * solution_alternative_params["bio_alive"]
)
solution_alternative_params["rate_no2"] = (
    solution_alternative_params["rate_no2_norm"]
    * solution_alternative_params["bio_alive"]
)
```

```python
solution_alternative_params.to_netcdf("solution_alternative_params.nc", group="bank_storage", mode="a")
```

```python
solution_alternative_params_converted_units = (
    solution_alternative_params.assign(
        doc=lambda x: x.doc.assign_attrs(units="M"),
        rate_no3=lambda x: x.rate_no3.assign_attrs(units="mol/L/s"),
        rate_no2=lambda x: x.rate_no2.assign_attrs(units="mol/L/s"),
    )
).map(nitrogene.plotting.convert_concentration_units)
```

```python
# Plot the concentration distribution at steady state
plotter = nitrogene.plotting.plot_periodic_constant_single_scenario(
    periodic_solution=solution_alternative_params_converted_units.squeeze(drop=True),
    subplots_kw=dict(
        figsize=(6, 3),
        gridspec_kw=dict(hspace=0.25, wspace=0.5, top=0.9, right=0.95, bottom=0.18),
    ),
)
plotter.fig.align_ylabels()
plotter.fig.savefig(f"{plot_path}/concentration_distributions_alternative_params.pdf")
```

<!-- #region tags=[] -->
### Transcript–rate relationship with the alternative parameters
<!-- #endregion -->

```python
rates_alternative_params = xr.concat(
    [solution_alternative_params["rate_no3"], solution_alternative_params["rate_no2"]],
    dim=pd.Index(["no3", "no2"], name="rate"),
    combine_attrs="drop_conflicts"
).rename("rate")

rates_ds_alternative_params = reactive_transport_model.arange_rates_for_plotting(
    rates_alternative_params, solution_alternative_params_converted_units
)
rates_ds_alternative_params = rates_ds_alternative_params.sortby("gene")
```

```python
# Plotting
g = (rates_ds_alternative_params.map(nitrogene.plotting.convert_concentration_units).mean("time")).plot.scatter(
    x="concentration",
    y="turnover_rate",
    col="variable",
    row="gene",
    hue="x",
    sharex=False,
    sharey="row",
    figsize=(6, 3.5),
    cmap="crest",
    s=20,
)
g.axes[1, 0].set_xlabel("$c$ [transcripts/L]")
g.axes[1, 1].set_xlabel("$c$ [pM]")
n_compound_labels = {
    "no3": "$\mathregular{{NO_3}^–}$",
    "no2": "$\mathregular{{NO_2}^–}$",
}
for a, row_name in zip(g.axes[:, 0], g.row_names):
    n_compound = str(rates_ds.rate.sel(gene=row_name).values)
    a.set_ylabel(
        f"{n_compound_labels[n_compound]} reaction rate\n [nmol/L/s]",
        labelpad=12,
    )
sns.despine(bottom=True, left=True)
g.fig.savefig(f"{plot_path}/rate_correlation_absolute_values_alternative_params.pdf", bbox_inches="tight")
```
