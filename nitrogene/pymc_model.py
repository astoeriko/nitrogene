import numpy as np
import pymc as pm
import aesara
import aesara.tensor as tt
import sunode.symode.problem
import sunode.wrappers
import operator
from functools import reduce
from scipy import stats as stats

import nitrogene.reaction_model as reaction_model
import nitrogene.utils
import nesttool as nested


def make_model(model_builder, data, time, t_dense):
    tox_opts = model_builder.toxicity_model
    coords = {
        "time": time / 3600,
        "time_dense": time_dense / 3600,
        "gas_time": time[data["gas_n2"]["flat_time_indices"]],
        "nitrogen_time": time[data["nitrogen_no2"]["flat_time_indices"]],
        "bio_time": time[data["bio_alive"]["flat_time_indices"]],
        "mrna_time": time[data["mrna_norm_nir"]["flat_time_indices"]],
        # "params_flat": ,
        # "variable":
        **tox_opts.toxicity_coords,
    }
    observed_dims = {
        "bio_alive": ["bio_time"],
        "mrna_norm_nir": ["mrna_time"],
        "mrna_norm_nar": ["mrna_time"],
        "nitrogen_no2": ["nitrogen_time"],
        "gas_o2": ["gas_time"],
        "gas_n2": ["gas_time"],
    }
    with pm.Model() as model:
        p = model_builder.default_params  # TODO rename to params

        # Define changeable variables with their prior distributions
        p["Y"]["o2"]["log"] = pm.StudentT("Y_o2_log", mu=np.log(7e14), sigma=0.5, nu=10)
        p["k_max"]["no3"]["log"] = pm.StudentT("kmax_no3_log", mu=7, sigma=2, nu=10)
        p["k_max"]["no2"]["log"] = pm.StudentT("kmax_no2_log", mu=7, sigma=2, nu=10)

        p["t_1_2"]["mrna"]["log"] = pm.Normal("t12_mrna_log", mu=1.8, sigma=0.4)
        p["t_1_2"]["enz"]["log"] = pm.Normal("t12_enz_log", mu=3.0, sigma=0.8)

        r_mrna_log = pm.StudentT("r_mrna_log", mu=-2, sigma=0.7, nu=10)
        p["r_mrna_log"] = r_mrna_log
        p["K_i_o2"]["transcription"]["nar"]["log"] = pm.StudentT(
            "K_i_o2_transcription_nar_log", mu=-11, sigma=2, nu=10
        )
        p["K_i_o2"]["transcription"]["nir"]["log"] = pm.StudentT(
            "K_i_o2_transcription_nir_log", mu=-11, sigma=2, nu=10
        )

        if tox_opts.inner_func_type in ["toxicity", "bounded_toxicity"]:
            K_tox_name = "K_tox"
            p_tox_name = "p_tox"
            f_min_name = "f_min"
            assert tox_opts.outer_func_type == "identity"
        elif tox_opts.outer_func_type in ["toxicity", "bounded_toxicity"]:
            K_tox_name = "K_tox_2"
            p_tox_name = "p_tox_2"
            f_min_name = "f_min_2"
            assert tox_opts.inner_func_type == "identity"
        else:
            assert False
        p[K_tox_name]["log"] = pm.Normal("K_tox_log", mu=-7, sigma=1)
        p[p_tox_name] = pm.TruncatedNormal("p_tox", lower=0, mu=4, sigma=2)
        if "bounded_toxicity" in [tox_opts.inner_func_type, tox_opts.outer_func_type]:
            p[f_min_name] = pm.Beta("f_min", alpha=2, beta=2)
        if tox_opts.distribution in ["gamma", "exponential", "sum_of_gammas"]:
            alpha = tox_opts.gamma_dist_alpha
            alpha = 1.0 if alpha is None else alpha
            log_t_12_tox = pm.Normal("t12_tox_log", mu=np.log(3 / alpha), sigma=1.0)
            p["t_1_2_tox"]["log"] = log_t_12_tox

        p["K_transcription"]["nir"]["log"] = pm.Normal(
            "K_transcription_nir_log", mu=-11, sigma=3
        )
        p["K_transcription"]["nar"]["no3"]["log"] = pm.Normal(
            "K_transcription_nar_no3_log", mu=-12, sigma=2
        )
        p["K_transcription"]["nar"]["no2"]["log"] = pm.Normal(
            "K_transcription_nar_no2_log", mu=-8, sigma=2
        )
        Ks_log = pm.StudentT("Ks_o2_log", mu=-13, sigma=2, nu=10)
        cfix = 0.6e-4
        # Approximate the prior of the reparametrized parameter based on
        # the prior of the original parameters
        prior_Ks_log = stats.t(loc=-13, scale=2, df=10).rvs(10000)
        prior_rmax_log = stats.norm(loc=-44, scale=1.5).rvs(10000)
        prior_r_cfix_log = prior_rmax_log + np.log(cfix / (np.exp(prior_Ks_log) + cfix))
        r_cfix_log = pm.StudentT(
            "r_cfix_o2_log",
            mu=prior_r_cfix_log.mean(),
            sd=prior_r_cfix_log.std(),
            nu=10,
        )
        rmax_log = r_cfix_log + np.log((np.exp(Ks_log) + cfix) / cfix)
        pm.Deterministic("rmax_o2_log", rmax_log)
        p["K_subs"]["o2"]["log"] = Ks_log
        p["r_max"]["o2"]["log"] = rmax_log

        K_i_o2_no3_log = pm.Normal("K_i_o2_no3_log", mu=-15, sigma=3)
        p["K_i_o2"]["reaction"]["no3"]["log"] = K_i_o2_no3_log
        K_i_o2_no2_log = pm.Normal("K_i_o2_no2_log", mu=-15, sigma=3)
        p["K_i_o2"]["reaction"]["no2"]["log"] = K_i_o2_no2_log

        model_builder.transform_parameters(p, module=tt)

        y0 = model_builder.generate_initial_values()
        y0["bio"]["alive"] = pm.StudentT("y0_bio_alive_log", mu=21, sigma=0.5, nu=10)

        # Set initial mRNA concentrations to their quasi-steady state value
        set_initial_mrna_concentrations_to_qss(
            y0,
            p,
            model_builder.genes,
            model_builder.log_scale,
            transcriptional_regulation_opts=model_builder.transcriptional_regulation_opts,
        )
        # Format parameters and initial values for use with sunode
        y0 = nested.apply_func_nested(y0, nitrogene.utils.as_double)
        y0_with_shapes = nested.apply_func_nested(y0, add_shapes_to_aesara_vars)
        params_with_shapes = nested.apply_func_nested(p, add_shapes_to_aesara_vars)
        params = nested.apply_func_nested(
            params_with_shapes, nitrogene.utils.as_double
        )

        # Solve the ode
        rhs = model_builder.generate_rhs_func()
        t0 = 0
        (
            y,
            flat_y,
            problem,
            solver,
            y0_flat,
            params_flat,
            y_dense,
            flat_y_dense,
        ) = solve_ode(
            time,
            t_dense,
            y0_with_shapes,
            params,
            rhs,
            t0,
            solver_kwargs={"checkpoint_n": 500_000},
            dense_solver_kwargs={"checkpoint_n": 10},
        )
        pm.Deterministic("y0_flat", y0_flat)
        # pm.Deterministic("params_flat", params_flat, dims="params_flat")
        # pm.Deterministic("flat_y", flat_y, dims=["time", "variable"])
        # pm.Deterministic("flat_y_dense", flat_y_dense, dims=["time_dense", "variable"])
        c, c_dense, log_c = unpack_ode_solution(y, y_dense, model_builder)

        # Normalize mRNA concentrations with biomass
        for conc, log_scale in [(c, False), (log_c, True), (c_dense, False)]:
            compute_normalized_mrna_concentrations(conc, log_scale)

        toxicity_dims = {"X": tox_opts.toxicity_dims}
        add_concentration_to_deterministics(
            c_dense, "time_dense", additional_dims=toxicity_dims
        )
        add_concentration_to_deterministics(c, "time", additional_dims=toxicity_dims)

        # Define the likelihood
        observed = {}
        observed_var_names = [
            "bio_alive",
            "gas_o2",
            "nitrogen_no2",
            "gas_n2",
            "mrna_norm_nir",
            "mrna_norm_nar",
        ]
        for data_var in observed_var_names:
            # Get the simulation results at the right time points
            simulated, sim_var_name = get_simulated(data_var, log_c)
            indices = data[data_var]["flat_time_indices"]

            # Get the simulated mean log concentrations by adding a background
            if data_var in ["bio_alive", "gas_n2", "mrna_norm_nir", "mrna_norm_nar"]:
                mu = simulated[indices]
            else:
                background = pm.Normal(f"background_{data_var}", mu=-18, sd=3)
                mu = tt.log1p(np.exp(simulated - background))[indices] + background

            # transform from log to box-cox
            lmbda = data[data_var]["lambda"]
            mu = tt.expm1(mu * lmbda) / lmbda
            pm.Deterministic(f"mu_{data_var}", mu, dims=observed_dims[data_var])

            # Estimate the data measurement error
            if False:  # variable error
                valid_time_indices = pd.unique(indices)
                valid_time_indices.sort()
                valid_times = t[valid_time_indices]

                error_mu = pm.Normal(f"measure_error_log_mu_{data_var}", mu=-2)
                std = pm.HalfNormal(f"measure_error_log_std_{data_var}")
                raw = tt.zeros(len(t))
                valid_raw = pm.Normal(
                    f"measure_error_log_raw_{data_var}", shape=len(valid_times)
                )
                raw = tt.set_subtensor(raw[valid_time_indices], valid_raw)
                measure_error = tt.exp(error_mu + std * raw)[indices]
            else:  # constant error
                measure_error = pm.HalfNormal(f"measure_error_{data_var}", sd=0.1)

            # Model measurements as the simulation results with a Student's T
            # distributed data error (of the Box-Cox transformed values)
            observed[sim_var_name] = pm.StudentT(
                "_".join([data_var, "observed"]),
                nu=7,
                mu=mu,
                #             sigma=data[data_var]["constant_std"],
                sigma=measure_error,
                observed=data[data_var]["flat_measurements"],
                dims=observed_dims[data_var],
            )
    return model, problem, solver


def add_shapes_to_aesara_vars(var):
    if isinstance(var, tt.TensorVariable):
        return (var, ())
    else:
        return var


def add_concentration_to_deterministics(c, time_dim, additional_dims=None):
    """Create pymc deterministic variable for every concentration."""
    names = {"time_dense": "c_dense", "time": "c"}
    if not time_dim in names:
        raise ValueError(f"time_dim must be one of {list(names.keys())}.")
    name = names[time_dim]
    if additional_dims is None:
        additional_dims = {}
    c_flattened = sunode.dtypesubset.as_flattened(c)
    for keys, vals in c_flattened.items():
        dims = [time_dim, *additional_dims.get(keys, [])]
        path = [name, *keys]
        pm.Deterministic("_".join(path), vals, dims=dims)


def compute_normalized_mrna_concentrations(c, log_scale=False):
    """Normalize the mRNA concentrations with the biomass."""
    mrna_norm = c.setdefault("mrna_norm", {})
    for gene in c["mrna"].keys():
        if log_scale:
            mrna_norm[gene] = c["mrna"][gene] - c["bio"]["alive"]
        else:
            mrna_norm[gene] = c["mrna"][gene] / c["bio"]["alive"]


def solve_ode(
    t, t_dense, y0, params, rhs, t0, solver_kwargs={}, dense_solver_kwargs={}
):
    """Call ode solver for measurement time points and dense times."""
    (
        y,
        flat_y,
        problem,
        solver,
        y0_flat,
        params_flat,
    ) = sunode.wrappers.as_aesara.solve_ivp(
        y0=y0,
        params=params,
        rhs=rhs,
        tvals=t,
        t0=t0,
        solver_kwargs=solver_kwargs,
    )
    y_dense, flat_y_dense, *_ = sunode.wrappers.as_aesara.solve_ivp(
        y0=y0,
        params=params,
        rhs=rhs,
        tvals=t_dense,
        t0=t0,
        solver_kwargs=dense_solver_kwargs,
    )
    return y, flat_y, problem, solver, y0_flat, params_flat, y_dense, flat_y_dense


def unpack_ode_solution(y, y_dense, model_builder):
    """Compute concentrations and log concentrations from the ODE solution."""
    if model_builder.log_scale:
        c = nested.apply_func_nested(y, np.exp)
        c_dense = nested.apply_func_nested(y_dense, np.exp)
        log_c = y
    else:
        c = model_builder.unnormalize_state(y)
        c_dense = model_builder.unnormalize_state(y_dense)
        log_c = nested.apply_func_nested(y, np.log)
    return c, c_dense, log_c


def set_initial_mrna_concentrations_to_qss(
    y0, p, genes, log_scale, transcriptional_regulation_opts, nir_activation
):
    """Set initial mRNA concentrations to their quasi-steady state value"""
    if log_scale:
        c_arg = {"log_c": y0}
    else:
        c_arg = {"c": y0}
    for gene in genes:
        regulation = transcriptional_regulation_opts[gene]
        activation = nir_activation if gene == "nir" else None
        y0["mrna"][gene] = (
            reaction_model.qss_mrna_concentration_val(
                gene=gene,
                p=p,
                log_scale=log_scale,
                transcriptional_regulation_opt=regulation,
                nir_activation=activation,
                **c_arg,
            ),
            (),
        )


def compute_qss_mrna_concentrations(
    y, p, genes, log_scale, transcriptional_regulation_opts
):
    """Set initial mRNA concentrations to their quasi-steady state value"""
    if log_scale:
        c_arg = {"log_c": y}
    else:
        c_arg = {"c": y}
    if not "mrna" in y:
        y["mrna"] = {}
    for gene in genes:
        regulation = transcriptional_regulation_opts[gene]
        y["mrna"][gene] = reaction_model.qss_mrna_concentration_val(
            gene=gene,
            p=p,
            log_scale=log_scale,
            transcriptional_regulation_opt=regulation,
            **c_arg,
        )


def get_simulated(data_var, log_c):
    """Extract variable from the simulation results."""
    sim_var_name = f"c_{data_var}"
    if "mrna_norm" in data_var:
        path = data_var.rsplit("_", 1)
    else:
        path = data_var.split("_")
    return reduce(operator.getitem, path, log_c), sim_var_name
